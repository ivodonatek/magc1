//******************************************************************************
//  Ovladani Vcc1
//*****************************************************************************/

#ifndef STEP_UP_H_INCLUDED
#define STEP_UP_H_INCLUDED

// Zapnuti/vypnuti
typedef enum
{
    STPUP_ON,  // zapnuti
    STPUP_OFF  // vypnuti

} TStepUpSetup;

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void StepUp_Init(void);

//------------------------------------------------------------------------------
//  Zapnuti/vypnuti Step-up.
//------------------------------------------------------------------------------
void StepUp_Set(TStepUpSetup setup);

#endif // STEP_UP_H_INCLUDED
