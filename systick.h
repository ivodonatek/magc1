#ifndef SYSTICK_H_INCLUDED
#define SYSTICK_H_INCLUDED

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//------------------------------------------------------------------------------
void SysTick_Init(void);

unsigned long SysTick_GetSysMilliSeconds(void);
unsigned char SysTick_IsSysMilliSecondTimeout(unsigned long userSysMilliSeconds, unsigned long delayMilliSeconds);

#endif // SYSTICK_H_INCLUDED
