#include <stdarg.h>
#include <string.h>
#include <stdlib.h>

#include "LPC23xx.h"
#include "type.h"
#include "target.h"
//#include "rtc.h"
//#include "timer.h"
#include "comm.h"
#include "monitor.h"
#include "diskio.h"
#include "tff.h"
#include "fio.h"
#include "maine.h"

#define debug_printf xprintf

FIL* fopen ( const char * filename, const char * mode )
{
    BYTE append = 0;
    BYTE _mode = 0;
    FIL *pfile = malloc( sizeof(FIL) );		/* File objects */
    if( !pfile )
    {
        debug_printf("malloc ERR\r\n");
        return NULL;
    }
    else
    {
        memset( pfile, 0, sizeof(FIL));
    }

    //parsuj mod
    char * ptr = mode;
    while( *ptr != '\0' )
    {
        if( *ptr == 'r' )
        {
            _mode |= FA_READ;
            ptr++;
            if( *ptr == '+' )
                _mode |= FA_WRITE;
        }
        else if( *ptr == 'w' )
        {
            _mode |= FA_WRITE|FA_CREATE_ALWAYS|FA_OPEN_ALWAYS;
            ptr++;
            if( *ptr == '+' )
                _mode |= FA_READ;
        }
        else if( strchr( mode, 'a' ))
        {
            _mode |= FA_WRITE | FA_OPEN_ALWAYS;
            append = 1;
            ptr++;
            if( *ptr == '+' )
                _mode |= FA_READ;
        }
        ptr++;
    }

    FRESULT result;
    char buff[256]; //buffer for filename without multiple '/'
    unsigned char end = strlen( filename );
    unsigned int i,j;
    //projed celou cestu a zkus vlezt do podadresare
    //pokud najdes nekolik '/' za sebou, akceptuj pouze jedno
    for( i=0, j=0; i<end; i++ )
    {
        buff[j] = filename[i];
        if( filename[i+1] =='/')
        {
            if( buff[j] != '/') //na konci nesmi byt lomitko
            {
                buff[j+1] = '\0';
                FRESULT result = f_mkdir( buff );
                if(( result != FR_OK ) && ( result != FR_EXIST ))
                {
                    //neznama chyba
                    free( pfile );
                    return NULL;
                }
            }
            else
            {
                continue;
            }
        }
        j++;
    }
    buff[j] = '\0'; //ukonci string
    result = f_open(pfile, buff, _mode);
    if( result == FR_OK )
    {
        debug_printf("Open file %s OK\r\n", buff);
        f_lseek( pfile, pfile->fsize );
        return pfile;
    }
    else
    {
        debug_printf("Error open file %s\r\n", buff);
        free( pfile );
        return NULL;
    }
}

int fclose( FIL *stream )
{
    if( !stream )
        return FR_INVALID_OBJECT;

    debug_printf("Close file\r\n");
    int Result = f_close( stream );
    free( stream );
    return Result;
}


int feof ( FIL *stream )
{
    return stream->fsize == stream->fptr;
}

/*-----------------------------------------------------------------------*/
/* Get a string from the file                                            */
/*-----------------------------------------------------------------------*/
char* fgets (
	char* buff,	/* Pointer to the string buffer to read */
	int len,	/* Size of string buffer */
	FIL* fil	/* Pointer to the file object */
)
{
	int i = 0;
	char *p = buff;
	UINT rc;

	if(!fil)
        return 0;


	while (i < len - 1) {			/* Read bytes until buffer gets filled */
		f_read(fil, p, 1, &rc);
		if (rc != 1) break;			/* Break when no data to read */
		i++;
		if (*p++ == '\n') break;	/* Break when reached end of line */
	}
	*p = 0;
	return i ? buff : 0;			/* When no data read (eof or error), return with error. */
}


/*-----------------------------------------------------------------------*/
/* Put a character to the file                                           */
/*-----------------------------------------------------------------------*/

int fputc (
	char chr,	// A character to be output
	FIL* fil	// Ponter to the file object
)
{
	UINT bw;
	char c;

	if (!fil) {	// Special value may be used to switch the destination to any other device
	//	put_console(chr);
		return EOF;
	}
	c = (char)chr;
	f_write(fil, &c, 1, &bw);	// Write a byte to the file
	return bw ? chr : EOF;		// Return the resulut
}




/*-----------------------------------------------------------------------*/
/* Put a string to the file                                              */
/*-----------------------------------------------------------------------*/
int fputs (
	const char* str,	/* Pointer to the string to be output */
	FIL* fil			/* Pointer to the file object */
)
{
	int n;

	for (n = 0; str[n]; n++) {
		if (fputc(str[n], fil) == EOF) return EOF;
	}
	return n;
}




/*-----------------------------------------------------------------------*/
/* Put a formatted string to the file                                    */
/*-----------------------------------------------------------------------*/
int fprintf (
	FIL* fil,			/* Pointer to the file object */
	const char* str,	/* Pointer to the format string */
	...					/* Optional arguments... */
)
{
	va_list arp;
	UCHAR c, f, r;
	ULONG val;
	char s[16];
	int i, w, res, cc;


	va_start(arp, str);

	for (cc = res = 0; cc != EOF; res += cc) {
		c = *str++;
		if (c == 0) break;			/* End of string */
		if (c != '%') {				/* Non escape cahracter */
			cc = fputc(c, fil);
			if (cc != EOF) cc = 1;
			continue;
		}
		w = f = 0;
		c = *str++;
		if (c == '0') {				/* Flag: '0' padding */
			f = 1; c = *str++;
		}
		while (c >= '0' && c <= '9') {	/* Precision */
			w = w * 10 + (c - '0');
			c = *str++;
		}
		if (c == 'l') {				/* Prefix: Size is long int */
			f |= 2; c = *str++;
		}
		if (c == 's') {				/* Type is string */
			cc = fputs(va_arg(arp, char*), fil);
			continue;
		}
		if (c == 'c') {				/* Type is character */
			cc = fputc(va_arg(arp, char), fil);
			if (cc != EOF) cc = 1;
			continue;
		}
		r = 0;
		if (c == 'd') r = 10;		/* Type is signed decimal */
		if (c == 'u') r = 10;		/* Type is unsigned decimal */
		if (c == 'X') r = 16;		/* Type is unsigned hexdecimal */
		if (r == 0) break;			/* Unknown type */
		if (f & 2) {				/* Get the value */
			val = (ULONG)va_arg(arp, long);
		} else {
			val = (c == 'd') ? (ULONG)(long)va_arg(arp, int) : (ULONG)va_arg(arp, unsigned int);
		}
		/* Put numeral string */
		if (c == 'd') {
			if (val >= 0x80000000) {
				val = 0 - val;
				f |= 4;
			}
		}
		i = sizeof(s) - 1; s[i] = 0;
		do {
			c = (UCHAR)(val % r + '0');
			if (c > '9') c += 7;
			s[--i] = c;
			val /= r;
		} while (i && val);
		if (i && (f & 4)) s[--i] = '-';
		w = sizeof(s) - 1 - w;
		while (i && i > w) s[--i] = (f & 1) ? '0' : ' ';
		cc = fputs(&s[i], fil);
	}

	va_end(arp);
	return (cc == EOF) ? cc : res;
}

void rewind( FIL *stream )
{
    fseek( stream, 0, SEEK_SET);
}

int fseek ( FIL * stream, long int offset, int origin )
{
    f_lseek( stream, origin+offset );
    return 1;
}
