#include "port.h"
#include "modbus.h"
#include "mb.h"
#include "mbport.h"
#include "maine.h"
#include "rtc.h"
#include "menu.h"
#include "type.h"
#include "sensor.h"
#include "math.h"
#include "delay.h"
#include "display.h"
#include "gsm.h"
#include "conversion.h"
#include "heat.h"
#include "pulse_outputs.h"
#include "led.h"
#include "extmeas/temperature/exttemp.h"
#include "mbus/mbus_slave.h"

volatile unsigned long modbus_datalogger_date = 0;
volatile unsigned int modbus_datalogger_time = 0;
volatile unsigned char modbus_read_datalogger_flag = 0;

void Modbus_init (void)
{
  unsigned int BaudRate = 0;
  unsigned char Parity = 0 ;

  switch (RAM_Read_Data_Long(MODBUS_BAUDRATE))
  {
      // prevedeni hodnoty v menu na skutecne cislo
  case 0 :  // 4800
      BaudRate = 4800;
      break;
  case 1 :  // 9600
      BaudRate = 9600;
      break;
  case 2 :  // 19200
      BaudRate = 19200;
      break;
  case 3 :  // 38400
      BaudRate = 38400;
      break;
  default:
      BaudRate = 9600;
      break;
  }

  switch (RAM_Read_Data_Long(MODBUS_PARITY))
  {
      // prevedeni hodnoty v menu na skutecne cislo
  case 0 : // Even, 1 stopbit
      Parity = MB_PAR_EVEN;
      break;
  case 1 : // Odd, 1 stopbit
      Parity = MB_PAR_ODD;
      break;
  case 2 : // None, 2 stopbit
      Parity = MB_PAR_NONE_TWO_STOPBITS;
      break;
  case 3 : // None, 1 stopbit
      Parity = MB_PAR_NONE;
      break;
  default: // Even, 1 stopbit
      Parity = MB_PAR_EVEN;
      break;
  }

  eMBErrorCode    eStatus;
  eStatus = eMBDisable();
  eStatus = eMBInit( MB_RTU, RAM_Read_Data_Long(MODBUS_SLAVE_ADDRESS), 0, BaudRate, Parity);
  eStatus = eMBEnable();
}

// Info
const unsigned int ModbusTable1[] =
{
  UNIT_NO,FLOW_SENSOR_UNIT_NO,ERROR_MIN,OK_MIN,DIAMETER,
  FLOW_RANGE,FIRM_WARE,ACTUAL_ERROR,FW_CHECKSUM,
  SD_PRESSENT,MODULE_PRESSENT,GSM_IP,GSM_SIGNAL,
  TEMP_SENSOR1_UNIT_NO,TEMP_SENSOR2_UNIT_NO
};

// Display
const unsigned int ModbusTable2[] =
{
  UNIT_FLOW,UNIT_VOLUME,UNIT_TEMPERATURE,UNIT_HEAT,
  ACT_LANGUAGE,CONTRAST,BACKLIGHT,STATUS_LED_MODE
};

// User Settings
const unsigned int ModbusTable3[] =
{
  DATALOGGER_INTERVAL, CSV_FORMAT, AIR_DETECTOR,
  DELETE_AUXILIARY_VOLUME, AVERAGE_SAMPLES,
  GRAPH_QUANTITY, HEAT_POWER_RANGE,
  EXT_TEMP_MEAS_STATE, EXT_TEMP_SENSOR_TYPE, EXT_TEMP_SENSOR_CONN,
  EXT_CAL_TEMPERATURE1, EXT_TEMP1_CAL_STARTNOW, EXT_CAL_TEMPERATURE2, EXT_TEMP2_CAL_STARTNOW,
  CURRENT_SET, I_DIRECT_DRIVING_QUANTITY, I_HEAT_POWER_MIN, I_HEAT_POWER_MAX,
  HEAT_POWER_CURRENT_MIN, HEAT_POWER_CURRENT_MAX,
  I_FLOW_MIN, I_FLOW_MAX, CURRENT_MIN, CURRENT_MAX,
  I_CAL_POINT_1, I_CAL_POINT_2, I_CAL_CONST_1, I_CAL_CONST_2,
  RE1_SET, RE2_SET, RE_FLOW_1, RE_FLOW_2, RE_HYSTERESIS_1, RE_HYSTERESIS_2,
  RE3_SET, RE4_SET, RE_VOLUME_PLUS, RE_VOLUME_MINUS, RE_DOSE,
  F_SET, F_FLOW1, F_FLOW2, F_FREQ1, F_FREQ2, F_DUTY_CYCLE,
  BIN_RE1_SET, BIN_RE1_HEAT_POWER_1, BIN_RE1_HEAT_POWER_2, BIN_RE1_HEAT_POWER_HYSTERESIS_1, BIN_RE1_HEAT_POWER_HYSTERESIS_2,
  BIN_RE2_SET, BIN_RE2_HEAT_POWER_1, BIN_RE2_HEAT_POWER_2, BIN_RE2_HEAT_POWER_HYSTERESIS_1, BIN_RE2_HEAT_POWER_HYSTERESIS_2,
  BIN_RE3_SET, BIN_RE3_HEAT_POWER_1, BIN_RE3_HEAT_POWER_2, BIN_RE3_HEAT_POWER_HYSTERESIS_1, BIN_RE3_HEAT_POWER_HYSTERESIS_2,
  BIN_RE3_F_HEAT_POWER1, BIN_RE3_F_HEAT_POWER2, BIN_RE3_F_HEAT_POWER_FREQ1, BIN_RE3_F_HEAT_POWER_FREQ2,
  BIN_RE4_SET, BIN_RE4_HEAT_POWER_1, BIN_RE4_HEAT_POWER_2, BIN_RE4_HEAT_POWER_HYSTERESIS_1, BIN_RE4_HEAT_POWER_HYSTERESIS_2,
  BIN_RE4_F_HEAT_POWER1, BIN_RE4_F_HEAT_POWER2, BIN_RE4_F_HEAT_POWER_FREQ1, BIN_RE4_F_HEAT_POWER_FREQ2,
  LOAD_DEFAULT_SETTING, PASSWORD_USER,
  MODBUS_SLAVE_ADDRESS, MODBUS_BAUDRATE, MODBUS_PARITY,
  MBUS_SLAVE_ADDRESS, MBUS_BAUDRATE, MBUS_PARITY,
  TOTALIZER_CYCLING, GSM_DATA_INTERVAL, GSM_PHONE_1_H,
  GSM_PHONE_1_L, GSM_PHONE_2_H, GSM_PHONE_2_L, GSM_PHONE_3_H, GSM_PHONE_3_L,
  GSM_EMPTY_PIPE, GSM_ZERO_FLOW, GSM_ERROR_DETECT, GSM_EMPTY_PIPE_SEND, GSM_ZERO_FLOW_SEND,
  GSM_GATEWAY0, GSM_GATEWAY1, GSM_GATEWAY2, GSM_GATEWAY3, GSM_GATEWAY4,
  GSM_GATEWAY5, GSM_GATEWAY6, GSM_GATEWAY7, GSM_GATEWAY8, GSM_GATEWAY9,
  GSM_GATEWAY10, GSM_GATEWAY11, GSM_GATEWAY12, GSM_GATEWAY13, GSM_GATEWAY14,
  GSM_GATEWAY15, GSM_USER0, GSM_USER1, GSM_USER2, GSM_PASSWORD0,
  GSM_PASSWORD1, GSM_PASSWORD2, GSM_PORT, GSM_PIN, WIFI_AP_MODE
};

// Service Settings
const unsigned int ModbusTable4[] =
{
  DELETE_ERROR_MIN, DELETE_OK_MIN, DELETE_NEGATIVE_VOLUME, DELETE_POSITIVE_VOLUME, DELETE_TOTAL_VOLUME, DELETE_TOTAL_HEAT,
  DEMO, SIMULATED_FLOW, SERVICE_MODE, MEASUREMENT_STATE, AIR_CONSTANT, START_DELAY, INVERT_FLOW,
  CLEAN_POWER, CLEAN_TIME, CLEAN_STARTNOW, LOW_FLOW_CUTOFF, FLOW_RANGE, DATE_SET, TIME_SETTING, SD_CARD_BUFFER, MBUS_MODULE
};

// Factory Settings
const unsigned int ModbusTable5[] =
{
  DIAMETER, UNIT_NO, CALIBRATION_POINT_ONE, CALIBRATION_POINT_TWO, CALIBRATION_POINT_THREE,
  MEASURED_POINT_ONE, MEASURED_POINT_TWO, MEASURED_POINT_THREE, ZERO_FLOW, ZERO_FLOW_CONSTANT,
  ZERO_FLOW_ERASE, EXCITATION_FREQUENCY, EXCITATION, SERVICE_MODE, SAVE_SETTINGS,
  FLOW_SENSOR_UNIT_NO, TOTAL_DIG_SET, TOTAL_DEC_SET, TOTAL_PLUS_DIG_SET, TOTAL_PLUS_DEC_SET,
  TOTAL_NEG_DIG_SET, TOTAL_NEG_DEC_SET, AUX_PLUS_DIG, AUX_PLUS_DEC,
  OPT_DU_BTN_LEVEL, OPT_LR_BTN_LEVEL, OPT_EE_BTN_LEVEL, TEMP_SENSOR1_UNIT_NO, TEMP_SENSOR2_UNIT_NO,
  FLOW_SENSOR_TYPE, PULSES_PER_LITRE, FLOW_PULSE_TIMEOUT, HEAT_TEST_TIME, HEAT_TEST_STARTNOW
};

// Authorize
const unsigned int ModbusTable6[] =
{
  PASSWORD_USER
};

#define SIZEOF_ModbusTable1 (sizeof(ModbusTable1)/sizeof(ModbusTable1[0]))
#define SIZEOF_ModbusTable2 (sizeof(ModbusTable2)/sizeof(ModbusTable2[0]))
#define SIZEOF_ModbusTable3 (sizeof(ModbusTable3)/sizeof(ModbusTable3[0]))
#define SIZEOF_ModbusTable4 (sizeof(ModbusTable4)/sizeof(ModbusTable4[0]))
#define SIZEOF_ModbusTable5 (sizeof(ModbusTable5)/sizeof(ModbusTable5[0]))
#define SIZEOF_ModbusTable6 (sizeof(ModbusTable6)/sizeof(ModbusTable6[0]))

// kontrola min a max hodnoty
unsigned char kontrola_hodnot(unsigned char Table_No, unsigned long data , USHORT usAddress)
{
  unsigned char error = 0;
  unsigned long address = 0;

  // rozhodne do ktere tabulky se ma podivat pro adresu v menu a do Table_No ulozi adresu
  switch ((unsigned int)(Table_No))
  {
    case 1 :
      address = (ModbusTable1[(usAddress-MODBUS_INFO_OFFSET)/2]);
      break;
    case 2 :
      address = (ModbusTable2[(usAddress-MODBUS_DISPLAY_OFFSET)/2]);
      break;
    case 3 :
      address = (ModbusTable3[(usAddress-MODBUS_USER_OFFSET)/2]);
      break;
    case 4 :
      address = (ModbusTable4[(usAddress-MODBUS_SERVICE_OFFSET)/2]);
      break;
    case 5 :
      address = (ModbusTable5[(usAddress-MODBUS_FACTORY_OFFSET)/2]);
      break;
    case 6 :
      address = (ModbusTable6[(usAddress-MODBUS_AUTHORIZE_OFFSET)/2]);
      break;
  }

  switch(address)
  {
      /// ------------------------------- display
      case UNIT_FLOW:
          if (data > 4)
              error = 1;
      break;
      case UNIT_VOLUME:
          if (data > 3)
              error = 1;
      break;
      case UNIT_TEMPERATURE:
          if (data > 1)
              error = 1;
      break;
      case UNIT_HEAT:
          if (data > 5)
              error = 1;
      break;
      case ACT_LANGUAGE:
          if (data > 4)
              error = 1;
      break;
      case CONTRAST:
          if (!((data >= RAM_Read_Data_Long(CONTRAST_MIN)) && (data <= RAM_Read_Data_Long(CONTRAST_MAX))))
              error = 1;
      break;
      case BACKLIGHT:
          if (data > 1)
              error = 1;
          else if (data == 1)
              DISPLAY_LIGHT_IOSET |= DISPLAY_LIGHT_PIN;
      break;
      case STATUS_LED_MODE:
          if (data > LEDM_Max)
              error = 1;
      break;
      /// ------------------------------- user
      case MEASUREMENT_STATE:
          if (data > 1)
              error = 1;
      break;
      case DATALOGGER_INTERVAL:
          if (data > 10)
              error = 1;
      break;
      case CSV_FORMAT:
          if (data > 1)
              error = 1;
      break;
      case AIR_DETECTOR:
          if (data > 1)
              error = 1;
      break;
      case FLOW_SENSOR_TYPE:
          if (data > 1)
              error = 1;
      break;
      case GSM_EMPTY_PIPE:
          if (data > 1)
              error = 1;
      break;
      case GSM_ZERO_FLOW:
          if (data > 1)
              error = 1;
      break;
      case GSM_ERROR_DETECT:
          if (data > 1)
              error = 1;
      break;
      case GSM_EMPTY_PIPE_SEND:
          if (data > 1)
              error = 1;
      break;
      case GSM_ZERO_FLOW_SEND:
          if (data > 1)
              error = 1;
      break;
      case PULSES_PER_LITRE:
          if (!((data >= RAM_Read_Data_Long(PULSES_PER_LITRE_MIN)) && (data <= RAM_Read_Data_Long(PULSES_PER_LITRE_MAX))))
              error = 1;
      break;
      case GRAPH_QUANTITY:
          if (data > GQ_Max)
              error = 1;
      break;
      case HEAT_POWER_RANGE:
          if (!((data >= RAM_Read_Data_Long(HEAT_POWER_RANGE_MIN)) && (data <= RAM_Read_Data_Long(HEAT_POWER_RANGE_MAX))))
              error = 1;
      break;
      case MBUS_MODULE:
          if (data > 1)
              error = 1;
      break;
      case AIR_CONSTANT:
          if (!((data >= RAM_Read_Data_Long(AIR_CONSTANT_MIN)) && (data <= RAM_Read_Data_Long(AIR_CONSTANT_MAX))))
              error = 1;
      break;
      case START_DELAY:
          if (!((data >= RAM_Read_Data_Long(START_DELAY_MIN)) && (data <= RAM_Read_Data_Long(START_DELAY_MAX))))
              error = 1;
      break;
      case AVERAGE_SAMPLES:
          if( flag_sensor8)
          {
              if (!((data >= RAM_Read_Data_Long(AVERAGE_SAMPLES_MIN)) && (data <= RAM_Read_Data_Long(AVERAGE_SAMPLES_MAX))))
              error = 1;
          }
          else    //pokud je sensor 7 tak je natvrdo 60 vyorku max
          {
              if (!((data >= RAM_Read_Data_Long(AVERAGE_SAMPLES_MIN)) && (data <= 60)))
              error = 1;
          }
      break;
      case LOW_FLOW_CUTOFF:
          if (data > 5)
              error = 1;
      break;
      case FLOW_RANGE:
          if (!((data >= RAM_Read_Data_Long(FLOW_RANGE_MIN)) && (data <= RAM_Read_Data_Long(FLOW_RANGE_MAX))))
              error = 1;
      break;
      case INVERT_FLOW:
          if (data > 1)
              error = 1;
      break;
      case CURRENT_SET:
          if (data > 6)
              error = 1;
      break;
      case I_DIRECT_DRIVING_QUANTITY:
          if (data > DDQ_Max)
              error = 1;
      break;
      case I_HEAT_POWER_MIN:
          if (!((data >= RAM_Read_Data_Long(I_HEAT_POWER_MIN_MIN)) && (data <= RAM_Read_Data_Long(I_HEAT_POWER_MIN_MAX))))
              error = 1;
      break;
      case I_HEAT_POWER_MAX:
          if (!((data >= RAM_Read_Data_Long(I_HEAT_POWER_MIN_MIN)) && (data <= RAM_Read_Data_Long(I_HEAT_POWER_MIN_MAX))))
              error = 1;
      break;
      case HEAT_POWER_CURRENT_MIN:
          if (!((data >= RAM_Read_Data_Long(HEAT_POWER_CURRENT_MIN_MIN)) && (data <= RAM_Read_Data_Long(HEAT_POWER_CURRENT_MIN_MAX))))
              error = 1;
      break;
      case HEAT_POWER_CURRENT_MAX:
          if (!((data >= RAM_Read_Data_Long(HEAT_POWER_CURRENT_MIN_MIN)) && (data <= RAM_Read_Data_Long(HEAT_POWER_CURRENT_MIN_MAX))))
              error = 1;
      break;
      case I_FLOW_MIN:
          if (!((data >= RAM_Read_Data_Long(I_FLOW_MIN_MIN)) && (data <= RAM_Read_Data_Long(I_FLOW_MIN_MAX))))
              error = 1;
      break;
      case I_FLOW_MAX:
          if (!((data >= RAM_Read_Data_Long(I_FLOW_MIN_MIN)) && (data <= RAM_Read_Data_Long(I_FLOW_MIN_MAX))))
              error = 1;
      break;
      case CURRENT_MIN:
          if (!((data >= RAM_Read_Data_Long(CURRENT_MIN_MIN)) && (data <= RAM_Read_Data_Long(CURRENT_MIN_MAX))))
              error = 1;
      break;
      case CURRENT_MAX:
          if (!((data >= RAM_Read_Data_Long(CURRENT_MIN_MIN)) && (data <= RAM_Read_Data_Long(CURRENT_MIN_MAX))))
              error = 1;
      break;
      case I_CAL_POINT_1:
          if (!((data >= RAM_Read_Data_Long(I_CAL_POINT_1_MIN)) && (data <= RAM_Read_Data_Long(I_CAL_POINT_1_MAX))))
              error = 1;
      break;
      case I_CAL_POINT_2:
          if (!((data >= RAM_Read_Data_Long(I_CAL_POINT_1_MIN)) && (data <= RAM_Read_Data_Long(I_CAL_POINT_1_MAX))))
              error = 1;
      break;
      case I_CAL_CONST_1:
          if (!((data >= RAM_Read_Data_Long(I_CAL_CONST_1_MIN)) && (data <= RAM_Read_Data_Long(I_CAL_CONST_1_MAX))))
              error = 1;
      break;
      case I_CAL_CONST_2:
          if (!((data >= RAM_Read_Data_Long(I_CAL_CONST_1_MIN)) && (data <= RAM_Read_Data_Long(I_CAL_CONST_1_MAX))))
              error = 1;
      break;
      case RE1_SET:
          if (data > 9)
              error = 1;
      break;
      case RE2_SET:
          if (data > 9)
              error = 1;
      break;
      case RE_FLOW_1:
          if (!((data >= RAM_Read_Data_Long(RE_FLOW_1_MIN)) && (data <= RAM_Read_Data_Long(RE_FLOW_1_MAX))))
              error = 1;
      break;
      case RE_FLOW_2:
          if (!((data >= RAM_Read_Data_Long(RE_FLOW_1_MIN)) && (data <= RAM_Read_Data_Long(RE_FLOW_1_MAX))))
              error = 1;
      break;
      case RE_HYSTERESIS_1:
          if (!((data >= RAM_Read_Data_Long(RE_HYSTERESIS_1_MIN)) && (data <= RAM_Read_Data_Long(RE_HYSTERESIS_1_MAX))))
              error = 1;
      break;
      case RE_HYSTERESIS_2:
          if (!((data >= RAM_Read_Data_Long(RE_HYSTERESIS_1_MIN)) && (data <= RAM_Read_Data_Long(RE_HYSTERESIS_1_MAX))))
              error = 1;
      break;
      case RE3_SET:
          if (data > 8)
              error = 1;
      break;
      case RE4_SET:
          if (data > 8)
              error = 1;
      break;
      case RE_VOLUME_PLUS:
          if (!((data >= RAM_Read_Data_Long(RE_VOLUME_PLUS_MIN)) && (data <= RAM_Read_Data_Long(RE_VOLUME_PLUS_MAX))))
              error = 1;
      break;
      case RE_VOLUME_MINUS:
          if (!((data >= RAM_Read_Data_Long(RE_VOLUME_MINUS_MIN)) && (data <= RAM_Read_Data_Long(RE_VOLUME_MINUS_MAX))))
              error = 1;
      break;
      case RE_DOSE:
          if (!((data >= RAM_Read_Data_Long(RE_DOSE_MIN)) && (data <= RAM_Read_Data_Long(RE_DOSE_MAX))))
              error = 1;
      break;
      case BIN_RE1_SET:
          if (data > PULSE_OUT_RF_Max)
              error = 1;
      break;
      case BIN_RE1_HEAT_POWER_1:
          if (!((data >= RAM_Read_Data_Long(BIN_RE1_HEAT_POWER_1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE1_HEAT_POWER_1_MAX))))
              error = 1;
      break;
      case BIN_RE1_HEAT_POWER_2:
          if (!((data >= RAM_Read_Data_Long(BIN_RE1_HEAT_POWER_1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE1_HEAT_POWER_1_MAX))))
              error = 1;
      break;
      case BIN_RE1_HEAT_POWER_HYSTERESIS_1:
          if (!((data >= RAM_Read_Data_Long(BIN_RE1_HEAT_POWER_HYSTERESIS_1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE1_HEAT_POWER_HYSTERESIS_1_MAX))))
              error = 1;
      break;
      case BIN_RE1_HEAT_POWER_HYSTERESIS_2:
          if (!((data >= RAM_Read_Data_Long(BIN_RE1_HEAT_POWER_HYSTERESIS_1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE1_HEAT_POWER_HYSTERESIS_1_MAX))))
              error = 1;
      break;
      case BIN_RE2_SET:
          if (data > PULSE_OUT_RF_Max)
              error = 1;
      break;
      case BIN_RE2_HEAT_POWER_1:
          if (!((data >= RAM_Read_Data_Long(BIN_RE2_HEAT_POWER_1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE2_HEAT_POWER_1_MAX))))
              error = 1;
      break;
      case BIN_RE2_HEAT_POWER_2:
          if (!((data >= RAM_Read_Data_Long(BIN_RE2_HEAT_POWER_1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE2_HEAT_POWER_1_MAX))))
              error = 1;
      break;
      case BIN_RE2_HEAT_POWER_HYSTERESIS_1:
          if (!((data >= RAM_Read_Data_Long(BIN_RE2_HEAT_POWER_HYSTERESIS_1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE2_HEAT_POWER_HYSTERESIS_1_MAX))))
              error = 1;
      break;
      case BIN_RE2_HEAT_POWER_HYSTERESIS_2:
          if (!((data >= RAM_Read_Data_Long(BIN_RE2_HEAT_POWER_HYSTERESIS_1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE2_HEAT_POWER_HYSTERESIS_1_MAX))))
              error = 1;
      break;
      case BIN_RE3_SET:
          if (data > PULSE_OUT_OF_Max)
              error = 1;
      break;
      case BIN_RE3_HEAT_POWER_1:
          if (!((data >= RAM_Read_Data_Long(BIN_RE3_HEAT_POWER_1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE3_HEAT_POWER_1_MAX))))
              error = 1;
      break;
      case BIN_RE3_HEAT_POWER_2:
          if (!((data >= RAM_Read_Data_Long(BIN_RE3_HEAT_POWER_1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE3_HEAT_POWER_1_MAX))))
              error = 1;
      break;
      case BIN_RE3_HEAT_POWER_HYSTERESIS_1:
          if (!((data >= RAM_Read_Data_Long(BIN_RE3_HEAT_POWER_HYSTERESIS_1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE3_HEAT_POWER_HYSTERESIS_1_MAX))))
              error = 1;
      break;
      case BIN_RE3_HEAT_POWER_HYSTERESIS_2:
          if (!((data >= RAM_Read_Data_Long(BIN_RE3_HEAT_POWER_HYSTERESIS_1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE3_HEAT_POWER_HYSTERESIS_1_MAX))))
              error = 1;
      break;
      case BIN_RE3_F_HEAT_POWER1:
          if (!((data >= RAM_Read_Data_Long(BIN_RE3_F_HEAT_POWER1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE3_F_HEAT_POWER1_MAX))))
              error = 1;
      break;
      case BIN_RE3_F_HEAT_POWER2:
          if (!((data >= RAM_Read_Data_Long(BIN_RE3_F_HEAT_POWER1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE3_F_HEAT_POWER1_MAX))))
              error = 1;
      break;
      case BIN_RE3_F_HEAT_POWER_FREQ1:
          if (!((data >= RAM_Read_Data_Long(BIN_RE3_F_HEAT_POWER_FREQ1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE3_F_HEAT_POWER_FREQ1_MAX))))
              error = 1;
      break;
      case BIN_RE3_F_HEAT_POWER_FREQ2:
          if (!((data >= RAM_Read_Data_Long(BIN_RE3_F_HEAT_POWER_FREQ1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE3_F_HEAT_POWER_FREQ1_MAX))))
              error = 1;
      break;
      case BIN_RE4_SET:
          if (data > PULSE_OUT_OF_Max)
              error = 1;
      break;
      case BIN_RE4_HEAT_POWER_1:
          if (!((data >= RAM_Read_Data_Long(BIN_RE4_HEAT_POWER_1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE4_HEAT_POWER_1_MAX))))
              error = 1;
      break;
      case BIN_RE4_HEAT_POWER_2:
          if (!((data >= RAM_Read_Data_Long(BIN_RE4_HEAT_POWER_1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE4_HEAT_POWER_1_MAX))))
              error = 1;
      break;
      case BIN_RE4_HEAT_POWER_HYSTERESIS_1:
          if (!((data >= RAM_Read_Data_Long(BIN_RE4_HEAT_POWER_HYSTERESIS_1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE4_HEAT_POWER_HYSTERESIS_1_MAX))))
              error = 1;
      break;
      case BIN_RE4_HEAT_POWER_HYSTERESIS_2:
          if (!((data >= RAM_Read_Data_Long(BIN_RE4_HEAT_POWER_HYSTERESIS_1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE4_HEAT_POWER_HYSTERESIS_1_MAX))))
              error = 1;
      break;
      case BIN_RE4_F_HEAT_POWER1:
          if (!((data >= RAM_Read_Data_Long(BIN_RE4_F_HEAT_POWER1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE4_F_HEAT_POWER1_MAX))))
              error = 1;
      break;
      case BIN_RE4_F_HEAT_POWER2:
          if (!((data >= RAM_Read_Data_Long(BIN_RE4_F_HEAT_POWER1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE4_F_HEAT_POWER1_MAX))))
              error = 1;
      break;
      case BIN_RE4_F_HEAT_POWER_FREQ1:
          if (!((data >= RAM_Read_Data_Long(BIN_RE4_F_HEAT_POWER_FREQ1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE4_F_HEAT_POWER_FREQ1_MAX))))
              error = 1;
      break;
      case BIN_RE4_F_HEAT_POWER_FREQ2:
          if (!((data >= RAM_Read_Data_Long(BIN_RE4_F_HEAT_POWER_FREQ1_MIN)) && (data <= RAM_Read_Data_Long(BIN_RE4_F_HEAT_POWER_FREQ1_MAX))))
              error = 1;
      break;
      case F_SET:
          if (data > 6)
              error = 1;
      break;
      case F_FLOW1:
          if (!((data >= RAM_Read_Data_Long(F_FLOW1_MIN)) && (data <= RAM_Read_Data_Long(F_FLOW1_MAX))))
              error = 1;
      break;
      case F_FLOW2:
          if (!((data >= RAM_Read_Data_Long(F_FLOW1_MIN)) && (data <= RAM_Read_Data_Long(F_FLOW1_MAX))))
              error = 1;
      break;
      case F_FREQ1:
          if (!((data >= RAM_Read_Data_Long(F_FREQ1_MIN)) && (data <= RAM_Read_Data_Long(F_FREQ1_MAX))))
              error = 1;
      break;
      case F_FREQ2:
          if (!((data >= RAM_Read_Data_Long(F_FREQ1_MIN)) && (data <= RAM_Read_Data_Long(F_FREQ1_MAX))))
              error = 1;
      break;
      case F_DUTY_CYCLE:
          if (!((data >= RAM_Read_Data_Long(F_DUTY_CYCLE_MIN)) && (data <= RAM_Read_Data_Long(F_DUTY_CYCLE_MAX))))
              error = 1;
      break;
      case PASSWORD_USER:
          if (!((data >= RAM_Read_Data_Long(PASSWORD_USER_MIN)) && (data <= RAM_Read_Data_Long(PASSWORD_USER_MAX))))
              error = 1;
      break;
      case MODBUS_SLAVE_ADDRESS:
          if (!((data >= RAM_Read_Data_Long(MODBUS_SLAVE_ADDRESS_MIN)) && (data <= RAM_Read_Data_Long(MODBUS_SLAVE_ADDRESS_MAX))))
              error = 1;
      break;
      case MODBUS_BAUDRATE:
          if (data > 3)
              error = 1;
      break;
      case MODBUS_PARITY:
          if (data > 3)
              error = 1;
      break;
      case MBUS_SLAVE_ADDRESS:
          if (!((data >= RAM_Read_Data_Long(MBUS_SLAVE_ADDRESS_MIN)) && (data <= RAM_Read_Data_Long(MBUS_SLAVE_ADDRESS_MAX))))
              error = 1;
      break;
      case MBUS_BAUDRATE:
          if (data > MBUS_BAUDRATE_Max)
              error = 1;
      break;
      case MBUS_PARITY:
          if (data > MBUS_PAR_Max)
              error = 1;
      break;
      case CLEAN_POWER:
          if (data > 1)
              error = 1;
      break;
      case CLEAN_TIME:
          if (!((data >= RAM_Read_Data_Long(CLEAN_TIME_MIN)) && (data <= RAM_Read_Data_Long(CLEAN_TIME_MAX))))
              error = 1;
      break;
      case GSM_DATA_INTERVAL:
          if (!((data >= RAM_Read_Data_Long(GSM_DATA_INTERVAL_MIN)) && (data <= RAM_Read_Data_Long(GSM_DATA_INTERVAL_MAX))))
              error = 1;
      break;
      case TOTALIZER_CYCLING:
          if (data > 1)
              error = 1;
      break;
      /// ------------------------------- External measurements
      case EXT_TEMP_MEAS_STATE:
          if (data > 1)
              error = 1;
      break;
      case EXT_TEMP_SENSOR_TYPE:
          if (data > 3)
              error = 1;
      break;
      case EXT_TEMP_SENSOR_CONN:
          if (data > 2)
              error = 1;
      break;
      case EXT_CAL_TEMPERATURE1:
          if (!((data >= RAM_Read_Data_Long(EXT_CAL_TEMPERATURE1_MIN)) && (data <= RAM_Read_Data_Long(EXT_CAL_TEMPERATURE1_MAX))))
              error = 1;
      break;
      case EXT_CAL_TEMPERATURE2:
          if (!((data >= RAM_Read_Data_Long(EXT_CAL_TEMPERATURE2_MIN)) && (data <= RAM_Read_Data_Long(EXT_CAL_TEMPERATURE2_MAX))))
              error = 1;
      break;
      /// ------------------------------- service
      case DEMO:
          if (data > 1)
              error = 1;
      break;
      case SIMULATED_FLOW:
          if (!((data >= RAM_Read_Data_Long(SIMULATED_FLOW_MIN)) && (data <= RAM_Read_Data_Long(SIMULATED_FLOW_MAX))))
              error = 1;
      break;
      case SD_CARD_BUFFER:
          if (data > 1)
              error = 1;
      break;
      /// ------------------------------- factory
      case DIAMETER:
          if (!((data >= RAM_Read_Data_Long(DIAMETER_MIN)) && (data <= RAM_Read_Data_Long(DIAMETER_MAX))))
              error = 1;
      break;
      case UNIT_NO:
          if (!((data >= RAM_Read_Data_Long(UNIT_NO_MIN)) && (data <= RAM_Read_Data_Long(UNIT_NO_MAX))))
              error = 1;
      break;
      case CALIBRATION_POINT_ONE:
          if (!((data >= RAM_Read_Data_Long(CALIBRATION_POINT_ONE_MIN)) && (data <= RAM_Read_Data_Long(CALIBRATION_POINT_ONE_MAX))))
              error = 1;
      break;
      case CALIBRATION_POINT_TWO:
          if (!((data >= RAM_Read_Data_Long(CALIBRATION_POINT_TWO_MIN)) && (data <= RAM_Read_Data_Long(CALIBRATION_POINT_TWO_MAX))))
              error = 1;
      break;
      case CALIBRATION_POINT_THREE:
          if (!((data >= RAM_Read_Data_Long(CALIBRATION_POINT_THREE_MIN)) && (data <= RAM_Read_Data_Long(CALIBRATION_POINT_THREE_MAX))))
              error = 1;
      break;
      case EXCITATION_FREQUENCY:
          if (data > 1)
              error = 1;
      break;
      case EXCITATION:
          if (data > 1)
              error = 1;
      break;
      case SERVICE_MODE:
          if (data > 1)
              error = 1;
      break;
      case OPT_DU_BTN_LEVEL:
          if (!((data >= RAM_Read_Data_Long(OPT_DU_BTN_LEVEL_MIN)) && (data <= RAM_Read_Data_Long(OPT_DU_BTN_LEVEL_MAX))))
              error = 1;
      break;
      case OPT_LR_BTN_LEVEL:
          if (!((data >= RAM_Read_Data_Long(OPT_LR_BTN_LEVEL_MIN)) && (data <= RAM_Read_Data_Long(OPT_LR_BTN_LEVEL_MAX))))
              error = 1;
      break;
      case OPT_EE_BTN_LEVEL:
          if (!((data >= RAM_Read_Data_Long(OPT_EE_BTN_LEVEL_MIN)) && (data <= RAM_Read_Data_Long(OPT_EE_BTN_LEVEL_MAX))))
              error = 1;
      break;
      case FLOW_PULSE_TIMEOUT:
          if (!((data >= RAM_Read_Data_Long(FLOW_PULSE_TIMEOUT_MIN)) && (data <= RAM_Read_Data_Long(FLOW_PULSE_TIMEOUT_MAX))))
              error = 1;
      break;
      case HEAT_TEST_TIME:
          if (!((data >= RAM_Read_Data_Long(HEAT_TEST_TIME_MIN)) && (data <= RAM_Read_Data_Long(HEAT_TEST_TIME_MAX))))
              error = 1;
      break;
      /// ------------------------------- authorize
      case PASSWORD_AUTHORIZE:
          if (!((data >= RAM_Read_Data_Long(PASSWORD_AUTHORIZE_MIN)) && (data <= RAM_Read_Data_Long(PASSWORD_AUTHORIZE_MAX))))
              error = 1;
      break;

  }

  if (!error)
    WriteEventLogParameterChange((address-(unsigned int)VALUES_OFFSET)+VALUES_OFFSET, data);

  return error;
}

void CallOutputFunction(unsigned char Table_No, UCHAR *pucRegBuffer, USHORT usAddress)
{
    // zavola prislusnou vystupni funkci
    unsigned int adress = 0;
    unsigned long data = 0;
    unsigned char fun[2];
    fun[F_OUT] = 0;

    switch ((unsigned int)(Table_No))
    {
        // rozhodne do ktere tabulky se ma podivat pro adresu v menu a do Table_No ulozi adresu
      case 1 :
        adress = (ModbusTable1[(usAddress-MODBUS_INFO_OFFSET)/2]);
        break;
      case 2 :
        adress = (ModbusTable2[(usAddress-MODBUS_DISPLAY_OFFSET)/2]);
        break;
      case 3 :
        adress = (ModbusTable3[(usAddress-MODBUS_USER_OFFSET)/2]);
        break;
      case 4 :
        adress = (ModbusTable4[(usAddress-MODBUS_SERVICE_OFFSET)/2]);
        break;
      case 5 :
        adress = (ModbusTable5[(usAddress-MODBUS_FACTORY_OFFSET)/2]);
        break;
      case 6 :
        adress = (ModbusTable6[(usAddress-MODBUS_AUTHORIZE_OFFSET)/2]);
        break;
    }

    data  = (unsigned long)(*pucRegBuffer++)<<8;
    data |= *pucRegBuffer++;
    data |= (unsigned long)(*pucRegBuffer++)<<24;
    data |= (unsigned long)(*pucRegBuffer++)<<16;

    switch (adress)
    {
      case UNIT_VOLUME             :
        fun[F_OUT] = 24;
        break;
      case DELETE_AUXILIARY_VOLUME :
        fun[F_OUT] =  2;
        break;
      case DATALOGGER_INTERVAL :
        fun[F_OUT] = 14;
        break;
      case CURRENT_SET             :
        fun[F_OUT] = 32;
        break;
      case I_FLOW_MIN              :
        fun[F_OUT] = 33;
        break;
      case I_FLOW_MAX              :
        fun[F_OUT] = 33;
        break;
      case CURRENT_MIN             :
        fun[F_OUT] = 33;
        break;
      case CURRENT_MAX             :
        fun[F_OUT] = 33;
        break;
      case I_CAL_POINT_1           :
        fun[F_OUT] = 33;
        break;
      case I_CAL_POINT_2           :
        fun[F_OUT] = 33;
        break;
      case I_CAL_CONST_1           :
        fun[F_OUT] = 33;
        break;
      case I_CAL_CONST_2           :
        fun[F_OUT] = 33;
        break;
      /*case VOLTAGE_SET             :
        fun[F_OUT] = 34;
        break;
      case V_FLOW_MIN              :
        fun[F_OUT] = 35;
        break;
      case V_FLOW_MAX              :
        fun[F_OUT] = 35;
        break;
      case VOLTAGE_MIN             :
        fun[F_OUT] = 35;
        break;
      case VOLTAGE_MAX             :
        fun[F_OUT] = 35;
        break;*/
      case RE1_SET                 :
        fun[F_OUT] = 15;
        break;
      case RE2_SET                 :
        fun[F_OUT] = 16;
        break;
      case RE3_SET                 :
        fun[F_OUT] = 18;
        break;
      case RE4_SET                 :
        fun[F_OUT] = 19;
        break;
      case RE_FLOW_1               :
      case RE_FLOW_2               :
      case RE_HYSTERESIS_1         :
      case RE_HYSTERESIS_2         :
        fun[F_OUT] = 17;
        break;
      case RE_VOLUME_PLUS          :
        fun[F_OUT] = 20;
        break;
      case RE_VOLUME_MINUS         :
        fun[F_OUT] = 21;
        break;
      case RE_DOSE                 :
        fun[F_OUT] = 22;
        break;
      case F_SET                   :
        fun[F_OUT] = 23;
        break;
      case F_FLOW1                 :
      case F_FLOW2                 :
      case F_FREQ1                 :
      case F_FREQ2                 :
        fun[F_OUT] = 27;
        break;
      case F_DUTY_CYCLE            :
        fun[F_OUT] = 28;
        break;
      case LOAD_DEFAULT_SETTING    :
        fun[F_OUT] =  3;
        break;
      case PASSWORD_USER           :
        fun[F_OUT] = 12;
        break;
      case DIAMETER                :
        fun[F_OUT] = 13;
        break;
      case DELETE_ERROR_MIN        :
        fun[F_OUT] =  6;
        break;
      case DELETE_OK_MIN           :
        fun[F_OUT] =  7;
        break;
      case DELETE_NEGATIVE_VOLUME  :
        fun[F_OUT] =  9;
        break;
      case DELETE_POSITIVE_VOLUME  :
        fun[F_OUT] = 10;
        break;
      case DELETE_TOTAL_VOLUME     :
        fun[F_OUT] = 11;
        break;
      case SAVE_SETTINGS           :
        fun[F_OUT] =  8;
        break;
      case MODBUS_SLAVE_ADDRESS    :
        RAM_Write_Data_Long(data, MODBUS_SLAVE_ADDRESS);
        ChangeCommunicationParam=true;
        modbus_transmitt_state = 1;
        fun[F_OUT] = 40;
        break;
      case MODBUS_BAUDRATE         :
      case MODBUS_PARITY           :
        fun[F_OUT] = 40;
        break;
      case EXCITATION_FREQUENCY    :
        fun[F_OUT] = 41;
        break;
      case EXCITATION              :
        fun[F_OUT] = 42;
        break;
      case CLEAN_POWER             :
        fun[F_OUT] = 43;
        break;
      case CLEAN_STARTNOW          :
        fun[F_OUT] = 44;
        break;
      case UNIT_NO                 :
        fun[F_OUT] = 45;
        break;
      case ZERO_FLOW               :
        fun[F_OUT] = 38;
        break;
      case ZERO_FLOW_ERASE         :
        fun[F_OUT] = 46;
        break;
      case CONTRAST                :
        fun[F_OUT] = 47;
        break;
      case FLOW_RANGE              :
        fun[F_OUT] = 48;
        break;
      case MEASUREMENT_STATE       :
        fun[F_OUT] = 49;
        break;
      case AIR_DETECTOR            :
        fun[F_OUT] = 50;
        break;
      case AIR_CONSTANT            :
        fun[F_OUT] = 51;
        break;
      case AVERAGE_SAMPLES         :
        fun[F_OUT] = 52;
        break;
      case LOW_FLOW_CUTOFF         :
        fun[F_OUT] = 53;
        break;
      case INVERT_FLOW             :
        fun[F_OUT] = 54;
        break;
      case CLEAN_TIME              :
        fun[F_OUT] = 55;
        break;
      case DEMO                    :
        fun[F_OUT] = 56;
        break;
      case SIMULATED_FLOW          :
        fun[F_OUT] = 57;
        break;
      case START_DELAY             :
        fun[F_OUT] = 58;
        break;
      case GSM_DATA_INTERVAL       :
        fun[F_OUT] = 59;
        break;
      case GSM_PHONE_1_H           :
      case GSM_PHONE_1_L           :
        fun[F_OUT] = 60;
        break;

      case GSM_PHONE_2_H           :
      case GSM_PHONE_2_L           :
        fun[F_OUT] = 61;
        break;

      case GSM_PHONE_3_H           :
      case GSM_PHONE_3_L           :
        fun[F_OUT] = 62;
        break;

      case GSM_EMPTY_PIPE          :
        fun[F_OUT] = 63;
        break;
      case GSM_ZERO_FLOW           :
        fun[F_OUT] = 64;
        break;
      case GSM_ERROR_DETECT        :
        fun[F_OUT] = 65;
        break;
      case GSM_EMPTY_PIPE_SEND     :
        fun[F_OUT] = 66;
        break;
      case GSM_ZERO_FLOW_SEND      :
        fun[F_OUT] = 67;
        break;
      case WIFI_AP_MODE            :
        fun[F_OUT] = 68;
        break;
      case EXT_TEMP_MEAS_STATE     :
        fun[F_OUT] = 69;
        break;
      case EXT_TEMP1_CAL_STARTNOW   :
        fun[F_OUT] = 70;
        break;
      case EXT_TEMP_SENSOR_TYPE    :
        fun[F_OUT] = 73;
        break;
      case EXT_TEMP_SENSOR_CONN    :
        fun[F_OUT] = 74;
        break;
      case EXT_TEMP2_CAL_STARTNOW   :
        fun[F_OUT] = 75;
        break;
      case DELETE_TOTAL_HEAT     :
        fun[F_OUT] = 76;
        break;
      case I_DIRECT_DRIVING_QUANTITY     :
        fun[F_OUT] = 77;
        break;

      case I_HEAT_POWER_MIN:
      case I_HEAT_POWER_MAX:
      case HEAT_POWER_CURRENT_MIN:
      case HEAT_POWER_CURRENT_MAX:
        fun[F_OUT] = 78;
        break;

      case BIN_RE1_SET:
        fun[F_OUT] = 79;
        break;

      case BIN_RE1_HEAT_POWER_1:
      case BIN_RE1_HEAT_POWER_2:
      case BIN_RE1_HEAT_POWER_HYSTERESIS_1:
      case BIN_RE1_HEAT_POWER_HYSTERESIS_2:
        fun[F_OUT] = 88;
        break;

      case BIN_RE2_SET:
        fun[F_OUT] = 80;
        break;

      case BIN_RE2_HEAT_POWER_1:
      case BIN_RE2_HEAT_POWER_2:
      case BIN_RE2_HEAT_POWER_HYSTERESIS_1:
      case BIN_RE2_HEAT_POWER_HYSTERESIS_2:
        fun[F_OUT] = 89;
        break;

      case BIN_RE3_SET:
        fun[F_OUT] = 81;
        break;

      case BIN_RE3_HEAT_POWER_1:
      case BIN_RE3_HEAT_POWER_2:
      case BIN_RE3_HEAT_POWER_HYSTERESIS_1:
      case BIN_RE3_HEAT_POWER_HYSTERESIS_2:
        fun[F_OUT] = 90;
        break;

      case BIN_RE3_F_HEAT_POWER1:
      case BIN_RE3_F_HEAT_POWER2:
      case BIN_RE3_F_HEAT_POWER_FREQ1:
      case BIN_RE3_F_HEAT_POWER_FREQ2:
        fun[F_OUT] = 92;
        break;

      case BIN_RE4_SET:
        fun[F_OUT] = 82;
        break;

      case BIN_RE4_HEAT_POWER_1:
      case BIN_RE4_HEAT_POWER_2:
      case BIN_RE4_HEAT_POWER_HYSTERESIS_1:
      case BIN_RE4_HEAT_POWER_HYSTERESIS_2:
        fun[F_OUT] = 91;
        break;

      case BIN_RE4_F_HEAT_POWER1:
      case BIN_RE4_F_HEAT_POWER2:
      case BIN_RE4_F_HEAT_POWER_FREQ1:
      case BIN_RE4_F_HEAT_POWER_FREQ2:
        fun[F_OUT] = 93;
        break;

      case MBUS_SLAVE_ADDRESS:
      case MBUS_BAUDRATE:
      case MBUS_PARITY:
        fun[F_OUT] = 83;
        break;

      case GRAPH_QUANTITY:
        fun[F_OUT] = 84;
        break;

      case HEAT_POWER_RANGE:
        fun[F_OUT] = 85;
        break;

      case MBUS_MODULE:
        fun[F_OUT] = 86;
        break;

      case FLOW_SENSOR_TYPE:
        fun[F_OUT] = 87;
        break;

      case STATUS_LED_MODE:
        fun[F_OUT] = 94;
        break;

      case FLOW_PULSE_TIMEOUT:
        fun[F_OUT] = 95;
        break;

      case HEAT_TEST_STARTNOW:
        fun[F_OUT] = 96;
        break;
    }

    g_value[0].val = data; // prirazeni posilane hodnoty
    EditDoFunction(F_OUT, fun); // provedeni vystupni funkce
}


eMBErrorCode eMBRegHoldingCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs, eMBRegisterMode eMode )
{
  eMBErrorCode    eStatus = MB_ENOERR;
  unsigned long TmpData;

  ///Online
  if ((usAddress>=MODBUS_ONLINE_OFFSET)&&(usAddress+usNRegs<= MODBUS_ONLINE_OFFSET+NUMBER_OF_ONLINE*2)) //Registry jsou 32bitove
  {
      if ( eMode==MB_REG_READ )
      {
          while (usNRegs)
          {
              switch ((usAddress-MODBUS_ONLINE_OFFSET)/2)//(online measuerement)
              {
                case MODBUS_MEAS_FLOW:
                  TmpData=(long)(g_measurement.actual*1000);
                  break;
                case MODBUS_MEAS_TOTAL_DIG:
                  TmpData=(unsigned long)(g_measurement.total/1000000.0);
                  break;
                case MODBUS_MEAS_TOTAL_DEC:
                  TmpData=(unsigned long)(((g_measurement.total)%1000000)/1000);
                  break;
                case MODBUS_MEAS_AUX_DIG:
                  TmpData=(unsigned long)(g_measurement.aux/1000000.0);
                  break;
                case MODBUS_MEAS_AUX_DEC:
                  TmpData=(unsigned long)(((g_measurement.aux)%1000000)/1000);
                  break;
                case MODBUS_MEAS_POS_DIG:
                  TmpData=(unsigned long)(g_measurement.flow_pos/1000000.0);
                  break;
                case MODBUS_MEAS_POS_DEC:
                  TmpData=(unsigned long)(((g_measurement.flow_pos)%1000000)/1000);
                  break;
                case MODBUS_MEAS_NEG_DIG:
                  TmpData=(unsigned long)(g_measurement.flow_neg/1000000.0);
                  break;
                case MODBUS_MEAS_NEG_DEC:
                  TmpData=(unsigned long)(((g_measurement.flow_neg)%1000000)/1000);
                  break;
                case MODBUS_MEAS_TEMP:
                  TmpData=(long)(g_measurement.temp*10);
                  break;
                case MODBUS_MEAS_ERROR:
                  TmpData=(unsigned long)(actual_error);
                  break;
                case MODBUS_EXT_MEAS_TEMP1:
                  {
                      float measValue = ExtTemp_GetLastCorrectMeasuredValue(TCHNL_1);
                      if (measValue != 0)
                        TmpData=(long)(Convert_KelvinToCelsius(measValue)*10);
                      else
                        TmpData=0;
                  }
                  break;
                case MODBUS_EXT_MEAS_TEMP2:
                  {
                      float measValue = ExtTemp_GetLastCorrectMeasuredValue(TCHNL_2);
                      if (measValue != 0)
                        TmpData=(long)(Convert_KelvinToCelsius(measValue)*10);
                      else
                        TmpData=0;
                  }
                  break;
                case MODBUS_MEAS_FLOW_ABS:
                  TmpData=(unsigned long)(fabs(g_measurement.actual*1000));
                  break;
                case MODBUS_MEAS_FLOW_SIGN:
                  TmpData=(g_measurement.actual >= 0) ? 0 : 1;
                  break;
                case MODBUS_MEAS_HEAT_PERFORM:
                  {
                      float heatPerform = Heat_GetHeatPerformance();
                      if (heatPerform >= 0)
                        TmpData = (long)(heatPerform*1000);
                      else
                        TmpData = 0;
                  }
                  break;
                case MODBUS_MEAS_HEAT_TOTAL:
                  {
                      float heatTotal = Heat_GetTotalHeat(HU_kWh);
                      if (heatTotal >= 0)
                        TmpData = (long)(heatTotal*1000);
                      else
                        TmpData = 0;
                  }
                  break;
                default:
                  TmpData=0;
              }
              if ((usAddress-MODBUS_ONLINE_OFFSET)%2) //cteni vyssiho slova
              {
                  *pucRegBuffer++ = ((long)TmpData>>24);
                  *pucRegBuffer++ = ((long)TmpData>>16);
              }
              else
              {
                  *pucRegBuffer++ = ((long)TmpData>>8);
                  *pucRegBuffer++ =  (long)TmpData;
              }
              usNRegs--;
              usAddress++;
          }
      }
      else
      {
          eStatus = MB_ENOREG; //Pouze cteni
      }
  }
  ///Online float
  else if ((usAddress>=MODBUS_ONLINE_FLOAT_OFFSET)&&(usAddress+usNRegs<= MODBUS_ONLINE_FLOAT_OFFSET+NUMBER_OF_ONLINE_FLOAT*2)) //Registry jsou 32bitove
  {
      if ( eMode==MB_REG_READ )
      {
          while (usNRegs)
          {
              switch ((usAddress-MODBUS_ONLINE_FLOAT_OFFSET)/2)//(online float measuerement)
              {
                case MODBUS_MEAS_FLOW_FLOAT:
                  TmpData = FloatToUlong(g_measurement.actual);
                  break;
                case MODBUS_MEAS_TOTAL_FLOAT:
                  TmpData = FloatToUlong(g_measurement.total/1000000.0);
                  break;
                case MODBUS_MEAS_AUX_FLOAT:
                  TmpData = FloatToUlong(g_measurement.aux/1000000.0);
                  break;
                case MODBUS_MEAS_POS_FLOAT:
                  TmpData = FloatToUlong(g_measurement.flow_pos/1000000.0);
                  break;
                case MODBUS_MEAS_NEG_FLOAT:
                  TmpData = FloatToUlong(g_measurement.flow_neg/1000000.0);
                  break;
                case MODBUS_MEAS_TEMP_FLOAT:
                  TmpData = FloatToUlong(g_measurement.temp);
                  break;
                case MODBUS_EXT_MEAS_TEMP1_FLOAT:
                  {
                      float measValue = ExtTemp_GetLastCorrectMeasuredValue(TCHNL_1);
                      if (measValue != 0)
                        TmpData = FloatToUlong(Convert_KelvinToCelsius(measValue));
                      else
                        TmpData = 0;
                  }
                  break;
                case MODBUS_EXT_MEAS_TEMP2_FLOAT:
                  {
                      float measValue = ExtTemp_GetLastCorrectMeasuredValue(TCHNL_2);
                      if (measValue != 0)
                        TmpData = FloatToUlong(Convert_KelvinToCelsius(measValue));
                      else
                        TmpData = 0;
                  }
                  break;
                case MODBUS_MEAS_HEAT_PERFORM_FLOAT:
                  {
                      float heatPerform = Heat_GetHeatPerformance();
                      if (heatPerform >= 0)
                        TmpData = FloatToUlong(heatPerform);
                      else
                        TmpData = 0;
                  }
                  break;
                case MODBUS_MEAS_HEAT_TOTAL_FLOAT:
                  {
                      float heatTotal = Heat_GetTotalHeat(HU_kWh);
                      if (heatTotal >= 0)
                        TmpData = FloatToUlong(heatTotal);
                      else
                        TmpData = 0;
                  }
                  break;
                default:
                  TmpData = 0;
              }
              if ((usAddress-MODBUS_ONLINE_FLOAT_OFFSET)%2) //cteni vyssiho slova
              {
                  *pucRegBuffer++ = ((long)TmpData>>24);
                  *pucRegBuffer++ = ((long)TmpData>>16);
              }
              else
              {
                  *pucRegBuffer++ = ((long)TmpData>>8);
                  *pucRegBuffer++ =  (long)TmpData;
              }
              usNRegs--;
              usAddress++;
          }
      }
      else
      {
          eStatus = MB_ENOREG; //Pouze cteni
      }
  }
  ///passwords
  else if ((usAddress>=MODBUS_PASS_OFFSET)&&(usAddress+usNRegs<= MODBUS_PASS_OFFSET+NUMBER_OF_PASS*2)) //Registry jsou 32bitove
  {
      if ( eMode==MB_REG_WRITE )
      {
          while (usNRegs)
          {
              unsigned char iRegIndex=(usAddress-MODBUS_PASS_OFFSET)/2;
              if ((usAddress-MODBUS_PASS_OFFSET)%2) //zapis vyssiho slova
              {
                  ModbusPass[iRegIndex]&=0x0000ffffUL;
                  ModbusPass[iRegIndex]|=(ULONG)(*pucRegBuffer)<<24;
                  pucRegBuffer++;
                  ModbusPass[iRegIndex]|=(ULONG)(*pucRegBuffer)<<16;
                  pucRegBuffer++;
              }
              else
              {
                  ModbusPass[iRegIndex]&=0xffff0000UL;
                  ModbusPass[iRegIndex]|=(ULONG)(*pucRegBuffer)<<8;
                  pucRegBuffer++;
                  ModbusPass[iRegIndex]|= (ULONG)(*pucRegBuffer);
                  pucRegBuffer++;
              }
              usNRegs--;
              usAddress++;
          }
      }
      else if ( eMode==MB_REG_READ)
      {
          while (usNRegs)
          {
              if (usAddress%2) //cteni vyssiho slova
              {
                  *pucRegBuffer++ = 0;
                  *pucRegBuffer++ = 0;
              }
              else if (((ModbusPass[0]==RAM_Read_Data_Long(PASSWORD_USER))&&(usAddress==MODBUS_PASS_OFFSET))
                       ||((ModbusPass[1]==RAM_Read_Data_Long(PASSWORD_SERVICE))&&(usAddress==MODBUS_PASS_OFFSET+2))
                       ||((ModbusPass[2]==RAM_Read_Data_Long(PASSWORD_FACTORY))&&(usAddress==MODBUS_PASS_OFFSET+4))
                       ||((ModbusPass[3]==RAM_Read_Data_Long(PASSWORD_AUTHORIZE))&&(usAddress==MODBUS_PASS_OFFSET+6)))
              {
                  *pucRegBuffer++ = 0;
                  *pucRegBuffer++ = 1;
              }
              else
              {
                  *pucRegBuffer++ = 0;
                  *pucRegBuffer++ = 0;
              }
              usNRegs--;
              usAddress++;
          }
      }
  }
  ///Online service
  else if ((usAddress>=MODBUS_ONLINE_SERVICE_OFFSET)&&(usAddress+usNRegs<= MODBUS_ONLINE_SERVICE_OFFSET+NUMBER_OF_ONLINE_SERVICE*2)) //Registry jsou 32bitove
  {
      if ( eMode==MB_REG_READ )
      {
          while (usNRegs)
          {
              switch ((usAddress-MODBUS_ONLINE_SERVICE_OFFSET)/2)//(online service measuerement)
              {
                case MODBUS_MEAS_A1:
                  TmpData=act_adc[0];
                  break;
                case MODBUS_MEAS_A2:
                  TmpData=act_adc[1];
                  break;
                case MODBUS_MEAS_A3:
                  TmpData=act_adc[2];
                  break;
                case MODBUS_MEAS_A:
                  TmpData=adc_average;
                  break;
                default:
                  TmpData=0;
              }
              if ((usAddress-MODBUS_ONLINE_SERVICE_OFFSET)%2) //cteni vyssiho slova
              {
                  *pucRegBuffer++ = ((long)TmpData>>24);
                  *pucRegBuffer++ = ((long)TmpData>>16);
              }
              else
              {
                  *pucRegBuffer++ = ((long)TmpData>>8);
                  *pucRegBuffer++ =  (long)TmpData;
              }
              usNRegs--;
              usAddress++;
          }
      }
      else
      {
          eStatus = MB_ENOREG; //Pouze cteni
      }
  }
  ///INFO 1000-1499
  else if ((usAddress>=MODBUS_INFO_OFFSET)&&(usAddress+usNRegs<= MODBUS_INFO_OFFSET+SIZEOF_ModbusTable1*2)) //Registry jsou 32bitove
  {
      switch ( eMode )
      {
      case MB_REG_READ:
      {
          while (usNRegs)
          {
              if( (usAddress == 1024) && (RAM_Read_Data_Long( GSM_SIGNAL )) )
              {
                  get_quality_signal_gsm();
                  delay_100ms();
                  delay_100ms();
                  delay_100ms();
                  zpracovat_data_gsm();
                  flag_gsm_dokoncil_prijem=0;
                  TmpData=RAM_Read_Data_Long(GSM_SIGNAL);
                  *pucRegBuffer++ = (UCHAR)(TmpData>>8);
                  *pucRegBuffer++ = (UCHAR)(TmpData);
              }
              else if( (usAddress == 1025) && ( RAM_Read_Data_Long( GSM_SIGNAL)))
              {
                  TmpData=RAM_Read_Data_Long(GSM_SIGNAL);
                  *pucRegBuffer++ = (UCHAR)(TmpData>>24);
                  *pucRegBuffer++ = (UCHAR)(TmpData>>16);
              }
              else
              {
                  if ((usAddress-MODBUS_INFO_OFFSET)%2) //cteni vyssiho slova
                  {
                      TmpData=RAM_Read_Data_Long((ModbusTable1[(usAddress-MODBUS_INFO_OFFSET)/2]));
                      *pucRegBuffer++ = (UCHAR)(TmpData>>24);
                      *pucRegBuffer++ = (UCHAR)(TmpData>>16);
                  }
                  else
                  {
                      TmpData=RAM_Read_Data_Long((ModbusTable1[(usAddress-MODBUS_INFO_OFFSET)/2]));
                      *pucRegBuffer++ = (UCHAR)(TmpData>>8);
                      *pucRegBuffer++ = (UCHAR)TmpData;
                  }
              }
              usNRegs--;
              usAddress++;
          }
      }
      break;
      case MB_REG_WRITE:
      {
          eStatus = MB_ENOREG; //Pouze cteni
      }
      break;
      }
  }
  ///DISPLAY 1500-1999
  else if ((usAddress>=MODBUS_DISPLAY_OFFSET)&&(usAddress+usNRegs<= MODBUS_DISPLAY_OFFSET+SIZEOF_ModbusTable2*2))
  {
      switch ( eMode )
      {
      case MB_REG_READ:
      {
          while (usNRegs)
          {
            if (usAddress == 1510)
            {
              // Date settings
              *pucRegBuffer++ = 0;
              *pucRegBuffer++ = SDread;
            }
            else if (usAddress == 1511)
            {
              // Date settings
              *pucRegBuffer++ = 0;
              *pucRegBuffer++ = 0;
            }
            else
            {
              if ((usAddress-MODBUS_DISPLAY_OFFSET)%2) //cteni vyssiho slova
              {
                  TmpData=RAM_Read_Data_Long((ModbusTable2[(usAddress-MODBUS_DISPLAY_OFFSET)/2]));
                  *pucRegBuffer++ = (USHORT)(TmpData>>24);
                  *pucRegBuffer++ = (USHORT)(TmpData>>16);
              }
              else
              {
                  TmpData=RAM_Read_Data_Long((ModbusTable2[(usAddress-MODBUS_DISPLAY_OFFSET)/2]));
                  *pucRegBuffer++ = (USHORT)TmpData>>8;
                  *pucRegBuffer++ = (USHORT)TmpData;
              }
            }
            usNRegs--;
            usAddress++;
          }
      }
      break;
      case MB_REG_WRITE:
      {
          unsigned long madata = 0;
          while (usNRegs)
          {
            if (usAddress == 1510)
            {
              madata = ((unsigned long)(*pucRegBuffer++))<<8;
              madata += *pucRegBuffer++;
              delay_100ms();delay_100ms();delay_100ms();
            }
            else if (usAddress == 1511)
            {
              madata += ((unsigned long)(*pucRegBuffer++))<<24;
              madata += ((unsigned long)(*pucRegBuffer++))<<16;
              if (madata == 1)
              {
                // vypni modbus
                eStatus = eMBDisable();
                // posli soubory
                sendFilesName();
                // zapni modbus
                eStatus = eMBEnable();
              }
              else if (madata > 1)
              {
                // vypni modbus
                eStatus = eMBDisable();
                // posli soubory
                sendFile(madata);
                // zapni modbus
                eStatus = eMBEnable();
              }
              SDread = (unsigned char)madata;
              delay_100ms();delay_100ms();delay_100ms();
            }
            else
            {
              if ((usAddress-MODBUS_DISPLAY_OFFSET)%2) //zapis vyssiho slova
              {
                  madata += ((unsigned long)(*pucRegBuffer++))<<24;
                  madata += ((unsigned long)(*pucRegBuffer++))<<16;

                  if (kontrola_hodnot(2, madata, usAddress))
                      eStatus = MB_EINVAL; // kontrola min a max hodnoty
                  else
                  {
                      *(unsigned long *)(ModbusTable2[(usAddress-MODBUS_DISPLAY_OFFSET)/2]) = madata;
                      CallOutputFunction(2, pucRegBuffer-4, usAddress); // zavola prislusnou vystupni funkci
                  }
              }
              else
              {
                  madata = ((unsigned long)(*pucRegBuffer++))<<8;
                  madata += *pucRegBuffer++;
              }
            }
            usAddress++;
            usNRegs--;
          }
      }
      break;
      }
  }
  ///USER 2000-2499
  else if ((usAddress>=MODBUS_USER_OFFSET)&&(usAddress+usNRegs<= MODBUS_USER_OFFSET+SIZEOF_ModbusTable3*2))
  {
      if (ModbusPass[0]!=RAM_Read_Data_Long(PASSWORD_USER))
      {
          eStatus = MB_ENOREG; //Chyba hesla
      }
      else
      {
          switch ( eMode )
          {
          case MB_REG_READ:
          {
              while (usNRegs)
              {
                  if ((usAddress-MODBUS_USER_OFFSET)%2) //cteni vyssiho slova
                  {
                      TmpData=RAM_Read_Data_Long((ModbusTable3[(usAddress-MODBUS_USER_OFFSET)/2]));
                      *pucRegBuffer++ = (USHORT)(TmpData>>24);
                      *pucRegBuffer++ = (USHORT)(TmpData>>16);
                  }
                  else
                  {
                      TmpData=RAM_Read_Data_Long((ModbusTable3[(usAddress-MODBUS_USER_OFFSET)/2]));
                      *pucRegBuffer++ = (USHORT)TmpData>>8;
                      *pucRegBuffer++ = (USHORT)TmpData;
                  }
                  usNRegs--;
                  usAddress++;
              }
          }
          break;
          case MB_REG_WRITE:
          {
              unsigned long madata = 0;
              while (usNRegs)
              {
                  if ((usAddress-MODBUS_USER_OFFSET)%2) //zapis vyssiho slova
                  {
                      madata += ((unsigned long)(*pucRegBuffer++))<<24;
                      madata += ((unsigned long)(*pucRegBuffer++))<<16;

                      if (kontrola_hodnot(3, madata, usAddress))
                        eStatus = MB_EINVAL; // kontrola min a max hodnoty
                      else
                      {
                        *(unsigned long *)(ModbusTable3[(usAddress-MODBUS_USER_OFFSET)/2]) = madata;
                        CallOutputFunction(3, pucRegBuffer-4, usAddress); // zavola prislusnou vystupni funkci
                      }
                  }
                  else
                  {
                      madata = ((unsigned long)(*pucRegBuffer++))<<8;
                      madata += *pucRegBuffer++;
                  }
                  usAddress++;
                  usNRegs--;
              }
          }
          break;
          }
      }
  }
  ///SERVICE 3000-3999
  else if ((usAddress>=MODBUS_SERVICE_OFFSET)&&(usAddress+usNRegs<= MODBUS_SERVICE_OFFSET+SIZEOF_ModbusTable4*2))
  {
      if (ModbusPass[1]!=RAM_Read_Data_Long(PASSWORD_SERVICE))
      {
          eStatus = MB_ENOREG; //Chyba hesla
      }
      else
      {
          switch ( eMode )
          {
          case MB_REG_READ:
          {
              while (usNRegs)
              {
                  if (usAddress == 3036)
                  {
                      // Date settings
                      *pucRegBuffer++ = (DateTime.month/10)<<4 | DateTime.month%10;  // mesic
                      *pucRegBuffer++ = (DateTime.day/10)<<4 | DateTime.day%10;      // den
                  }
                  else if (usAddress == 3037)
                  {
                      // Date settings
                      *pucRegBuffer++ = (DateTime.year/1000)<<4  | (DateTime.year%1000/100); // rok
                      *pucRegBuffer++ = (DateTime.year%100/10)<<4 | (DateTime.year%10);      // rok
                  }
                  else if (usAddress == 3038)
                  {
                      // Time Settings
                      *pucRegBuffer++ = (DateTime.minute/10)<<4 | DateTime.minute%10;  // minuta
                      *pucRegBuffer++ = (DateTime.second/10)<<4 | DateTime.second%10;  // sekunda
                  }
                  else if (usAddress == 3039)
                  {
                      // Time Settings
                      *pucRegBuffer++ = 0;
                      *pucRegBuffer++ = (DateTime.hour/10)<<4 | DateTime.hour%10;      // hodina
                  }
                  else
                  {
                    if ((usAddress-MODBUS_SERVICE_OFFSET)%2) //cteni vyssiho slova
                    {
                        TmpData=RAM_Read_Data_Long((ModbusTable4[(usAddress-MODBUS_SERVICE_OFFSET)/2]));
                        *pucRegBuffer++ = (USHORT)(TmpData>>24);
                        *pucRegBuffer++ = (USHORT)(TmpData>>16);
                    }
                    else
                    {
                        TmpData=RAM_Read_Data_Long((ModbusTable4[(usAddress-MODBUS_SERVICE_OFFSET)/2]));
                        *pucRegBuffer++ = (USHORT)TmpData>>8;
                        *pucRegBuffer++ = (USHORT)TmpData;
                    }
                  }
                  usNRegs--;
                  usAddress++;
              }
          }
          break;
          case MB_REG_WRITE:
          {
              unsigned long madata = 0;
              while (usNRegs)
              {
                  if (usAddress == 3036)
                  {
                      // RTC - datum mesic, den
                      RTC_set_month(((*pucRegBuffer)&0x0000000F) + 10*((*pucRegBuffer>>4)&0x0000000F));
                      pucRegBuffer++;
                      RTC_set_day(((*pucRegBuffer)&0x0000000F) + 10*((*pucRegBuffer>>4)&0x0000000F));
                      pucRegBuffer++;
                  }
                  else if (usAddress == 3037)
                  {
                      // RTC - datum rok
                      unsigned long date = 0;
                      date = 100*((*pucRegBuffer)&0x0000000F) + 1000*((*pucRegBuffer>>4)&0x0000000F);
                      pucRegBuffer++;
                      date += ((*pucRegBuffer)&0x0000000F) + 10*((*pucRegBuffer>>4)&0x0000000F);
                      pucRegBuffer++;
                      RTC_set_year(date);
                  }
                  else if (usAddress == 3038)
                  {
                      // RTC - cas minuta, sekunda
                      RTC_set_minute(((*pucRegBuffer)&0x0000000F) + 10*((*pucRegBuffer>>4)&0x0000000F));
                      pucRegBuffer++;
                      RTC_set_second((*pucRegBuffer&0x0000000F) + 10*((*pucRegBuffer>>4)&0x0000000F));
                      pucRegBuffer++;
                  }
                  else if (usAddress == 3039)
                  {
                      // RTC - cas, hodina
                      pucRegBuffer++;
                      RTC_set_hour(((*pucRegBuffer)&0x0000000F) + 10*((*pucRegBuffer>>4)&0x0000000F));
                      pucRegBuffer++;
                  }
                  else
                  if ((usAddress-MODBUS_SERVICE_OFFSET)%2) //zapis vyssiho slova
                  {
                      madata += ((unsigned long)(*pucRegBuffer++))<<24;
                      madata += ((unsigned long)(*pucRegBuffer++))<<16;

                      if (kontrola_hodnot(4, madata, usAddress))
                          eStatus = MB_EINVAL; // kontrola min a max hodnoty
                      else
                      {
                          *(unsigned long *)(ModbusTable4[(usAddress-MODBUS_SERVICE_OFFSET)/2]) = madata;
                          CallOutputFunction(4, pucRegBuffer-4, usAddress); // zavola prislusnou vystupni funkci
                      }
                  }
                  else
                  {
                      madata = ((unsigned long)(*pucRegBuffer++))<<8;
                      madata += *pucRegBuffer++;
                  }
                  usAddress++;
                  usNRegs--;
              }
          }
          break;
          }
      }
  }
  ///FACTORY 4000-4999
  else if ((usAddress>=MODBUS_FACTORY_OFFSET)&&(usAddress+usNRegs<= MODBUS_FACTORY_OFFSET+SIZEOF_ModbusTable5*2))
  {
      if (ModbusPass[2]!=RAM_Read_Data_Long(PASSWORD_FACTORY))
      {
          eStatus = MB_ENOREG; //Chyba hesla
      }
      else
      {
          if ( (eMode==MB_REG_WRITE))
          {
              long data = 0;
              while (usNRegs)
              {
                  if (usAddress == 4004)
                  {
                      // CALIBRATION POINT ONE LOW                                 kalibrace prvni konstanty pomoci prutoku etalonu
                      data = (unsigned long)(*pucRegBuffer++)<<8;  //    1
                      data += *pucRegBuffer++; // LSB 0
                  }
                  else if (usAddress == 4005)
                  {
                      // CALIBRATION POINT ONE HIGH                                kalibrace prvni konstanty pomoci prutoku etalonu
                      data += (unsigned long)(*pucRegBuffer++)<<24; // MSB 3
                      data += (unsigned long)(*pucRegBuffer++)<<16; //     2

                      RAM_Write_Data_Long(data, CALIBRATION_POINT_ONE);
                      RAM_Write_Data_Long(adc_average, MEASURED_POINT_ONE);

                      send_calibration(1);
                  }
                  else if (usAddress == 4006)
                  {
                      // CALIBRATION POINT TWO LOW                                 kalibrace druhe konstanty pomoci prutoku etalonu
                      data = (unsigned long)(*pucRegBuffer++)<<8;  //    1
                      data += *pucRegBuffer++; // LSB 0
                  }
                  else if (usAddress == 4007)
                  {
                      // CALIBRATION POINT TWO HIGH                                kalibrace druhe konstanty pomoci prutoku etalonu
                      data += (unsigned long)(*pucRegBuffer++)<<24; // MSB 3
                      data += (unsigned long)(*pucRegBuffer++)<<16; //     2

                      RAM_Write_Data_Long(data, CALIBRATION_POINT_TWO);
                      RAM_Write_Data_Long(adc_average, MEASURED_POINT_TWO);

                      send_calibration(2);
                  }
                  else if (usAddress == 4008)
                  {
                      // CALIBRATION POINT THREE LOW                               kalibrace treti konstanty pomoci prutoku etalonu
                      data = (unsigned long)(*pucRegBuffer++)<<8;  //    1
                      data += *pucRegBuffer++; // LSB 0
                  }
                  else if (usAddress == 4009)
                  {
                      // CALIBRATION POINT THREE HIGH                              kalibrace treti konstanty pomoci prutoku etalonu
                      data += (unsigned long)(*pucRegBuffer++)<<24; // MSB 3
                      data += (unsigned long)(*pucRegBuffer++)<<16; //     2

                      RAM_Write_Data_Long(data, CALIBRATION_POINT_THREE);
                      if( RAM_Read_Data_Long(CALIBRATION_POINT_THREE))
                      {
                          RAM_Write_Data_Long(adc_average,MEASURED_POINT_THREE);
                      }
                      else    //pokud je 0 tak se vymaze 3 kalib bobd
                      {
                          RAM_Write_Data_Long(0,MEASURED_POINT_THREE);
                      }

                      send_calibration(3);
                  }
                  else if (usAddress == 4010)
                  {
                      // MEASURED POINT ONE LOW
                      data = (unsigned long)(*pucRegBuffer++)<<8;  //    1
                      data += *pucRegBuffer++; // LSB 0
                  }
                  else if (usAddress == 4011)
                  {
                      // MEASURED POINT ONE HIGH                                kalibrace prvni konstanty - zadani prutoku etalonu
                      data += (unsigned long)(*pucRegBuffer++)<<24; // MSB 3
                      data += (unsigned long)(*pucRegBuffer++)<<16; //     2

                      RAM_Write_Data_Long(data, MEASURED_POINT_ONE);

                      send_calibration(1);
                  }
                  else if (usAddress == 4012)
                  {
                      // MEASURED POINT TWO LOW
                      data = (unsigned long)(*pucRegBuffer++)<<8;  //    1
                      data += *pucRegBuffer++; // LSB 0
                  }
                  else if (usAddress == 4013)
                  {
                      // MEASURED POINT TWO HIGH                                   kalibrace prvni konstanty - zadani hodnoty AD prevodniku
                      data += (unsigned long)(*pucRegBuffer++)<<24; // MSB 3
                      data += (unsigned long)(*pucRegBuffer++)<<16; //     2

                      RAM_Write_Data_Long(data, MEASURED_POINT_TWO);

                      send_calibration(2);
                  }
                  else if (usAddress == 4014)
                  {
                      // MEASURED POINT THREE LOW
                      data = (unsigned long)(*pucRegBuffer++)<<8;  //    1
                      data += *pucRegBuffer++; // LSB 0
                  }
                  else if (usAddress == 4015)
                  {
                      // MEASURED POINT THREE HIGH                               kalibrace druhe konstanty - zadani prutoku etalonu
                      data += (unsigned long)(*pucRegBuffer++)<<24; // MSB 3
                      data += (unsigned long)(*pucRegBuffer++)<<16; //     2

                      RAM_Write_Data_Long(data, MEASURED_POINT_THREE);

                      send_calibration(3);
                  }
                  else if (usAddress == 4018)
                  {
                      // ZERO FLOW CONSTANT LOW
                      data = (unsigned long)(*pucRegBuffer++)<<8;  //    1
                      data += *pucRegBuffer++; // LSB 0
                  }
                  else if (usAddress == 4019)
                  {
                      // ZERO FLOW CONSTANT HIGH                               kalibrace druhe konstanty - zadani prutoku etalonu
                      data += (unsigned long)(*pucRegBuffer++)<<24; // MSB 3
                      data += (unsigned long)(*pucRegBuffer++)<<16; //     2

                      RAM_Write_Data_Long(data, ZERO_FLOW_CONSTANT);

                      send_zero_constant_sensor8((long)data);
                  }
                  else if (usAddress == 4030 )
                  {
                      //set sensor unit no
                      data = (unsigned long)(*pucRegBuffer++)<<8;  //    1
                      data += *pucRegBuffer++; // LSB 0
                  }
                  else if( usAddress == 4031 )
                  {
                      data += (unsigned long)(*pucRegBuffer++)<<24; // MSB 3
                      data += (unsigned long)(*pucRegBuffer++)<<16; //     2

                      RAM_Write_Data_Long(data, FLOW_SENSOR_UNIT_NO);
                      send_unit_no_sensor8();
                  }

                  else if (usAddress == 4032 ) // total dig set
                  {
                      //set sensor unit no
                      data = (unsigned long)(*pucRegBuffer++)<<8;  //    1
                      data += *pucRegBuffer++; // LSB 0
                  }
                  else if( usAddress == 4033 ) // total dig set
                  {
                      data += (unsigned long)(*pucRegBuffer++)<<24; // MSB 3
                      data += (unsigned long)(*pucRegBuffer++)<<16; //     2

                      g_measurement.total = ((long long)(data))*1000000;
                  }
                  else if (usAddress == 4034 ) // total dec set
                  {
                      //set sensor unit no
                      data = (unsigned long)(*pucRegBuffer++)<<8;  //    1
                      data += *pucRegBuffer++; // LSB 0
                  }
                  else if( usAddress == 4035 ) //total dec set
                  {
                      data += (unsigned long)(*pucRegBuffer++)<<24; // MSB 3
                      data += (unsigned long)(*pucRegBuffer++)<<16; //     2

                      g_measurement.total += data;
                  }
                  else if (usAddress == 4036 ) // total plus dig set
                  {
                      //set sensor unit no
                      data = (unsigned long)(*pucRegBuffer++)<<8;  //    1
                      data += *pucRegBuffer++; // LSB 0
                  }
                  else if( usAddress == 4037 ) // total plus dig set
                  {
                      data += (unsigned long)(*pucRegBuffer++)<<24; // MSB 3
                      data += (unsigned long)(*pucRegBuffer++)<<16; //     2

                      g_measurement.flow_pos = ((long long)(data))*1000000;
                  }
                  else if (usAddress == 4038 ) // total plus dec set
                  {
                      //set sensor unit no
                      data = (unsigned long)(*pucRegBuffer++)<<8;  //    1
                      data += *pucRegBuffer++; // LSB 0
                  }
                  else if( usAddress == 4039 ) // total plus dec set
                  {
                      data += (unsigned long)(*pucRegBuffer++)<<24; // MSB 3
                      data += (unsigned long)(*pucRegBuffer++)<<16; //     2

                      g_measurement.flow_pos += data;
                  }
                  else if (usAddress == 4040 ) // total neg dig set
                  {
                      //set sensor unit no
                      data = (unsigned long)(*pucRegBuffer++)<<8;  //    1
                      data += *pucRegBuffer++; // LSB 0
                  }
                  else if( usAddress == 4041 ) // total neg dig set
                  {
                      data += (unsigned long)(*pucRegBuffer++)<<24; // MSB 3
                      data += (unsigned long)(*pucRegBuffer++)<<16; //     2

                      g_measurement.flow_neg = ((long long)(data))*1000000;
                  }
                  else if (usAddress == 4042 ) // total neg dec set
                  {
                      //set sensor unit no
                      data = (unsigned long)(*pucRegBuffer++)<<8;  //    1
                      data += *pucRegBuffer++; // LSB 0
                  }
                  else if( usAddress == 4043 ) // total neg dec set
                  {
                      data += (unsigned long)(*pucRegBuffer++)<<24; // MSB 3
                      data += (unsigned long)(*pucRegBuffer++)<<16; //     2

                      g_measurement.flow_neg += data;
                  }
                  else if (usAddress == 4044 ) // aux plus dig set
                  {
                      //set sensor unit no
                      data = (unsigned long)(*pucRegBuffer++)<<8;  //    1
                      data += *pucRegBuffer++; // LSB 0
                  }
                  else if( usAddress == 4045 ) // aux plus dig set
                  {
                      data += (unsigned long)(*pucRegBuffer++)<<24; // MSB 3
                      data += (unsigned long)(*pucRegBuffer++)<<16; //     2

                      g_measurement.aux = ((long long)(data))*1000000;
                  }
                  else if (usAddress == 4046 ) // aux plus dec set
                  {
                      //set sensor unit no
                      data = (unsigned long)(*pucRegBuffer++)<<8;  //    1
                      data += *pucRegBuffer++; // LSB 0
                  }
                  else if( usAddress == 4047 ) // aux plus dec set
                  {
                      data += (unsigned long)(*pucRegBuffer++)<<24; // MSB 3
                      data += (unsigned long)(*pucRegBuffer++)<<16; //     2

                      g_measurement.aux += data;
                  }

                  else
                  {
                      if ((usAddress-MODBUS_FACTORY_OFFSET)%2) //zapis vyssiho slova
                      {
                          data += ((unsigned long)(*pucRegBuffer++))<<24;
                          data += ((unsigned long)(*pucRegBuffer++))<<16;

                          if (kontrola_hodnot(5, data, usAddress))
                              eStatus = MB_EINVAL; // kontrola min a max hodnoty
                          else
                          {
                              *(unsigned long *)(ModbusTable5[(usAddress-MODBUS_FACTORY_OFFSET)/2]) = data;
                              CallOutputFunction(5, pucRegBuffer-4, usAddress); // zavola prislusnou vystupni funkci
                          }
                      }
                      else
                      {
                          data = ((unsigned long)(*pucRegBuffer++))<<8;
                          data += *pucRegBuffer++;
                      }
                  }
                  usNRegs--;
                  usAddress++;
              }
          }
          else if ( eMode==MB_REG_READ )
          {
              while (usNRegs)
              {
                  if ((usAddress-MODBUS_FACTORY_OFFSET)%2) //cteni vyssiho slova
                  {
                      TmpData=RAM_Read_Data_Long((ModbusTable5[(usAddress-MODBUS_FACTORY_OFFSET)/2]));
                      *pucRegBuffer++ = (USHORT)(TmpData>>24);
                      *pucRegBuffer++ = (USHORT)(TmpData>>16);
                  }
                  else
                  {
                      TmpData=RAM_Read_Data_Long((ModbusTable5[(usAddress-MODBUS_FACTORY_OFFSET)/2]));
                      *pucRegBuffer++ = (USHORT)TmpData>>8;
                      *pucRegBuffer++ = (USHORT)TmpData;
                  }
                  usNRegs--;
                  usAddress++;
              }
          }
      }
  }
  ///AUTHORIZE
  else if ((usAddress>=MODBUS_AUTHORIZE_OFFSET)&&(usAddress+usNRegs<= MODBUS_AUTHORIZE_OFFSET+SIZEOF_ModbusTable6*2))
  {
      if (ModbusPass[3]!=RAM_Read_Data_Long(PASSWORD_AUTHORIZE))
      {
          eStatus = MB_ENOREG; //Chyba hesla
      }
      else
      {
          switch ( eMode )
          {
          case MB_REG_READ:
          {
              while (usNRegs)
              {
                  if ((usAddress-MODBUS_AUTHORIZE_OFFSET)%2) //cteni vyssiho slova
                  {
                      TmpData=RAM_Read_Data_Long((ModbusTable6[(usAddress-MODBUS_AUTHORIZE_OFFSET)/2]));
                      *pucRegBuffer++ = (USHORT)(TmpData>>24);
                      *pucRegBuffer++ = (USHORT)(TmpData>>16);
                  }
                  else
                  {
                      TmpData=RAM_Read_Data_Long((ModbusTable6[(usAddress-MODBUS_AUTHORIZE_OFFSET)/2]));
                      *pucRegBuffer++ = (USHORT)TmpData>>8;
                      *pucRegBuffer++ = (USHORT)TmpData;
                  }
                  usNRegs--;
                  usAddress++;
              }
          }
          break;
          case MB_REG_WRITE:
          {
              unsigned long madata = 0;
              while (usNRegs)
              {
                  if ((usAddress-MODBUS_AUTHORIZE_OFFSET)%2) //zapis vyssiho slova
                  {
                      madata += ((unsigned long)(*pucRegBuffer++))<<24;
                      madata += ((unsigned long)(*pucRegBuffer++))<<16;

                      if (kontrola_hodnot(6, madata, usAddress))
                          eStatus = MB_EINVAL; // kontrola min a max hodnoty
                      else
                      {
                          *(unsigned long *)(ModbusTable6[(usAddress-MODBUS_AUTHORIZE_OFFSET)/2]) = madata;
                          CallOutputFunction(6, pucRegBuffer-4, usAddress); // zavola prislusnou vystupni funkci
                      }
                  }
                  else
                  {
                      madata = ((unsigned long)(*pucRegBuffer++))<<8;
                      madata += *pucRegBuffer++;
                  }
                  usAddress++;
                  usNRegs--;
              }
          }
          break;
          }
      }
  }

  /// DATALOGGER
  else if ((usAddress>=MODBUS_DATALOGGER_OFFSET)&&(usAddress+usNRegs<= MODBUS_DATALOGGER_OFFSET+MODBUS_DATALOGGER_SIZE))
  {
          switch ( eMode )
          {
          case MB_REG_READ:
          {
              while (usNRegs)
              {
                  if (usAddress == 10000) // R date
                  {
                      TmpData = modbus_datalogger_date;
                      *pucRegBuffer++ = (USHORT)TmpData>>8;
                      *pucRegBuffer++ = (USHORT)TmpData;
                      *pucRegBuffer++ = (USHORT)(TmpData>>24);
                      *pucRegBuffer++ = (USHORT)(TmpData>>16);
                  }
                  else if (usAddress == 10002) // R time
                  {
                      TmpData = modbus_datalogger_time;
                      *pucRegBuffer++ = (USHORT)TmpData>>8;
                      *pucRegBuffer++ = (USHORT)TmpData;
                      *pucRegBuffer++ = (USHORT)(TmpData>>24);
                      *pucRegBuffer++ = (USHORT)(TmpData>>16);
                  }
                  else if (usAddress == 10004) // Date
                  {
                      if ((odesilaci_buffer[0]<0x30) || (odesilaci_buffer[0]>0x39)) // prvni znak neni cislo - chyba dataloggeru
                      {
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          eStatus = MB_EIO;
                      }
                      else
                      {
                          char znak = 0;
                          unsigned char pozice = 0;
                          while((znak!=' ') && (pozice<100)) // najdi prvni mezeru
                          {
                              znak = odesilaci_buffer[pozice];
                              pozice++;
                          }
                          if (pozice < 100)
                          {
                              TmpData =  (odesilaci_buffer[0+pozice]-0x30)*10000000;
                              TmpData += (odesilaci_buffer[1+pozice]-0x30)*1000000;
                              TmpData += (odesilaci_buffer[2+pozice]-0x30)*100000;
                              TmpData += (odesilaci_buffer[3+pozice]-0x30)*10000;
                              TmpData += (odesilaci_buffer[4+pozice]-0x30)*1000;
                              TmpData += (odesilaci_buffer[5+pozice]-0x30)*100;
                              TmpData += (odesilaci_buffer[6+pozice]-0x30)*10;
                              TmpData += (odesilaci_buffer[7+pozice]-0x30);
                              *pucRegBuffer++ = (USHORT)TmpData>>8;
                              *pucRegBuffer++ = (USHORT)TmpData;
                              *pucRegBuffer++ = (USHORT)(TmpData>>24);
                              *pucRegBuffer++ = (USHORT)(TmpData>>16);
                          }
                      }
                  }
                  else if (usAddress == 10006) // Time
                  {
                      if ((odesilaci_buffer[0]<0x30) || (odesilaci_buffer[0]>0x39)) // prvni znak neni cislo - chyba dataloggeru
                      {
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          eStatus = MB_EIO;
                      }
                      else
                      {
                          char znak = 0;
                          unsigned char pozice = 0;
                          while((znak!=' ') && (pozice<100)) // nalezeni prvni mezery
                          {
                              znak = odesilaci_buffer[pozice];
                              pozice++;
                          }
                          znak = 0;
                          while((znak!=' ') && (pozice<100)) // nalezeni druhe mezery
                          {
                              znak = odesilaci_buffer[pozice];
                              pozice++;
                          }
                          znak = 0;
                          if (pozice < 100)
                          {
                              TmpData =  (odesilaci_buffer[pozice+0]-0x30)*1000;
                              TmpData += (odesilaci_buffer[pozice+1]-0x30)*100;
                              TmpData += (odesilaci_buffer[pozice+2]-0x30)*10;
                              TmpData += (odesilaci_buffer[pozice+3]-0x30);
                              *pucRegBuffer++ = (USHORT)TmpData>>8;
                              *pucRegBuffer++ = (USHORT)TmpData;
                              *pucRegBuffer++ = (USHORT)(TmpData>>24);
                              *pucRegBuffer++ = (USHORT)(TmpData>>16);
                          }
                      }
                  }
                  else if (usAddress == 10008) //Total+ DIG
                  {
                      if ((odesilaci_buffer[0]<0x30) || (odesilaci_buffer[0]>0x39)) // prvni znak neni cislo - chyba dataloggeru
                      {
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          eStatus = MB_EIO;
                      }
                      else
                      {
                          unsigned char pocet_cislic = 0;
                          char znak = 0;
                          unsigned char pozice = 0;
                          while((znak!=' ') && (pozice<100)) // nalezeni prvni mezery
                          {
                              znak = odesilaci_buffer[pozice];
                              pozice++;
                          }
                          znak = 0;
                          while((znak!=' ') && (pozice<100)) // nalezeni druhe mezery
                          {
                              znak = odesilaci_buffer[pozice];
                              pozice++;
                          }
                          znak = 0;
                          while((znak!=' ') && (pozice<100)) // nalezeni treti mezery
                          {
                              znak = odesilaci_buffer[pozice];
                              pozice++;
                          }
                          znak = 0;
                          do
                          {
                              znak = odesilaci_buffer[pozice];
                              if ((znak >= 0x30) && (znak <= 0x39)) pocet_cislic++; // pokud je to cislice tak zvysim pocitadlo cislic
                              pozice++;
                          }
                          while((znak != '.') && (pozice<100));
                          pozice--;

                          TmpData = 0;

                          while(pocet_cislic > 0)
                          {
                              TmpData += ((odesilaci_buffer[pozice-pocet_cislic])-0x30) * pow(10,pocet_cislic-1);
                              pocet_cislic--;
                          }

                          *pucRegBuffer++ = (USHORT)TmpData>>8;
                          *pucRegBuffer++ = (USHORT)TmpData;
                          *pucRegBuffer++ = (USHORT)(TmpData>>24);
                          *pucRegBuffer++ = (USHORT)(TmpData>>16);
                      }
                  }
                  else if (usAddress == 10010) //Total+ DEC
                  {
                      if ((odesilaci_buffer[0]<0x30) || (odesilaci_buffer[0]>0x39)) // prvni znak neni cislo - chyba dataloggeru
                      {
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          eStatus = MB_EIO;
                      }
                      else
                      {
                          unsigned char pocet_cislic = 0;
                          char znak = 0;
                          unsigned char pozice = 0;
                          while((znak!='.') && (pozice<100)) // nalezeni prvni mezery
                          {
                              znak = odesilaci_buffer[pozice];
                              pozice++;
                          }
                          znak = 0;
                          do
                          {
                              znak = odesilaci_buffer[pozice];
                              if ((znak >= 0x30) && (znak <= 0x39)) pocet_cislic++; // pokud je to cislice tak zvysim pocitadlo cislic
                              pozice++;
                          }
                          while((znak != ' ') && (pozice<100));
                          pozice--;

                          TmpData = 0;

                          while(pocet_cislic > 0)
                          {
                              TmpData += ((odesilaci_buffer[pozice-pocet_cislic])-0x30) * pow(10,pocet_cislic-1);
                              pocet_cislic--;
                          }

                          *pucRegBuffer++ = (USHORT)TmpData>>8;
                          *pucRegBuffer++ = (USHORT)TmpData;
                          *pucRegBuffer++ = (USHORT)(TmpData>>24);
                          *pucRegBuffer++ = (USHORT)(TmpData>>16);
                      }
                  }
                  else if (usAddress == 10012) //Total- DIG
                  {
                      if ((odesilaci_buffer[0]<0x30) || (odesilaci_buffer[0]>0x39)) // prvni znak neni cislo - chyba dataloggeru
                      {
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          eStatus = MB_EIO;
                      }
                      else
                      {
                          unsigned char pocet_cislic = 0;
                          char znak = 0;
                          unsigned char pozice = 0;
                          while((znak!='.') && (pozice<100)) // nalezeni prvni mezery
                          {
                              znak = odesilaci_buffer[pozice];
                              pozice++;
                          }
                          znak = 0;
                          while((znak!=' ') && (pozice<100)) // nalezeni druhe mezery
                          {
                              znak = odesilaci_buffer[pozice];
                              pozice++;
                          }
                          znak = 0;
                          do
                          {
                              znak = odesilaci_buffer[pozice];
                              if ((znak >= 0x30) && (znak <= 0x39)) pocet_cislic++; // pokud je to cislice tak zvysim pocitadlo cislic
                              pozice++;
                          }
                          while((znak != '.') && (pozice<100));
                          pozice--;

                          TmpData = 0;

                          while(pocet_cislic > 0)
                          {
                              TmpData += ((odesilaci_buffer[pozice-pocet_cislic])-0x30) * pow(10,pocet_cislic-1);
                              pocet_cislic--;
                          }

                          *pucRegBuffer++ = (USHORT)TmpData>>8;
                          *pucRegBuffer++ = (USHORT)TmpData;
                          *pucRegBuffer++ = (USHORT)(TmpData>>24);
                          *pucRegBuffer++ = (USHORT)(TmpData>>16);
                      }
                  }
                  else if (usAddress == 10014) //Total- DEC
                  {
                      if ((odesilaci_buffer[0]<0x30) || (odesilaci_buffer[0]>0x39)) // prvni znak neni cislo - chyba dataloggeru
                      {
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          eStatus = MB_EIO;
                      }
                      else
                      {
                          unsigned char pocet_cislic = 0;
                          char znak = 0;
                          unsigned char pozice = 0;
                          while((znak!='.') && (pozice<100)) // nalezeni prvni mezery
                          {
                              znak = odesilaci_buffer[pozice];
                              pozice++;
                          }
                          znak = 0;
                          while((znak!='.') && (pozice<100)) // nalezeni druhe mezery
                          {
                              znak = odesilaci_buffer[pozice];
                              pozice++;
                          }
                          znak = 0;
                          do
                          {
                              znak = odesilaci_buffer[pozice];
                              if ((znak >= 0x30) && (znak <= 0x39)) pocet_cislic++; // pokud je to cislice tak zvysim pocitadlo cislic
                              pozice++;
                          }
                          while((znak != ' ') && (pozice<100));
                          pozice--;

                          TmpData = 0;

                          while(pocet_cislic > 0)
                          {
                              TmpData += ((odesilaci_buffer[pozice-pocet_cislic])-0x30) * pow(10,pocet_cislic-1);
                              pocet_cislic--;
                          }

                          *pucRegBuffer++ = (USHORT)TmpData>>8;
                          *pucRegBuffer++ = (USHORT)TmpData;
                          *pucRegBuffer++ = (USHORT)(TmpData>>24);
                          *pucRegBuffer++ = (USHORT)(TmpData>>16);
                      }
                  }
                  else if (usAddress == 10016) //Error min
                  {
                      if ((odesilaci_buffer[0]<0x30) || (odesilaci_buffer[0]>0x39)) // prvni znak neni cislo - chyba dataloggeru
                      {
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          eStatus = MB_EIO;
                      }
                      else
                      {
                          unsigned char pocet_cislic = 0;
                          char znak = 0;
                          unsigned char pozice = 0;
                          while((znak!='.') && (pozice<100)) // nalezeni prvni mezery
                          {
                              znak = odesilaci_buffer[pozice];
                              pozice++;
                          }
                          znak = 0;
                          while((znak!='.') && (pozice<100)) // nalezeni prvni mezery
                          {
                              znak = odesilaci_buffer[pozice];
                              pozice++;
                          }
                          znak = 0;
                          while((znak!=' ') && (pozice<100)) // nalezeni druhe mezery
                          {
                              znak = odesilaci_buffer[pozice];
                              pozice++;
                          }
                          znak = 0;
                          do
                          {
                              znak = odesilaci_buffer[pozice];
                              if ((znak >= 0x30) && (znak <= 0x39)) pocet_cislic++; // pokud je to cislice tak zvysim pocitadlo cislic
                              pozice++;
                          }
                          while((znak != ' ') && (pozice<100));
                          pozice--;

                          TmpData = 0;

                          while(pocet_cislic > 0)
                          {
                              TmpData += ((odesilaci_buffer[pozice-pocet_cislic])-0x30) * pow(10,pocet_cislic-1);
                              pocet_cislic--;
                          }

                          *pucRegBuffer++ = (USHORT)TmpData>>8;
                          *pucRegBuffer++ = (USHORT)TmpData;
                          *pucRegBuffer++ = (USHORT)(TmpData>>24);
                          *pucRegBuffer++ = (USHORT)(TmpData>>16);
                      }
                  }
                  else if (usAddress == 10018) //Error
                  {
                      if ((odesilaci_buffer[0]<0x30) || (odesilaci_buffer[0]>0x39)) // prvni znak neni cislo - chyba dataloggeru
                      {
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          *pucRegBuffer++ = 0;
                          eStatus = MB_EIO;
                      }
                      else
                      {
                          unsigned char pocet_cislic = 0;
                          char znak = 0;
                          unsigned char pozice = 0;
                          while((znak!='.') && (pozice<100)) // nalezeni prvni mezery
                          {
                              znak = odesilaci_buffer[pozice];
                              pozice++;
                          }
                          znak = 0;
                          while((znak!='.') && (pozice<100)) // nalezeni prvni mezery
                          {
                              znak = odesilaci_buffer[pozice];
                              pozice++;
                          }
                          znak = 0;
                          while((znak!=' ') && (pozice<100)) // nalezeni prvni mezery
                          {
                              znak = odesilaci_buffer[pozice];
                              pozice++;
                          }
                          znak = 0;
                          while((znak!=' ') && (pozice<100)) // nalezeni druhe mezery
                          {
                              znak = odesilaci_buffer[pozice];
                              pozice++;
                          }
                          znak = 0;
                          do
                          {
                              znak = odesilaci_buffer[pozice];
                              if ((znak >= 0x30) && (znak <= 0x39)) pocet_cislic++; // pokud je to cislice tak zvysim pocitadlo cislic
                              pozice++;
                          }
                          while((znak != ';') && (pozice<100));
                          pozice--;

                          TmpData = 0;

                          while(pocet_cislic > 0)
                          {
                              TmpData += ((odesilaci_buffer[pozice-pocet_cislic])-0x30) * pow(10,pocet_cislic-1);
                              pocet_cislic--;
                          }
                          *pucRegBuffer++ = (USHORT)TmpData>>8;
                          *pucRegBuffer++ = (USHORT)TmpData;
                          *pucRegBuffer++ = (USHORT)(TmpData>>24);
                          *pucRegBuffer++ = (USHORT)(TmpData>>16);
                      }
                  }
                  else
                  {
                      eStatus = MB_EIO;
                  }
                  usNRegs--;
                  usNRegs--;
                  usAddress++;
                  usAddress++;
              }
          }
          break;
          case MB_REG_WRITE:
          {
              while (usNRegs)
              {
                  if (usAddress == 10000)
                  {
                      modbus_datalogger_date = ((unsigned long)(*pucRegBuffer++))<<8;
                      modbus_datalogger_date += *pucRegBuffer++;
                  }
                  else if (usAddress == 10001)
                  {
                      modbus_datalogger_date += ((unsigned long)(*pucRegBuffer++))<<24;
                      modbus_datalogger_date += ((unsigned long)(*pucRegBuffer++))<<16;
                      modbus_read_datalogger_flag = 1;
                  }
                  else if (usAddress == 10002)
                  {
                      modbus_datalogger_time = ((unsigned long)(*pucRegBuffer++))<<8;
                      modbus_datalogger_time += *pucRegBuffer++;
                  }
                  else if (usAddress == 10003)
                  {
                      modbus_datalogger_time += ((unsigned long)(*pucRegBuffer++))<<24;
                      modbus_datalogger_time += ((unsigned long)(*pucRegBuffer++))<<16;
                      modbus_read_datalogger_flag = 1;
                  }
                  else
                  {
                      eStatus = MB_EIO; // pri zapisu na jinou adresu nez je povoleno
                  }
                  usAddress++;
                  usNRegs--;
              }
          }
          break;
          }
  }

  //jinak
  else
  {
    eStatus = MB_ENOREG;
  }
  return eStatus;
}

#include <string.h>
#include "VCOM.h"
#include "tff.h"
#include "rprintf.h"
#include "fio.h"

static BOOL IsFileToSend(const char *fileName)
{
    if (strstr(fileName, "_") == NULL)
        return FALSE;

    if (strstr(fileName, ".csv") == NULL)
        return FALSE;

    return TRUE;
}

void sendFilesName()
{
  // pockej nez si aplikace prepne modbus na seriovou linku
  delay_100ms();delay_100ms();

  // nacti list souboru a posli jejich nazvy
  FRESULT res;
  FILINFO fno;
  DIR dir;

  char *fn;
  char path[16];
  sprintf(path,"/MagC1/%08d",RAM_Read_Data_Long(UNIT_NO));

  res = f_opendir(&dir, path);    // Open the directory
  if (res == FR_OK)
  {
    VCOM_putchar(1);
    for (;;)
    {
      res = f_readdir(&dir, &fno);

      if (res != FR_OK || fno.fname[0] == 0) break;
      if (fno.fname[0] == '.') continue;
      if (res != FR_OK || fno.fname[0] == 0)
      fn = fno.fname;
      if (fno.fattrib & AM_DIR)
      {}
      else
      {
          if (IsFileToSend(fn))
          {
            VCOM_putchar(2);
            for(int i = 0; i < strlen(fn); i++)
            {
                VCOM_putchar(fn[i]);
            }
            VCOM_putchar(3);
          }
      }
    }
    VCOM_putchar(4);
  }
}

void sendFile(unsigned long data)
{
  delay_100ms();delay_100ms();
  // z cisla, ktere poslala aplikace do modbus registru
  // vytvorim jmeno souboru, abych ho mohl otevrit

  uint16_t year = data >> 16;
  uint16_t month = data & 0xFF;
  char fileName[12];
  sprintf(fileName,"%d_%02d.csv",year,month);

  // send SOH
  VCOM_putchar(1);

  // send file
  #define VELIKOST_ZASOBNIKU 100 // maximalni delka jednoho radku v dataloggeru
  char c[2];
  unsigned char cr = 0;
  char pole[50];
  unsigned long pozice_celkem = 0;
  UINT rc;
  sprintf(pole,"/MagC1/%08d/%s",RAM_Read_Data_Long(UNIT_NO), fileName);
  FIL *file = fopen (pole,"r");
  if(file == NULL)
  {
    // dej vedet aplikaci ze se neco pokazilo
    VCOM_putchar(5);
  }
  else
  {
    unsigned long file_velikost = file->fsize;
    rewind(file);

    while(1)
    {
      // precti jeden byte souboru
      f_read(file, c, 1, &rc);
      pozice_celkem++;

      // konec souboru
      if( rc != 1 || (file_velikost <= pozice_celkem) )
        break;

      // v souboru je cr cr lf, takze to druhe cr se neposila
      if( c[0] == '\r' )
      {
        if (cr)
        {
          cr = 0;
          delay_1ms();
          delay_1ms();
          delay_1ms();
          delay_1ms();
        }
        else
        {
          cr = 1;
        }
      }
      else
        VCOM_putchar(c[0]);
    }
  }
  // send EOT
  VCOM_putchar(4);
}
