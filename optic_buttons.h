#ifndef OPTIC_BUTTONS_H_INCLUDED
#define OPTIC_BUTTONS_H_INCLUDED

#define BTN_DOWN  0
#define BTN_UP    1
#define BTN_LEFT  2
#define BTN_RIGHT 3
#define BTN_ESC   4
#define BTN_ENTER 5
#define BTN_COUNT 6

extern volatile unsigned char pressed_key;
extern unsigned char flag_timer1;
extern volatile unsigned char flag_tlacitko;
extern int down, left, esc, up, right, enter;
extern int timer1_running;
extern unsigned char flag_ir_buttons;
extern unsigned char serviceInfoPage;

int OpticBtn_GetAvgLevel(void);
void readButtons( void );

#endif // OPTIC_BUTTONS_H_INCLUDED
