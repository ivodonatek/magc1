//----------------------------------------------------------------------------
//  Ovladac ADP5063 (Linear LiFePO4 Battery Charger).
//----------------------------------------------------------------------------

#ifndef ADP5063_H_INCLUDED
#define ADP5063_H_INCLUDED

#ifndef BOOL_TYPEDEF
#define BOOL_TYPEDEF
typedef unsigned char BOOL;
#endif

#ifndef TRUE
#define TRUE            1
#endif

#ifndef FALSE
#define FALSE           0
#endif

// ADP5063 registry
#define ADP5063_Manuf_Model_ID_REG               (0x00)    /* Manufacturer and model ID */
#define ADP5063_VINx_Pin_Set_REG                 (0x02)    /* VINx pin settings */
#define ADP5063_Termination_Set_REG              (0x03)    /* Termination settings */
#define ADP5063_Volt_Thresholds_REG              (0x05)    /* Voltage thresholds */
#define ADP5063_Func_Set_1_REG                   (0x07)    /* Functional Settings 1 */
#define ADP5063_Charger_Status_1_REG             (0x0B)    /* Charger Status 1 */
#define ADP5063_Charger_Status_2_REG             (0x0C)    /* Charger Status 2 */

//------------------------------------------------------------------------------
//  Configures the IO ports and initializes it.
//------------------------------------------------------------------------------
void ADP5063_IOSetup(void);

//------------------------------------------------------------------------------
//  Configures the I2C port and initializes it.
//------------------------------------------------------------------------------
void ADP5063_I2CSetup(void);

//------------------------------------------------------------------------------
//  Get I2STAT
//------------------------------------------------------------------------------
unsigned long ADP5063_GetI2STAT(void);

//------------------------------------------------------------------------------
//  Writes "value" to a single register at address "addr".
//------------------------------------------------------------------------------
BOOL ADP5063_I2CWriteReg(uint8_t addr, uint8_t value);

//------------------------------------------------------------------------------
//  Reads a single register at address "addr" and returns the value read.
//  Returns -1 if error detected.
//------------------------------------------------------------------------------
int16_t ADP5063_I2CReadReg(uint8_t addr);

//------------------------------------------------------------------------------
//  Writes "value" to a single register at address "addr". Reads a single
//  register at address "addr" and returns TRUE if the value read is the same
//  as in write operation.
//------------------------------------------------------------------------------
BOOL ADP5063_I2CWriteReadReg(uint8_t addr, uint8_t value);

//------------------------------------------------------------------------------
//  Get SYS_EN output level.
//------------------------------------------------------------------------------
BOOL ADP5063_GetSysEnLevel(void);

#endif // ADP5063_H_INCLUDED
