//******************************************************************************
//  This file contains functions that allow to access the SPI
//  interface of the LMP90100.
//*****************************************************************************/

#include <stdint.h>
#include "LPC23xx.h"
#include "TI_LMP90100.h"
#include "TI_LMP90100_spi.h"
#include "delay.h"

/* Test if include URA transaction */
//#define TEST_INCLUDE_URA_TRANSACTION

/* Power Control for Peripherals register (PCONP) SSP bits
    - The SSP1 interface power/clock enabled */
#define PCONP_SSP_MASK   0x400
#define PCONP_SSP_VALUE  0x400

/* Peripheral Clock Selection register (PCLKSELx)
    - Peripheral clock selection for SSP1 */
#define PCLKSEL PCLKSEL0

/* Peripheral Clock Selection register (PCLKSELx) SSP bits
    - PCLK_xyz = CCLK/4 */
#define PCLKSEL_SSP_MASK   0x300000
#define PCLKSEL_SSP_VALUE  0x0

/* SSPx Clock Prescale Register (SSPxCPSR) */
#define SSPCPSR SSP1CPSR

/* SSPx Clock Prescale Register (SSPxCPSR) value */
#define SSPCPSR_VALUE   30

/* Pin Function Select register (PINSELx) */
#define PINSEL PINSEL0

/* Pin Function Select register (PINSELx) SSP bits
    - GPIO: SSEL1,
    - SSP: SCK1, MISO1, MOSI1 */
#define PINSEL_SSP_MASK   0xFF000
#define PINSEL_SSP_VALUE  0xA8000

/* Pin Mode select register (PINMODEx) */
#define PINMODE PINMODE0

/* Pin Mode select register (PINMODEx) SSP bits
    - SSEL1: pull-up resistor enabled
    - SCK1: pull-up resistor enabled
    - MISO1: pull-up resistor enabled
    - MOSI1: pull-up resistor enabled */
#define PINMODE_SSP_MASK   0xFF000
#define PINMODE_SSP_VALUE  0x0

/* SSPx Interrupt Mask Set/Clear Register (SSPxIMSC) */
#define SSPIMSC SSP1IMSC

/* SSPx Interrupt Mask Set/Clear Register (SSPxIMSC) value
    - all interrupts disabled */
#define SSPIMSC_VALUE   0

/* Interrupt Enable Clear Register (VICIntEnClear) value
    - SSP1 */
#define VICIntEnClr_SSP_VALUE 0x800

/* SSPx Control Register 0 (SSPxCR0) */
#define SSPCR0 SSP1CR0

/* SSPx Control Register 0 (SSPxCR0) value
    - SCR=0
    - CPHA=1
    - CPOL=1
    - Frame Format SPI
    - Data Size 8 bit transfer */
#define SSPCR0_VALUE 0xC7

/* SSPx Control Register 1 (SSPxCR1) */
#define SSPCR1 SSP1CR1

/* SSPx Control Register 1 (SSPxCR1) value
    - The SSP controller acts as a master on the bus
    - The SSP controller is disabled
    - normal operation */
#define SSPCR1_VALUE 0x0

/* Enable SSPx */
#define SSP_ENABLE() (SSPCR1 = (SSPCR1 & 0xD) | 2)

/* Disable SSPx */
#define SSP_DISABLE() (SSPCR1 &= 0xD)

/* GPIO Port Direction control register */
#define IODIR   IODIR0

/* GPIO Port Direction control register SSP bits
    - SSEL: output */
#define IODIR_SSP_MASK   0x01
#define IODIR_SSP_VALUE  0x01

/* GPIO Port SSEL pin mask */
#define SSEL_PIN_MASK   0x01

/* GPIO Port Output Set register */
#define IOSET   IOSET0

/* GPIO Port Output Clear register */
#define IOCLR   IOCLR0

/* SSEL pin high */
#define SSEL_PIN_HIGH() (IOSET = SSEL_PIN_MASK)

/* SSEL pin low */
#define SSEL_PIN_LOW() (IOCLR = SSEL_PIN_MASK)

/* SSPx Status Register (SSPxSR) */
#define SSPSR  SSP1SR

/* SSPx Busy */
#define SSP_BUSY() (SSPSR & 0x10)

/* SSPx Transmit FIFO Not Full */
#define SSP_TXBUF_READY() (SSPSR & 2)

/* SSPx Receive FIFO Empty */
#define SSP_RXBUF_EMPTY() (!(SSPSR & 4))

/* SSPx Data Register (SSPxDR) */
#define SSPDR  SSP1DR

//------------------------------------------------------------------------------
//  Select chip (select==TRUE).
//------------------------------------------------------------------------------
static void TI_LMP90100_SPIChipSelect(BOOL select)
{
    if (select)
    {
        TI_LMP90100_SPISetup();
        SSEL_PIN_LOW();
        delay_us();
    }
    else
    {
        delay_us();
        SSEL_PIN_HIGH();
    }
}

//------------------------------------------------------------------------------
//  Configures the SPI port and initializes it.
//------------------------------------------------------------------------------
void TI_LMP90100_SPISetup(void)
{
    SSP_DISABLE();

    PCONP = (PCONP & ~PCONP_SSP_MASK) | PCONP_SSP_VALUE;
    PCLKSEL = (PCLKSEL & ~PCLKSEL_SSP_MASK) | PCLKSEL_SSP_VALUE;
    SSPCPSR = SSPCPSR_VALUE;
    PINSEL = (PINSEL & ~PINSEL_SSP_MASK) | PINSEL_SSP_VALUE;
    PINMODE = (PINMODE & ~PINMODE_SSP_MASK) | PINMODE_SSP_VALUE;
    SSPIMSC = SSPIMSC_VALUE;
    VICIntEnClr = VICIntEnClr_SSP_VALUE;
    SSPCR0 = SSPCR0_VALUE;
    SSPCR1 = SSPCR1_VALUE;
    IODIR = (IODIR & ~IODIR_SSP_MASK) | IODIR_SSP_VALUE;

    TI_LMP90100_SPIChipSelect(FALSE);
    SSP_ENABLE();
}

//------------------------------------------------------------------------------
//  URA Setup if "addr" does not lie within the same segment as "*pURA".
//------------------------------------------------------------------------------
static void SPIWriteURA(uint8_t addr, uint8_t *pURA)
{
  uint8_t new_URA, inst;

  new_URA = (addr & LMP90100_URA_MASK) >> 4;                                   // extract upper register address

#ifdef TEST_INCLUDE_URA_TRANSACTION
  if (*pURA != new_URA)                                                        // if new and previous URA not same, add transaction 1
  {
#endif // TEST_INCLUDE_URA_TRANSACTION

    inst = LMP90100_INSTRUCTION_BYTE1_WRITE;                                   // Transaction-1

    while (!SSP_TXBUF_READY());                                                // Wait for TXBUF ready
    SSPDR = inst;                                                              // Send instruction

    while (!SSP_TXBUF_READY());                                                // Wait for TXBUF ready
    SSPDR = new_URA;                                                           // Send upper register address

    *pURA = new_URA;                                                           // save new URA

#ifdef TEST_INCLUDE_URA_TRANSACTION
  }
#endif // TEST_INCLUDE_URA_TRANSACTION
}

//------------------------------------------------------------------------------
//  Clear SSP Rx FIFO buffer.
//------------------------------------------------------------------------------
static void SPIClearRxBuffer(void)
{
  uint8_t dummy;

  while (SSP_BUSY());
  while (!SSP_RXBUF_EMPTY())
  {
      dummy = SSPDR;
  }
}

//------------------------------------------------------------------------------
//  Writes "value" to a single configuration register at address "addr". If
//  "addr" lies within the same segment as "*pURA", it takes 1 less transaction
//  to write the value.
//------------------------------------------------------------------------------
void TI_LMP90100_SPIWriteReg(uint8_t addr, uint8_t value, uint8_t *pURA)
{
  uint8_t inst;

  TI_LMP90100_SPIChipSelect(TRUE);                                             // /CS enable

  SPIWriteURA(addr, pURA);                                                     // URA Setup

  inst = LMP90100_WRITE_BIT | LMP90100_SIZE_1B | (addr & LMP90100_LRA_MASK);   // lower register address
  while (!SSP_TXBUF_READY());                                                  // Wait for TXBUF ready
  SSPDR = inst;                                                                // Send lower register address

  while (!SSP_TXBUF_READY());                                                  // Wait for TXBUF ready
  SSPDR = value;                                                               // Send data value

  while (SSP_BUSY());                                                          // Wait for TX complete

  TI_LMP90100_SPIChipSelect(FALSE);                                            // /CS disable
}

//------------------------------------------------------------------------------
//  Reads a single configuration register at address "addr" and returns the
//  value read. If "addr" lies within the same segment as "*pURA", it takes 1
//  less transaction to read the value.
//------------------------------------------------------------------------------
uint8_t TI_LMP90100_SPIReadReg(uint8_t addr, uint8_t *pURA)
{
  uint8_t value, inst;

  TI_LMP90100_SPIChipSelect(TRUE);                                             // /CS enable

  SPIWriteURA(addr, pURA);                                                     // URA Setup

  inst = LMP90100_READ_BIT | LMP90100_SIZE_1B | (addr & LMP90100_LRA_MASK);    // Transaction-2
  while (!SSP_TXBUF_READY());                                                  // Wait for TXBUF ready
  SSPDR = inst;                                                                // Send lower register address

  while (SSP_BUSY());                                                          // Wait for TX complete
  SPIClearRxBuffer();                                                          // Clear Rx buffer before reading data
  SSPDR = 0;                                                                   // Dummy write so we can read data

  while (SSP_BUSY());                                                          // Wait for TX complete
  value = SSPDR;                                                               // Read data

  TI_LMP90100_SPIChipSelect(FALSE);                                            // /CS disable

  return value;
}

//------------------------------------------------------------------------------
//  Writes "value" to a single configuration register at address "addr". If
//  "addr" lies within the same segment as "*pURA", it takes 1 less transaction
//  to write the value. Reads a single configuration register at address "addr"
//  and returns TRUE if the value read is the same as in write operation.
//------------------------------------------------------------------------------
BOOL TI_LMP90100_SPIWriteReadReg(uint8_t addr, uint8_t value, uint8_t *pURA)
{
    TI_LMP90100_SPIWriteReg(addr, value, pURA);
    return TI_LMP90100_SPIReadReg(addr, pURA) == value ? TRUE : FALSE;
}

//------------------------------------------------------------------------------
//  Writes values to multiple configuration registers, the first register being
//  at address "addr".  First data byte is at "buffer", and both addr and
//  buffer are incremented sequentially until "count" writes have been performed.
//  If "addr" lies within the same segment as "*pURA", it takes 1 less
//  transaction to write the value.
//------------------------------------------------------------------------------
void TI_LMP90100_SPINormalStreamWriteReg(uint8_t addr, uint8_t *buffer,
                                         uint8_t count, uint8_t *pURA)
{
  uint8_t inst, i;

  TI_LMP90100_SPIChipSelect(TRUE);                                             // /CS enable

  SPIWriteURA(addr, pURA);                                                     // URA Setup

  inst = LMP90100_WRITE_BIT | LMP90100_SIZE_STREAM
                            | (addr & LMP90100_LRA_MASK);                      // lower register address
  while (!SSP_TXBUF_READY());                                                  // Wait for TXBUF ready
  SSPDR = inst;                                                                // Send lower register address

  for (i = 0; i < count; i++)
  {
    while (!SSP_TXBUF_READY());                                                // Wait for TXBUF ready
    SSPDR = *(buffer+i);                                                       // Send data value
  }

  while (SSP_BUSY());                                                          // Wait for TX complete

  TI_LMP90100_SPIChipSelect(FALSE);                                            // /CS disable
}

//------------------------------------------------------------------------------
//  Reads multiple configuration registers, the first register being at address
//  "addr".  Values read are deposited sequentially starting at address
//  "buffer", until "count" registers have been read. If "addr" lies within
//  the same segment as "*pURA", it takes 1 less transaction to read the value.
//------------------------------------------------------------------------------
void TI_LMP90100_SPINormalStreamReadReg(uint8_t addr, uint8_t *buffer,
                                        uint8_t count, uint8_t *pURA)
{
  uint8_t i, inst;

  TI_LMP90100_SPIChipSelect(TRUE);                                             // /CS enable

  SPIWriteURA(addr, pURA);                                                     // URA Setup

  inst = LMP90100_READ_BIT | LMP90100_SIZE_STREAM
                           | (addr & LMP90100_LRA_MASK);                       // Transaction-2
  while (!SSP_TXBUF_READY());                                                  // Wait for TXBUF ready
  SSPDR = inst;                                                                // Send lower register address

  while (SSP_BUSY());                                                          // Wait for TX complete
  SPIClearRxBuffer();                                                          // Clear Rx buffer before reading data

  for (i = 0; i < count; i++)
  {
    SSPDR = 0;                                                                 // Dummy write so we can read data

    while (SSP_BUSY());                                                        // Wait for TX complete
    *(buffer+i) = SSPDR;                                                       // Read data
  }

  TI_LMP90100_SPIChipSelect(FALSE);                                            // /CS disable
}

//------------------------------------------------------------------------------
//  Writes values to multiple configuration registers, the first register being
//  at address "addr".  First data byte is at "writeBuffer", and both addr and
//  buffer are incremented sequentially until "count" writes have been performed.
//  If "addr" lies within the same segment as "*pURA", it takes 1 less
//  transaction to write the value.
//  Reads multiple configuration registers, the first register being at address
//  "addr".  Values read are deposited sequentially starting at address
//  "readBuffer", until "count" registers have been read. If "addr" lies within
//  the same segment as "*pURA", it takes 1 less transaction to read the value.
//  Returns TRUE if the values read is the same as in write operation.
//------------------------------------------------------------------------------
BOOL TI_LMP90100_SPINormalStreamWriteReadReg(uint8_t addr, uint8_t *writeBuffer,
                                         uint8_t *readBuffer, uint8_t count,
                                         uint8_t *pURA)
{
    // Stream write
    TI_LMP90100_SPINormalStreamWriteReg(addr, writeBuffer, count, pURA);

    // Stream Read back for test
    TI_LMP90100_SPINormalStreamReadReg(addr, readBuffer, count, pURA);

    // test if stream write/read values match
    for (uint8_t i = 0; i < count; i++)
    {
        if (writeBuffer[i] != readBuffer[i])
            return FALSE;
    }

    return TRUE;
}
