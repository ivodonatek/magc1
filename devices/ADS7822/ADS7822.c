//******************************************************************************
//  This file contains functions that allow to access the synchronous 3-wire
//  serial interface of the ADS7822.
//*****************************************************************************/

#include <stdint.h>
#include "LPC23xx.h"
#include "delay.h"
#include "ADS7822.h"
#include "../../maine.h"

/* Pin Function Select register (PINSELx)
    - ADS7822 CS/SHDN Pin */
#define PINSEL_CS PINSEL4

/* Pin Function Select register (PINSELx) bits for ADS7822 CS/SHDN Pin
    - GPIO: P2.7 */
#define PINSEL_CS_MASK   0xC000
#define PINSEL_CS_VALUE  0x0

/* Pin Function Select register (PINSELx)
    - ADS7822 DCLOCK Pin */
#define PINSEL_DCLOCK PINSEL4

/* Pin Function Select register (PINSELx) bits for ADS7822 DCLOCK Pin
    - GPIO: P2.6 */
#define PINSEL_DCLOCK_MASK   0x3000
#define PINSEL_DCLOCK_VALUE  0x0

/* Pin Function Select register (PINSELx)
    - ADS7822 DOUT Pin */
#define PINSEL_DOUT PINSEL4

/* Pin Function Select register (PINSELx) bits for ADS7822 DOUT Pin
    - GPIO: P2.5 */
#define PINSEL_DOUT_MASK   0xC00
#define PINSEL_DOUT_VALUE  0x0

/* Pin Mode select register (PINMODEx)
    - ADS7822 CS/SHDN Pin */
#define PINMODE_CS PINMODE4

/* Pin Mode select register (PINMODEx) bits for ADS7822 CS/SHDN Pin
    - P2.7: pull-up resistor enabled */
#define PINMODE_CS_MASK   0xC000
#define PINMODE_CS_VALUE  0x0

/* Pin Mode select register (PINMODEx)
    - ADS7822 DCLOCK Pin */
#define PINMODE_DCLOCK PINMODE4

/* Pin Mode select register (PINMODEx) bits for ADS7822 DCLOCK Pin
    - P2.6: pull-up resistor enabled */
#define PINMODE_DCLOCK_MASK   0x3000
#define PINMODE_DCLOCK_VALUE  0x0

/* Pin Mode select register (PINMODEx)
    - ADS7822 DOUT Pin */
#define PINMODE_DOUT PINMODE4

/* Pin Mode select register (PINMODEx) bits for ADS7822 DOUT Pin
    - P2.5: pull-up resistor enabled */
#define PINMODE_DOUT_MASK   0xC00
#define PINMODE_DOUT_VALUE  0x0

/* GPIO Port registers for ADS7822 CS/SHDN Pin */
#define IODIR_CS   FIO2DIR
#define IOPIN_CS   FIO2PIN
#define IOSET_CS   FIO2SET
#define IOCLR_CS   FIO2CLR

/* GPIO Port registers for ADS7822 DCLOCK Pin */
#define IODIR_DCLOCK   FIO2DIR
#define IOPIN_DCLOCK   FIO2PIN
#define IOSET_DCLOCK   FIO2SET
#define IOCLR_DCLOCK   FIO2CLR

/* GPIO Port registers for ADS7822 DOUT Pin */
#define IODIR_DOUT   FIO2DIR
#define IOPIN_DOUT   FIO2PIN
#define IOSET_DOUT   FIO2SET
#define IOCLR_DOUT   FIO2CLR

/* GPIO Port Pin masks for ADS7822 */
#define CS_PIN_MASK         (1 << 7)
#define DCLOCK_PIN_MASK     (1 << 6)
#define DOUT_PIN_MASK       (1 << 5)

/* GPIO Port Direction control register values for ADS7822 pins */
#define IODIR_CS_VALUE          (1 << 7)
#define IODIR_DCLOCK_VALUE      (1 << 6)
#define IODIR_DOUT_VALUE        (0 << 5)

/* ADS7822 CS/SHDN Pin high */
#define CS_PIN_HIGH() (IOSET_CS = CS_PIN_MASK)

/* ADS7822 CS/SHDN Pin low */
#define CS_PIN_LOW() (IOCLR_CS = CS_PIN_MASK)

/* ADS7822 DCLOCK Pin high */
#define DCLOCK_PIN_HIGH() (IOSET_DCLOCK = DCLOCK_PIN_MASK)

/* ADS7822 DCLOCK Pin low */
#define DCLOCK_PIN_LOW() (IOCLR_DCLOCK = DCLOCK_PIN_MASK)

/* ADS7822 DOUT Pin read */
#define DOUT_PIN_READ() (IOPIN_DOUT & DOUT_PIN_MASK)

/* ADS7822 DCLOCK Pin read */
#define DCLOCK_PIN_READ() (IOPIN_DCLOCK & DCLOCK_PIN_MASK)

/* ADS7822 CS Pin read */
#define CS_PIN_READ() (IOPIN_CS & CS_PIN_MASK)

/* ADS7822 CS falling to DCLOCK rising delay in microseconds */
#define DELAY_TSUCS_MICROSEC        100

/* ADS7822 half clock period in microseconds */
#define DELAY_HALF_CLOCK_MICROSEC   250

//------------------------------------------------------------------------------
//  Delay in microseconds.
//------------------------------------------------------------------------------
static void delay(uint8_t microseconds)
{
    uint8_t i;

    for (i = 0; i < microseconds; i++)
    {
        delay_us();
    }
}

//------------------------------------------------------------------------------
//  Drive clock period.
//------------------------------------------------------------------------------
static void clock_period(void)
{
    DCLOCK_PIN_HIGH();
    delay(DELAY_HALF_CLOCK_MICROSEC);
    DCLOCK_PIN_LOW();
    delay(DELAY_HALF_CLOCK_MICROSEC);
}

//------------------------------------------------------------------------------
//  Configures the interface and initializes it.
//------------------------------------------------------------------------------
void ADS7822_Setup(void)
{
    // ADS7822 CS/SHDN Pin
    PINSEL_CS = (PINSEL_CS & ~PINSEL_CS_MASK) | PINSEL_CS_VALUE;
    PINMODE_CS = (PINMODE_CS & ~PINMODE_CS_MASK) | PINMODE_CS_VALUE;
    IODIR_CS = (IODIR_CS & ~CS_PIN_MASK) | IODIR_CS_VALUE;
    CS_PIN_HIGH();

    // ADS7822 DCLOCK Pin
    PINSEL_DCLOCK = (PINSEL_DCLOCK & ~PINSEL_DCLOCK_MASK) | PINSEL_DCLOCK_VALUE;
    PINMODE_DCLOCK = (PINMODE_DCLOCK & ~PINMODE_DCLOCK_MASK) | PINMODE_DCLOCK_VALUE;
    IODIR_DCLOCK = (IODIR_DCLOCK & ~DCLOCK_PIN_MASK) | IODIR_DCLOCK_VALUE;
    DCLOCK_PIN_HIGH();

    // ADS7822 DOUT Pin
    PINSEL_DOUT = (PINSEL_DOUT & ~PINSEL_DOUT_MASK) | PINSEL_DOUT_VALUE;
    PINMODE_DOUT = (PINMODE_DOUT & ~PINMODE_DOUT_MASK) | PINMODE_DOUT_VALUE;
    IODIR_DOUT = (IODIR_DOUT & ~DOUT_PIN_MASK) | IODIR_DOUT_VALUE;
}

//------------------------------------------------------------------------------
//  Get 12-bit sample from ADS7822. Returns -1 if sample error.
//------------------------------------------------------------------------------
int16_t ADS7822_GetSample(void)
{
    uint8_t clocks = 0;          // pocitadlo clk period
    uint16_t sample = 0;         // hodnota AD prevodu

    // zacatek prenosu
    DCLOCK_PIN_LOW();            // CS falling to DCLOCK low max 0ns
    CS_PIN_LOW();
    delay(DELAY_TSUCS_MICROSEC);

    // detekce null bitu
    do
    {
        clock_period();
        clocks++;
        if (clocks > 5)
        {
            CS_PIN_HIGH();
            DCLOCK_PIN_HIGH();
            return -1;           // chyba
        }
    }
    while (DOUT_PIN_READ());

    // prijem bitu dat
    for (clocks = 0; clocks < 12; clocks++)
    {
        clock_period();

        sample <<= 1;
        if (DOUT_PIN_READ())
            sample |= 1;
    }

    // konec prenosu
    DCLOCK_PIN_HIGH();
    delay(DELAY_HALF_CLOCK_MICROSEC);
    CS_PIN_HIGH();

    return sample;
}
