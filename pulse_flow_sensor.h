//******************************************************************************
//  Ovladac pulsniho sensoru prutoku
//*****************************************************************************/

#ifndef PULSE_FLOW_SENSOR_H_INCLUDED
#define PULSE_FLOW_SENSOR_H_INCLUDED

// Vzorek pulsu
typedef struct
{
    // stav pocitani pulsu
    unsigned long pulseCount;

    // cas
    unsigned long time;

    // platnost vzorku
    BOOL valid;

} TPulseSample;

// Fw module state
typedef struct
{
    // open state
    BOOL isOpen;

    // pocitani pulsu
    unsigned long pulseCount;

    // stary vzorek pulsu
    TPulseSample oldSample;

    // aktualni vzorek pulsu
    TPulseSample actualSample;

    // cas posledniho impulsu
    unsigned long lastPulseTime;

    // cas urovne high na impulznim vstupu
    unsigned long highLevelTime;

    // aktualni casovy interval mezi merenimi [ms]
    unsigned long dTms;

    // aktualni prirustek detekovanych impulsu
    unsigned long dPulse;

    // aktualni prirustek proteceneho objemu [m3]
    float dM3;

} TPulseFlowSensorState;

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//------------------------------------------------------------------------------
void PulseFlowSensor_Init(BOOL open);

//------------------------------------------------------------------------------
//  Hlavni uloha. Volat co nejcasteji.
//------------------------------------------------------------------------------
void PulseFlowSensor_Task(void);

//------------------------------------------------------------------------------
//  Vrati stav.
//------------------------------------------------------------------------------
const TPulseFlowSensorState* PulseFlowSensor_GetState(void);

#endif // PULSE_FLOW_SENSOR_H_INCLUDED
