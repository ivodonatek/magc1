/**********************************************************************************************************
** FW pro komunikaci mezi uPC a senzorem pres UART3, zaroven RS485, rizeni toku, celou dobu je nastaveno **
** na prijemm jen pri odesilani je smer toku prepnut, po odeslani se                                     **
** zase prepne na prijem (mozny error s prerusenim, kdyz neco prijde a nevyprazni se se buffer).         **
**                                                                                                       **
**                                                                                                       **
** VYTVORENO 11.5.2010  POSLEDNI UPRAVY 11.5.2010                                                        **
**********************************************************************************************************/

#include "LPC23xx.h"
#include "type.h"
#include "maine.h"
#include "display.h"
#include "rprintf.h"
#include "sensor.h"
#include "menu_tables.h"
#include "mbcrc.h"
#include "uart.h"

#define SENSOR_REQUEST_MSEC 300

unsigned char size_recv=0;
unsigned int yes=0;
unsigned int sensorRequestMsec=0; // pocet msec pro casovani dotazu na sensor

// prijimaci bufer
unsigned char recv_buffer[RECV_BUFFER_LGT];
unsigned char recv_buffer_count=0;

void send_calibration(unsigned char pos)
{
  if (flag_sensor8)
    send_calibration_sensor8();
  else
    ;
}
void sensor_cisteni(unsigned char clean)
{
  if ( flag_sensor8)
    send_cisteni_sensor8(clean);
  else
    ;
}

void sensor_buzeni(unsigned char power)
{
  if ( flag_sensor8)
    nastav_buzeni_sensor8();    //zapne jak frekvenci tak power
  else
    ;
}
void sensor_frekvence(unsigned char frequenc)
{
  if ( flag_sensor8)
    nastav_buzeni_sensor8();    //zapne jak frekvenci tak power
  else
    ;
}

void sensor_stav_frekvence()
{
}

void sensor_stav_buzeni()
{
  if ( flag_sensor8)
    sensor_stav_buzeni_sensor8();
  else
    ;
}

void request_data()
{
  if ( flag_sensor8)
    request_data_sensor8();
  else
    ;
}


void recv_calibration(unsigned char id)
{
  if ( flag_sensor8)
    sensor_stav_buzeni_sensor8();
  else
    ;
}

unsigned char zpracovat_data()
{
  if ( flag_sensor8)
    zpracovat_data_sensor8();
  else
    ;

  return 1;
}

void recv_ser_numb(void)
{
  if ( flag_sensor8)
    recv_firmware_sensor8();
  else
    ;
}

void send_zero_constant(void)
{
  if ( flag_sensor8)
    ;//send_zero_constant_sensor8();
  else
    ;
}

void recv_zero_constant(void)
{
  if ( flag_sensor8)
    recv_zero_constant_sensor8();
  else
    ;
}

void sensor_communication()      //preruseni od UART3
{
    unsigned char rcvByte = Read_sensor();

    if ((!flag_receive) && (flag_cekam_odpoved_od_sensor8) && (recv_buffer_count < RECV_BUFFER_LGT))
    {
        recv_buffer[recv_buffer_count]=rcvByte;
        recv_buffer_count++;

        if (recv_buffer_count >= 2)
        {
            switch(recv_buffer[1])
            {
            case WRITE_MULTIPLE_REGISTER:
                if(recv_buffer_count==8)
                {
                    flag_receive=1;
                }
                break;

            case READ_HOLDING_REGISTER:
                if(recv_buffer_count>=3)
                {
                    if(recv_buffer_count==(recv_buffer[2]+5))
                    {
                        flag_receive=1;
                    }
                }
                break;

            default:
                break;
            }
        }
    }

    VICVectAddr=0;
}

// Volat kazdou milisekundu
void Sensor_MillisecondTick(void)
{
    if (sensorRequestMsec < SENSOR_REQUEST_MSEC)
        sensorRequestMsec++;
}

// Je doba na dotaz na sensor?
unsigned char Sensor_IsTimeToRequest(void)
{
    if (sensorRequestMsec >= SENSOR_REQUEST_MSEC)
        return 1;
    else
        return 0;
}

// Resetovani doby dotazu na sensor?
void Sensor_ResetTimeToRequest(void)
{
    sensorRequestMsec = 0;
}
