//******************************************************************************
//  Mereni tepla
//*****************************************************************************/

#ifndef HEAT_H_INCLUDED
#define HEAT_H_INCLUDED

#include "maine.h"

// Jednotka tepla
typedef enum HeatUnit
{
    HU_kWh      = 0,   // kWh (Kilowatthour) [kWh]
    HU_BTU      = 1,   // BTU (British thermal unit) [BTU]
    HU_Joule    = 2,   // Joule (base unit) [J]
    HU_Kpm      = 3,   // Kpm (Kilopondmetre) [kpm]
    HU_Nm       = 4,   // Nm (Newtonmetre) [Nm]
    HU_Cal      = 5,   // Cal (Calorie) [cal]

    HU_MIN      = HU_kWh,
    HU_MAX      = HU_Cal,
    HU_COUNT    = (HU_MAX + 1)

} THeatUnit;

// Stav testu mereni tepla
typedef struct HeatTestState
{
    // platnost testu
    BOOL valid;

    // start/stop testu
    BOOL run;

    // cas posledniho mereni
    unsigned long lastMeasureTime;

    // aktualni doba trvani testu [s]
    unsigned long actualSeconds;

    // celkovy objem [m3]
    float totalVolume;

    // celkove teplo [J]
    float totalHeat;

} THeatTestState;

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//------------------------------------------------------------------------------
void Heat_Init(void);

//------------------------------------------------------------------------------
// Mereni tepla
//
// Parametry:
// flow ... aktualni prutok v m3/h
// dTime ... doba toku v s
//------------------------------------------------------------------------------
void Heat_MeasureWithFlow(float flow, float dTime);

//------------------------------------------------------------------------------
// Mereni tepla
//
// Parametry:
// dVolume ... aktualni prirustek proteceneho objemu v m3
// dTime ... doba toku v s
//------------------------------------------------------------------------------
void Heat_MeasureWithVolume(float dVolume, float dTime);

//------------------------------------------------------------------------------
// Spusteni/zastaveni testu mereni tepla (TRUE==start, FALSE==stop)
//------------------------------------------------------------------------------
void Heat_RunTest(BOOL run);

//------------------------------------------------------------------------------
// Uloha testu mereni tepla (volat co nejcasteji)
//------------------------------------------------------------------------------
void Heat_TestTask(void);

//------------------------------------------------------------------------------
// Vrati stav testu mereni tepla
//------------------------------------------------------------------------------
const THeatTestState* Heat_GetTestState(void);

//------------------------------------------------------------------------------
//  Vrati celkove teplo v zadanych jednotkach
//------------------------------------------------------------------------------
float Heat_GetTotalHeat(THeatUnit heatUnit);

//------------------------------------------------------------------------------
//  Vynuluje celkove teplo
//------------------------------------------------------------------------------
void Heat_DeleteTotalHeat(void);

//------------------------------------------------------------------------------
//  Vrati aktualni tepelny vykon [W]
//------------------------------------------------------------------------------
float Heat_GetHeatPerformance(void);

//------------------------------------------------------------------------------
//  Vrati text jednotky tepla
//------------------------------------------------------------------------------
const char* Heat_GetUnitText(THeatUnit heatUnit);

//------------------------------------------------------------------------------
//  Vrati teplo v Joule pro zadanou 1 jednotku
//------------------------------------------------------------------------------
float Heat_GetUnitValueInJoule(THeatUnit heatUnit);

#endif // HEAT_H_INCLUDED
