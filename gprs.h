/******************************************************************************
*** gprs.h ********************************************************************
*******************************************************************************
*   Rozhrani pro komunikaci s GPRS modulem                                    *
*                                                                             *
******************************************************************************/

#include "type.h"
#include "LPC23xx.h"

#ifndef gprs
#define gprs

///#define GPRS_DDR      DDRD
///#define GPRS_PORT     PORTD
    #define GPRS_PIN      FIO2PIN
///#define GPRS_CS      (1<<PD7)
    #define GPRS_CS      (1<<3)
///#define GPRS_ER      (1<<PD6)
    #define GPRS_ER      (1<<2)

#define GPRS_HEADER             0x55

#define GPRS_COMMAND_INSIDE     0
#define GPRS_COMMAND_GATEWAY    2
#define GPRS_COMMAND_USER       3
#define GPRS_COMMAND_PASSWD     4
#define GPRS_COMMAND_PORT       5
#define GPRS_COMMAND_ERROR      6
#define GPRS_COMMAND_IP         7
#define GPRS_COMMAND_PIN        8

#define GPRS_NO_ERROR               0
/**
#define GPRS_ERROR_COMMUNICATION    4   //spatna hlavicka nebo velikost prijate zpravy
#define GPRS_ERROR_CHECK            5   //prijata zprava neni stejna jako odeslana
#define GPRS_ERROR_TIMEOUT          6   //vyprsel tiomeout odpovedi
*/
/*
#define GPRS_ERROR_RESET            7   //vnitrni reset GPRS modulu
#define GPRS_ERROR_RESET            8   //nejde nastavit echo OFF u GPRS
#define GPRS_ERROR_RESET            9   //chyba SIM karty nebo PINu
#define GPRS_ERROR_RESET           10   //neni signal
#define GPRS_ERROR_RESET           11   //nelze vytocit
#define GPRS_ERROR_RESET           12   //nelze zjistit IP
#define GPRS_ERROR_RESET           13   //nelze nejde zapnout online data mode
#define GPRS_ERROR_RESET           14   //nelze nejde zapnout online data mode
*/

/**
unsigned char GPRS_Error = GPRS_NO_ERROR;       // chyba vznikla pri komunikaci s GPRS modulem
unsigned char GPRS_Error_State = GPRS_NO_ERROR; // chyba vracena GPRS modulem
unsigned char GPRS_INSIDE = FALSE;
unsigned GPRS_init_error = FALSE;                   // chyba pri inicializaci modulu GPRS
*/
extern unsigned long GPRS_Error;       // chyba vznikla pri komunikaci s GPRS modulem
extern unsigned long GPRS_Error_State; // chyba vracena GPRS modulem
extern unsigned char GPRS_INSIDE;
extern unsigned GPRS_init_error;                   // chyba pri inicializaci modulu GPRS

//extern void usart0_init(void);
extern void GPRS_Init(void);
extern void usart1_putc(unsigned char c);
extern void usart1_putGPRScommand(unsigned char *data);
extern unsigned char usart1_CheckGPRSmessage(unsigned char *send, unsigned char *receive, unsigned char size);
extern void GPRS_Send_Command(unsigned char Command);
extern unsigned long GPRS_Error_test (void);
extern void GPRS_get_IP (void);
extern void smaz_GPRS_errory(void);

#endif //gprs
