#ifndef MAINE_H_INCLUDED
#define MAINE_H_INCLUDED

#include"menu_tables.h"
#include"type.h"

#define FW_VERSION 6000

#ifndef BOOL_TYPEDEF
#define BOOL_TYPEDEF
typedef unsigned char BOOL;
#endif

#ifndef false
#define false 0
#endif

#ifndef true
#define true 1
#endif

//#define SENSOR7   1
//#define SENSOR8   1

#define VELIKOST_POLE_TEPLOTA_EP 10
#define BACKLIGHT_SEC    10
#define SAMPLES_MAX    60 // maximalni pocet vyorku
#define SEC_TO_WRITE_FLASH 43200
#define PI (3.1415926535897932384626433832795)

typedef void (TSimpleHandler)	(void);

extern void write_to_RAM(void);
extern void write_data_to_flash(void);
extern void obsluha_tlacitek (void);

extern volatile char odesilaci_buffer[160];
//extern unsigned long debug_code;
extern unsigned long actual_error;
extern unsigned long actual_error_datalogger;
extern long adc_average;
extern unsigned char flag_cisteni;
extern unsigned char flag_used_sensor;
extern volatile unsigned char flag_aktivni_zadost;
extern unsigned char flag_zapis_prutok;
extern unsigned char flag_sensor8;
extern unsigned char flag_key_lock_time;
extern volatile unsigned char flag_gsm_dokoncil_prijem;
extern volatile unsigned char flag_gsm_datalogger_first_send;

unsigned long GSM_Date_backup;

extern unsigned char pocet_znaku_gsm;
extern volatile unsigned char flag_timeout_timer2;
extern volatile unsigned int flag_second_gsm;
extern unsigned char flag_totaliser_cycling;
extern unsigned char flag_datalogger;
extern unsigned int flag_zapsat_flash;
extern unsigned char flag_reset_after_load_default;
extern unsigned char flag_start;
extern volatile unsigned char flag_komunikacni_modul;
extern volatile unsigned char flag_usb_modul_insert;
extern unsigned int flag_cisteni_cas;
extern unsigned int second_to_datalogger;
extern long zero_constant;
extern char read_datalogger(unsigned long date, unsigned int time, unsigned int pocet);
extern void gsm_busy_pin(unsigned char state);

extern unsigned char flag_event_empty_on_odeslano;
extern unsigned char flag_event_empty_off_odeslano;
extern unsigned char flag_event_zero_on_odeslano;
extern unsigned char flag_event_zero_off_odeslano;

extern unsigned char last_key;
extern unsigned char key_ESC_time_sec;
extern unsigned char key_dummy;
extern volatile long act_adc[SAMPLES_MAX];
extern volatile long pole_teplota[VELIKOST_POLE_TEPLOTA_EP];
extern volatile long pole_ep[VELIKOST_POLE_TEPLOTA_EP];
extern long teplota_avg;
extern unsigned long ep_avg;
extern unsigned char pocet_pokusu_send;
extern unsigned char pocet_spatnych_prijmu;
extern unsigned char  pocet_pokusu_recv;
extern unsigned char pocet_pokusu_zadost_sensor;
extern unsigned char flag_probiha_prijem_od_gsm;
extern volatile unsigned char flag_posli_datalogger_do_gsm;
extern unsigned char flag_chci_odeslat_event;
extern char buffer_gsm_event[100];
extern unsigned char aktualni_cislo_pro_odeslani;
extern unsigned long flag_aktualni_event;
extern unsigned long flag_dalsi_zadost_event;
extern unsigned long last_sending_error_gsm;
extern unsigned char flag_chci_odeslat_interval;
extern unsigned char aktualni_cislo_interval;
extern char buffer_gsm_interval[160];
extern volatile unsigned char flag_gsm_potvrdit_nastaveni;
extern char buffer_gsm_potvrzeni[100];
extern volatile unsigned char flag_sms_zastaveny;

extern unsigned char gsm_odesila(void);
extern void gsm_odesli_interval(void);
extern void gsm_odesli_event(unsigned long udalost);
extern unsigned char backlight_second;
extern void ukonceni_odesilani_datalogger_gsm(void);

#define MODUL_USB           (1<<0)
#define MODUL_RS232_485     (1<<1)
#define MODUL_TCPIP_BLUE    (1<<2)
#define MODUL_GPRS          (1<<4)
#define MODUL_GSM           (1<<5)
#define MODUL_WIFI          (1<<6)

extern unsigned char flag_sensor_zadosti;
extern unsigned char aktivni_cisteni;
extern unsigned long error_min;
extern unsigned char error_sec;
extern unsigned long ok_min;
extern unsigned char ok_sec;
extern unsigned long RAM_Read_Data_Long(unsigned long Address);
extern float RAM_Read_Data_Float(unsigned long Address);

/*zmena parametru komunikace behem chodu*/
volatile char ChangeCommunicationParam;
volatile unsigned char modbus_transmitt_state;

extern unsigned long sensor_fw_version; // fw verze senzoru

unsigned char g_fun[2];              // Funkce provadena pred vstupem do okna a po vyskoceni z okna. [0=IN] [1=OUT]. Pokud =0, neprovadi se zadna akce.
/* Provadene funkce */
#define F_IN                 0 // Akce po vstupu do okna
#define F_OUT                1 // Akce po ukonceni okna

///errory
#define ERR_EMPTY_PIPE          (1<<0)
#define ERR_OVERLOAD            (1<<1)
#define ERR_EXCITATION          (1<<2)
#define ERR_SENSOR              (1<<3)
#define ERR_OPEN_FILE           (1<<4)
#define ERR_NOT_INSERT_CARD     (1<<5)
#define ERR_WRITE_FLASH         (1<<6)
#define ERR_ADC                 (1<<7)
#define ERR_GSM_TIMEOUT         (1<<8)
#define ERR_GSM_SIGNAL          (1<<9)
#define ERR_GSM_SIMCARD         (1<<10)
#define ERR_GSM_SENDING         (1<<11)
#define ERR_GSM_OTHERS          (1<<12)
#define ERR_TEMPERATURE         (1<<13)

#define GPRS_ERROR_COMMUNICATION   (1<<14)   //spatna hlavicka nebo velikost prijate zpravy
#define GPRS_ERROR_CHECK           (1<<15)   //prijata zprava neni stejna jako odeslana
#define GPRS_ERROR_TIMEOUT         (1<<16)   //vyprsel tiomeout odpovedi
#define GPRS_ERROR_RESET           (1<<17)   //vnitrni reset GPRS modulu
#define GPRS_ERROR_ECHO            (1<<18)   //nejde nastavit echo OFF u GPRS
#define GPRS_ERROR_SIM_PIN         (1<<19)   //chyba SIM karty nebo PINu
#define GPRS_ERROR_SIGNAL          (1<<20)   //neni signal
#define GPRS_ERROR_CALL            (1<<21)   //nelze vytocit
#define GPRS_ERROR_IP              (1<<22)   //nelze zjistit IP
#define GPRS_ERROR_ONLINE          (1<<23)   //nelze nejde zapnout online data mode

#define ERR_OVERLOAD2           (1<<24)
#define ERR_BUTTONS             (1<<25)
#define ERR_EXT_TEMPERATURE     (1<<26)
#define ERR_EXT_PRESSURE        (1<<27)
#define ERR_FIRMWARE            (1<<28)
#define ERR_TEMPERATURE_DIFF    (1<<29)

// pro automaticke nastaveni vystupu
#define DN0QN       0
#define DN10QN      800
#define DN15QN      2000
#define DN20QN      3200
#define DN25QN      5000
#define DN32QN      8000
#define DN40QN      13000
#define DN50QN      20000
#define DN65QN      35000
#define DN80QN      50000
#define DN100QN     80000
#define DN125QN     150000
#define DN150QN     200000
#define DN200QN     300000
#define DN250QN     500000
#define DN300QN     800000
#define DN350QN     1000000
#define DN400QN     1300000
#define DN500QN     2000000
#define DN600QN     3000000
#define DN700QN     4000000
#define DN800QN     5000000
#define DN900QN     6000000
#define DN1000QN    8000000

/// Funkce spodniho pocitadla
#define MEAS_COUNT          (12) // Pocet moznosti
#define MEAS_TOTAL          (0)
#define MEAS_AUX            (1)
#define MEAS_FLOW_POS       (2)
#define MEAS_FLOW_NEG       (3)
#define MEAS_TEMP           (4) // teplota
#define MEAS_SPEED          (5) // rychlost proudeni
#define MEAS_BAR            (6) // graficke znazorneni aktualniho prutoku
#define MEAS_GRAPH          (7) // historie prutoku v grafu
#define MEAS_EXT_TEMP1      (8) // externi teplota - kanal 1
#define MEAS_EXT_TEMP2      (9) // externi teplota - kanal 2
#define MEAS_FLOW           (10) // aktualni prutok
#define MEAS_TOTAL_HEAT     (11) // celkove teplo

/// Stranky informaci servisniho modu
#define SERVICE_INFO_PAGES              (11) // Pocet stranek
#define SERVICE_MAIN_PAGE               (0) // hlavni
#define SERVICE_OPTIC_BUTTONS_PAGE      (1) // opticka tlacitka
#define SERVICE_TEMP_PAGE               (2) // teploty
#define SERVICE_EXT_TEMP1_PAGE          (3) // externi teplota - kanal 1
#define SERVICE_EXT_TEMP2_PAGE          (4) // externi teplota - kanal 2
#define SERVICE_EXT_TEMP1_AD_PAGE       (5) // externi teplota - kanal 1 AD
#define SERVICE_EXT_TEMP2_AD_PAGE       (6) // externi teplota - kanal 2 AD
#define SERVICE_MBUS_PAGE               (7) // MBus
#define SERVICE_PULSE_FLOW_SENSOR_PAGE  (8) // pulsni sensor prutoku
#define SERVICE_HEAT_TEST_PAGE          (9) // test mereni tepla
#define SERVICE_CHARGER_PAGE            (10) // nabijecka

///vystupni moduly
extern unsigned char flag_outputs_modules;
#define IOUT_MODUL  (1<<0)
#define VOUT_MODUL  (1<<1)

// struktura pro jeden sloupec grafu
typedef struct
{
  unsigned char top;    // vrchni cast grafu 4 body na vysku
  unsigned char center; // prostredni cast grafu vysoka 8 bodu
  unsigned char bottom; // spodni cast grafu vysoka 6 bodu, kde 6 bod je spodni linka
}
t_graph;

#define GRAPH_WIDTH 119
t_graph graphHistory[GRAPH_WIDTH];

/* Stav menu */
typedef struct TVALUE
{                           // 4. HODNOTY URCUJICI VLASTNOSTI MENENYCH HODNOT
  int  ptr;                              // Offset v tabulce values, smeruje na prislusnou hodnotu, ptr muze byt aji -1
  unsigned char typ;                     // 0=Float 1=LongInt
  unsigned char dig;                     // Pocet mist pred des. carkou
  unsigned char dec;                     // Pocet mist za des. carkou
  unsigned int  uni;                     // unit, jednotka
  unsigned long min;                     // Minimum, 9 mist + des tecka
  unsigned long max;                     // Maximum, 9 mist + des tecka
  unsigned long val;                     // Momentalne menena hodnota
  unsigned int  tex;                     // Odkaz do tabulky text
  unsigned char unichar[UNIT_SIZE];      // unit, jednotka textove
  unsigned char minchar[10];             // Minimum, 9 mist + des tecka, textove
  unsigned char maxchar[10];             // Maximum, 9 mist + des tecka, textove
  unsigned char valchar[16];             // Momentalne menena hodnota, textove (max 255.255.255.255 = 15 znaku + 0)
}
TVALUE;
volatile TVALUE         g_value[4];             // Menene hodnoty

typedef struct TMeasurement
{                          // ZOBRAZOVANE VELKE CISLA V OKNE MERENI
  float actual;
  //unsigned char actual_signed;    // Znamenko prutoku 0 = +; 1 = -
  //unsigned char last_signed;      // Znamenko prutoku v minule sekunde 0 = +; 1 = -
  long long total;
  long long aux;
  long long flow_pos;
  long long flow_neg;
  float temp;
  //unsigned char temp_signed;    // Znamenko teploty 0 = +; 1 = -
  unsigned char act_rot_meas;   // Poradove cislo aktualniho spodniho pocitadla
  //TMeVALUE up;                  // zobrazovane horni cilso
  //TMeVALUE down;                // zobrazovane dolni cislo
}
TMeasurement;

extern volatile TMeasurement g_measurement;

typedef struct
{
    // VLASTNOSTI ZOBRAZOVANE HLASKY
    unsigned char error_allow;   // Tento byte urcuje, kolik vterin jsou zakazane hlasky, 0 = povoleno
    unsigned char typ;           // Typ zobrazovane hlasky
    unsigned char prevmode;      // predchozi rezim (do ktereho se stavovy automat vraci po ukonceni hlasky)
    TSimpleHandler *callback;    // obecny callback pro odlozene procedury
    unsigned char str[100];      // zobrazovany text, muze obsahovat i ~ znaky (text pokracuje na dalsi radce) 100
}
TMESSAGE;                       // Zobrazovana hlasky

volatile static unsigned char g_scroll  = 0;     // cislo radku, ktery ma byt inverzni. Prvni radek je 0
volatile static unsigned int  g_count   = 0;     // pocet polozek v aktualnim menu
volatile static unsigned int  g_adr     = 0;
volatile unsigned char g_mode;          // Rezim: Mereni=0 Menu=1 Editace=2 Hlaska=3
volatile static unsigned char g_edittyp = 0;         // Typ editacniho okna. 0=Info 1=Edit 2=4Edit 3=Isel 4=Bool 5=Time 6=Date 7=Calw 8=Pasw
volatile static unsigned char g_level   = 0;         // uroven zanoreni
volatile static unsigned char menu_levels[11];       // Zanoreni, v menu, dulezite pri navratech (esc)
volatile static TMESSAGE       g_message;             // Hlaska

extern unsigned char key_lock; // promenna pro zamykani klaves

extern void Reload_meas_value (void);  // spravne zobrazeni spodniho pocitadla
extern char * ReadStringFromFlash (unsigned int adress, char *g_s1);
extern char * ReadUnitFromFlash (unsigned int adress, char *g_s1);
extern void RAM_Write_Data_Long(unsigned long data, unsigned long Address);
extern void RAM_Write_Data_Float(float data, unsigned long Address);
extern void RAM_Write_Data(unsigned long data, unsigned long Address);

extern unsigned char write_datalogger( void );
extern void ShowMeasurement(void);

// typ pripojeneho modulu na Iout rozhrani
typedef enum IoutModule
{
    // current loop output/V output module
    TIM_IV_OUT,

    // external pressure sensor
    TIM_EXT_PRESS

} TIoutModule;

// Velicina pro prime ovladani vystupu typu Iout, Fout, ...
typedef enum DirectDrivingQuantity
{
    DDQ_HEAT_POWER = 0,
    DDQ_FLOW,

    DDQ_Min = DDQ_HEAT_POWER,
    DDQ_Max = DDQ_FLOW,
    DDQ_Count = DDQ_Max + 1

} TDirectDrivingQuantity;

// Velicina zobrazena v grafech
typedef enum GraphQuantity
{
    GQ_HEAT_POWER = 0,
    GQ_FLOW,

    GQ_Min = GQ_HEAT_POWER,
    GQ_Max = GQ_FLOW,
    GQ_Count = GQ_Max + 1

} TGraphQuantity;

BOOL IsUart1ForModul3(void); // Test, zda je UART1 vyhrazen pro Modul3
void InitUart1Function(void); // init funkcnosti pres rozhrani UART1
void InitSensorFunction(void); // init funkcnosti sensoru prutoku
void InitIoutModuleFunction(void); // init funkcnosti pres rozhrani Iout modulu
void InitPulseModuleFunction(void); // init funkcnosti pres rozhrani Pulse modulu
TIoutModule GetIoutModuleType(void); // vraci typ modulu pripojeneho pres rozhrani Iout
void ExternalMeasurements(void); // externi mereni
void ShiftForwardDispMeasurement(void); // Shift forward currently displayed measurement
void ShiftBackDispMeasurement(void); // Shift back currently displayed measurement
void ShiftForwardDispServiceInfoPage(void); // Shift forward currently displayed service info page
void ShiftBackDispServiceInfoPage(void); // Shift back currently displayed service info page

void WriteEventLogMessage(const char *message);
void WriteEventLogParameterChange(unsigned long valueOffset, unsigned long data);
void WriteEventLogParameter(const char *name, const char *value, const char *unit);
void EventLogTask(void);

BOOL IsActualFlowUnderLowCutoffLevel(void);
void InitGraphHistory(void); // inicializace bufferu pro graf

BYTE Convert4BitsToHexChar(BYTE fourBits);
void ConvertByteArrayToHexStr(const BYTE* byteArray, BYTE byteArrayLength, BYTE* hexString);

#endif // MAINE_H_INCLUDED
