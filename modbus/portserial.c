/*
  * FreeModbus Libary: LPC214X Port
  * Copyright (C) 2007 Tiago Prado Lone <tiago@maxwellbohr.com.br>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id: portserial.c,v 1.1 2007/04/24 23:15:18 wolti Exp $
 */

#include <LPC23xx.h>
#include <string.h>
#include "port.h"
#include "irq.h"
#include "gsm.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"
#include "VCOM.h"
#include "menu_tables.h"
#include "maine.h"
#include "rprintf.h"
#include "display.h"

// ----------------------- static functions ---------------------------------/
static void sio_irq( void ) __irq;
static void uart1_irq( void ) __irq;

static void     prvvUARTTxReadyISR( void );
static void     prvvUARTRxISR( void );

// ----------------------- Start implementation -----------------------------/
void vMBPortSerialEnable( BOOL xRxEnable, BOOL xTxEnable )
{
    BOOL modul3 = IsUart1ForModul3();

    //rs232/485
    if( xRxEnable )
    {
        U2IER |= 0x01;
        if (modul3) U1IER |= 0x01;
    }
    else
    {
        U2IER &= ~0x01;
        if (modul3) U1IER &= ~0x01;
    }
    if( xTxEnable )
    {
        U2IER |= 0x02;
        if(!USBconnected())
        {
            PINSEL0 |= (1<<20);       /* TxD2 RxD2 P0.10 */
        }
        prvvUARTTxReadyISR(  );

        if (modul3) U1IER |= 0x02;
        prvvUARTTxReadyISR(  );
    }
    else
    {
        U2IER &= ~0x02;
        PINSEL0 &= ~(1<<21) & ~(1<<20);       /* TxD2 RxD2 P0.10 P0.11*/
        IODIR0 &= ~(1<<10);
        if (modul3) U1IER &= ~0x02;
    }
}


void vMBPortClose( void )
{
};

BOOL
xMBPortSerialInit( UCHAR ucPORT, ULONG ulBaudRate, UCHAR ucDataBits, eMBParity eParity )
{
    BOOL            bInitialized = TRUE;
    USHORT          cfg = 0;
    ULONG           reload = ( ( PCLK / ulBaudRate ) / 16UL );
    volatile char   dummy;

#ifdef RTS_ENABLE
    RTS_INIT;
    RTS_IN
#endif

    ( void )ucPORT;


	//PINSEL0 = 0x0000000A;       /* TxD3 RXD3 P0.0 P0.1*/

            //USB,RS232/485
            //if(!USBconnected())      //pokud je pripojene USB
            {
                PINSEL0 |= (1<<20) | (1<<22);       /* TxD2 RxD2 P0.10 P0.11*/
                PCONP |= (1<<24);      //set power on UART2
            }
            //TCP/IP
            PINSEL4 |= (1<<1) | (1<<3);       //TxD1 RxD1 P2.0 P2.1
            PCONP |= (1<<4);     //set power on UART1


    switch ( ucDataBits )
    {
    case 5:
        break;

    case 6:
        cfg |= 0x00000001;
        break;

    case 7:
        cfg |= 0x00000002;
        break;

    case 8:
        cfg |= 0x00000003;
        break;

    default:
        bInitialized = FALSE;
    }

    switch ( eParity )
    {
    case MB_PAR_NONE:
        break;

    case MB_PAR_ODD:
        cfg |= 0x00000008;
        break;

    case MB_PAR_EVEN:
        cfg |= 0x00000018;
        break;
    case MB_PAR_NONE_TWO_STOPBITS:
        cfg |= 0x00000004;
        break;
    }

    //FIO2DIR |= (1<<3);   //vystupni pin  pro reset TCP/IP modulu
    //FIO2SET |= (1<<3);      //reset - TCP/IP pin P2.3 do "1"
    if( bInitialized )
    {
      // RS232/485
      {
        U2LCR = cfg;            /* Configure Data Bits and Parity */
        U2IER = 0;              /* Disable UART1 Interrupts */

        U2LCR |= 0x80;          /* Set DLAB */
        U2DLL = reload;         /* Set Baud     */
        U2DLM = reload >> 8;    /* Set Baud */
        U2LCR &= ~0x80;         /* Clear DLAB */

        // Configure UART2 Interrupt
        VICVectAddr28 = ( unsigned long )sio_irq;
        VICVectCntl28 = (0x20 | 28);
        VICIntEnable |= (1 << 28);  // Enable UART2 Interrupt
      }

      // TCP/IP, Bluetooth, GPRS, WiFi
      U1LCR = 0;            // Configure Data Bits and Parity
      U1LCR |= 0x00000003;    //none parity, 8 bits
      U1IER = 0;              // Disable UART1 Interrupts
      U1LCR |= 0x80;          // Set DLAB
      ULONG rychlost = ( ( PCLK / 19200 ) / 16UL );
      U1DLL = rychlost;         // Set Baud
      U1DLM = rychlost >> 8;    // Set Baud
      U1LCR &= ~0x80;         // Clear DLAB

      // Configure UART1 Interrupt
      VICVectAddr7 = ( unsigned long )uart1_irq;
      VICVectCntl7 = (0x20 | 7);
      VICIntEnable |= (1 << 7);  // Enable UART1 Interrupt

      dummy = U1IIR;          // Required to Get Interrupts Started
    }
    return bInitialized;
}

BOOL
xMBPortSerialPutByte( CHAR ucByte )
{
    if( (USBconnected()) && ( flag_usb_modul_insert==1 ) )
    {
        VCOM_putchar(ucByte);
    }
    else
    {
        flag_usb_modul_insert=0;
        #ifdef  RTS_ENABLE
        RTS_OUT;
        #endif
            if( flag_komunikacni_modul & MODUL_RS232_485)
            {     //RS232/485
                U2THR = ucByte;
                /* Wait till U0THR and U0TSR are both empty */
                while( !( U2LSR & 0x40 ) )
                {
                }
            }
            else if( (flag_komunikacni_modul & MODUL_GPRS) || ( flag_komunikacni_modul & MODUL_TCPIP_BLUE) )
            {
                 //TCP/IP,bluetooth,gprs
                U1THR = ucByte;
                while( !( U1LSR & 0x40 ) )
                {
                }
            }
        #ifdef  RTS_ENABLE
        RTS_IN;
        #endif
    }

    return TRUE;
}

BOOL xMBPortSerialGetByte( CHAR * pucByte )
{
  if( (USBconnected()) && ( flag_usb_modul_insert==1) )
  {
    flag_usb_modul_insert=1;
    *pucByte = VCOM_getchar();
  }
  else
  {
    flag_usb_modul_insert=0;
    if( flag_komunikacni_modul & MODUL_RS232_485)
    {
      while( !( U2LSR & 0x01 ) )
      {
      }
      /* Receive Byte */
      *pucByte = U2RBR;
    }
    else if( (flag_komunikacni_modul & MODUL_GPRS) || ( flag_komunikacni_modul & MODUL_TCPIP_BLUE) )
    {
      while( !( U1LSR & 0x01 ) )
      {
      }
      /* Receive Byte */
      *pucByte = U1RBR;
    }
  }
  return TRUE;
}


void
sio_irq( void )
    __irq
{
    volatile char   dummy;
    volatile char   IIR;

    ///tady asi staci jen nastavit flag komunikacni modul, zbytecne se nemusi kontrolovat

    flag_komunikacni_modul=MODUL_RS232_485;

    while( ( ( IIR = U2IIR ) & 0x01 ) == 0 )
    {
        switch ( IIR & 0x0E )
        {
        case 0x06:             /* Receive Line Status */
            dummy = U2LSR;      /* Just clear the interrupt source */
            break;

        case 0x04:             /* Receive Data Available */
        case 0x0C:             /* Character Time-Out */
            prvvUARTRxISR(  );
            break;

        case 0x02:             /* THRE Interrupt */
            prvvUARTTxReadyISR(  );
            break;

        case 0x00:             /* Modem Interrupt */
            //dummy = U1MSR;      /* Just clear the interrupt source */
            break;

        default:
            break;
        }
    }

    VICVectAddr = 0xFF;         /* Acknowledge Interrupt */
}

extern char flag_wifi_test;

void uart1_irq( void ) __irq
{
  if( flag_wifi_test )
  {
    unsigned char response = 0;
    unsigned long timeout = 0;            //pomocna pro timeout

    char msg[10];
    msg[9] = 0;

    //cekej na prichozi zpravu [OK]
    while ( response < 10 )
    {
      if( ( U1LSR & 0x01 ) )
      {
        msg[response] = U1RBR;  //uloz prichozi byte do prijimaciho bufferu
        response++;
        timeout = 0;
      }
      else      //kontrola timeout
      {
        timeout++;
        if (timeout == 5000)           // timeout
          break;
      }
    }

    // modul po resetu posle na seriovou linku zpravu WizFi250 Version... je pripojeny wifi modul
    if ( strstr(msg, "WizFi250") )
    {
      RAM_Write_Data_Long(1,MODULE_PRESSENT);
      flag_wifi_test = 0;
    }
    flag_wifi_test = 0;
  }

  if( flag_komunikacni_modul & MODUL_GSM )
  {
    flag_probiha_prijem_od_gsm=1;
    gsm_recv_buffer[gsm_pozice_buff++]=U1RBR;

    if(gsm_pozice_buff >= gsm_recv_buffer[1] && gsm_pozice_buff >= 2)    //protoze na zacatku je v [2] "0" a 4 je nejmensi moznu pocet prijmute delky
    {
      flag_gsm_dokoncil_prijem=1;
      gsm_pozice_buff=0;
    }
  }
  else
  {
      volatile char   dummy;
      volatile char   IIR;
      if( (!(flag_komunikacni_modul & MODUL_TCPIP_BLUE)) || (!(flag_komunikacni_modul & MODUL_GPRS))) //pokud neni nastaven jeden ze dvou modulu
      {
          ///kontrola GPRS
          ///jestlize je to GPRS, nastavit flag GPRS,odstavit pin pro RESET TCP
          ///jestli ne, nastavit flag TCFIP a ponechat ty piny

          if(1) //zatim nesplnena podminka
          {
              flag_komunikacni_modul=MODUL_GPRS;
              //FIO2CLR |= (1<<3);      //reset - TCP/IP pin P2.3 do "0"

          }
          else //modul TCPIP,NLUETOOTH
          {
              flag_komunikacni_modul=MODUL_TCPIP_BLUE;
              FIO2SET |= (1<<3);      //reset - TCP/IP pin P2.3 do "1"
              ///nastavit tystupni pin na reset
          }
      }

       while( ( ( IIR = U1IIR ) & 0x01 ) == 0 )
       {
          switch ( IIR & 0x0E )
          {
          case 0x06:             /* Receive Line Status */
              dummy = U1LSR;      /* Just clear the interrupt source */
              break;

          case 0x04:             /* Receive Data Available */
          case 0x0C:             /* Character Time-Out */
              prvvUARTRxISR(  );
              //unsigned char hnup=U1RBR;
              break;

          case 0x02:             /* THRE Interrupt */
              prvvUARTTxReadyISR(  );
              break;

          case 0x00:             /* Modem Interrupt */
              //dummy = U1MSR;      /* Just clear the interrupt source */
              break;

          default:
              break;
          }
      }
  }
  VICVectAddr = 0xFF;         /* Acknowledge Interrupt */
}

/*
 * Create an interrupt handler for the transmit buffer empty interrupt
 * (or an equivalent) for your target processor. This function should then
 * call pxMBFrameCBTransmitterEmpty( ) which tells the protocol stack that
 * a new character can be sent. The protocol stack will then call
 * xMBPortSerialPutByte( ) to send the character.
 */
static void
prvvUARTTxReadyISR( void )
{
    pxMBFrameCBTransmitterEmpty(  );
}

/*
 * Create an interrupt handler for the receive interrupt for your target
 * processor. This function should then call pxMBFrameCBByteReceived( ). The
 * protocol stack will then call xMBPortSerialGetByte( ) to retrieve the
 * character.
 */
static void
prvvUARTRxISR( void )
{
    pxMBFrameCBByteReceived(  );
}
