//******************************************************************************
//  Mereni tepla
//*****************************************************************************/

#include <stdint.h>
#include <float.h>
#include <math.h>
#include "heat.h"
#include "maine.h"
#include "systick.h"
#include "conversion.h"
#include "extmeas/temperature/exttemp.h"

// The specific gas constant of ordinary water - IAPWS R7-97(2012)
#define R_CONST   0.461526 // kJ/(kg K)

// Heat unit conversions
#define BTU_TO_JOULE        1055
#define KPM_TO_JOULE        9.80665
#define NM_TO_JOULE         1
#define KWH_TO_JOULE        3600000
#define CAL_TO_JOULE        4.1868

// fyzikalni konstanty
#define H2O_HEAT_CAPACITY   4181.8 // merna tepelna kapacita vody pri 20�C [J/(kg*K)]
#define H2O_DENSITY         998.205 // hustota vody pri 20�C [kg/m3]

#define HEAT_FLOAT_INVALID  FLT_MAX // neplatna hodnota typu THeatFloat
typedef float THeatFloat;

// Fw module state
static struct
{
    // celkove teplo [J]
    float totalHeat;

    // aktualni tepelny vykon [W]
    float heatPerformance;

    // Stav testu mereni tepla
    THeatTestState heatTestState;

} state;

//------------------------------------------------------------------------------
//  Konstantni pole pro vypocty
//------------------------------------------------------------------------------
const float h1_pT_I1_Array[] =
{
    0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 8, 8, 21, 23, 29, 30, 31, 32
};

const float h1_pT_J1_Array[] =
{
    -2, -1, 0, 1, 2, 3, 4, 5, -9, -7, -1, 0, 1, 3, -3, 0, 1, 3, 17, -4, 0, 6, -5, -2, 10, -8, -11, -6, -29, -31, -38, -39, -40, -41
};

const float h1_pT_n1_Array[] =
{
    0.14632971213167, -0.84548187169114, -3.756360367204, 3.3855169168385, -0.95791963387872, 0.15772038513228, -0.016616417199501, 8.1214629983568E-04, 2.8319080123804E-04, -6.0706301565874E-04, -0.018990068218419, -0.032529748770505, -0.021841717175414, -5.283835796993E-05, -4.7184321073267E-04, -3.0001780793026E-04, 4.7661393906987E-05, -4.4141845330846E-06, -7.2694996297594E-16, -3.1679644845054E-05, -2.8270797985312E-06, -8.5205128120103E-10, -2.2425281908E-06, -6.5171222895601E-07, -1.4341729937924E-13, -4.0516996860117E-07, -1.2734301741641E-09, -1.7424871230634E-10, -6.8762131295531E-19, 1.4478307828521E-20, 2.6335781662795E-23, -1.1947622640071E-23, 1.8228094581404E-24, -9.3537087292458E-26
};

const float h2_pT_J0_Array[] =
{
    0, 1, -5, -4, -3, -2, -1, 2, 3
};

const float h2_pT_n0_Array[] =
{
    -9.6927686500217, 10.086655968018, -0.005608791128302, 0.071452738081455, -0.40710498223928, 1.4240819171444, -4.383951131945, -0.28408632460772, 0.021268463753307
};

const float h2_pT_Ir_Array[] =
{
    1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 5, 6, 6, 6, 7, 7, 7, 8, 8, 9, 10, 10, 10, 16, 16, 18, 20, 20, 20, 21, 22, 23, 24, 24, 24
};

const float h2_pT_Jr_Array[] =
{
    0, 1, 2, 3, 6, 1, 2, 4, 7, 36, 0, 1, 3, 6, 35, 1, 2, 3, 7, 3, 16, 35, 0, 11, 25, 8, 36, 13, 4, 10, 14, 29, 50, 57, 20, 35, 48, 21, 53, 39, 26, 40, 58
};

const float h2_pT_nr_Array[] =
{
    -1.7731742473213E-03, -0.017834862292358, -0.045996013696365, -0.057581259083432, -0.05032527872793, -3.3032641670203E-05, -1.8948987516315E-04, -3.9392777243355E-03, -0.043797295650573, -2.6674547914087E-05, 2.0481737692309E-08, 4.3870667284435E-07, -3.227767723857E-05, -1.5033924542148E-03, -0.040668253562649, -7.8847309559367E-10, 1.2790717852285E-08, 4.8225372718507E-07, 2.2922076337661E-06, -1.6714766451061E-11, -2.1171472321355E-03, -23.895741934104, -5.905956432427E-18, -1.2621808899101E-06, -0.038946842435739, 1.1256211360459E-11, -8.2311340897998, 1.9809712802088E-08, 1.0406965210174E-19, -1.0234747095929E-13, -1.0018179379511E-09, -8.0882908646985E-11, 0.10693031879409, -0.33662250574171, 8.9185845355421E-25, 3.0629316876232E-13, -4.2002467698208E-06, -5.9056029685639E-26, 3.7826947613457E-06, -1.2768608934681E-15, 7.3087610595061E-29, 5.5414715350778E-17, -9.436970724121E-07
};

const float p3sat_h_Ii_Array[] =
{
    0, 1, 1, 1, 1, 5, 7, 8, 14, 20, 22, 24, 28, 36
};

const float p3sat_h_Ji_Array[] =
{
    0, 1, 3, 4, 36, 3, 0, 24, 16, 16, 3, 18, 8, 24
};

const float p3sat_h_ni_Array[] =
{
    0.600073641753024, -9.36203654849857, 24.6590798594147, -107.014222858224, -91582131580576.8, -8623.32011700662, -23.5837344740032, 2.52304969384128E+17, -3.89718771997719E+18, -3.33775713645296E+22, 35649946963.6328, -1.48547544720641E+26, 3.30611514838798E+18, 8.13641294467829E+37
};

const float T3_ph_a_Ii_Array[] =
{
    -12, -12, -12, -12, -12, -12, -12, -12, -10, -10, -10, -8, -8, -8, -8, -5, -3, -2, -2, -2, -1, -1, 0, 0, 1, 3, 3, 4, 4, 10, 12
};

const float T3_ph_a_Ji_Array[] =
{
    0, 1, 2, 6, 14, 16, 20, 22, 1, 5, 12, 0, 2, 4, 10, 2, 0, 1, 3, 4, 0, 2, 0, 1, 1, 0, 1, 0, 3, 4, 5
};

const float T3_ph_a_ni_Array[] =
{
    -1.33645667811215E-07, 4.55912656802978E-06, -1.46294640700979E-05, 6.3934131297008E-03, 372.783927268847, -7186.54377460447, 573494.7521034, -2675693.29111439, -3.34066283302614E-05, -2.45479214069597E-02, 47.8087847764996, 7.64664131818904E-06, 1.28350627676972E-03, 1.71219081377331E-02, -8.51007304583213, -1.36513461629781E-02, -3.84460997596657E-06, 3.37423807911655E-03, -0.551624873066791, 0.72920227710747, -9.92522757376041E-03, -0.119308831407288, 0.793929190615421, 0.454270731799386, 0.20999859125991, -6.42109823904738E-03, -0.023515586860454, 2.52233108341612E-03, -7.64885133368119E-03, 1.36176427574291E-02, -1.33027883575669E-02
};

const float T3_ph_b_Ii_Array[] =
{
    -12, -12, -10, -10, -10, -10, -10, -8, -8, -8, -8, -8, -6, -6, -6, -4, -4, -3, -2, -2, -1, -1, -1, -1, -1, -1, 0, 0, 1, 3, 5, 6, 8
};

const float T3_ph_b_Ji_Array[] =
{
    0, 1, 0, 1, 5, 10, 12, 0, 1, 2, 4, 10, 0, 1, 2, 0, 1, 5, 0, 4, 2, 4, 6, 10, 14, 16, 0, 2, 1, 1, 1, 1, 1
};

const float T3_ph_b_ni_Array[] =
{
    3.2325457364492E-05, -1.27575556587181E-04, -4.75851877356068E-04, 1.56183014181602E-03, 0.105724860113781, -85.8514221132534, 724.140095480911, 2.96475810273257E-03, -5.92721983365988E-03, -1.26305422818666E-02, -0.115716196364853, 84.9000969739595, -1.08602260086615E-02, 1.54304475328851E-02, 7.50455441524466E-02, 2.52520973612982E-02, -6.02507901232996E-02, -3.07622221350501, -5.74011959864879E-02, 5.03471360939849, -0.925081888584834, 3.91733882917546, -77.314600713019, 9493.08762098587, -1410437.19679409, 8491662.30819026, 0.861095729446704, 0.32334644281172, 0.873281936020439, -0.436653048526683, 0.286596714529479, -0.131778331276228, 6.76682064330275E-03
};

const float h5_pT_Ji0_Array[] =
{
    0, 1, -3, -2, -1, 2
};

const float h5_pT_ni0_Array[] =
{
    -13.179983674201, 6.8540841634434, -0.024805148933466, 0.36901534980333, -3.1161318213925, -0.32961626538917
};

const float h5_pT_Iir_Array[] =
{
    1, 1, 1, 2, 3
};

const float h5_pT_Jir_Array[] =
{
    0, 1, 3, 9, 3
};

const float h5_pT_nir_Array[] =
{
    -1.2563183589592E-04, 2.1774678714571E-03, -0.004594282089991, -3.9724828359569E-06, 1.2919228289784E-07
};

const float v1_pT_I1_Array[] =
{
    0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 8, 8, 21, 23, 29, 30, 31, 32
};

const float v1_pT_J1_Array[] =
{
    -2, -1, 0, 1, 2, 3, 4, 5, -9, -7, -1, 0, 1, 3, -3, 0, 1, 3, 17, -4, 0, 6, -5, -2, 10, -8, -11, -6, -29, -31, -38, -39, -40, -41
};

const float v1_pT_n1_Array[] =
{
    0.14632971213167, -0.84548187169114, -3.756360367204, 3.3855169168385, -0.95791963387872, 0.15772038513228, -0.016616417199501, 8.1214629983568E-04, 2.8319080123804E-04, -6.0706301565874E-04, -0.018990068218419, -0.032529748770505, -0.021841717175414, -5.283835796993E-05, -4.7184321073267E-04, -3.0001780793026E-04, 4.7661393906987E-05, -4.4141845330846E-06, -7.2694996297594E-16, -3.1679644845054E-05, -2.8270797985312E-06, -8.5205128120103E-10, -2.2425281908E-06, -6.5171222895601E-07, -1.4341729937924E-13, -4.0516996860117E-07, -1.2734301741641E-09, -1.7424871230634E-10, -6.8762131295531E-19, 1.4478307828521E-20, 2.6335781662795E-23, -1.1947622640071E-23, 1.8228094581404E-24, -9.3537087292458E-26
};

const float v2_pT_J0_Array[] =
{
    0, 1, -5, -4, -3, -2, -1, 2, 3
};

const float v2_pT_n0_Array[] =
{
    -9.6927686500217, 10.086655968018, -0.005608791128302, 0.071452738081455, -0.40710498223928, 1.4240819171444, -4.383951131945, -0.28408632460772, 0.021268463753307
};

const float v2_pT_Ir_Array[] =
{
    1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 5, 6, 6, 6, 7, 7, 7, 8, 8, 9, 10, 10, 10, 16, 16, 18, 20, 20, 20, 21, 22, 23, 24, 24, 24
};

const float v2_pT_Jr_Array[] =
{
    0, 1, 2, 3, 6, 1, 2, 4, 7, 36, 0, 1, 3, 6, 35, 1, 2, 3, 7, 3, 16, 35, 0, 11, 25, 8, 36, 13, 4, 10, 14, 29, 50, 57, 20, 35, 48, 21, 53, 39, 26, 40, 58
};

const float v2_pT_nr_Array[] =
{
    -1.7731742473213E-03, -0.017834862292358, -0.045996013696365, -0.057581259083432, -0.05032527872793, -3.3032641670203E-05, -1.8948987516315E-04, -3.9392777243355E-03, -0.043797295650573, -2.6674547914087E-05, 2.0481737692309E-08, 4.3870667284435E-07, -3.227767723857E-05, -1.5033924542148E-03, -0.040668253562649, -7.8847309559367E-10, 1.2790717852285E-08, 4.8225372718507E-07, 2.2922076337661E-06, -1.6714766451061E-11, -2.1171472321355E-03, -23.895741934104, -5.905956432427E-18, -1.2621808899101E-06, -0.038946842435739, 1.1256211360459E-11, -8.2311340897998, 1.9809712802088E-08, 1.0406965210174E-19, -1.0234747095929E-13, -1.0018179379511E-09, -8.0882908646985E-11, 0.10693031879409, -0.33662250574171, 8.9185845355421E-25, 3.0629316876232E-13, -4.2002467698208E-06, -5.9056029685639E-26, 3.7826947613457E-06, -1.2768608934681E-15, 7.3087610595061E-29, 5.5414715350778E-17, -9.436970724121E-07
};

const float v3_ph_a_Ii_Array[] =
{
    -12, -12, -12, -12, -10, -10, -10, -8, -8, -6, -6, -6, -4, -4, -3, -2, -2, -1, -1, -1, -1, 0, 0, 1, 1, 1, 2, 2, 3, 4, 5, 8
};

const float v3_ph_a_Ji_Array[] =
{
    6, 8, 12, 18, 4, 7, 10, 5, 12, 3, 4, 22, 2, 3, 7, 3, 16, 0, 1, 2, 3, 0, 1, 0, 1, 2, 0, 2, 0, 2, 2, 2
};

const float v3_ph_a_ni_Array[] =
{
    5.29944062966028E-03, -0.170099690234461, 11.1323814312927, -2178.98123145125, -5.06061827980875E-04, 0.556495239685324, -9.43672726094016, -0.297856807561527, 93.9353943717186, 1.92944939465981E-02, 0.421740664704763, -3689141.2628233, -7.37566847600639E-03, -0.354753242424366, -1.99768169338727, 1.15456297059049, 5683.6687581596, 8.08169540124668E-03, 0.172416341519307, 1.04270175292927, -0.297691372792847, 0.560394465163593, 0.275234661176914, -0.148347894866012, -6.51142513478515E-02, -2.92468715386302, 6.64876096952665E-02, 3.52335014263844, -1.46340792313332E-02, -2.24503486668184, 1.10533464706142, -4.08757344495612E-02
};

const float v3_ph_b_Ii_Array[] =
{
    -12, -12, -8, -8, -8, -8, -8, -8, -6, -6, -6, -6, -6, -6, -4, -4, -4, -3, -3, -2, -2, -1, -1, -1, -1, 0, 1, 1, 2, 2
};

const float v3_ph_b_Ji_Array[] =
{
    0, 1, 0, 1, 3, 6, 7, 8, 0, 1, 2, 5, 6, 10, 3, 6, 10, 0, 2, 1, 2, 0, 1, 4, 5, 0, 0, 1, 2, 6
};

const float v3_ph_b_ni_Array[] =
{
    -2.25196934336318E-09, 1.40674363313486E-08, 2.3378408528056E-06, -3.31833715229001E-05, 1.07956778514318E-03, -0.271382067378863, 1.07202262490333, -0.853821329075382, -2.15214194340526E-05, 7.6965608822273E-04, -4.31136580433864E-03, 0.453342167309331, -0.507749535873652, -100.475154528389, -0.219201924648793, -3.21087965668917, 607.567815637771, 5.57686450685932E-04, 0.18749904002955, 9.05368030448107E-03, 0.285417173048685, 3.29924030996098E-02, 0.239897419685483, 4.82754995951394, -11.8035753702231, 0.169490044091791, -1.79967222507787E-02, 3.71810116332674E-02, -5.36288335065096E-02, 1.6069710109252
};

const float v5_pT_Iir_Array[] =
{
    1, 1, 1, 2, 3
};

const float v5_pT_Jir_Array[] =
{
    0, 1, 3, 9, 3
};

const float v5_pT_nir_Array[] =
{
    -1.2563183589592E-04, 2.1774678714571E-03, -0.004594282089991, -3.9724828359569E-06, 1.2919228289784E-07
};

// deklarace
static float h1_pT(float p, float T);
static float h2_pT(float p, float T);

//------------------------------------------------------------------------------
//  Vrati ulozene celkove teplo
//------------------------------------------------------------------------------
static float GetSavedTotalHeat(void)
{
    return RAM_Read_Data_Float(HEAT_TOTAL_FLOAT);
}

//------------------------------------------------------------------------------
//  Ulozi celkove teplo
//------------------------------------------------------------------------------
static void SaveTotalHeat(void)
{
    RAM_Write_Data_Float(state.totalHeat, HEAT_TOTAL_FLOAT);
}

//------------------------------------------------------------------------------
//  Vraci nastavenou dobu trvani testu mereni tepla [s]
//------------------------------------------------------------------------------
static unsigned long GetHeatMeasurementTestDuration(void)
{
    unsigned long dur = RAM_Read_Data_Long(HEAT_TEST_TIME);
    unsigned long durMin = RAM_Read_Data_Long(HEAT_TEST_TIME_MIN);
    unsigned long durMax = RAM_Read_Data_Long(HEAT_TEST_TIME_MAX);

    if (dur < durMin)
        return durMin;
    else if (dur > durMax)
        return durMax;
    else
        return dur;
}

//------------------------------------------------------------------------------
//  Vraci tlak [MPa]
//------------------------------------------------------------------------------
static float GetPressure(void)
{
    return 1.6;
}

//------------------------------------------------------------------------------
// Spocita objem protecene kapaliny v m3 a vrati ho.
//
// Parametry:
// flow ... prutok v m3/s
// dTime ... doba v s
//------------------------------------------------------------------------------
static float CalculateFlowVolume(float flow, float dTime)
{
    return flow * dTime;
}

//------------------------------------------------------------------------------
// Boundary between region 2 and 3.
//
// Release on the IAPWS Industrial Formulation 1997 for the Thermodynamic
// Properties of Water and Steam 1997
// Section 4 Auxiliary Equation for the Boundary between Regions 2 and 3
// Eq 5, Page 5
//------------------------------------------------------------------------------
static float B23p_T(float T)
{
    return 348.05185628969 - (1.1671859879975 * T) + (1.0192970039326E-03 * pow(T, 2));
}

//------------------------------------------------------------------------------
// Boundary between region 2 and 3.
//
// Release on the IAPWS Industrial Formulation 1997 for the Thermodynamic
// Properties of Water and Steam 1997
// Section 4 Auxiliary Equation for the Boundary between Regions 2 and 3
// Eq 6, Page 6
//------------------------------------------------------------------------------
static float B23T_p(float p)
{
    return 572.54459862746 + pow((p - 13.91883977887) / 1.0192970039326E-03, 0.5);
}

//------------------------------------------------------------------------------
// Function for region 3.
//
// Revised Supplementary Release on Backward Equations for the Functions T(p,h),
// v(p,h) and T(p,s), v(p,s) for Region 3 of the IAPWS Industrial Formulation 1997
// for the Thermodynamic Properties of Water and Steam 2004
// Section 4 Boundary Equations psat(h) and psat(s) for the Saturation Lines of Region 3
// Se pictures Page 17, Eq 10, Table 17, Page 18
//------------------------------------------------------------------------------
static float p3sat_h(float h)
{
    float ps;
    int i;
    h = h / 2600;
    ps = 0;
    for (i = 0; i <= 13; i++)
    {
        ps += p3sat_h_ni_Array[i] * pow(h - 1.02, p3sat_h_Ii_Array[i]) * pow(h - 0.608, p3sat_h_Ji_Array[i]);
    }
    return ps * 22;
}

//------------------------------------------------------------------------------
// Function for region 3.
//
// Revised Supplementary Release on Backward Equations for the Functions T(p,h),
// v(p,h) and T(p,s), v(p,s) for Region 3 of the IAPWS Industrial Formulation 1997
// for the Thermodynamic Properties of Water and Steam 2004
// Section 3.3 Backward Equations T(p,h) and v(p,h) for Subregions 3a and 3b
// Boundary equation, Eq 1 Page 5
//------------------------------------------------------------------------------
static float T3_ph(float p, float h)
{
    int i;
    float h3ab, ps, hs, Ts;
    h3ab = 2014.64004206875 + 3.74696550136983 * p - 2.19921901054187E-02 * pow(p, 2) + 8.7513168600995E-05 * pow(p, 3);
    if (h < h3ab)
    {
        // Subregion 3a
        // Eq 2, Table 3, Page 7
        ps = p / 100;
        hs = h / 2300;
        Ts = 0;
        for (i = 0; i <= 30; i++)
        {
            Ts += T3_ph_a_ni_Array[i] * pow(ps + 0.24, T3_ph_a_Ii_Array[i]) * pow(hs - 0.615, T3_ph_a_Ji_Array[i]);
        }
        return Ts * 760;
    }
    else
    {
        // Subregion 3b
        // Eq 3, Table 4, Page 7,8
        hs = h / 2800;
        ps = p / 100;
        Ts = 0;
        for (i = 0; i <= 32; i++)
        {
            Ts += T3_ph_b_ni_Array[i] * pow(ps + 0.298, T3_ph_b_Ii_Array[i]) * pow(hs - 0.72, T3_ph_b_Ji_Array[i]);
        }
        return Ts * 860;
    }
}

//------------------------------------------------------------------------------
// Function for region 3.
//
// Revised Supplementary Release on Backward Equations for the Functions T(p,h),
// v(p,h) and T(p,s), v(p,s) for Region 3 of the IAPWS Industrial Formulation 1997
// for the Thermodynamic Properties of Water and Steam 2004
// Section 3.3 Backward Equations T(p,h) and v(p,h) for Subregions 3a and 3b
// Boundary equation, Eq 1 Page 5
//------------------------------------------------------------------------------
static float v3_ph(float p, float h)
{
    int i;
    float h3ab, ps, hs, vs;
    h3ab = 2014.64004206875 + 3.74696550136983 * p - 2.19921901054187E-02 * pow(p, 2) + 8.7513168600995E-05 * pow(p, 3);
    if (h < h3ab)
    {
        // Subregion 3a
        // Eq 4, Table 6, Page 9
        ps = p / 100;
        hs = h / 2100;
        vs = 0;
        for (i = 0; i <= 31; i++)
        {
            vs += v3_ph_a_ni_Array[i] * pow(ps + 0.128, v3_ph_a_Ii_Array[i]) * pow(hs - 0.727, v3_ph_a_Ji_Array[i]);
        }
        return vs * 0.0028;
    }
    else
    {
        // Subregion 3b
        // Eq 5, Table 7, Page 9
        ps = p / 100;
        hs = h / 2800;
        vs = 0;
        for (i = 0; i <= 29; i++)
        {
            vs += v3_ph_b_ni_Array[i] * pow(ps + 0.0661, v3_ph_b_Ii_Array[i]) * pow(hs - 0.72, v3_ph_b_Ji_Array[i]);
        }
        return vs * 0.0088;
    }
}

//------------------------------------------------------------------------------
// Function for region 4.
//
// Release on the IAPWS Industrial Formulation 1997 for the Thermodynamic
// Properties of Water and Steam, September 1997
// Section 8.1 The Saturation-Pressure Equation
// Eq 30, Page 33
//------------------------------------------------------------------------------
static float p4_T(float T)
{
    float teta, a, b, c;
    teta = T - 0.23855557567849 / (T - 650.17534844798);
    a = pow(teta, 2) + 1167.0521452767 * teta - 724213.16703206;
    b = -17.073846940092 * pow(teta, 2) + 12020.82470247 * teta - 3232555.0322333;
    c = 14.91510861353 * pow(teta, 2) - 4823.2657361591 * teta + 405113.40542057;
    return pow(2 * c / (-b + sqrt(pow(b, 2) - 4 * a * c)) , 4);
}

//------------------------------------------------------------------------------
// Function for region 4.
//
// Release on the IAPWS Industrial Formulation 1997 for the Thermodynamic
// Properties of Water and Steam, September 1997
// Section 8.2 The Saturation-Temperature Equation
// Eq 31, Page 34
//------------------------------------------------------------------------------
static float T4_p(float p)
{
    float beta, e, f, g, d;
    beta = pow(p, 0.25);
    e = pow(beta, 2) - 17.073846940092 * beta + 14.91510861353;
    f = 1167.0521452767 * pow(beta, 2) + 12020.82470247 * beta - 4823.2657361591;
    g = -724213.16703206 * pow(beta, 2) - 3232555.0322333 * beta + 405113.40542057;
    d = 2 * g / (-f - pow(pow(f, 2) - 4 * e * g, 0.5));
    return (650.17534844798 + d - pow(pow(650.17534844798 + d, 2) - 4 * (-0.23855557567849 + 650.17534844798 * d), 0.5)) / 2;
}

//------------------------------------------------------------------------------
// Function for region 4.
// Returns float or HEAT_FLOAT_INVALID.
//------------------------------------------------------------------------------
static THeatFloat h4L_p(float p)
{
    float Low_Bound, High_Bound, hs, ps, Ts;
    if ((p > 0.000611657) && (p < 22.06395))
    {
        if (p < 16.529)
        {
            Ts = T4_p(p);
            return h1_pT(p, Ts);
        }
        else
        {
            // Iterate to find the the backward solution of p3sat_h
            ps = hs = 0;
            Low_Bound = 1670.858218;
            High_Bound = 2087.23500164864;
            while (fabs(p - ps) > 0.00001)
            {
                hs = (Low_Bound + High_Bound) / 2;
                ps = p3sat_h(hs);
                if (ps > p)
                    High_Bound = hs;
                else
                    Low_Bound = hs;
            }
            return hs;
        }
    }
    else
    {
        return HEAT_FLOAT_INVALID;
    }
}

//------------------------------------------------------------------------------
// Function for region 4.
// Returns float or HEAT_FLOAT_INVALID.
//------------------------------------------------------------------------------
static THeatFloat h4V_p(float p)
{
    float Low_Bound, High_Bound, hs, ps, Ts;
    if ((p > 0.000611657) && (p < 22.06395))
    {
        if (p < 16.529)
        {
            Ts = T4_p(p);
            return h2_pT(p, Ts);
        }
        else
        {
            // Iterate to find the the backward solution of p3sat_h
            ps = hs = 0;
            Low_Bound = 2087.23500164864;
            High_Bound = 2563.592004 + 5; // 5 added to extrapolate to ensure even the border ==350�C solved.
            while (fabs(p - ps) > 0.000001)
            {
                hs = (Low_Bound + High_Bound) / 2;
                ps = p3sat_h(hs);
                if (ps < p)
                    High_Bound = hs;
                else
                    Low_Bound = hs;
            }
            return hs;
        }
    }
    else
    {
        return HEAT_FLOAT_INVALID;
    }
}

//------------------------------------------------------------------------------
// Regions as a function of pT.
//------------------------------------------------------------------------------
static unsigned char region_pT(float p, float T)
{
    float ps;
    unsigned char region;

    if ((T > 1073.15) && (p < 10) && (T < 2273.15) && (p > 0.000611))
    {
        region = 5;
    }
    else if ((T <= 1073.15) && (T > 273.15) && (p <= 100) && (p > 0.000611))
    {
        if (T > 623.15)
        {
            if (p > B23p_T(T))
            {
                region = 3;
                if (T < 647.096)
                {
                    ps = p4_T(T);
                    if (fabs(p - ps) < 0.00001)
                    {
                        region = 4;
                    }
                }
            }
            else
            {
                region = 2;
            }
        }
        else
        {
            ps = p4_T(T);
            if (fabs(p - ps) < 0.00001)
                region = 4;
            else if (p > ps)
                region = 1;
            else
                region = 2;
        }
    }
    else
    {
        region = 0; //Error, Outside valid area
    }

    return region;
}

//------------------------------------------------------------------------------
// Function for region 1
//
// Release on the IAPWS Industrial Formulation 1997 for the Thermodynamic
// Properties of Water and Steam, September 1997
// 5 Equations for Region 1, Section. 5.1 Basic Equation
// Eqution 7, Table 3, Page 6
//
// Returns float or HEAT_FLOAT_INVALID.
//------------------------------------------------------------------------------
static THeatFloat h1_pT(float p, float T)
{
    int i;
    float tau, g_t;

    if (T == 0)
        return HEAT_FLOAT_INVALID;

    p = p / 16.53;
    tau = 1386 / T;
    g_t = 0;
    for (i = 0; i <= 33; i++)
    {
        g_t += h1_pT_n1_Array[i] * pow(7.1 - p, h1_pT_I1_Array[i]) * h1_pT_J1_Array[i] * pow(tau - 1.222, (h1_pT_J1_Array[i] - 1));
    }
    return R_CONST * T * tau * g_t;
}

//------------------------------------------------------------------------------
// Function for region 2
//
// Release on the IAPWS Industrial Formulation 1997 for the Thermodynamic
// Properties of Water and Steam, September 1997
// 6 Equations for Region 2, Section. 6.1 Basic Equation
// Table 11 and 12, Page 14 and 15
//
// Returns float or HEAT_FLOAT_INVALID.
//------------------------------------------------------------------------------
static THeatFloat h2_pT(float p, float T)
{
    int i;
    float tau, g0_tau, gr_tau;

    if (T == 0)
        return HEAT_FLOAT_INVALID;

    tau = 540 / T;
    g0_tau = 0;
    for (i = 0; i <= 8; i++)
    {
        g0_tau += h2_pT_n0_Array[i] * h2_pT_J0_Array[i] * pow(tau, (h2_pT_J0_Array[i] - 1));
    }
    gr_tau = 0;
    for (i = 0; i <= 42; i++)
    {
        gr_tau += h2_pT_nr_Array[i] * pow(p, h2_pT_Ir_Array[i]) * h2_pT_Jr_Array[i] * pow(tau - 0.5, (h2_pT_Jr_Array[i] - 1));
    }
    return R_CONST * T * tau * (g0_tau + gr_tau);
}

//------------------------------------------------------------------------------
// Function for region 3
//
// Not avalible with IF 97
// Solve function T3_ph-T=0 with half interval method.
//
// viz. XSteam_Excel_v2.6.xls
//
// Returns float or HEAT_FLOAT_INVALID.
//------------------------------------------------------------------------------
static THeatFloat h3_pT(float p, float T)
{
    float Ts, Low_Bound, High_Bound, hs;
    // ver2.6 Start corrected bug
    if (p < 22.06395) // Bellow tripple point
    {
        Ts = T4_p(p);   // Saturation temperature
        if (T <= Ts)    // Liquid side
        {
            High_Bound = h4L_p(p);   // Max h �r liauid h.
            if (High_Bound == HEAT_FLOAT_INVALID)
                return HEAT_FLOAT_INVALID;
            Low_Bound = h1_pT(p, 623.15);
            if (Low_Bound == HEAT_FLOAT_INVALID)
                return HEAT_FLOAT_INVALID;
        }
        else
        {
            Low_Bound = h4V_p(p);   // Min h �r Vapour h.
            if (Low_Bound == HEAT_FLOAT_INVALID)
                return HEAT_FLOAT_INVALID;
            High_Bound = h2_pT(p, B23T_p(p));
            if (High_Bound == HEAT_FLOAT_INVALID)
                return HEAT_FLOAT_INVALID;
        }
    }
    else // Above tripple point. R3 from R2 till R3.
    {
        Low_Bound = h1_pT(p, 623.15);
        if (Low_Bound == HEAT_FLOAT_INVALID)
            return HEAT_FLOAT_INVALID;
        High_Bound = h2_pT(p, B23T_p(p));
        if (High_Bound == HEAT_FLOAT_INVALID)
            return HEAT_FLOAT_INVALID;
    }
    // ver2.6 End corrected bug
    Ts = T + 1;
    hs = 0;
    while (fabs(T - Ts) > 0.000001)
    {
        hs = (Low_Bound + High_Bound) / 2;
        Ts = T3_ph(p, hs);
        if (Ts > T)
            High_Bound = hs;
        else
            Low_Bound = hs;
    }
    return hs;
}

//------------------------------------------------------------------------------
// Function for region 5
//
// Release on the IAPWS Industrial Formulation 1997 for the Thermodynamic
// Properties of Water and Steam, September 1997
// Basic Equation for Region 5
// Eq 32,33, Page 36, Tables 37-41
//
// Returns float or HEAT_FLOAT_INVALID.
//------------------------------------------------------------------------------
static THeatFloat h5_pT(float p, float T)
{
    float tau, gamma0_tau, gammar_tau;
    int i;

    if (T == 0)
        return HEAT_FLOAT_INVALID;

    tau = 1000 / T;
    gamma0_tau = 0;
    for (i = 0; i <= 5; i++)
    {
        gamma0_tau += h5_pT_ni0_Array[i] * h5_pT_Ji0_Array[i] * pow(tau, h5_pT_Ji0_Array[i] - 1);
    }
    gammar_tau = 0;
    for (i = 0; i <= 4; i++)
    {
        gammar_tau += h5_pT_nir_Array[i] * pow(p, h5_pT_Iir_Array[i]) * h5_pT_Jir_Array[i] * pow(tau, h5_pT_Jir_Array[i] - 1);
    }
    return R_CONST * T * tau * (gamma0_tau + gammar_tau);
}

//------------------------------------------------------------------------------
// Function for region 1
//
// Release on the IAPWS Industrial Formulation 1997 for the Thermodynamic
// Properties of Water and Steam, September 1997
// 5 Equations for Region 1, Section. 5.1 Basic Equation
// Eqution 7, Table 3, Page 6
//
// Returns float or HEAT_FLOAT_INVALID.
//------------------------------------------------------------------------------
static THeatFloat v1_pT(float p, float T)
{
    int i;
    float ps, tau, g_p;

    if ((p == 0) || (T == 0))
        return HEAT_FLOAT_INVALID;

    ps = p / 16.53;
    tau = 1386 / T;
    g_p = 0;
    for (i = 0; i <= 33; i++)
    {
        g_p = g_p - v1_pT_n1_Array[i] * v1_pT_I1_Array[i] * pow(7.1 - ps, v1_pT_I1_Array[i] - 1) * pow(tau - 1.222, v1_pT_J1_Array[i]);
    }
    return R_CONST * T / p * ps * g_p / 1000;
}

//------------------------------------------------------------------------------
// Function for region 2
//
// Release on the IAPWS Industrial Formulation 1997 for the Thermodynamic
// Properties of Water and Steam, September 1997
// 6 Equations for Region 2, Section. 6.1 Basic Equation
// Table 11 and 12, Page 14 and 15
//
// Returns float or HEAT_FLOAT_INVALID.
//------------------------------------------------------------------------------
static THeatFloat v2_pT(float p, float T)
{
    int i;
    float tau, g0_pi, gr_pi;

    if ((p == 0) || (T == 0))
        return HEAT_FLOAT_INVALID;

    tau = 540 / T;
    g0_pi = 1 / p;
    gr_pi = 0;
    for (i = 0; i <= 42; i++)
    {
        gr_pi += v2_pT_nr_Array[i] * v2_pT_Ir_Array[i] * pow(p, v2_pT_Ir_Array[i] - 1) * pow(tau - 0.5, v2_pT_Jr_Array[i]);
    }
    return R_CONST * T / p * p * (g0_pi + gr_pi) / 1000;
}

//------------------------------------------------------------------------------
// Function for region 5
//
// Release on the IAPWS Industrial Formulation 1997 for the Thermodynamic
// Properties of Water and Steam, September 1997
// Basic Equation for Region 5
// Eq 32,33, Page 36, Tables 37-41
//
// Returns float or HEAT_FLOAT_INVALID.
//------------------------------------------------------------------------------
static THeatFloat v5_pT(float p, float T)
{
    float tau, gamma0_pi, gammar_pi;
    int i;

    if ((p == 0) || (T == 0))
        return HEAT_FLOAT_INVALID;

    tau = 1000 / T;
    gamma0_pi = 1 / p;
    gammar_pi = 0;
    for (i = 0; i <= 4; i++)
    {
        gammar_pi += v5_pT_nir_Array[i] * v5_pT_Iir_Array[i] * pow(p, v5_pT_Iir_Array[i] - 1) * pow(tau, v5_pT_Jir_Array[i]);
    }
    return R_CONST * T / p * p * (gamma0_pi + gammar_pi) / 1000;
}

//------------------------------------------------------------------------------
// Entalpy as a function of pressure (MPa) and temperature (Kelvin).
// Returns float or HEAT_FLOAT_INVALID.
//------------------------------------------------------------------------------
static THeatFloat h_pT(float p, float T)
{
    switch (region_pT(p, T))
    {
    case 1:
        return h1_pT(p, T);
    case 2:
        return h2_pT(p, T);
    case 3:
        return h3_pT(p, T);
    case 4:
        return HEAT_FLOAT_INVALID;
    case 5:
        return h5_pT(p, T);
    default:
        return HEAT_FLOAT_INVALID;
    }
}

//------------------------------------------------------------------------------
// Specific Volume (v) as a function of pressure (MPa) and temperature (Kelvin).
// Returns float or HEAT_FLOAT_INVALID.
//------------------------------------------------------------------------------
static THeatFloat v_pT(float p, float T)
{
    switch (region_pT(p, T))
    {
    case 1:
        return v1_pT(p, T);
    case 2:
        return v2_pT(p, T);
    case 3:
        {
            THeatFloat h3pT = h3_pT(p, T);
            if (h3pT == HEAT_FLOAT_INVALID)
                return HEAT_FLOAT_INVALID;
            else
                return v3_ph(p, h3pT);
        }
    case 4:
        return HEAT_FLOAT_INVALID;
    case 5:
        return v5_pT(p, T);
    default:
        return HEAT_FLOAT_INVALID;
    }
}

//------------------------------------------------------------------------------
// Density (rho) as a function of pressure (MPa) and temperature (Kelvin).
// Density is calculated as 1/v.
// Returns float or HEAT_FLOAT_INVALID.
//------------------------------------------------------------------------------
static THeatFloat rho_pT(float p, float T)
{
    THeatFloat vpt = v_pT(p, T);
    if ((vpt == HEAT_FLOAT_INVALID) || (vpt == 0))
        return HEAT_FLOAT_INVALID;
    else
        return 1 / vpt;
}

//------------------------------------------------------------------------------
// Spocita teplo v Joule a vrati ho.
// viz. IAPWS IF-97
//
// Parametry:
// flowVolume ... objem protecene kapaliny v m3
// Temp1 ... teplota tepleho konce [Kelvin]
// Temp2 ... teplota studeneho konce [Kelvin]
//
// Returns float or HEAT_FLOAT_INVALID.
//------------------------------------------------------------------------------
static THeatFloat CalculateHeat(float flowVolume, float Temp1, float Temp2)
{
    float p = GetPressure();

    THeatFloat h1 = h_pT(p, Temp1);
    if (h1 == HEAT_FLOAT_INVALID)
        return HEAT_FLOAT_INVALID;

    THeatFloat h2 = h_pT(p, Temp2);
    if (h2 == HEAT_FLOAT_INVALID)
        return HEAT_FLOAT_INVALID;

    float dH = h1 - h2;
    if (dH < 0)
        return HEAT_FLOAT_INVALID;

    THeatFloat density = rho_pT(p, Temp2);
    if (density == HEAT_FLOAT_INVALID)
        return HEAT_FLOAT_INVALID;

    return density * flowVolume * dH * 1000;
}

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//------------------------------------------------------------------------------
void Heat_Init(void)
{
    state.totalHeat = GetSavedTotalHeat();
    state.heatPerformance = 0;
    state.heatTestState.valid = FALSE;
    state.heatTestState.run = FALSE;
}

//------------------------------------------------------------------------------
// Mereni tepla
//
// Parametry:
// flow ... aktualni prutok v m3/h
// dTime ... doba toku v s
//------------------------------------------------------------------------------
void Heat_MeasureWithFlow(float flow, float dTime)
{
    float temp1 = ExtTemp_GetLastCorrectMeasuredValue(TCHNL_1);
    float temp2 = ExtTemp_GetLastCorrectMeasuredValue(TCHNL_2);
    float dTemp = temp1 - temp2;

    if ((temp1 == 0) || (temp2 == 0))
        dTemp = -1; //error

    if (dTemp >= 0)
        actual_error &= ~ERR_TEMPERATURE_DIFF;
    else
        actual_error |= ERR_TEMPERATURE_DIFF;

    if ((dTemp <= 0) || (dTime <= 0) || (flow <= 0))
    {
        state.heatPerformance = 0;
        return;
    }

    flow = flow / 3600;
    float flowVolume = CalculateFlowVolume(flow, dTime);

    THeatFloat immediateHeat = CalculateHeat(flowVolume, temp1, temp2);
    if (immediateHeat == HEAT_FLOAT_INVALID)
    {
        state.heatPerformance = 0;
        return;
    }

    state.totalHeat += immediateHeat;
    SaveTotalHeat();
    state.heatPerformance = immediateHeat / dTime;
}

//------------------------------------------------------------------------------
// Mereni tepla
//
// Parametry:
// dVolume ... aktualni prirustek proteceneho objemu v m3
// dTime ... doba toku v s
//------------------------------------------------------------------------------
void Heat_MeasureWithVolume(float dVolume, float dTime)
{
    float temp1 = ExtTemp_GetLastCorrectMeasuredValue(TCHNL_1);
    float temp2 = ExtTemp_GetLastCorrectMeasuredValue(TCHNL_2);
    float dTemp = temp1 - temp2;

    if ((temp1 == 0) || (temp2 == 0))
        dTemp = -1; //error

    if (dTemp >= 0)
        actual_error &= ~ERR_TEMPERATURE_DIFF;
    else
        actual_error |= ERR_TEMPERATURE_DIFF;

    if ((dTemp <= 0) || (dTime <= 0) || (dVolume <= 0))
    {
        state.heatPerformance = 0;
        return;
    }

    THeatFloat immediateHeat = CalculateHeat(dVolume, temp1, temp2);
    if (immediateHeat == HEAT_FLOAT_INVALID)
    {
        state.heatPerformance = 0;
        return;
    }

    state.totalHeat += immediateHeat;
    SaveTotalHeat();
    state.heatPerformance = immediateHeat / dTime;
}

//------------------------------------------------------------------------------
// Spusteni/zastaveni testu mereni tepla (TRUE==start, FALSE==stop)
//------------------------------------------------------------------------------
void Heat_RunTest(BOOL run)
{
    state.heatTestState.run = run;

    if (!run)
        return;

    state.heatTestState.valid = TRUE;
    state.heatTestState.lastMeasureTime = SysTick_GetSysMilliSeconds();
    state.heatTestState.actualSeconds = 0;
    state.heatTestState.totalVolume = 0;
    state.heatTestState.totalHeat = 0;
}

//------------------------------------------------------------------------------
// Uloha testu mereni tepla (volat co nejcasteji)
//------------------------------------------------------------------------------
void Heat_TestTask(void)
{
    if (!state.heatTestState.run)
        return;

    if (!SysTick_IsSysMilliSecondTimeout(state.heatTestState.lastMeasureTime, 1000))
        return;

    state.heatTestState.lastMeasureTime = SysTick_GetSysMilliSeconds();
    state.heatTestState.actualSeconds++;

    if (state.heatTestState.actualSeconds >= GetHeatMeasurementTestDuration())
        state.heatTestState.run = FALSE;

    float flow = g_measurement.actual;
    float temp1 = ExtTemp_GetLastCorrectMeasuredValue(TCHNL_1);
    float temp2 = ExtTemp_GetLastCorrectMeasuredValue(TCHNL_2);
    float dTemp = temp1 - temp2;

    if ((temp1 == 0) || (temp2 == 0))
        dTemp = -1; //error

    if ((dTemp <= 0) || (flow <= 0))
        return;

    flow = flow / 3600;
    float flowVolume = CalculateFlowVolume(flow, 1);

    THeatFloat immediateHeat = CalculateHeat(flowVolume, temp1, temp2);
    if (immediateHeat == HEAT_FLOAT_INVALID)
        return;

    state.heatTestState.totalHeat += immediateHeat;
    state.heatTestState.totalVolume += flowVolume;
}

//------------------------------------------------------------------------------
// Vrati stav testu mereni tepla
//------------------------------------------------------------------------------
const THeatTestState* Heat_GetTestState(void)
{
    return &state.heatTestState;
}

//------------------------------------------------------------------------------
//  Vrati celkove teplo v zadanych jednotkach
//------------------------------------------------------------------------------
float Heat_GetTotalHeat(THeatUnit heatUnit)
{
    return state.totalHeat / Heat_GetUnitValueInJoule(heatUnit);
}

//------------------------------------------------------------------------------
//  Vynuluje celkove teplo
//------------------------------------------------------------------------------
void Heat_DeleteTotalHeat(void)
{
    state.totalHeat = 0;
    SaveTotalHeat();
}

//------------------------------------------------------------------------------
//  Vrati aktualni tepelny vykon [W]
//------------------------------------------------------------------------------
float Heat_GetHeatPerformance(void)
{
    return state.heatPerformance;
}

//------------------------------------------------------------------------------
//  Vrati text jednotky tepla
//------------------------------------------------------------------------------
const char* Heat_GetUnitText(THeatUnit heatUnit)
{
    switch (heatUnit)
    {
    case HU_kWh:
    default:
        return "kWh";
    case HU_BTU:
        return "BTU";
    case HU_Joule:
        return "J";
    case HU_Kpm:
        return "kpm";
    case HU_Nm:
        return "Nm";
    case HU_Cal:
        return "cal";
    }
}

//------------------------------------------------------------------------------
//  Vrati teplo v Joule pro zadanou 1 jednotku
//------------------------------------------------------------------------------
float Heat_GetUnitValueInJoule(THeatUnit heatUnit)
{
    switch (heatUnit)
    {
    case HU_kWh:
    default:
        return KWH_TO_JOULE;
    case HU_BTU:
        return BTU_TO_JOULE;
    case HU_Joule:
        return 1;
    case HU_Kpm:
        return KPM_TO_JOULE;
    case HU_Nm:
        return NM_TO_JOULE;
    case HU_Cal:
        return CAL_TO_JOULE;
    }
}
