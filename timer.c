#include "timer.h"
#include "LPC23xx.h"
#include "target.h"
#include "maine.h"
#include "menu.h"
#include "display.h"
#include "optic_buttons.h"

void init_timer1()      /// casovac pro rolovani tlacitek
{
  flag_timer1 = 0;
  T1MR0         = Fcclk/4;                   // 1s pri 57.6MHz
  T1MCR         = 3;                         // Interrupt and Reset on MR0
  VICVectAddr5  = (unsigned long)timer1_overflow;// Set Interrupt Vector
  VICIntEnable  = (1  << 5);                 // Enable Timer0 Interrupt
}

void disable_timer1()
{
  T1TCR          = 0;
  T1MR0          = Fcclk/4; // 0.5 pri 57.6MHz
  T1TC           = 0;       //reset timer
  if (flag_ir_buttons)
  {
    timer1_running = 0;
    flag_timer1    = 0;
  }
}

void enable_timer1()
{
  T1TCR |= (1<<0);    //counter enable
  if (flag_ir_buttons)
    timer1_running = 1;
}

void timer1_overflow()
{
  T1IR = 1;			/* clear interrupt flag */
  if ((flag_komunikacni_modul & MODUL_GPRS) || ( flag_komunikacni_modul & MODUL_GSM)) // kvuli osetreni samomackani tlacitek
    T1MR0         = Fcclk/4;                   // 1s pri 57.6MHz
  else
    T1MR0         = Fcclk/8;                   // 0.25 pri 57.6MHz

  if (flag_ir_buttons)
  {
    disable_timer1();
  }
  else
  {
    pressed_key = 0xff;
    if (IOPIN0 & (1<<26))
    {
      flag_timer1=1;
      pressed_key = stisk();
    }
    else
      disable_timer1();
  }

  VICVectAddr = 0;		/* Acknowledge Interrupt */
}

void timer2_overflow()
{
  disable_timer2();  //vypnuti temru
  flag_timeout_timer2=1;
  T2IR |= (1<<2);    //clear interrupt flag on MC2
  VICVectAddr = 0;   //vynulovani registru preruseni
}

void init_timer2()      ///casovac pro timeout pri komunikaci se sens8
{
  PCONP |= (1<<22);        //power on
  T2MR2          = Fcclk/4;
  T2MCR         |= (1<<6) | (1<<7);            // Interrupt and Reset on MR0
  VICVectAddr26 = (unsigned long)timer2_overflow;// Set Interrupt Vector
  VICIntEnable  = (1  << 26);                 // Enable Timer0 Interrupt
}

void disable_timer2()
{
  T2TCR &= ~(1<<0);
  T2TC = 0;    //reset timer
}

void enable_timer2()
{
  T2TC = 0;    //reset timer
  T2TCR |= (1<<0);    //counter enable
}
