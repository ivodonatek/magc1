#include "optic_buttons.h"
#include "LPC23xx.h"
#include "timer.h"
#include "maine.h"
#include "port.h"

extern void delay_5us(void);
extern void delay_10ms(void);
extern void delay_1ms(void);

#define MIN_STABLE_COUNT        125 //500ms (4*125)
#define MIN_AVG_DIFF_PERCENT    50

typedef struct ButtonState
{
    int actualDiff; // aktualni diference
    int stableCount; // pocet mereni stabilni diference

} TButtonState;

static BOOL init = FALSE;
static int avgLevel = 0;
static TButtonState buttonState[BTN_COUNT];

int OpticBtn_GetAvgLevel(void)
{
    return avgLevel;
}

static float GetOpticalButtonLevel(unsigned char btn)
{
    unsigned long ulLevel = 0;

    switch (btn)
    {
        case BTN_DOWN:
        case BTN_UP:
            ulLevel = RAM_Read_Data_Long(OPT_DU_BTN_LEVEL);
            break;

        case BTN_LEFT:
        case BTN_RIGHT:
            ulLevel = RAM_Read_Data_Long(OPT_LR_BTN_LEVEL);
            break;

        case BTN_ESC:
        case BTN_ENTER:
            ulLevel = RAM_Read_Data_Long(OPT_EE_BTN_LEVEL);
            break;
    }

    return ulLevel / 100.0;
}

static int adcRead(int button)
{
  // Selects which of the AD0.7:0 pins is (are) to be sampled and converted
  AD0CR &= 0xFFFFFF00;
  switch(button)
  {
    case BTN_DOWN:
    case BTN_UP:
      AD0CR |= (1 << 24) | (1 << 0);
    break;
    case BTN_LEFT:
    case BTN_RIGHT:
      AD0CR |= (1 << 24) | (1 << 3);
    break;
    case BTN_ESC:
    case BTN_ENTER:
      AD0CR |= (1 << 24) | (1 << 2);
    break;
  }

  switch(button)
  {
    case BTN_DOWN:
    case BTN_UP:
      // Wait for the conversion to complete
      while((AD0DR0 & (1<<31)) == 0);
      AD0CR &= 0xF8FFFFFF;	/* stop ADC now */
      return (AD0DR0 >> 6) & 0x3ff;
    break;
    case BTN_LEFT:
    case BTN_RIGHT:
      // Wait for the conversion to complete
      while((AD0DR3 & (1<<31)) == 0);
      AD0CR &= 0xF8FFFFFF;	/* stop ADC now */
      return (AD0DR3 >> 6) & 0x3ff;
    break;
    case BTN_ESC:
    case BTN_ENTER:
      // Wait for the conversion to complete
      while((AD0DR2 & (1<<31)) == 0);
      AD0CR &= 0xF8FFFFFF;	/* stop ADC now */
      return (AD0DR2 >> 6) & 0x3ff;
    break;
  }

  return 1024;
}

static void InitButtonState(unsigned char button)
{
    buttonState[button].stableCount = 0;
}

static void InitButtonStates()
{
    for (unsigned char i = 0; i < BTN_COUNT; i++)
        InitButtonState(i);
}

static void UpdateButtonStates()
{
    float sumDif = 0;
    for (unsigned char i = 0; i < BTN_COUNT; i++)
        sumDif += buttonState[i].actualDiff;

    float avgDif = sumDif / BTN_COUNT;
    if (avgDif == 0)
        avgDif = 0.1;

    for (unsigned char i = 0; i < BTN_COUNT; i++)
    {
        int percent = ((buttonState[i].actualDiff - avgDif) / avgDif) * 100;

        if (percent >= MIN_AVG_DIFF_PERCENT)
        {
            if (buttonState[i].stableCount < MIN_STABLE_COUNT)
                buttonState[i].stableCount++;
        }
        else
            buttonState[i].stableCount = 0;
    }
}

void readButtons()
{
  // pokud nebezi timer, ktery osetruje opakovane zmacknuti tlacitek,
  // tak precti stisknute tlacitko
  if (!timer1_running)
  {
    pressed_key = 0xff;
    actual_error &= ~ERR_BUTTONS;

    IOSET1 |= (1<<0);
    delay_1ms();

    // read DOWN, LEFT, ESC
    buttonState[BTN_DOWN].actualDiff = adcRead(BTN_DOWN);
    buttonState[BTN_LEFT].actualDiff = adcRead(BTN_LEFT);
    buttonState[BTN_ESC].actualDiff = adcRead(BTN_ESC);

    IOCLR1 |= (1<<0);
    delay_1ms();

    // read DOWN, LEFT, ESC
    buttonState[BTN_DOWN].actualDiff = adcRead(BTN_DOWN) - buttonState[BTN_DOWN].actualDiff;
    buttonState[BTN_LEFT].actualDiff = adcRead(BTN_LEFT) - buttonState[BTN_LEFT].actualDiff;
    buttonState[BTN_ESC].actualDiff = adcRead(BTN_ESC) - buttonState[BTN_ESC].actualDiff;

    IOSET1 |= (1<<1);
    delay_1ms();

    // read UP, RIGHT, ENTER
    buttonState[BTN_UP].actualDiff = adcRead(BTN_UP);
    buttonState[BTN_RIGHT].actualDiff = adcRead(BTN_RIGHT);
    buttonState[BTN_ENTER].actualDiff = adcRead(BTN_ENTER);

    IOCLR1 |= (1<<1);
    delay_1ms();

    // read UP, RIGHT, ENTER
    buttonState[BTN_UP].actualDiff = adcRead(BTN_UP) - buttonState[BTN_UP].actualDiff;
    buttonState[BTN_RIGHT].actualDiff = adcRead(BTN_RIGHT) - buttonState[BTN_RIGHT].actualDiff;
    buttonState[BTN_ENTER].actualDiff = adcRead(BTN_ENTER) - buttonState[BTN_ENTER].actualDiff;

    if (!init)
    {
        InitButtonStates();
        init = TRUE;
    }

    UpdateButtonStates();

    down = buttonState[BTN_DOWN].stableCount;;
    up = buttonState[BTN_UP].stableCount;
    left = buttonState[BTN_LEFT].stableCount;
    right = buttonState[BTN_RIGHT].stableCount;
    esc = buttonState[BTN_ESC].stableCount;
    enter = buttonState[BTN_ENTER].stableCount;

    // Kolik zmacknutych tlacitek?
    unsigned char btnCount = 0;
    unsigned char btnIndex = 0;
    for (unsigned char i = 0; i < BTN_COUNT; i++)
    {
        if (buttonState[i].stableCount >= MIN_STABLE_COUNT)
        {
            btnCount++;
            btnIndex = i;
        }
    }

    // Zmacknute 1 tlacitko?
    if (btnCount == 1)
    {
      switch (btnIndex)
      {
        case BTN_DOWN:
          pressed_key = 2;
        break;
        case BTN_UP:
          pressed_key = 4;
        break;
        case BTN_LEFT:
          pressed_key = 3;
        break;
        case BTN_RIGHT:
          pressed_key = 1;
        break;
        case BTN_ESC:
          pressed_key = 5;
        break;
        case BTN_ENTER:
          pressed_key = 0;
        break;
      }
      flag_tlacitko = 1;
    }
//   else
//   {
//      // neni zmacknute zadne tlacitko nebo vic
//   }
  }
  else
  {
      init = FALSE;
  }
}
