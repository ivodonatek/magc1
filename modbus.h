
/* ----------------------- Modbus includes ----------------------------------*/
//#include "mb.h"
//#include "mbport.h"
#include "LPC23xx.h"
#include "menu_tables.h"

/* ----------------------- Defines ------------------------------------------*/
/*
#define REG_INPUT_START 1000
#define REG_INPUT_NREGS 4
#define REG_HOLDING_START 1000
#define REG_HOLDING_NREGS 4
*/

extern void Modbus_init(void);

extern unsigned char kontrola_hodnot(unsigned char Table_No, unsigned long data , USHORT usAddress); // kontrola min a max hodnoty
extern void CallOutputFunction(unsigned char Table_No, UCHAR *pucRegBuffer, USHORT usAddress);
extern unsigned char SDread;

// modbus vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
#define MODBUS_PASS_OFFSET 2
#define MODBUS_ONLINE_OFFSET 100
#define MODBUS_ONLINE_FLOAT_OFFSET 150
#define MODBUS_CALIB_OFFSET 200
#define MODBUS_ONLINE_SERVICE_OFFSET 500
#define MODBUS_INFO_OFFSET 1000
#define MODBUS_DISPLAY_OFFSET 1500
#define MODBUS_USER_OFFSET 2000
#define MODBUS_SERVICE_OFFSET 3000
#define MODBUS_FACTORY_OFFSET 4000
#define MODBUS_AUTHORIZE_OFFSET 5000
#define MODBUS_DATALOGGER_OFFSET 10000
#define MODBUS_DATALOGGER_SIZE 20
//#define MODBUS_STATISTICS_OFFSET 49998 // zacatek prostoru pro vycitani statistik
//#define MODBUS_STATISTICS_SIZE   130   // celkovy pocet registru pro vycteni stranky vcetne adresy stranky

#define MODBUS_CALIB_FLOW1_L (unsigned char)     4010
#define MODBUS_CALIB_FLOW2_L (unsigned char)     4012
#define MODBUS_CALIB_FLOW3_L (unsigned char)     4014
#define MODBUS_CALIB_FLOW1_M (unsigned char)     4016
#define MODBUS_CALIB_DATA1_M (unsigned char)     4018
#define MODBUS_CALIB_FLOW2_M (unsigned char)     4020
#define MODBUS_CALIB_DATA2_M (unsigned char)     4022
#define MODBUS_CALIB_FLOW3_M (unsigned char)     4024
#define MODBUS_CALIB_DATA3_M (unsigned char)     4026
//#define NUMBER_OF_CALIB (unsigned char)          9

#define MODBUS_MEAS_FLOW (unsigned char)           0
#define MODBUS_MEAS_TOTAL_DIG (unsigned char)      1
#define MODBUS_MEAS_TOTAL_DEC (unsigned char)      2
#define MODBUS_MEAS_AUX_DIG (unsigned char)        3
#define MODBUS_MEAS_AUX_DEC (unsigned char)        4
#define MODBUS_MEAS_POS_DIG (unsigned char)        5
#define MODBUS_MEAS_POS_DEC (unsigned char)        6
#define MODBUS_MEAS_NEG_DIG (unsigned char)        7
#define MODBUS_MEAS_NEG_DEC (unsigned char)        8
#define MODBUS_MEAS_TEMP (unsigned char)           9
#define MODBUS_MEAS_ERROR (unsigned char)          10
#define MODBUS_EXT_MEAS_TEMP1 (unsigned char)      11
#define MODBUS_EXT_MEAS_TEMP2 (unsigned char)      12
#define MODBUS_MEAS_FLOW_ABS (unsigned char)       13
#define MODBUS_MEAS_FLOW_SIGN (unsigned char)      14
#define MODBUS_MEAS_HEAT_PERFORM (unsigned char)   15
#define MODBUS_MEAS_HEAT_TOTAL (unsigned char)     16
#define NUMBER_OF_ONLINE (unsigned char)           17

#define MODBUS_MEAS_FLOW_FLOAT (unsigned char)          0
#define MODBUS_MEAS_TOTAL_FLOAT (unsigned char)         1
#define MODBUS_MEAS_AUX_FLOAT (unsigned char)           2
#define MODBUS_MEAS_POS_FLOAT (unsigned char)           3
#define MODBUS_MEAS_NEG_FLOAT (unsigned char)           4
#define MODBUS_MEAS_TEMP_FLOAT (unsigned char)          5
#define MODBUS_EXT_MEAS_TEMP1_FLOAT (unsigned char)     6
#define MODBUS_EXT_MEAS_TEMP2_FLOAT (unsigned char)     7
#define MODBUS_MEAS_HEAT_PERFORM_FLOAT (unsigned char)  8
#define MODBUS_MEAS_HEAT_TOTAL_FLOAT (unsigned char)    9
#define NUMBER_OF_ONLINE_FLOAT (unsigned char)          10

#define MODBUS_MEAS_A1 (unsigned char)              0
#define MODBUS_MEAS_A2 (unsigned char)              1
#define MODBUS_MEAS_A3 (unsigned char)              2
#define MODBUS_MEAS_A (unsigned char)               3
#define NUMBER_OF_ONLINE_SERVICE (unsigned char)    4

#define NUMBER_OF_PASS (unsigned char) 4

volatile static unsigned long ModbusPass[NUMBER_OF_PASS] = {};
volatile static unsigned long Modbus_Page_Adress = 0; // adresa stranky v memory modulu
volatile static unsigned char Kalibrace2Modbus = 0;
extern volatile unsigned char modbus_read_datalogger_flag;
extern volatile unsigned long modbus_datalogger_date;
extern volatile unsigned int modbus_datalogger_time;

void sendFilesName(void);
void sendFile(unsigned long data);
