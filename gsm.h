
#define GSM_GET_FLOWRATE    0x01
#define GSM_GET_TOTALPOS    0x02
#define GSM_GET_TOTALNEG    0x03
#define GSM_GET_TOTAL       0x04
#define GSM_GET_AUX         0x05
#define GSM_GET_ACTUAL_ERR  0x06
#define GSM_GET_DATALOGGER  0x07
#define GSM_SET_INTERVAL    0x08
#define GSM_SET_EVENT       0x09
#define GSM_SET_UNITNO      0x10
#define GSM_INTERVAL_DATA   0x11
#define GSM_QUALITY_SIGNAL  0x12
#define GSM_ERROR           0x13
#define GSM_SEND_EVENT      0x14
#define GSM_SET_PHONE_1     0x15
#define GSM_SET_PHONE_2     0x16
#define GSM_SET_PHONE_3     0x17
#define GSM_STOP_SEND       0x18
#define GSM_START_SEND      0x19

#define GSM_EMPTY_ON        0x0A
#define GSM_EMPTY_OFF       0x0B
#define GSM_ERROR_ON        0x0C
#define GSM_ERROR_OFF       0x0D
#define GSM_ZERO_FLOW_ON    0x0E
#define GSM_ZERO_FLOW_OFF   0x0F
#define GSM_EMPTY_ON_OFF    0x1A
#define GSM_ZERO_FLOW_ON_OFF     0x1B

//pro dalsi zadost

#define GSM_NEXT_EMPTY_ON   (1<<0)
#define GSM_NEXT_EMPTY_OFF  (1<<1)
#define GSM_NEXT_ERROR_ON   (1<<2)
#define GSM_NEXT_ZERO_ON    (1<<4)
#define GSM_NEXT_ZERO_OFF   (1<<5)

#define GSM_TIMEOUT_BUSY_PIN    900

void get_flow_rate_gsm(void);
void get_totalpos_gsm(void);
void get_totalneg_gsm(void);
void get_total_gsm(void);
void get_aux_gsm(void);
void get_actual_error_gsm(void);
extern void get_datalogger_gsm(unsigned char velikost);
void set_interval_gsm(unsigned int second);
void set_event(unsigned char event_id);
void gsm_init(void);
void set_unitno_gsm(void);
void set_service_center(void);
void get_interval_data_gsm(void);
void get_quality_signal_gsm(void);
void zpracovat_data_gsm(void);

extern volatile unsigned long flag_gsm_zije;


extern unsigned char gsm_recv_buffer[50];
extern unsigned char gsm_pozice_buff;
extern void odesli_gsm_buffer(const char *BufferPtr, unsigned char Length );
