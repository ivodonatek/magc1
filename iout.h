

#define IV_PORT FIO2PIN
#define IV_SYNC (1<<7)
#define IV_SCLK (1<<6)
#define IV_DIN  (1<<5)

#define DAC_10mA    1536

void DAC7512out(unsigned int value);
void DAC7512_init(void);
void Set_port(void);
void I_Out(void);
void V_Out(float flow);
