/******************************************************************************
*** bluetooth.c ***************************************************************
*******************************************************************************
*   Rozhrani pro komunikaci s BlueTooth modulem                               *
*                                                                             *
******************************************************************************/

#include "gprs.h"
#include "LPC23xx.h"
#include "menu_tables.h"
#include "maine.h"
#include "rprintf.h"
#include "delay.h"

unsigned long GPRS_Error = GPRS_NO_ERROR;       // chyba vznikla pri komunikaci s GPRS modulem
unsigned long GPRS_Error_State = GPRS_NO_ERROR; // chyba vracena GPRS modulem
unsigned char GPRS_INSIDE = FALSE;
unsigned GPRS_init_error = FALSE;                   // chyba pri inicializaci modulu GPRS


/*****************************************************
 *           init serial link                        *
 *****************************************************/
/** inicializace uz tam je

void usart0_init(void) {
  UBRR0H = (uint8_t)(UART_BAUD_CALC1(19200,F_OSC1)>>8);
  UBRR0L = (uint8_t)UART_BAUD_CALC1(19200,F_OSC1);

  UCSR0B &= ~(1<<RXCIE0); // vymazani RX preruseni
  UCSR0B |= (1<<RXEN0) | (1<<TXEN0); // Enable receiver and transmitter
  UCSR0C |= (3<<UCSZ0);          // set 8 bit, 1 stop bit, non parity
}

*/
/*****************************************************
 *           send char over serial link              *
 *****************************************************/

// send byte over serial link
void usart1_putc(unsigned char c)
{
  while ( !(U1LSR & (1<<5) ) ); // Wait until Transmitter Holding Register is not Empty
	U1THR = c;
  ///  while(!(UCSR0A & (1 << UDRE0)));           // wait until UDR1 ready (ceka na uvolneni odesilaciho bufferu)
  ///  UDR0 = c;                                     // send character
}


/*****************************************************
 *       send GPRS buffer over serial link           *
 *****************************************************/

// send string over serial link
void usart1_putGPRScommand(unsigned char *data)
{
  // loop until *s != NULL
  for (unsigned char i = 0; i < data[1]; i++)
  {
    usart1_putc(data[i]);
  }
  while ( !(U1LSR & (1<<6) ) ); //pockat nez se neodesle cely ramec
}


/*****************************************************
 *              check GPRS message                   *
 *****************************************************/

unsigned char usart1_CheckGPRSmessage(unsigned char *send, unsigned char *receive, unsigned char size) //kontrola zpravy od GPRS
{
  unsigned char compare = TRUE;
  for (unsigned char i = 0; i < size; i++)
    if (send[i] != receive[i]) compare = FALSE;
  return compare;
}


/*****************************************************
 *             GPRS commands                         *
 *****************************************************/

void GPRS_Send_Command(unsigned char Command)
{
  unsigned char GPRS_BUFFER_SEND[68] = {};   //vytvoreni GPRS bufferu
  unsigned char GPRS_BUFFER_RECEIVE[68] = {};   //vytvoreni GPRS bufferu pro prijem
  unsigned long data = 0; //pomocna pro cteni z FRAM
  unsigned char buffer_pozice = 0; //pomocna indexaci prijimaciho bufferu
  switch(Command) //priprava bufferu na odeslani do GPRS
  {
    case GPRS_COMMAND_INSIDE :
                            GPRS_BUFFER_SEND[0] = GPRS_HEADER;           //hlavicka
                            GPRS_BUFFER_SEND[1] = 1+3;                   //velikost
                            GPRS_BUFFER_SEND[2] = GPRS_COMMAND_INSIDE;   //command
                            GPRS_BUFFER_SEND[3] = 0;                     //dumy data
    break;
    case GPRS_COMMAND_IP :
                            GPRS_BUFFER_SEND[0] = GPRS_HEADER;           //hlavicka
                            GPRS_BUFFER_SEND[1] = 4+3;                   //velikost
                            GPRS_BUFFER_SEND[2] = GPRS_COMMAND_IP;       //command
                            GPRS_BUFFER_SEND[3] = 0;                     //dumy data
                            GPRS_BUFFER_SEND[4] = 0;                     //dumy data
                            GPRS_BUFFER_SEND[5] = 0;                     //dumy data
                            GPRS_BUFFER_SEND[6] = 0;                     //dumy data
    break;
    case GPRS_COMMAND_ERROR :
                            GPRS_BUFFER_SEND[0] = GPRS_HEADER;           //hlavicka
                            GPRS_BUFFER_SEND[1] = 1+3;                   //velikost
                            GPRS_BUFFER_SEND[2] = GPRS_COMMAND_ERROR;    //command
                            GPRS_BUFFER_SEND[3] = 0;                     //dumy data
    break;
    case GPRS_COMMAND_GATEWAY :
                            GPRS_BUFFER_SEND[0] = GPRS_HEADER;           //hlavicka
                            GPRS_BUFFER_SEND[1] = 64+3;                  //velikost
                            GPRS_BUFFER_SEND[2] = GPRS_COMMAND_GATEWAY;  //command
                            for (unsigned char i = 0; i < 64; i+=4) {    //naplneni odesilaciho bufferu
                                data = RAM_Read_Data_Long(GSM_GATEWAY0+i);
                                GPRS_BUFFER_SEND[i+3] = data;      //LSB
                                GPRS_BUFFER_SEND[i+4] = data>>8;   //
                                GPRS_BUFFER_SEND[i+5] = data>>16; //
                                GPRS_BUFFER_SEND[i+6] = data>>24;  //MSB
                            }
    break;
    case GPRS_COMMAND_USER :
                            GPRS_BUFFER_SEND[0] = GPRS_HEADER;           //hlavicka
                            GPRS_BUFFER_SEND[1] = 12+3;                  //velikost
                            GPRS_BUFFER_SEND[2] = GPRS_COMMAND_USER;     //command
                            for (unsigned char i = 0; i < 12; i+=4) {    //naplneni odesilaciho bufferu
                                data = RAM_Read_Data_Long(GSM_USER0+i);
                                GPRS_BUFFER_SEND[i+3] = data;      //LSB
                                GPRS_BUFFER_SEND[i+4] = data>>8;   //
                                GPRS_BUFFER_SEND[i+5] = data>>16; //
                                GPRS_BUFFER_SEND[i+6] = data>>24;  //MSB
                            }
    break;
    case GPRS_COMMAND_PASSWD :
                            GPRS_BUFFER_SEND[0] = GPRS_HEADER;           //hlavicka
                            GPRS_BUFFER_SEND[1] = 12+3;                  //velikost
                            GPRS_BUFFER_SEND[2] = GPRS_COMMAND_PASSWD;   //command
                            for (unsigned char i = 0; i < 12; i+=4) {    //naplneni odesilaciho bufferu
                                data = RAM_Read_Data_Long(GSM_PASSWORD0+i);
                                GPRS_BUFFER_SEND[i+3] = data;      //LSB
                                GPRS_BUFFER_SEND[i+4] = data>>8;   //
                                GPRS_BUFFER_SEND[i+5] = data>>16; //
                                GPRS_BUFFER_SEND[i+6] = data>>24;  //MSB
                            }
    break;
    case GPRS_COMMAND_PORT :
                            GPRS_BUFFER_SEND[0] = GPRS_HEADER;           //hlavicka
                            GPRS_BUFFER_SEND[1] = 4+3;                  //velikost
                            GPRS_BUFFER_SEND[2] = GPRS_COMMAND_PORT;   //command
                            data = RAM_Read_Data_Long(GSM_PORT);
                            GPRS_BUFFER_SEND[6] = data;      //LSB
                            GPRS_BUFFER_SEND[5] = data>>8;   //
                            GPRS_BUFFER_SEND[4] = data>>16; //
                            GPRS_BUFFER_SEND[3] = data>>24;  //MSB
    break;
    case GPRS_COMMAND_PIN :
                            GPRS_BUFFER_SEND[0] = GPRS_HEADER;           //hlavicka
                            GPRS_BUFFER_SEND[1] = 4+3;                  //velikost
                            GPRS_BUFFER_SEND[2] = GPRS_COMMAND_PIN;   //command
                            data = RAM_Read_Data_Long(GSM_PIN);
                            GPRS_BUFFER_SEND[6] = data;      //LSB
                            GPRS_BUFFER_SEND[5] = data>>8;   //
                            GPRS_BUFFER_SEND[4] = data>>16; //
                            GPRS_BUFFER_SEND[3] = data>>24;  //MSB
    break;
  }
  usart1_putGPRScommand(GPRS_BUFFER_SEND); //odeslani bufferu do GPRS
  unsigned long aa = 0;            //pomocna pro timeout

  while(buffer_pozice != GPRS_BUFFER_SEND[1]) //pijem gprs message
  {
    ///if((UCSR0A & (1<<RXC0)))  //cekej na prichozi byte
    if( ( U1LSR & 0x01 ) )
    {
      ///GPRS_BUFFER_RECEIVE[buffer_pozice] = UDR0;  //uloz prichozi byte do prijimaciho bufferu
      GPRS_BUFFER_RECEIVE[buffer_pozice] = U1RBR;  //uloz prichozi byte do prijimaciho bufferu
      buffer_pozice++;
      aa = 0;
    }
    else      //kontrola timeout
    {
      aa++;
      ///asm volatile("wdr");                          // watchdog timer-out reset
      if (aa == 0xfffff)           // timeout
      {
          GPRS_Error = GPRS_ERROR_TIMEOUT;
          aa = 0;
          break;
      }
    }
  }

  if (GPRS_Error != GPRS_ERROR_TIMEOUT)
  {
    switch(GPRS_BUFFER_SEND[2]) //zpracovani prijateho bufferu
    {
      case GPRS_COMMAND_INSIDE : //zpracovani dotazu na inside test
                              if ((GPRS_BUFFER_RECEIVE[0] == GPRS_HEADER) && (GPRS_BUFFER_RECEIVE[1] == 4))
                              {
                                  RAM_Write_Data_Long(GPRS_BUFFER_RECEIVE[3], MODULE_PRESSENT);
                                  GPRS_INSIDE = GPRS_BUFFER_RECEIVE[3];
                                  //GPRS_Error = GPRS_NO_ERROR;
                              }
                              else
                                  GPRS_Error = GPRS_ERROR_COMMUNICATION;
      break;
      case GPRS_COMMAND_IP : //zpracovani dotazu na IP
                              if ((GPRS_BUFFER_RECEIVE[0] == GPRS_HEADER) && (GPRS_BUFFER_RECEIVE[1] == 7))
                              {
                                  unsigned long ip_address = 0;
                                  ip_address = GPRS_BUFFER_RECEIVE[3];
                                  ip_address <<= 8;
                                  ip_address += GPRS_BUFFER_RECEIVE[4];
                                  ip_address <<= 8;
                                  ip_address += GPRS_BUFFER_RECEIVE[5];
                                  ip_address <<= 8;
                                  ip_address += GPRS_BUFFER_RECEIVE[6];
                                  RAM_Write_Data_Long(ip_address, GSM_IP);
                                  //GPRS_Error = GPRS_NO_ERROR;
                              }
                              else
                                  GPRS_Error = GPRS_ERROR_COMMUNICATION;
      break;
      case GPRS_COMMAND_ERROR : //zpracovani dotazu na cislo chyby
                              if ((GPRS_BUFFER_RECEIVE[0] == GPRS_HEADER) && (GPRS_BUFFER_RECEIVE[1] == 4))
                              {
                                  GPRS_Error_State = GPRS_BUFFER_RECEIVE[3] << 17; //errory pro GPRS zacinaji na 17.pozici
                                  //GPRS_Error = GPRS_NO_ERROR;
                              }
                              else
                                  GPRS_Error = GPRS_ERROR_COMMUNICATION;
      break;
      case GPRS_COMMAND_GATEWAY : //kontrola prijate zpravy
      case GPRS_COMMAND_USER    :
      case GPRS_COMMAND_PASSWD  :
      case GPRS_COMMAND_PORT    :
      case GPRS_COMMAND_PIN     :
                              if (!(usart1_CheckGPRSmessage(GPRS_BUFFER_RECEIVE, GPRS_BUFFER_SEND, GPRS_BUFFER_SEND[1]))) //kontrola zpravy od GPRS
                                  GPRS_Error = GPRS_ERROR_CHECK;
      break;
    }
  }
}


/*****************************************************
 *             GPRS main init                        *
 *****************************************************/

void GPRS_Init (void)
{
  ///usart0_init();                    //inicializace USART0
  ///GPRS_DDR  |= GPRS_CS;             //GPRS_CS nastaveni na vystup
  FIO2DIR |= GPRS_CS;
  FIO2DIR &= ~GPRS_ER;
  ///GPRS_PORT &= ~GPRS_CS;            //GPRS_CS = 0 command mode
  FIO2CLR |= GPRS_CS;
  GPRS_Error = GPRS_NO_ERROR;       //vynulovani chyby
  smaz_GPRS_errory();
  delay_10ms();                     //cekej 5ms
  RAM_Write_Data_Long(0, MODULE_PRESSENT); //vynulovani pressent flagu
  RAM_Write_Data_Long(0, GSM_IP);       //vynulovani IP po zapnuti

  unsigned long zaloha_U1IER = U1IER;
  U1IER=0;

  GPRS_init_error=TRUE;

  GPRS_Send_Command(GPRS_COMMAND_INSIDE);     //dotaz na pritomnost GPRS modulu

  if (GPRS_Error != GPRS_NO_ERROR)            // pokud byla chyba posli dotaz znovu
  {
    GPRS_Error = GPRS_NO_ERROR;             //vynulovani chyby
    GPRS_Send_Command(GPRS_COMMAND_INSIDE); //dotaz na pritomnost GPRS modulu
  }

  if ((GPRS_INSIDE) && (GPRS_Error == GPRS_NO_ERROR)) //je pritomen a bez chyby - odesle prihlasovaci informace
  {
    GPRS_Send_Command(GPRS_COMMAND_GATEWAY);
    if (GPRS_Error != GPRS_NO_ERROR)                // pokud byla chyba posli dotaz znovu
    {
      delay_10ms();                               //cekej 5ms
      GPRS_Error = GPRS_NO_ERROR;                 //vynulovani chyby
      GPRS_Send_Command(GPRS_COMMAND_GATEWAY);
    }

    if ((GPRS_Error == GPRS_NO_ERROR))              // bez chyby
    {
      GPRS_Send_Command(GPRS_COMMAND_USER);
      if (GPRS_Error != GPRS_NO_ERROR)            // pokud byla chyba posli dotaz znovu
      {
        delay_10ms();                           //cekej 5ms
        GPRS_Error = GPRS_NO_ERROR;             //vynulovani chyby
        GPRS_Send_Command(GPRS_COMMAND_USER);
      }
    }

    if ((GPRS_Error == GPRS_NO_ERROR))              // bez chyby
    {
      GPRS_Send_Command(GPRS_COMMAND_PASSWD);
      if (GPRS_Error != GPRS_NO_ERROR)            // pokud byla chyba posli dotaz znovu
      {
        delay_10ms();                           //cekej 5ms
        GPRS_Error = GPRS_NO_ERROR;             //vynulovani chyby
        GPRS_Send_Command(GPRS_COMMAND_PASSWD);
      }
    }

    if ((GPRS_Error == GPRS_NO_ERROR))              // bez chyby
    {
      GPRS_Send_Command(GPRS_COMMAND_PORT);
      if (GPRS_Error != GPRS_NO_ERROR)            // pokud byla chyba posli dotaz znovu
      {
        delay_10ms();                           //cekej 5ms
        GPRS_Error = GPRS_NO_ERROR;             //vynulovani chyby
        GPRS_Send_Command(GPRS_COMMAND_PORT);
      }
    }

    if ((GPRS_Error == GPRS_NO_ERROR))              // bez chyby
    {
      GPRS_Send_Command(GPRS_COMMAND_PIN);
      if (GPRS_Error != GPRS_NO_ERROR)            // pokud byla chyba posli dotaz znovu
      {
        delay_10ms();                           //cekej 5ms
        GPRS_Error = GPRS_NO_ERROR;             //vynulovani chyby
        GPRS_Send_Command(GPRS_COMMAND_PIN);
      }
    }

    if (GPRS_Error != GPRS_NO_ERROR)                // pokud se pri inicializaci vyskytla chyba
      GPRS_init_error = TRUE;                     // nastav flag error init
    else
      GPRS_init_error = FALSE;
  }
  ///GPRS_PORT |= GPRS_CS;             //GPRS_CS = 1 data mode
  FIO2SET |= GPRS_CS;
  ///GPRS_PORT |= GPRS_ER;             //pull up pro signal gprs error (PD6)
  PINMODE4 &= ~(1<<5) & ~(1<<4);  //u lpc nejde nastavit pro vstupni pin jen tak pull up tak se to musi resit pres pinmode
  ///asm volatile("wdr");                          // watchdog timer-out reset
  U1IER=zaloha_U1IER;
}

/*****************************************************
 *             GPRS get error number                 *
 *****************************************************/

unsigned long GPRS_Error_test (void)
{
  ///GPRS_PORT &= ~GPRS_CS;                  //GPRS_CS = 0 command mode
  FIO2CLR |= GPRS_CS;
  delay_10ms();                           //cekej 5ms
  GPRS_Send_Command(GPRS_COMMAND_ERROR);  //dotaz na cislo chyby
  ///GPRS_PORT |= GPRS_CS;                   //GPRS_CS = 1 data mode
  FIO2SET |= GPRS_CS;
  return GPRS_Error_State;                //vrati cislo chyby
}

void GPRS_get_IP (void)
{
  ///GPRS_PORT &= ~GPRS_CS;                  //GPRS_CS = 0 command mode
  FIO2CLR |= GPRS_CS;
  delay_10ms();                           //cekej 5ms
  GPRS_Send_Command(GPRS_COMMAND_IP);     //dotaz na IP adresu
  ///GPRS_PORT |= GPRS_CS;                   //GPRS_CS = 1 data mode
  FIO2SET |= GPRS_CS;
}

void smaz_GPRS_errory()
{
  actual_error &= ~GPRS_ERROR_COMMUNICATION & ~GPRS_ERROR_CHECK & ~GPRS_ERROR_TIMEOUT & ~GPRS_ERROR_RESET &
                  ~GPRS_ERROR_ECHO & ~GPRS_ERROR_SIM_PIN & ~GPRS_ERROR_SIGNAL & ~GPRS_ERROR_CALL & ~GPRS_ERROR_IP &
                  ~GPRS_ERROR_ONLINE;

  GPRS_Error=GPRS_NO_ERROR;
}
