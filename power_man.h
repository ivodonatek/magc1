//******************************************************************************
//  Management nabijeni/uspavani/probouzeni.
//*****************************************************************************/

#ifndef POWER_MAN_H_INCLUDED
#define POWER_MAN_H_INCLUDED

// Stav baterie
typedef enum
{
    BATS_NO_BATTERY = 0,
    BATS_LOW_BATTERY = 1,
    BATS_MEDIUM_BATTERY = 2,
    BATS_HIGH_BATTERY = 3,
    BATS_CHARGING = 4,

} TBatteryStatus;

// Fw module state
typedef struct
{
    // Power-down mode
    //BOOL powerDownMode;

    // pomoc. pocitadlo pro probuzeni
    //unsigned char wakeUpCounter;

    // casovac initu nabijecky
    unsigned long initTimer;

    // stav baterie
    TBatteryStatus batteryStatus;

    // data registru Charger Status 1, -1 if error detected
    int16_t chargerStatus1;

    // data registru Charger Status 2, -1 if error detected
    int16_t chargerStatus2;

} TPowerManState;

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat jenom jednou pred ostatnimi funkcemi rozhrani.
//------------------------------------------------------------------------------
void PowerMan_Init(void);

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void PowerMan_Task(void);

//------------------------------------------------------------------------------
//  Vypnout.
//------------------------------------------------------------------------------
//void PowerMan_SwitchOff(void);

//------------------------------------------------------------------------------
//  Vrati stav baterie.
//------------------------------------------------------------------------------
TBatteryStatus PowerMan_GetBatteryStatus(void);

//------------------------------------------------------------------------------
//  Vrati stav.
//------------------------------------------------------------------------------
const TPowerManState* PowerMan_GetState(void);

#endif // POWER_MAN_H_INCLUDED
