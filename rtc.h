
typedef struct DateTime
{
  float msecond;
  unsigned char second;   //enter the current time, date, month, and year
  unsigned char minute;
  unsigned char hour;
  unsigned char day;
  unsigned char month;
  unsigned int year;
}TDateTime;

void RTC_init(void);
void RTC_set(unsigned char hodina,unsigned char minuta, unsigned char sekunda, unsigned char den, unsigned char mesic, unsigned int rok);
void RTC_read(void);
void RTC_interrupt(void);
void RTC_set_interrupt(void);
extern void RTC_set_year(unsigned int year);
extern void RTC_set_month(unsigned char month);
extern void RTC_set_day(unsigned char day);
extern void RTC_set_hour(unsigned char hour);
extern void RTC_set_minute(unsigned char minute);
extern void RTC_set_second(unsigned char second);
unsigned long RTC_GetSysSeconds(void);
unsigned char RTC_IsSysSecondTimeout(unsigned long userSysSeconds, unsigned long delaySeconds);
void RTC_EventLogTask(void);

extern volatile TDateTime DateTime;
extern volatile unsigned char flag_RTC;
