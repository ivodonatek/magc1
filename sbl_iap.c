//-----------------------------------------------------------------------------
// Software that is described herein is for illustrative purposes only
// which provides customers with programming information regarding the
// products. This software is supplied "AS IS" without any warranties.
// NXP Semiconductors assumes no responsibility or liability for the
// use of the software, conveys no license or title under any patent,
// copyright, or mask work right to the product. NXP Semiconductors
// reserves the right to make changes in the software without
// notification. NXP Semiconductors also make no representation or
// warranty that such application will be suitable for the specified
// use without further testing or modification.
//-----------------------------------------------------------------------------
//=============================================================================

#include <LPC23XX.h>
#include <string.h>
#include "type.h"
#include "monitor.h"
#include "sbl_iap.h"
#include "sbl_config.h"

/*============================================================================*/
/*                                 DEFINES                                    */
/*============================================================================*/

#define min(x, y) (x>y? y: x )
#define IAP_ADDRESS 0x7FFFFFF1
//#define DBG xprintf
#define DBG(...)

/*============================================================================*/
/*                             GLOBAL VARIABLES                               */
/*============================================================================*/
const unsigned crp __attribute__((section(".ARM.__at_0x1FC"))) = CRP;

/*============================================================================*/
/*                              LOCAL VARIABLES                               */
/*============================================================================*/
static unsigned param_table[5];
static unsigned result_table[5];
static unsigned char pPageBuffer[FLASH_BUF_SIZE];

/*============================================================================*/
/*                        PROTOTYPES OF PUBLIC FUNCTIONS                      */
/*============================================================================*/
unsigned write_flash(unsigned * dst, char * src, unsigned size);
void execute_user_code(void);
BOOL user_code_present(void);
BOOL erase_user_flash(void);

/*============================================================================*/
/*                    PROTOTYPES OF LOCAL (PRIVATE) FUNCTIONS                  */
/*============================================================================*/
static void write_data(unsigned cclk,unsigned flash_address,unsigned * flash_data_buf, unsigned count);
//static void find_erase_prepare_sector(unsigned cclk, unsigned flash_address);
static void erase_sector(unsigned start_sector,unsigned end_sector,unsigned cclk);
static void prepare_sector(unsigned start_sector,unsigned end_sector,unsigned cclk);
static void iap_entry(unsigned param_tab[],unsigned result_tab[]);
static void enable_interrupts(unsigned interrupts);
static void disable_interrupts(unsigned interrupts);
static unsigned address_to_sector(unsigned *address);
static unsigned get_sector_size(unsigned *address);
static BOOL compare_data(unsigned flash_address,unsigned * flash_data_buf, unsigned count);
//------------------------------------------------------------------------------


unsigned address_to_sector(unsigned *address)
{
  if ((unsigned)address<0x8000)
    return (((unsigned)address)>>12);
  if ((unsigned)address<0x78000)
    return ((((unsigned)address)>>15)+7);
  if ((unsigned)address<0x7D000)
    return ((((unsigned)address)>>12)-98);
  return 26;
}

unsigned get_sector_size(unsigned *address)
{
  if ((unsigned)address<0x8000)
    return 0x1000;
  if ((unsigned)address<0x78000)
    return 0x8000;
  if ((unsigned)address<0x7D000)
    return 0x1000;
  return 0x1000;
}

//------------------------------------------------------------------------------
/// Writes a data buffer in the internal flash. This function works in polling
/// mode, and thus only returns when the data has been effectively written.
/// Returns 0 if successful; otherwise returns an error code.
/// \param address  Write address.
/// \param pBuffer  Data buffer.
/// \param size  Size of data buffer in bytes.
//------------------------------------------------------------------------------
unsigned write_flash(unsigned * dst, char * src, unsigned size)
{
  // Translate write address
  unsigned offset = ((unsigned int)dst)&(FLASH_BUF_SIZE-1);
  unsigned writeAddress = ((unsigned int)dst)&(~(FLASH_BUF_SIZE-1));
  unsigned writeSize;
  unsigned padding;

  DBG("Address: %d", writeAddress);
  DBG("offset: %d\n", offset);

  // Write all pages
  while (size > 0)
  {
      // Copy data in temporary buffer to avoid alignment problems
      writeSize = min(FLASH_BUF_SIZE - offset, size);
      DBG("writeSize: %lu\r\n", writeSize);

      //ComputeAddress(sector, 0, &sectorAddress);
      padding = FLASH_BUF_SIZE - offset - writeSize;
      DBG("padding: %lu\r\n", padding);

      // Pre-buffer data (mask with 0xFF)
      //memcpy(pPageBuffer, (void *) writeAddress, offset);
      memset(pPageBuffer, 0xff, offset);

      // Buffer data
      memcpy(pPageBuffer + offset, src, writeSize);

      // Post-buffer data
      memset(pPageBuffer + offset + writeSize, 0xff, padding);

      // Write
      prepare_sector(address_to_sector((unsigned*)writeAddress), address_to_sector((unsigned*)writeAddress), CCLK2);
      if(result_table[0] != CMD_SUCCESS)
      {
          /* Error */
          DBG("Error 1\r\n");
          return 1;
      }
      write_data(CCLK2,(unsigned)writeAddress,(unsigned*)pPageBuffer,FLASH_BUF_SIZE);
      //write_data(CCLK,(unsigned)writeAddress,(unsigned*)pPageBuffer,size);
      if(result_table[0] != CMD_SUCCESS)
      {
          /* Error */
          DBG("Error 2\r\n");
          return 2;
      }
      if( !compare_data( (unsigned)writeAddress,(unsigned*)pPageBuffer,FLASH_BUF_SIZE))
      //if( !compare_data( (unsigned)writeAddress,(unsigned*)pPageBuffer,size))
      {
          /* Error */
          DBG("Error 3\r\n");
          return 3;
      }

      // Progression
      writeAddress += FLASH_BUF_SIZE;
      src = (char *) ((unsigned int) src + writeSize);
      size -= writeSize;
      offset = 0;
  }

  return 0;
}

BOOL compare_data(unsigned flash_address,unsigned * flash_data_buf, unsigned count)
{
  param_table[0] = COMPARE;
  param_table[1] = flash_address;
  param_table[2] = (unsigned)flash_data_buf;
  param_table[3] = count;
  param_table[4] = 0;
  iap_entry(param_table,result_table);
  return result_table[0] == 0;
}

void write_data(unsigned cclk,unsigned flash_address,unsigned * flash_data_buf, unsigned count)
{
  unsigned interrupts;

  interrupts  = VICIntEnable;
	disable_interrupts(interrupts);
  param_table[0] = COPY_RAM_TO_FLASH;
  param_table[1] = flash_address;
  param_table[2] = (unsigned)flash_data_buf;
  param_table[3] = count;
  param_table[4] = cclk;
  iap_entry(param_table,result_table);
	enable_interrupts(interrupts);
}

void erase_sector(unsigned start_sector,unsigned end_sector,unsigned cclk)
{
  unsigned interrupts;

  interrupts = VICIntEnable;
  disable_interrupts(interrupts);
  param_table[0] = ERASE_SECTOR;
  param_table[1] = start_sector;
  param_table[2] = end_sector;
  param_table[3] = cclk;
  iap_entry(param_table,result_table);
  enable_interrupts(interrupts);
}

void prepare_sector(unsigned start_sector,unsigned end_sector,unsigned cclk)
{
  param_table[0] = PREPARE_SECTOR_FOR_WRITE;
  param_table[1] = start_sector;
  param_table[2] = end_sector;
  param_table[3] = cclk;
  iap_entry(param_table,result_table);
}

void iap_entry(unsigned param_tab[],unsigned result_tab[])
{
  void (*iap)(unsigned [],unsigned []);

  iap = (void (*)(unsigned [],unsigned []))IAP_ADDRESS;
  iap(param_tab,result_tab);
}

void enable_interrupts(unsigned interrupts)
{
  VICIntEnable = interrupts;
}

void disable_interrupts(unsigned interrupts)
{
  VICIntEnClr = interrupts;
}

void execute_user_code(void)
{
  disable_interrupts(VICIntEnable);
  void (*user_code_entry)(void);
  user_code_entry = (void (*)(void))USER_FLASH_START;
  user_code_entry();
}

BOOL user_code_present(void)
{
  param_table[0] = BLANK_CHECK_SECTOR;
  param_table[1] = address_to_sector( (unsigned*)USER_FLASH_START );
  param_table[2] = address_to_sector( (unsigned*)USER_FLASH_START );
  iap_entry(param_table,result_table);
	if( result_table[0] == CMD_SUCCESS )
	{
    return (FALSE);
	}
	else
	{
    return (TRUE);
	}
}

BOOL erase_user_flash(void)
{
  prepare_sector(address_to_sector( (unsigned*)USER_FLASH_START ), address_to_sector( (unsigned*)USER_FLASH_END ), CCLK2);
  erase_sector(address_to_sector( (unsigned*)USER_FLASH_START ), address_to_sector( (unsigned*)USER_FLASH_END ), CCLK2);
	return result_table[0] == CMD_SUCCESS;
}
