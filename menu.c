#include "maine.h"
#include "display.h"
#include "menu.h"
#include "images.h"
#include "menu_tables.h"
#include "rtc.h"
#include "font.h"
#include "string.h"
#include "VCOM.h"
#include "rprintf.h"
#include "iout.h"
#include "type.h"
#include "sensor.h"
#include "modbus_bin.h"
#include "delay.h"
#include "LPC23xx.h"
#include "gsm.h"
#include "conversion.h"
#include "heat.h"
#include "pulse_outputs.h"
#include "led.h"
#include "extmeas/temperature/exttemp.h"
#include "mbus/mbus_slave.h"

// Zmena jazyka za chodu
#ifndef ACT_LANGUAGE
#define LANGUAGE_OFFSET 0 // Pokud neexistuje okno na zmenu jazyka
#else
// Pokud existuje okno na zmenu jazyka, values v tomto okne se musi jmenovat ACT_LANGUAGE
#define LANGUAGE_OFFSET ( RAM_Read_Data_Long(ACT_LANGUAGE)*LANGUAGE_PAGE_SIZE ) // Urcuje, kde se naleza momentalne pouzivany jazyk
#endif

volatile TMeasurement g_measurement;

unsigned long Exponent ( unsigned long e, unsigned long n )
{
  unsigned long  i = e;
  unsigned char b = 0;
  if (n>0)
  {
    for (b=2; b<=n; b++)
    {
      i *= e;
    }
    return ( i );
  }
  else
    return ( 1 );
}

unsigned long PointStrToLong ( char * str )
{
  // Tato funkce prevede retezec tvaru long point na na cislo long
  int i;
  unsigned long out= 0;
  unsigned long pom =1;
  for ( i=strlen( str ) - 1; i >= 0; i-- )
  {
    if ( str[i] != '.' )
    {
      out += ( str[i] - '0' ) * pom;
      pom *= 10;
    }
  }
  return ( out );
}

char * LongToPointStr ( unsigned long number, unsigned char digits,unsigned char decimal, char *str )
{
  // Tato funkce sestavi z Long cisla a informace o pozici desetinne tecky cislo v textove podobe
  //   unsigned long number  - vstupni cislo
  //   unsigned char digits  - pocet celich mist
  //   unsigned char decimal - pocet desetinnych mist
  unsigned long i, pom;
  const unsigned char numbers[CONST_LENGTH_STR_NUMBERS]= CONST_STR_NUMBERS;
  if ( ((digits+decimal) > 9) || ((digits+decimal) < 1) )
  {
    strcpy( str, "print Error");
    return ( str );
  }
  strcpy(str,"");
  i = 1000000000;
  while (i != Exponent(10,digits+decimal-1) ) // Vynecha nuly na zacatku (pouze ty co tam nepatri)
  {
    pom = number / i;
    number -= pom * i;
    i = ( i>1 ) ? i/10 : 0;
  }

  do
  {
    pom = number / i;
    strcat(str," ");
    str[strlen(str)-1]=numbers[pom];
    if ( (i == Exponent(10,decimal)) && ( decimal != 0) )
    {
      strcat(str," ");
      str[strlen(str)-1]='.';
    }
    number -= pom * i;
    i = ( i>1 ) ? i/10 : 0;
  } while (i!=0);

  return ( str );
}

void ShowTextMessage(void)
{
  // Funkce zobrazuje na displej ruzne typy hlasek. vstup napr :
  //  sprintf(str, "Info~~Minimalni pripustna~hodnota je~%s~" , g_value[0].minchar);
  //  RunMessage(str);
  //  PAk staci zavolat po jake koli zmene ShowTextMessage(), neboli control(SHOW)
  //  IN  : g_message.*
  //  OUT :
  unsigned char i, y;
  char g_s[80];
  WriteErase();
  strcpy( (char*)g_s, "");
  y = 0;

  for (i=0; i<strlen((char*)g_message.str); i++)
  {
    if ( g_message.str[i]!='~' )
    {
      strcat( g_s, "_" );
      g_s[strlen(g_s)-1] = g_message.str[i];
    }
    else
    {
      if ( y<LAST_ROW  )
        WriteCenter( y, g_s ); // Na posledni radku se nepise ( ta je zde pro tlacitka)
      strcpy( g_s, "");
      y++;
    }
  }

  if (g_message.str[strlen((char*)g_message.str)-1]!='~')
  {
    // Osetreni, zda je nakonci vykreslovane hlasky ~
    Write( 2, 0, "!Retezec nekonci tildou");
  }

  WriteLastRow( BUTTON_OK );
  InvertLine( 0, 0, NUMBER_OF_COLUMNS ); // inverze vrchni radky

  // kdyz je zamnkuta klavesnice vypise se znak zamku na poslednim radku
  if (key_lock)
  {
    char srt[2];
    srt[0] = 21;
    srt[1] = 0;
    Write(20,7,srt);
  }
  if(flag_cisteni)
  {
    char srt[2];
    srt[0] = 27;
    srt[1] = 0;
    Write(30,7,srt);
  }
  WriteNow();
}

void ShowOkMessage(void)
{
  // Funkce zobrazuje informativni hlasku s obarzkem OK.
  // - WriteErase(); - Specialitka :D nemazeme display
  WriteImage(34, 2, IMG_OK_RAMECEK);
  WriteNow();
}

void ShowErrorMessage(void)
{
  // Funkce zobrazuje prednastavenou hlasku informujici o chybe vznikle tzv. za behu
  // - WriteErase(); - Specialitka :D nemazeme display
  WriteImage(34, 2, IMG_ERR_RAMECEK);
  Write     (62, 3, (char*)g_message.str);

  // kdyz je zamnkuta klavesnice vypise se znak zamku na poslednim radku
  if (key_lock)
  {
    char srt[2];
    srt[0] = 21;
    srt[1] = 0;
    Write(20,7,srt);
  }

  if(flag_cisteni)
  {
    char srt[2];
    srt[0] = 27;
    srt[1] = 0;
    Write(30,7,srt);
  }
  WriteNow();
}

void ShowConfirmMessage(void)
{
  // Funkce zobrazuje zpravu kvuli potvrzeni nebo zamitnuti uzivatelem.
  WriteErase();
  WriteCenter(3, (char*)g_message.str);
  WriteLastRow(BUTTON_OK | BUTTON_ESC);
  WriteNow();
}

void ShowStrMessage(char *str)
{
  // Funkce pouze tupe naplni hodnoty potrebne pro zobrazeni hlasky
  // a zavola jeji zobrazeni
  // Tato funkce nepracuje nezajistuje vicejazycnou podporu. Vhodnejisi je InitAndShowTextMessage.
  if (g_mode != MESSAGE)
    g_message.prevmode = g_mode;
  g_mode = MESSAGE; // Message
  g_message.typ = MSG_TEXT;
  g_message.callback = 0;
  strcpy((char*)g_message.str, str);
  control(SHOW);
}

void InitAndShowTextMessage(const unsigned int value)
{
  // Funkce zobrazuje chybovou hlasku na displeji. Aby hlaska zmizela, musite ji odmacknout.
  //  Pokud je jiz nejaka hlaska na displeji zobrazena, prepise se posledni zobrazenou hlaskou. Drive zobrzena hlaska
  //  nenavratne zmizi, aniz by byla odmacknuta. Pokud se jednalo o zavaznou chybu, pristroj si musi sam zajistit nove
  //  volani hlasky po urcitem case - pokud zdroj chyby pretrvava.
  // Pozor! Vsechny texty musi koncit znakem ~.
  // Znak ~ slouzi jako oddelovac radku (text pokracuje na dalsi radce)

  if (g_mode != MESSAGE)
    g_message.prevmode = g_mode;
  g_mode = MESSAGE; // Message
  g_message.typ = MSG_TEXT;
  g_message.callback = 0;
  ReadStringFromFlash( value+LANGUAGE_OFFSET, (char*)g_message.str );
  control(SHOW);
}

void InitAndShowErrorMessage(unsigned char value)
{
  // Funkce inicializuje a zobrazi hlasku chyby. Pouziva se predevsim u chyb vzniklich za chodu pristroje
  // hodnota value predstavuje cislo vznikle chyby.
  // Zvlastnosti teto funkce je, ze je mozne zakazat jeji provedeni. Toto je pouzite v pripade, ze by se funkce
  // volala treba kazdou vterinu. Nadrazeny software proto zobrazeni chyby na nejakou dobu zakaze. (zakaze vsechny
  // chyby tohoto typu-za behu)
  if (!g_message.error_allow)
  {
    // Kontrola povoleni zobrazeni chyboveho hlaseni (0 = povoleno)
    if (g_mode != MESSAGE)
      g_message.prevmode = g_mode;
    RAM_Write_Data_Long(value, ACTUAL_ERROR);
    g_mode = MESSAGE; // Message
    g_message.typ = MSG_ERROR;
    g_message.callback = 0;
    sprintf((char*)g_message.str, "%03d", value);
    control(SHOW);
  }
}

void InitAndShowConfirmMessage(const char *str, TSimpleHandler *pYesHandler)
{
  // Funkce zobrazuje potvrzovaci hlasku na displeji.
  // Uzivatel pak musi hlasku potvrdit nebo zamitnout.

  if (g_mode != MESSAGE)
    g_message.prevmode = g_mode;
  g_mode = MESSAGE; // Message
  g_message.typ = MSG_CONF;
  g_message.callback = pYesHandler;
  strcpy((char*)g_message.str, str);
  control(SHOW);
}

static void WriteBoolValuesConfirmed(void)
{
    WriteValues();    // Zapise hodnoty do flash
    // Pruchod zpet
    if (g_adr>0)
    {
        // NEnasleduje rezim mereni
        g_adr    = (P_menu[(g_adr+TO_BACK)]);// Vrati Back
        g_count  = (P_menu[g_adr]);
        menu_levels[g_level] = 0;
        g_level--;
        g_scroll = menu_levels[g_level];
        if ( menu_levels[g_level] > LAST_ROW )
            g_scroll = LAST_ROW;
        g_mode = (g_count!=0) ? MENU : EDIT; // Menu nebo Edit
    }
    else
    {
        g_mode = MEASUREMENT;   // Mereni
    }
    /* Provedeni akce */
    EditDoFunction(F_OUT, g_fun);
    control(INIT);
    control(SHOW);
}

unsigned char ActValue ( void )
{
  // Funkce zjistuje, ktera values je momentalne aktivni ( kterou editujeme ).
  //  IN  : g_value[i].dig, g_value[i].dec, g_scroll
  //  OUT : cislo value. Prvni je 0
  unsigned char position;
  int i = 0;
  position = g_scroll + 1;
  while ( position > (g_value[i].dig + g_value[i].dec + (( g_value[i].dec != 0 ) ?  1 : 0)) )
  {
    // S des. teckou a bez ni
    position -= ( g_value[i].dig + g_value[i].dec + (( g_value[i].dec != 0 ) ?  1 : 0) );
    i++;
  }
  position--;
  return (i);
}

void ReadValues ( void )
{
  // Funkce naplni presilusne values z Flash
  //  IN  : g_value[i].ptr, g_value[i].typ
  //  OUT : g_value[i].val
  int i = 0;
  for (i=0;i<=3;i++)
  {
    if ( g_value[i].ptr > -1 )
    {
      // cte jen ty, ktere maji adresu vetsi -1
      switch ( g_value[i].typ )
      {
        case 0/*Float  */:
          //g_value[i].val = FloatToLong(Read___FromFlash(g_value[i].ptr*sizeof(values[0]+BASE_POINTER_0), g_value[i].dec );
          break;
        case 1/*LongInt*/:
          g_value[i].val = RAM_Read_Data_Long(g_value[i].ptr * 4 + VALUES_OFFSET);
          break;
      }  // switch
    }  // if
  }  // for
}  // end


void WriteValues ( void )
{
  // Funkce zapisuje hodnoty g_value do flash
  //  IN  : g_value[i].val, g_value[i].typ
  //  OUT : none
  int i;
  for (i=0;i<=3;i++)
  {
    if (g_value[i].ptr > -1)
    {
      WriteEventLogParameterChange(g_value[i].ptr*4+VALUES_OFFSET, g_value[i].val);

      switch ( g_value[i].typ )
      {
        case 0 /* Float   */ : // dodelat, bude-li potreba
          break;
        case 1 /* LongInt */ :
          RAM_Write_Data_Long( g_value[i].val , g_value[i].ptr*4+VALUES_OFFSET );
          break;
      }
    }
  }
}

void Save_Settings (void)
{
  /*unsigned long i;
  for (i=1; i<(sizeof_P_values/sizeof(P_values[0])); i++)
  {*/
    //Vytvorit kopii tabulky values, primo za soucasnou tabulku values
    /* Kopie vramci zalohovane ram */
    //RAM_Write_Data_Long( RAM_Read_Data_Long(VALUES_OFFSET + i*4), VALUES_OFFSET + sizeof_P_values + i*4 );
    flag_zapsat_flash=1; // zapise se i do flash pameti
    //RAM_Write_Data_Long( RAM_Read_Data_Long(VALUES_OFFSET + i*4), UNIT_NO );
  //}
}


void EditDoFunction( unsigned char action, unsigned char fun[])
{
  // Provadi kod pro funkce (ocislovane od 1-n). Tento kod je pro kazdou funkci specificky
  // Pr:
  //    case 0 :
  //      if (g_fun[F_OUT]) {
  //        RAM_Write_Data_Long( 0, Erro )
  //      }
  // F_IN  - Funkce se provede jeste pred vykreslenim okna (ale okno je jiz nainicializovane)
  // F_OUT - Funkce se provede pri ukonceni okna - OnClose :D

  // F_IN
  if ( action == F_IN)
    switch (fun[F_IN])
    {
      case 0        :
        // Zde nic nedavat!
        break;
      case 1        :
      {
        // nastaveni datumu
        g_value[0].valchar[1] = (DateTime.day % 10) + 0x30;       // jednotky dnu
        g_value[0].valchar[0] = (DateTime.day / 10) + 0x30;       // desitky dnu
        g_value[0].valchar[3] = (DateTime.month % 10) + 0x30;       // jednotky mesicu
        g_value[0].valchar[2] = (DateTime.month / 10) + 0x30;  // desitky mesicu
        g_value[0].valchar[4] = (DateTime.year / 1000) + 0x30;  // tisice roku
        g_value[0].valchar[5] = (DateTime.year - (g_value[0].valchar[4] - 0x30) * 1000) / 100 + 0x30;  // stovky roku
        g_value[0].valchar[6] = (DateTime.year - (g_value[0].valchar[4] - 0x30) * 1000 - (g_value[0].valchar[5] - 0x30) * 100) / 10 + 0x30;  // desitky roku
        g_value[0].valchar[7] = (DateTime.year % 10) + 0x30;       // jednotky roku
      }
      break;
      case 2        :
      {
        // nastaveni casu
        g_value[0].valchar[1] = (DateTime.hour % 10) + 0x30;       // jednotky hodin
        g_value[0].valchar[0] = (DateTime.hour / 10) + 0x30;  // desitky hodin
        g_value[0].valchar[3] = (DateTime.minute % 10) + 0x30;       // jednotky minut
        g_value[0].valchar[2] = (DateTime.minute / 10) + 0x30;  // desitky minut
      }
      break;
      case 3        :
        // datalogger delete? (memorymodul)
        g_value[0].ptr = -1;
        break;
      case 4        :
        // delete auxiliary volume?
        g_value[0].ptr = -1;
        break;
      case 5        :
        // Load default settings?
        g_value[0].ptr = -1;
        break;
      case 6        :
        // Smazat Erro?
        g_value[0].ptr = -1;
        break;
      case 7        :
        // Smazat OK?
        g_value[0].ptr = -1;
        break;
      case 8        :
        // Save settings?
        g_value[0].ptr = -1;
        break;
      case 9        :
        // delete negative volume?
        g_value[0].ptr = -1;
        break;
      case 10       :
        // delete positive volume?
        g_value[0].ptr = -1;
        break;
      case 11       :
        // delete total volume?
        g_value[0].ptr = -1;
        break;
      case 12       :
        // prednastaveni IP pro zobrazeni?
        {
          unsigned long i = RAM_Read_Data_Long(GSM_IP);
          g_value[0].ptr = -1;
          sprintf((char*)g_value[0].valchar, "%d.%d.%d.%d", (unsigned char) (i>>24),(unsigned char) (i>>16),(unsigned char) (i>>8),(unsigned char) (i)); // prednastaveni
        }
        break;
      case 13       :
        // Actual Error
        g_value[0].ptr = -1;
        sprintf((char*)g_value[0].valchar, "%08x (hex)", actual_error); // prednastaveni
        break;

      case 14:        //gsm signal
        get_quality_signal_gsm();
        delay_100ms();
        delay_100ms();
        delay_100ms();
        zpracovat_data_gsm();
        sprintf((char*)g_value[0].valchar, "%03d ",RAM_Read_Data_Long(GSM_SIGNAL) ); // prednastaveni
        flag_gsm_dokoncil_prijem=0;
        break;

      case 23       :
        /* Vypocet hesla authorize (UNIT_NO^3333) & 0x1FFF = nyni 1419 */
        break;

      case 38       :
        // Zero FLow?
        g_value[0].ptr = -1;
        break;

      case 46       :
        // Zero FLow Erase?
        g_value[0].ptr = -1;
        break;

      case 68       :
        // Wi-Fi AP mode
        g_value[0].ptr = -1;
        break;

      case 76       :
        // delete total heat?
        g_value[0].ptr = -1;
        break;

      default       :
        ShowStrMessage("Neznama operace~");
        break;
    }
    // F_OUT
  else if (action == F_OUT)
    switch (fun[F_OUT])
    {
      case 0        :
        // Zde nic nedavat!
        break;
      case 1        :
        /* datalogger delete? (memorymodul) */
        break;
      case 2        :
        /* delete auxiliary volume? */
        if (g_value[0].val==0)
          {}
        else/* (g_value[0].val==1)*/
        {
          if( flag_sensor8 )
          {
            delete_aux_sensor8();
          }
          else
          {
            RAM_Write_Data_Long(0, VOLUME_AUX_FLOAT);
            RAM_Write_Data_Long(0, VOLUME_AUX_DIG);
            RAM_Write_Data_Long(0, VOLUME_AUX_DEC);
            g_measurement.aux = 0;       // aktualizace pocitadla
          }
          RAM_Write_Data_Long(0, DELETE_AUXILIARY_VOLUME);
        }
        g_value[0].val=0;
        break;
      case 3        :
        /* Load default settings? */
        if (g_value[0].val==0)
          {}
        if (g_value[0].val==1)
        {
          /* Tady je seznam hodnot, ktere budou obnoveny */
          /*
          RestoreConstantFromDefault(AIR_DETECTOR);
          RestoreConstantFromDefault(AIR_CONSTANT);
          RestoreConstantFromDefault(START_DELAY);
          RestoreConstantFromDefault(AVERAGE_SAMPLES);
          RestoreConstantFromDefault(LOW_FLOW_CUTOFF);
          RestoreConstantFromDefault(FLOW_RANGE);
          RestoreConstantFromDefault(INVERT_FLOW);
          RestoreConstantFromDefault(CURRENT_SET);
          RestoreConstantFromDefault(I_FLOW_MAX);
          RestoreConstantFromDefault(I_FLOW_MIN);
          RestoreConstantFromDefault(CURRENT_MAX);
          RestoreConstantFromDefault(CURRENT_MIN);
          RestoreConstantFromDefault(I_CAL_POINT_1);
          RestoreConstantFromDefault(I_CAL_POINT_2);
          RestoreConstantFromDefault(I_CAL_CONST_1);
          RestoreConstantFromDefault(I_CAL_CONST_2);
          RestoreConstantFromDefault(VOLTAGE_SET);
          RestoreConstantFromDefault(V_FLOW_MAX);
          RestoreConstantFromDefault(V_FLOW_MIN);
          RestoreConstantFromDefault(VOLTAGE_MAX);
          RestoreConstantFromDefault(VOLTAGE_MIN);
          RestoreConstantFromDefault(RE1_SET);
          RestoreConstantFromDefault(RE2_SET);
          RestoreConstantFromDefault(RE_FLOW_1);
          RestoreConstantFromDefault(RE_FLOW_2);
          RestoreConstantFromDefault(RE_HYSTERESIS_1);
          RestoreConstantFromDefault(RE_HYSTERESIS_2);
          RestoreConstantFromDefault(RE3_SET);
          RestoreConstantFromDefault(RE4_SET);
          RestoreConstantFromDefault(RE_VOLUME_PLUS);
          RestoreConstantFromDefault(RE_VOLUME_MINUS);
          RestoreConstantFromDefault(RE_DOSE);
          RestoreConstantFromDefault(F_SET);
          RestoreConstantFromDefault(F_FLOW1);
          RestoreConstantFromDefault(F_FLOW2);
          RestoreConstantFromDefault(F_FREQ1);
          RestoreConstantFromDefault(F_FREQ2);
          RestoreConstantFromDefault(F_FLOW1_MIN);
          RestoreConstantFromDefault(F_FLOW1_MAX);
          RestoreConstantFromDefault(F_FREQ1_MIN);
          RestoreConstantFromDefault(F_FREQ1_MAX);
          RestoreConstantFromDefault(F_DUTY_CYCLE);
          RestoreConstantFromDefault(PASSWORD_USER);
          RestoreConstantFromDefault(MODBUS_SLAVE_ADDRESS);
          RestoreConstantFromDefault(MODBUS_BAUDRATE);
          RestoreConstantFromDefault(MODBUS_PARITY);
          RestoreConstantFromDefault(PASSWORD_SERVICE);
          RestoreConstantFromDefault(DEMO);
          RestoreConstantFromDefault(PASSWORD_FACTORY);
          RestoreConstantFromDefault(DIAMETER);
          RestoreConstantFromDefault(UNIT_NO);
          RestoreConstantFromDefault(ZERO_FLOW_CONSTANT);
          RestoreConstantFromDefault(EXCITATION_FREQUENCY);
          RestoreConstantFromDefault(EXCITATION);*/

          g_value[0].val=0;
          RAM_Write_Data_Long(0, LOAD_DEFAULT_SETTING);

          unsigned long zaloha_unit_no = RAM_Read_Data_Long(UNIT_NO); // zaloha unit no
          unsigned long zaloha_excitation_frequency = RAM_Read_Data_Long(EXCITATION_FREQUENCY); // zaloha unit no
          unsigned long zaloha_diameter = RAM_Read_Data_Long(DIAMETER); // zaloha unit no
          unsigned long zaloha_flow_range = RAM_Read_Data_Long(FLOW_RANGE); // zaloha unit no


          for (unsigned int i=1; i<(sizeof_P_values/sizeof(P_values[0])); i++)
          {
            //Vytvorit kopii tabulky values, primo za soucasnou tabulku values
            // Z flash do fram
            // nulta pozice je first run check point
            {
              RAM_Write_Data_Long( P_values[i], VALUES_OFFSET + i*4 ); // nahrani defaultnich hodnot z flash do ram
              // Kopie vramci eeprom
              //RAM_Write_Data_Long( P_values[i], VALUES_OFFSET + sizeof_P_values + i*4 ); // nahrani defaultnich hodnot z flash do zalohovaci ram
            }
          }

          RAM_Write_Data_Long(zaloha_unit_no, UNIT_NO); //obnova unit no
          RAM_Write_Data_Long(zaloha_diameter, DIAMETER); //obnova diameter
          RAM_Write_Data_Long(zaloha_excitation_frequency, EXCITATION_FREQUENCY); //obnova zexcitation frequency
          RAM_Write_Data_Long(zaloha_flow_range, FLOW_RANGE); //obnova zexcitation frequency

          flag_zapsat_flash=1; // zapise se i do flash pameti
          flag_reset_after_load_default = 1;
        }
        break;
      case 4        :
      /* Nastaveni datumu v RTC */
      {
        RTC_set_year((g_value[0].valchar[4]-'0')*1000 + (g_value[0].valchar[5]-'0')*100 + (g_value[0].valchar[6]-'0')*10 + (g_value[0].valchar[7]-'0'));  // ulozeni roku do RTC
        RTC_set_month((g_value[0].valchar[2]-'0')*10 + g_value[0].valchar[3]-'0'); // ulozeni mesice do RTC
        RTC_set_day((g_value[0].valchar[0]-'0')*10 + g_value[0].valchar[1]-'0');   // ulozeni dne do RTC
      }
      break;
      /* Nastaveni casu v RTC */
      case 5        :
      {
        RTC_set_minute((g_value[0].valchar[2]-'0')*10 + g_value[0].valchar[3]-'0');  // ulozeni minut do RTC
        RTC_set_hour((g_value[0].valchar[0]-'0')*10 + g_value[0].valchar[1]-'0'); // ulozeni hodin do RTC
        RTC_set_second(0);  // ulozeni sekund do RTC
      }
      break;
      /* Smazat Erro? */
      case 6        :
        if (g_value[0].val==1)
        {
          RAM_Write_Data_Long(0, ERROR_MIN);
          error_min = 0;
        }
        RAM_Write_Data_Long(0, DELETE_ERROR_MIN);
        g_value[0].val=0;
        break;
      case 7        :
      /* Smazat OK? */
        if (g_value[0].val==1)
        {
          RAM_Write_Data_Long(0, OK_MIN);
          ok_min = 0;
        }
        RAM_Write_Data_Long(0, DELETE_OK_MIN);
        g_value[0].val=0;
        break;
      case 8        :
      /* Save settings? */
        if (g_value[0].val==1)
        {
          Save_Settings();
        }
        RAM_Write_Data_Long(0, SAVE_SETTINGS);
        g_value[0].val=0;
        break;
      case 9        :
      /* delete negative volume?   */
        if (g_value[0].val==1)
        {
          if ( flag_sensor8)
            delete_negative_sensor8();
          else
          {
            RAM_Write_Data_Long(0, VOLUME_NEG_FLOAT);
            RAM_Write_Data_Long(0, VOLUME_NEG_DIG);
            RAM_Write_Data_Long(0, VOLUME_NEG_DEC);
            g_measurement.flow_neg = 0;       // aktualizace pocitadla
          }
        }
        RAM_Write_Data_Long(0, DELETE_NEGATIVE_VOLUME);
        g_value[0].val=0;
        break;
      case 10        :
      /* delete positive volume?   */
        if (g_value[0].val==1)
        {
          if( flag_sensor8 )
            delete_positive_sensor8();
          else
          {
            RAM_Write_Data_Long(0, VOLUME_POS_FLOAT);
            RAM_Write_Data_Long(0, VOLUME_POS_DIG);
            RAM_Write_Data_Long(0, VOLUME_POS_DEC);
            g_measurement.flow_pos = 0;       // aktualizace pocitadla
          }
        }
        RAM_Write_Data_Long(0, DELETE_POSITIVE_VOLUME);
        g_value[0].val=0;
        break;
      case 11        :
      /* delete total volume? */
        if (g_value[0].val==1)
        {
          if( flag_sensor8)
            delete_volume_sensor8();
          else
          {
            RAM_Write_Data_Long(0, VOLUME_TOTAL_FLOAT);
            RAM_Write_Data_Long(0, VOLUME_TOTAL_DIG);
            RAM_Write_Data_Long(0, VOLUME_TOTAL_DEC);
            g_measurement.total = 0;       // aktualizace pocitadla
          }
        }
        RAM_Write_Data_Long(0, DELETE_TOTAL_VOLUME);
        g_value[0].val=0;
        break;
      case 12        :
      /* password changed */
      {
      }
      break;
      case 13        :
      /* diameter changed */
      {
        switch (RAM_Read_Data_Long(DIAMETER))
        {
          case 0   : // zde nic nedavat
            RAM_Write_Data_Long(DN0QN, F_FLOW2);
            RAM_Write_Data_Long(DN0QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN0QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN0QN, FLOW_RANGE);
            break;
          case 10  : // zde nic nedavat
            RAM_Write_Data_Long(DN10QN, F_FLOW2);
            RAM_Write_Data_Long(DN10QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN10QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN10QN, FLOW_RANGE);
            break;
          case 15  : // zde nic nedavat
            RAM_Write_Data_Long(DN15QN, F_FLOW2);
            RAM_Write_Data_Long(DN15QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN15QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN15QN, FLOW_RANGE);
            break;
          case 20  : // zde nic nedavat
            RAM_Write_Data_Long(DN20QN, F_FLOW2);
            RAM_Write_Data_Long(DN20QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN20QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN20QN, FLOW_RANGE);
            break;
          case 25  : // zde nic nedavat
            RAM_Write_Data_Long(DN25QN, F_FLOW2);
            RAM_Write_Data_Long(DN25QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN25QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN25QN, FLOW_RANGE);
            break;
          case 32  : // zde nic nedavat
            RAM_Write_Data_Long(DN32QN, F_FLOW2);
            RAM_Write_Data_Long(DN32QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN32QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN32QN, FLOW_RANGE);
            break;
          case 40  : // zde nic nedavat
            RAM_Write_Data_Long(DN40QN, F_FLOW2);
            RAM_Write_Data_Long(DN40QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN40QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN40QN, FLOW_RANGE);
            break;
          case 50  : // zde nic nedavat
            RAM_Write_Data_Long(DN50QN, F_FLOW2);
            RAM_Write_Data_Long(DN50QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN50QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN50QN, FLOW_RANGE);
            break;
          case 65  : // zde nic nedavat
            RAM_Write_Data_Long(DN65QN, F_FLOW2);
            RAM_Write_Data_Long(DN65QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN65QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN65QN, FLOW_RANGE);
            break;
          case 80  : // zde nic nedavat
            RAM_Write_Data_Long(DN80QN, F_FLOW2);
            RAM_Write_Data_Long(DN80QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN80QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN80QN, FLOW_RANGE);
            break;
          case 100 : // zde nic nedavat
            RAM_Write_Data_Long(DN100QN, F_FLOW2);
            RAM_Write_Data_Long(DN100QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN100QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN100QN, FLOW_RANGE);
            break;
          case 125 : // zde nic nedavat
            RAM_Write_Data_Long(DN125QN, F_FLOW2);
            RAM_Write_Data_Long(DN125QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN125QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN125QN, FLOW_RANGE);
            break;
          case 150 : // zde nic nedavat
            RAM_Write_Data_Long(DN150QN, F_FLOW2);
            RAM_Write_Data_Long(DN150QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN150QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN150QN, FLOW_RANGE);
            break;
          case 200 : // zde nic nedavat
            RAM_Write_Data_Long(DN200QN, F_FLOW2);
            RAM_Write_Data_Long(DN200QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN200QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN200QN, FLOW_RANGE);
            break;
          case 250 : // zde nic nedavat
            RAM_Write_Data_Long(DN250QN, F_FLOW2);
            RAM_Write_Data_Long(DN250QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN250QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN250QN, FLOW_RANGE);
            break;
          case 300 : // zde nic nedavat
            RAM_Write_Data_Long(DN300QN, F_FLOW2);
            RAM_Write_Data_Long(DN300QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN300QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN300QN, FLOW_RANGE);
            break;
          case 350 : // zde nic nedavat
            RAM_Write_Data_Long(DN350QN, F_FLOW2);
            RAM_Write_Data_Long(DN350QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN350QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN350QN, FLOW_RANGE);
            break;
          case 400 : // zde nic nedavat
            RAM_Write_Data_Long(DN400QN, F_FLOW2);
            RAM_Write_Data_Long(DN400QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN400QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN400QN, FLOW_RANGE);
            break;
          case 500 : // zde nic nedavat
            RAM_Write_Data_Long(DN500QN, F_FLOW2);
            RAM_Write_Data_Long(DN500QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN500QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN500QN, FLOW_RANGE);
            break;
          case 600 : // zde nic nedavat
            RAM_Write_Data_Long(DN600QN, F_FLOW2);
            RAM_Write_Data_Long(DN600QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN600QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN600QN, FLOW_RANGE);
            break;
          case 700 : // zde nic nedavat
            RAM_Write_Data_Long(DN700QN, F_FLOW2);
            RAM_Write_Data_Long(DN700QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN700QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN700QN, FLOW_RANGE);
            break;
          case 800 : // zde nic nedavat
            RAM_Write_Data_Long(DN800QN, F_FLOW2);
            RAM_Write_Data_Long(DN800QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN800QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN800QN, FLOW_RANGE);
            break;
          case 900 : // zde nic nedavat
            RAM_Write_Data_Long(DN900QN, F_FLOW2);
            RAM_Write_Data_Long(DN900QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN900QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN900QN, FLOW_RANGE);
            break;
          case 1000: // zde nic nedavat
            RAM_Write_Data_Long(DN1000QN, F_FLOW2);
            RAM_Write_Data_Long(DN1000QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN1000QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN1000QN, FLOW_RANGE);
            break;
          default :  // kdyz je nastaveno jinak
            RAM_Write_Data_Long(DN0QN, F_FLOW2);
            RAM_Write_Data_Long(DN0QN, I_FLOW_MAX);
            //RAM_Write_Data_Long(DN0QN, V_FLOW_MAX);
            RAM_Write_Data_Long(DN0QN, FLOW_RANGE);
            break;
        }

        Save_Settings();                               // Ulozeni nastaveni
        if( flag_sensor8 )
          send_diameter_sensor8();

        //odeslani nastaveni horni meze do BINu
        Send_Float(FOFL2, RAM_Read_Data_Long( F_FLOW2)/1000.0);
        delay_1ms();
      }
      break;
      case 14       :
      /* Datalogger Interval */
        switch ( RAM_Read_Data_Long(DATALOGGER_INTERVAL))
        {
          case 0:
            flag_datalogger=0;
            second_to_datalogger=0;
            actual_error &= ~ERR_NOT_INSERT_CARD;
            actual_error &= ~ERR_OPEN_FILE;
          break;

          case 1:
          case 2:
          case 3:
          case 4:
          case 5:
          case 6:
          case 7:
          case 8:
          case 9:
          case 10:
            flag_datalogger=1;
            second_to_datalogger=1;
            actual_error_datalogger=0;
          break;

          default:
            flag_datalogger=0;
            second_to_datalogger=0;
          break;
        }
      break;
      case 15       :
      /* RE1_SET*/
        Send_Func( RE1_F, F_OFF);
        delay_1ms();

        switch( RAM_Read_Data_Long( RE1_SET) )  ///RELE 1
        {
          case 0: //OFF
            Send_Func(RE1_F,F_OFF);
          break;

          case 1: //FIXED
            Send_Func(RE1_F,F_FIX);
          break;

          case 2: //FLOW+
            Send_Func(RE1_F,F_QPLUS);
          break;

          case 3: //FLOW -
            Send_Func(RE1_F,F_QMINUS);
          break;

          case 4: //ERROR
            Send_Func(RE1_F,F_ERR);
          break;

          case 5: //EMPTY PIPE
            Send_Func(RE1_F,F_AIR);
          break;

          case 6: //ON IN
            Send_Func(RE1_F,F_CONIN);
          break;

          case 7: //ON OUT
            Send_Func(RE1_F,F_CONOUT);
          break;

          case 8: //ON<F1
            Send_Func(RE1_F,F_CONMF1);
          break;

          case 9: //ON>F1
            Send_Func(RE1_F,F_CONVF1);
          break;

          default:    //DEFAULTNE OFF
            Send_Func(RE1_F,F_OFF);
          break;
        }
        delay_1ms();
        break;
      case 16       :
      /* RE2_SET*/
        Send_Func( RE2_F, F_OFF);
        delay_1ms();
        switch( RAM_Read_Data_Long( RE2_SET) )  ///RELE 2
        {
          case 0: //OFF
            Send_Func(RE2_F,F_OFF);
          break;

          case 1: //FIXED
            Send_Func(RE2_F,F_FIX);
          break;

          case 2: //FLOW+
            Send_Func(RE2_F,F_QPLUS);
          break;

          case 3: //FLOW -
            Send_Func(RE2_F,F_QMINUS);
          break;

          case 4: //ERROR
            Send_Func(RE2_F,F_ERR);
          break;

          case 5: //EMPTY PIPE
            Send_Func(RE2_F,F_AIR);
          break;

          case 6: //ON IN
            Send_Func(RE2_F,F_CONIN);
          break;

          case 7: //ON OUT
            Send_Func(RE2_F,F_CONOUT);
          break;

          case 8: //ON<F1
            Send_Func(RE2_F,F_CONMF1);
          break;

          case 9: //ON>F1
            Send_Func(RE2_F,F_CONVF1);
          break;

          default:    //DEFAULTNE OFF
            Send_Func(RE2_F,F_OFF);
          break;
        }
        delay_1ms();
        break;

      case 17       :
        /* porovnavaci hodnoty pro rele 1 a 2*/
        //Ralay 1 - Comparator Mode
        //po zmene v menu se musi znovu nacist hodnota do RAM
        Send_Float(CF1,RAM_Read_Data_Long( RE_FLOW_1)/1000.0);
        delay_1ms();
        Send_Float(CF2,RAM_Read_Data_Long( RE_FLOW_2)/1000.0);
        delay_1ms();
        Send_Float(CH1,RAM_Read_Data_Long( RE_HYSTERESIS_1)/1000.0);
        delay_1ms();
        Send_Float(CH2,RAM_Read_Data_Long( RE_HYSTERESIS_2)/1000.0);
        delay_1ms();
        break;

      case 18       :
        /* RELE3_SET*/
        Send_Func(RE3_F, F_OFF);
        delay_1ms();

        switch( RAM_Read_Data_Long( RE3_SET) )  ///RELE 3
        {
          case 0: //OFF
            Send_Func(RE3_F,F_OFF);
          break;

          case 1: //FIXED
            Send_Func(RE3_F,F_FIX);
          break;

          case 2: //FLOW+
            Send_Func(RE3_F,F_QPLUS);
          break;

          case 3: //FLOW -
            Send_Func(RE3_F,F_QMINUS);
          break;

          case 4: //ERROR
            Send_Func(RE3_F,F_ERR);
          break;

          case 5: //EMPTY PIPE
            Send_Func(RE3_F,F_AIR);
          break;

          case 6: //PULSE/litr +
            Send_Func(RE3_F,F_FPLUS);
          break;

          case 7: //PULSE/litr -
            Send_Func(RE3_F,F_FMINUS);
          break;

          case 8: //ON<F1
            Send_Func(RE3_F,F_DOSE);
          break;

          default:    //DEFAULTNE OFF
            Send_Func(RE3_F,F_OFF);
          break;
        }
        delay_1ms();
        break;

      case 19       :
        Send_Func( RE4_F, F_OFF);
        delay_1ms();
        /* RELE4_SET */
        switch( RAM_Read_Data_Long( RE4_SET) )  ///RELE 4
        {
          case 0: //OFF
            Send_Func(RE4_F,F_OFF);
          break;

          case 1: //FIXED
            Send_Func(RE4_F,F_FIX);
          break;

          case 2: //FLOW+
            Send_Func(RE4_F,F_QPLUS);
          break;

          case 3: //FLOW -
            Send_Func(RE4_F,F_QMINUS);
          break;

          case 4: //ERROR
            Send_Func(RE4_F,F_ERR);
          break;

          case 5: //EMPTY PIPE
            Send_Func(RE4_F,F_AIR);
          break;

          case 6: //PULSE/litr +
            Send_Func(RE4_F,F_FPLUS);
          break;

          case 7: //PULSE/litr -
            Send_Func(RE4_F,F_FMINUS);
          break;

          case 8: //ON<F1
            Send_Func(RE4_F,F_DOSE);
          break;

          default:    //DEFAULTNE OFF
            Send_Func(RE4_F,F_OFF);
          break;
        }
        delay_1ms();
        break;

      case 20       :
        /* RE_VOLUME_PLUS */
        Send_Long(QPLUS,RAM_Read_Data_Long( RE_VOLUME_PLUS));
        delay_1ms();
        break;

      case 21       :
        /* RE_VOLUME_MINUS */
        Send_Long(QMINUS,RAM_Read_Data_Long( RE_VOLUME_MINUS));
        delay_1ms();
        break;

      case 22       :
        /* RE_DOSE */
        Send_Long(DOSE,RAM_Read_Data_Long( RE_DOSE));
        delay_1ms();
        break;

      case 23       :
        /* F_SET */
        Send_Func(FO_F,F_OFF);
        delay_1ms();

        switch( RAM_Read_Data_Long( F_SET))
        {
          case 0: //OFF
            Send_Func(FO_F,F_OFF);
          break;

          case 1: //FIXED
            Send_Func(FO_F,F_FIX);
          break;

          case 2: //flow +
            Send_Func(FO_F,F_QPLUS);
          break;

          case 3: //flow -
            Send_Func(FO_F,F_QMINUS);
          break;

          case 4: //Error
            Send_Func(FO_F,F_ERR);
          break;

          case 5: //Empty pipe
            Send_Func(FO_F,F_AIR);
          break;

          case 6:
            Send_Func(FO_F,F_DIRECT);
          break;

          default:
            Send_Func(FO_F,F_OFF);
          break;
        }
        delay_1ms();
        break;

      case 24        :
        /* Prekresleni pocitadla pri zmene jednotky */
        Reload_meas_value();
        break;
      case 25        :
        //zmena kalibracni konstanty 1
        //spolecne s aktualni namerenou hodnotou od senzoru a posle ji take do senzoru jako mearured point 1
        {
          if( flag_sensor8)
            RAM_Write_Data_Long(adc_average+RAM_Read_Data_Long(ZERO_FLOW_CONSTANT),MEASURED_POINT_ONE);

          send_calibration(1);
        }
        break;
      case 26        :
        //zmena kalibracni konstanty 2
        //spolecne s aktualni namerenou hodnotou od senzoru a posle ji take do senzoru jako mearured point 2
        {
          if ( flag_sensor8)
            RAM_Write_Data_Long(adc_average+RAM_Read_Data_Long(ZERO_FLOW_CONSTANT),MEASURED_POINT_TWO);

          send_calibration(2);
        }
        break;
      case 27        :
        //DIRECT DRIVING RE3 a RE4
        Send_Float(FOFL1, RAM_Read_Data_Long( F_FLOW1)/1000.0);
        delay_1ms();
        Send_Float(FOFL2, RAM_Read_Data_Long( F_FLOW2)/1000.0);
        delay_1ms();
        Send_Float(FOFR1, (float)RAM_Read_Data_Long( F_FREQ1));
        delay_1ms();
        Send_Float(FOFR2, (float)RAM_Read_Data_Long( F_FREQ2));
        delay_1ms();
        break;

      case 28        :
        //DUTY CYCLE
        Send_Long(FODUTY, RAM_Read_Data_Long( F_DUTY_CYCLE));
        delay_1ms();
        break;

      case 29        :
        //Ralay 2 - Litres/1 (Q+)
        //po zmene v menu se musi znovu nacist hodnota do RAM
        break;

      case 30        :
        //Ralay 2 - Litres/1 (Q-)
        //po zmene v menu se musi znovu nacist hodnota do RAM
        break;

      case 31        :
        //Ralay 2 - Dosing
        //po zmene v menu se musi znovu nacist hodnota do RAM
        break;

      case 32        :
        //Current Loop - Current_Set
        //po zmene v menu se musi znovu nacist hodnota do RAM
        //RAM_Write_Data_Long(6,VOLTAGE_SET);     //vypne se V-OUT
        flag_outputs_modules = IOUT_MODUL;  //do flagu se nastavi IOUT a zaroven se tak vymaze VOUT
        I_Out();
        break;

      case 33        :
        //Current_Loop - Direct Driving
        //Current_Loop - Calibration
        //po zmene v menu se musi znovu nacist hodnota do RAM
        break;

      case 34        :
        //Voltage Output - Voltage_Set
        //po zmene v menu se musi znovu nacist hodnota do RAM
        RAM_Write_Data_Long(6,CURRENT_SET);
        flag_outputs_modules = VOUT_MODUL;
        V_Out(g_measurement.actual);
        break;

      case 35        :
        //Voltage_Output - Direct Driving
        //po zmene v menu se musi znovu nacist hodnota do RAM
        break;

      case 36        :
        //F-Out - F_Set
        //po zmene v menu se musi znovu nacist hodnota do RAM
        break;

      case 37        :
        //F-Out - Direct Driving
        //po zmene v menu se musi znovu nacist hodnota do RAM
        break;

      case 38        :
        // Zero Flow
        if (g_value[0].val==1)
        {
          if( flag_sensor8 )
            send_zero_flow_sensor8();
        }
        RAM_Write_Data_Long(0, ZERO_FLOW);
        g_value[0].val=0;
        break;

      case 39        :
        //zmena kalibracni konstanty 3
        //spolecne s aktualni namerenou hodnotou od senzoru a posle ji take do senzoru jako mearured point 3
        if( RAM_Read_Data_Long(CALIBRATION_POINT_THREE) )
        {
          if( flag_sensor8)
            RAM_Write_Data_Long(adc_average+RAM_Read_Data_Long(ZERO_FLOW_CONSTANT),MEASURED_POINT_THREE);
        }
        else    //pokud je 0 tak se vymaze 3 kalib bobd
        {
          RAM_Write_Data_Long(0,MEASURED_POINT_THREE);
        }
        send_calibration(3);
        break;

      case 40        : // inicializace MODBUS
        if (!USBconnected())
          ChangeCommunicationParam=true;
        break;

      case 41        : // Change Excitation Frequency
        ///reseni jestli sens7 nebo 8 je uvnitr funkce sensor_frekvence
        if( RAM_Read_Data_Long(EXCITATION_FREQUENCY))   //6.25
        {
          sensor_frekvence(FREKVENCE_6HZ);
        }
        else                                            //3.125
        {
          sensor_frekvence(FREKVENCE_3HZ);
        }
        break;

      case 42        : // Excitation Frequency ON / OFF
        if( RAM_Read_Data_Long(EXCITATION)) //OFF
        {
          sensor_buzeni(0);
        }
        else
        {
          sensor_buzeni(1);
        }
        break;

      case 43:         // cleaning
        switch(RAM_Read_Data_Long( CLEAN_POWER ))
        {
          case 0: //clean off
            sensor_cisteni(SENSOR_OFF);
            break;
          default:
            break;
        }
        RAM_Write_Data_Long(1,CLEAN_POWER);
        break;

      case 44:        //cleaning now
        if ((g_value[0].val==1) && !(actual_error & ERR_SENSOR) && !IsActualFlowUnderLowCutoffLevel())
        {
          flag_cisteni_cas=RAM_Read_Data_Long(CLEAN_TIME);
          sensor_cisteni(SENSOR_ON);
        }
        RAM_Write_Data_Long(0, CLEAN_STARTNOW);
        RAM_Write_Data_Long(1, CLEAN_POWER);
        g_value[0].val=0;
        break;

      case 45:    //zmena serioveho cisla
        // vypocet authorize password proi zmene serioveho cisla
        RAM_Write_Data_Long((unsigned int)(((RAM_Read_Data_Long(UNIT_NO) ^ 33333333)) & 0x03FFFFFF), PASSWORD_AUTHORIZE);
        flag_zapsat_flash=1;
        if(flag_komunikacni_modul & MODUL_GSM)
          set_unitno_gsm();
        break;

      case 46:    // vynulovani zero flow constant
        if (g_value[0].val==1)
        {
          if( flag_sensor8 )
          {
            send_zero_erase_sensor8();
            RAM_Write_Data_Long(0,ZERO_FLOW_CONSTANT);
          }
        }
        g_value[0].val=0;
        RAM_Write_Data_Long(0, ZERO_FLOW_ERASE);
        break;

      case 47:
        DisplaySetContrast(RAM_Read_Data_Long(CONTRAST));
      break;

      case 48:        //flow QN
        if( flag_sensor8)
          send_flow_range_sensor8();
        if (GetGraphQuantityType() == GQ_FLOW)
            InitGraphHistory();
      break;

      case 49:        //measurement
        if( flag_sensor8)
          send_measure_state_sensor8();
      break;

      case 50:        //air detector
        if( flag_sensor8)
          send_air_detector_sensor8();
      break;

      case 51:        //air constant
        if( flag_sensor8)
          send_air_constant_sensor8();
      break;

      case 52:        //samples per average
        if( flag_sensor8)
          send_average_samples_sensor8();
      break;

      case 53:        //low flow cut off
        if( flag_sensor8)
          send_low_flow_cut_sensor8();
      break;

      case 54:        //invert flow
        if( flag_sensor8)
          send_invert_flow_sensor8();
      break;

      case 55:        //clean tim
        /*if( flag_sensor8)
            send_cisteni_sensor8();
          */
      break;

      case 56:        //demo
        if( flag_sensor8)
          send_demo_sensor8();
      break;

      case 57:        //simulated flow
        if( flag_sensor8)
          send_simulated_flow_sensor8();
      break;

      case 58:        //start delay
        if( flag_sensor8)
          send_start_delay_sensor8();
      break;

      case 59:        //gsm data interval
        flag_second_gsm=((unsigned int)RAM_Read_Data_Long(GSM_DATA_INTERVAL))*60;
      break;

      case 60:        //gsm phone 1
      break;

      case 61:        //gsm phone 2
      break;

      case 62:        //gsm phone 3
      break;

      case 63:        //gsm event empty pipe
        if( RAM_Read_Data_Long(GSM_EMPTY_PIPE))
        {
          if( actual_error&ERR_EMPTY_PIPE)
          {
            flag_event_empty_on_odeslano=1;
            flag_event_empty_off_odeslano=0;
          }
          else
          {
            flag_event_empty_on_odeslano=0;
            flag_event_empty_off_odeslano=1;
          }
        }
        else
        {
          //flag_event_empty_on_odeslano=1;
          //flag_event_empty_off_odeslano=0;
        }
      break;

      case 64:        //gsm event zero flow
        if( RAM_Read_Data_Long( GSM_ZERO_FLOW))
        {
          if( g_measurement.actual != 0)
          {
            flag_event_zero_on_odeslano=0;
            flag_event_zero_off_odeslano=1;
          }
          else
          {
            flag_event_zero_on_odeslano=1;
            flag_event_zero_off_odeslano=0;
          }
        }
        else
        {
          //flag_event_zero_on_odeslano=0;
          //flag_event_zero_off_odeslano=1;
        }
      break;

      case 65:        //gsm event error detect
        if( RAM_Read_Data_Long( GSM_ERROR_DETECT))
        {
          last_sending_error_gsm=actual_error;
        }
        else
        {
          //flag_event_error_on_odeslano=0;
          //Sflag_event_error_off_odeslano=1;
        }
      break;

      case 66:
      case 67:
      break;

      case 68:
        // Wi-Fi AP mode
        if (g_value[0].val == 1)
          if(flag_komunikacni_modul & MODUL_WIFI)
          {
            FIO2CLR |= (1<<3);  // P2.3
            delay_100ms();
            FIO2SET |= (1<<3);
          }
      break;

      case 69: // external temperature measurement on/off
        InitPulseModuleFunction();
      break;

      case 70: // external temperature measurement calibration now - kanal 1
        if (g_value[0].val==1)
        {
          float tempCelsius = RAM_Read_Data_Long(EXT_CAL_TEMPERATURE1) / 10.0;
          ExtTemp_CalibrateMeasurement(TCHNL_1, Convert_CelsiusToKelvin(tempCelsius));
        }
        RAM_Write_Data_Long(0, EXT_TEMP1_CAL_STARTNOW);
        g_value[0].val=0;
      break;

      case 71: // external pressure measurement on/off
        InitIoutModuleFunction();
      break;

      case 72: // external current/pressure parameters changed
      break;

      case 73: // external temperature sensor type changed
        ExtTemp_OnSensorTypeChanged();
      break;

      case 74: // external temperature sensor connection changed
        ExtTemp_OnSensorConnectionChanged();
      break;

      case 75: // external temperature measurement calibration now - kanal 2
        if (g_value[0].val==1)
        {
          float tempCelsius = RAM_Read_Data_Long(EXT_CAL_TEMPERATURE2) / 10.0;
          ExtTemp_CalibrateMeasurement(TCHNL_2, Convert_CelsiusToKelvin(tempCelsius));
        }
        RAM_Write_Data_Long(0, EXT_TEMP2_CAL_STARTNOW);
        g_value[0].val=0;
      break;

      case 76:
        /* delete total heat? */
        if (g_value[0].val==1)
        {
            Heat_DeleteTotalHeat();
        }
        RAM_Write_Data_Long(0, DELETE_TOTAL_HEAT);
        g_value[0].val=0;
        break;

      case 77:
        /* Iout Direct Driving � Quantity */
        break;

      case 78:
        /* Iout Heat Power min. - max. */
        break;

      case 79:
        /* Bin RE1 Function */
        PulseOutputs_OnParameterChanged(PULSE_OUT_1);
        break;

      case 80:
        /* Bin RE2 Function */
        PulseOutputs_OnParameterChanged(PULSE_OUT_2);
        break;

      case 81:
        /* Bin RE3 Function */
        PulseOutputs_OnParameterChanged(PULSE_OUT_3);
        break;

      case 82:
        /* Bin RE4 Function */
        PulseOutputs_OnParameterChanged(PULSE_OUT_4);
        break;

      case 83:
        /* zmena MBus parametru */
        MBusSlave_OnParameterChanged();
        break;

      case 84:
        /* zmena zobrazovane veliciny v grafech */
        InitGraphHistory();
        break;

      case 85:
        /* zmena rozsahu tepelneho vykonu */
        if (GetGraphQuantityType() == GQ_HEAT_POWER)
            InitGraphHistory();
        break;

      case 86:
        /* Detect Modul3 */
        InitUart1Function();
        break;

      case 87:
        /* Typ sensoru prutoku */
        InitSensorFunction();
        break;

      case 88:
        /* Bin RE1 Heat Power Comparator */
        PulseOutputs_OnParameterChanged(PULSE_OUT_1);
        break;

      case 89:
        /* Bin RE2 Heat Power Comparator */
        PulseOutputs_OnParameterChanged(PULSE_OUT_2);
        break;

      case 90:
        /* Bin RE3 Heat Power Comparator */
        PulseOutputs_OnParameterChanged(PULSE_OUT_3);
        break;

      case 91:
        /* Bin RE4 Heat Power Comparator */
        PulseOutputs_OnParameterChanged(PULSE_OUT_4);
        break;

      case 92:
        /* Bin RE3 Heat Power Fout */
        PulseOutputs_OnParameterChanged(PULSE_OUT_3);
        break;

      case 93:
        /* Bin RE4 Heat Power Fout */
        PulseOutputs_OnParameterChanged(PULSE_OUT_4);
        break;

      case 94:
        /* LED Mode */
        Led_OnParameterChanged();
        break;

      case 95:
        /* Flow Pulse Timeout */
        break;

      case 96:
        /* Start Heat Test */
        if (g_value[0].val==1)
        {
            Heat_RunTest(TRUE);
        }
        RAM_Write_Data_Long(0, HEAT_TEST_STARTNOW);
        g_value[0].val=0;
        break;

      default       :
        ShowStrMessage("Neznama operace~");
      break;
    }
}


unsigned char KursorPositionInValue ( void )///
{
    // Funkce zjistuje podle scroll, ktery ze znaku valchar se ma zobrazit inverzne.
    //  IN  : g_value[i].dig, g_value[i].dec, g_scroll
    //  OUT : pozice znaku. Prvni je 0
    unsigned char position;
    int i;
    i        = 0;
    position = g_scroll + 1;
    while ( position > (g_value[i].dig + g_value[i].dec + (( g_value[i].dec != 0 ) ?  1 : 0)) )
    {
        position -= ( g_value[i].dig + g_value[i].dec + (( g_value[i].dec != 0 ) ?  1 : 0) ); // S des. teckou a bez ni
        i++;
    }
    position--;

    return ( position );
}

void EditInit(void)///
{
    // Funkce zjisti s jakou editacni polozkou se bude pracova, precte z tabulek prislusne hodnoty,
    // ktere jsou potreba k inicializaci polozky.
    //  IN  : g_adr, menu[], edit[]
    //  OUT : var g_edittyp, g_value[i].*, g_scroll
    int i,actindex;
    for (i=0;i<=3;i++)
    {
        g_value[i].ptr = -1;
        g_value[i].typ = 0;
        g_value[i].dig = 0;
        g_value[i].dec = 0;
        g_value[i].min = 0; //<-- Verze s ciselnymy typy
        g_value[i].max = 0; //<-- Verze s ciselnymy typy
        g_value[i].val = 0; //<-- Verze s ciselnymy typy
        g_value[i].tex = 0;
        strcpy((char*)g_value[i].unichar,"");
        strcpy((char*)g_value[i].minchar,"");
        strcpy((char*)g_value[i].maxchar,"");
        strcpy((char*)g_value[i].valchar,"");
    }
    g_fun[F_IN ]=0;
    g_fun[F_OUT]=0;
    g_value[0].tex= (P_menu[(g_adr+TO_TEXT+menu_levels[g_level]*ONE_ITEM_SIZE)]); // odkaz na text, ktery se bude vypisovat
    actindex  = (P_menu[(g_adr+menu_levels[g_level]*ONE_ITEM_SIZE+TO_EDIT)]);     // ted actadr ukazuje do tab. editu
    g_edittyp = (P_edit[actindex]);
    actindex++;
    switch (g_edittyp)
    {
    case 0/*Info*/:
        // Offset
        g_value[0].ptr = (P_edit[actindex]);
        actindex++;
        g_value[0].typ = 1; // Longint
        // Unit
        g_value[0].uni = (P_edit[actindex]);
        actindex++;
        // Digits
        g_value[0].dig = (P_edit[actindex]);
        actindex++;
        // Decimal
        g_value[0].dec = (P_edit[actindex]);
        actindex++;
        // Funkce pred vstupem do okna
        g_fun[F_IN ]   = (unsigned char)(P_edit[actindex]);
        actindex++;
        // Funkce pred po vystupu z okna
        g_fun[F_OUT ]  = (unsigned char)(P_edit[actindex]);
        actindex++;

        ReadValues();
        g_value[0].ptr = -1;
        LongToPointStr(g_value[0].val, g_value[0].dig, g_value[0].dec, (char*)g_value[0].valchar );
        ReadUnitFromFlash(g_value[0].uni, (char*)g_value[0].unichar);
        /* Provedeni akce */
        EditDoFunction(F_IN, g_fun);
        break;
    case 1/*Edit*/:
        // Offset
        g_value[0].ptr = (P_edit[actindex]);
        actindex++;
        g_value[0].typ = 1; // Longint
        // Unit
        g_value[0].uni = (P_edit[actindex]);
        actindex++;
        // Digits
        g_value[0].dig = (P_edit[actindex]);
        actindex++;
        // Decimal
        g_value[0].dec = (P_edit[actindex]);
        actindex++;
        // Min
        g_value[0].min = (P_values[(P_edit[actindex])]);
        actindex++;
        // Max
        g_value[0].max = (P_values[(P_edit[actindex])]);
        actindex++;
        // Funkce pred vstupem do okna
        g_fun[F_IN ]   = (unsigned char)(P_edit[actindex]);
        actindex++;
        // Funkce pred po vystupu z okna
        g_fun[F_OUT ]  = (unsigned char)(P_edit[actindex]);
        actindex++;

        ReadValues();
        LongToPointStr(g_value[0].min, g_value[0].dig, g_value[0].dec, (char*)g_value[0].minchar );
        LongToPointStr(g_value[0].max, g_value[0].dig, g_value[0].dec, (char*)g_value[0].maxchar );
        LongToPointStr(g_value[0].val, g_value[0].dig, g_value[0].dec, (char*)g_value[0].valchar );
        ReadUnitFromFlash(g_value[0].uni,(char*)g_value[0].unichar);
        g_scroll = g_value[0].dig + g_value[0].dec - 1 + (( g_value[0].dec != 0 ) ? 1 : 0); // s desetinnou teckou a bez ni
        /* Provedeni akce */
        EditDoFunction(F_IN, g_fun);
        break;
    case 2/*4win*/:
        for (i=0; i<=3; i++)
        {
            // Offset
            g_value[i].ptr = (P_edit[actindex]);
            actindex++;
            g_value[i].typ = 1; // Longint
        }                                                                                                                    // Unit
        // Unit1
        g_value[0].uni = (P_edit[actindex]);
        actindex++;
        g_value[1].uni = g_value[0].uni;
        // Digits1
        g_value[0].dig = (P_edit[actindex]);
        actindex++;
        g_value[1].dig = g_value[0].dig;
        // Decimal1
        g_value[0].dec = (P_edit[actindex]);
        actindex++;
        g_value[1].dec = g_value[0].dec;

        // Unit2
        g_value[2].uni = (P_edit[actindex]);
        actindex++;
        g_value[3].uni = g_value[2].uni;
        // Digits2
        g_value[2].dig = (P_edit[actindex]);
        actindex++;
        g_value[3].dig = g_value[2].dig;
        // Decimal2
        g_value[2].dec = (P_edit[actindex]);
        actindex++;
        g_value[3].dec = g_value[2].dec;

        // Min1
        g_value[0].min = (P_values[(P_edit[actindex])]);
        actindex++;
        g_value[1].min = g_value[0].min;
        // Max1
        g_value[0].max = (P_values[(P_edit[actindex])]);
        actindex++;
        g_value[1].max = g_value[0].max;

        // Min2
        g_value[2].min = (P_values[(P_edit[actindex])]);
        actindex++;
        g_value[3].min = g_value[2].min;
        // Max2
        g_value[2].max = (P_values[(P_edit[actindex])]);
        actindex++;
        g_value[3].max = g_value[2].max;

        // Funkce pred vstupem do okna
        g_fun[F_IN ]   = (unsigned char)(P_edit[actindex]);
        actindex++;
        // Funkce pred po vystupu z okna
        g_fun[F_OUT ]  = (unsigned char)(P_edit[actindex]);
        actindex++;

        // Text2
        g_value[2].tex = (P_edit[actindex]);
        actindex++;
        g_value[3].tex = g_value[2].tex;

        g_value[1].tex = g_value[0].tex; // Text 0 uz je prirazeny na zacatku EditInfo

        ReadValues();

        for (i=0; i<=3; i++)
        {
            LongToPointStr(g_value[i].min, g_value[i].dig, g_value[i].dec, (char*)g_value[i].minchar );
            LongToPointStr(g_value[i].max, g_value[i].dig, g_value[i].dec, (char*)g_value[i].maxchar );
            LongToPointStr(g_value[i].val, g_value[i].dig, g_value[i].dec, (char*)g_value[i].valchar );
            ReadUnitFromFlash(g_value[i].uni,(char*)g_value[i].unichar);
        }
        g_scroll = g_value[0].dig + g_value[0].dec - 1 + (( g_value[0].dec != 0 ) ? 1 : 0); // s desetinnou teckou a bez ni
        /* Provedeni akce */
        EditDoFunction(F_IN, g_fun);
        break;
    case 3/*Isel*/:
        // Offset
        g_value[0].ptr = (P_edit[actindex]);
        actindex++; // Oznacena polozka >
        g_value[0].typ = 1; // Longint
        // Count
        g_value[1].val = (P_edit[actindex]);
        actindex++; // pocet polozek je ulozeny ve value 1
        // Funkce pred vstupem do okna
        g_fun[F_IN ]   = (unsigned char)(P_edit[actindex]);
        actindex++;
        // Funkce pred po vystupu z okna
        g_fun[F_OUT ]  = (unsigned char)(P_edit[actindex]);
        actindex++;
        // Odkaz na prvni text
        g_value[2].val = actindex;                  // ve value 2 je ulozeny index, ktery edit[g_value[2].val] ukauzje na prvni text

        ReadValues();

        // g_scroll - radek, na kterem stoji kursor (0-5), kdyz k nenu date + 1, budete mit skutecnej radek(1. radek je popisek)
        // g_value[0].val - Cislo polozky ktera je oznacena sipkou(0..n-1)
        // g_value[1].val - Pocet polozek (1..n)
        // g_value[3].val - Cislo polozky, na ktere stoji kursor  (0..n-1)

        g_value[3].val = g_value[0].val;
        g_scroll = g_value[3].val;      // Presunuti kursoru na znackou oznacenou polozku
        //if (g_value[3].val>(LAST_ROW-2)) g_scroll = LAST_ROW-1; // -2 = chyby jedna polozka ze zvrchu a jedna ze spodu
        if (g_value[3].val>(LAST_ROW-2))
            g_scroll = LAST_ROW-2; // -2 = chyby jedna polozka ze zvrchu a jedna ze spodu
        /* Provedeni akce */
        EditDoFunction(F_IN, g_fun);
        break;
    case 4/*Bool*/:
        // Offset
        g_value[0].ptr = (P_edit[actindex]);
        actindex++;
        g_value[0].typ = 1; // Longint
        // Funkce pred vstupem do okna
        g_fun[F_IN ]   = (unsigned char)(P_edit[actindex]);
        actindex++;
        // Funkce pred po vystupu z okna
        g_fun[F_OUT ]  = (unsigned char)(P_edit[actindex]);
        actindex++;
        ReadValues();
        /* Provedeni akce */
        EditDoFunction(F_IN, g_fun);
        break;
    case 5/*Time*/:
        // Offset
        g_value[0].ptr = (P_edit[actindex]);
        actindex++;
        g_value[0].typ = 1; // Longint
        // Funkce pred vstupem do okna
        g_fun[F_IN ]   = (unsigned char)(P_edit[actindex]);
        actindex++;
        // Funkce pred po vystupu z okna
        g_fun[F_OUT ]  = (unsigned char)(P_edit[actindex]);
        actindex++;
        /* Provedeni akce */
        EditDoFunction(F_IN, g_fun);
        break;
    case 6/*Date*/:
        // Offset
        g_value[0].ptr = (P_edit[actindex]);
        actindex++;
        g_value[0].typ = 1; // Longint
        // Funkce pred vstupem do okna
        g_fun[F_IN ]   = (unsigned char)(P_edit[actindex]);
        actindex++;
        // Funkce pred po vystupu z okna
        g_fun[F_OUT ]  = (unsigned char)(P_edit[actindex]);
        actindex++;
        /* Provedeni akce */
        EditDoFunction(F_IN, g_fun);
        g_scroll=7; // Kursor stoji na poslednim miste
        break;
    case 7/*CalW*/:
        /* Provedeni akce */
        EditDoFunction(F_IN, g_fun);
        break;
    case 8/*Pasw*/:
        // Offset
        g_value[1].ptr = (P_edit[actindex]);
        actindex++;
        g_value[1].typ = 1; // Longint
        // Unit
        g_value[0].uni = (P_edit[actindex]);
        actindex++;
        // Digits
        g_value[0].dig = (P_edit[actindex]);
        actindex++;
        // Decimal
        g_value[0].dec = (P_edit[actindex]);
        actindex++;
        // Min
        g_value[0].min = (P_values[(P_edit[actindex])]);
        actindex++;
        // Max
        g_value[0].max = (P_values[(P_edit[actindex])]);
        actindex++;
        // Funkce pred vstupem do okna
        g_fun[F_IN ]   = (unsigned char)(P_edit[actindex]);
        actindex++;
        // Funkce pred po vystupu z okna
        g_fun[F_OUT ]  = (unsigned char)(P_edit[actindex]);
        actindex++;

        ReadValues();
        g_value[1].ptr = -1; // heslo neukladat
        LongToPointStr(g_value[0].min, g_value[0].dig, g_value[0].dec, (char*)g_value[0].minchar );
        LongToPointStr(g_value[0].max, g_value[0].dig, g_value[0].dec, (char*)g_value[0].maxchar );
        LongToPointStr(g_value[0].val, g_value[0].dig, g_value[0].dec, (char*)g_value[0].valchar );
        //g_scroll = ( g_value[0].dec != 0 ) ? g_value[0].dig + g_value[0].dec - 1 + 1 : g_value[0].dig + g_value[0].dec - 1; // S des. teckou a bez ni
        g_scroll = 0; // U hesla se nastavi kursor na prvni polozku a ne na posledni jako u ustatnich editu
        /* Provedeni akce */
        EditDoFunction(F_IN, g_fun);
        break;
    case 9/*Func*/:
        // Tento typ editacniho okna je specialni. Provadi se v nem konkretni akce,
        // specificke pro kazde pouziti zvlast. Predpoklada se u nej i specialni
        // disain, ktereho neni mozne dosahnout soucasnymi okny.
        g_value[0].val = (P_edit[actindex]);
        actindex++;
        // Funkce pred vstupem do okna
        g_fun[F_IN]   = (unsigned char)(P_edit[actindex]);
        actindex++;
        // Funkce po vystupu z okna
        g_fun[F_OUT]  = (unsigned char)(P_edit[actindex]);
        actindex++;
        /* Provedeni akce */
        EditDoFunction(F_IN, g_fun);
        break;
    }
}

void ShowEdit(void)///
{
  // Funkce zobrazuje prislusne editacni okno.
  //  IN  : g_edittyp, g_value[i].*, g_scroll, menu[], texty[]
  //  OUT :
  char g_s[80];
  WriteErase();
  switch (g_edittyp)
  {
    case 0/*Info*/:
    ReadStringFromFlash( g_value[0].tex+LANGUAGE_OFFSET , g_s);
    Write     ( 0, 0, g_s );
    WriteCenter( 4, (char*)g_value[0].valchar );

    if ( strcmp((char*)g_value[0].unichar,"")!=0 )
    {
      //Pokud neni jednotka prazdna, tak se vypise
      strcpy( g_s, "[" );
      strcat( g_s, (char*)g_value[0].unichar );
      strcat( g_s, "]" );
      Write ( LAST_COLUMN - SizeInPixel(g_s), 4, g_s);
    }

    WriteLastRow( BUTTON_ESC );
    #ifdef CONTROL_OUTPUT
      if ( ControlOutputIsOn )
      {}
    #endif
    break;

  case 1/*Edit*/:
    ReadStringFromFlash( g_value[0].tex+LANGUAGE_OFFSET,g_s);
    Write     ( 0, 0, g_s );
    WriteCenter( 4, (char*)g_value[0].valchar );
    if ( strcmp((char*)g_value[0].unichar,"")!=0 )
    {
      //Pokud neni jednotka prazdna, tak se vypise
      strcpy( g_s, "[" );
      strcat( g_s, (char*)g_value[0].unichar );
      strcat( g_s, "]" );
      Write ( LAST_COLUMN - SizeInPixel(g_s), 4, g_s);
    }
    {
      // tento usek zjistuje, ktera cislica ma byt zabarvena, a zabarvi ji.
      int j     = 0;
      int Where = 0;
      while (j!=g_scroll)
      {
        Where+=(GetCharacterWidth(g_value[0].valchar[j]));
        j+=1;
      }
      InvertLine( CENTR((char*)g_value[0].valchar) + Where -1, 4, (GetCharacterWidth(g_value[0].valchar[j])) +1 ); // Zabarvi editovane cislo. -1, +1 - zajistuje zabarveni jednoho sloupce vlevo pred cislem
    }
    WriteLastRow( BUTTON_ESC | BUTTON_OK );
    #ifdef CONTROL_OUTPUT
      if ( ControlOutputIsOn )
      {}
    #endif

    break;
  case 2/*4win*/:
    // Value 0 a 1
    ReadStringFromFlash( g_value[0].tex+LANGUAGE_OFFSET ,g_s );
    strcat( g_s, " [" );
    strcat( g_s, (char*)g_value[0].unichar );
    strcat( g_s, "]" );
    Write     ( 0, 0, g_s );
    WriteCenter( 1, (char*)g_value[0].valchar );
    WriteCenter( 2, (char*)g_value[1].valchar );
    // Value 2 a 3
    ReadStringFromFlash( g_value[2].tex+LANGUAGE_OFFSET , g_s);
    strcat( g_s, " [" );
    strcat( g_s, (char*)g_value[2].unichar );
    strcat( g_s, "]" );
    Write     ( 0, 3, g_s );
    WriteCenter( 4, (char*)g_value[2].valchar );
    WriteCenter( 5, (char*)g_value[3].valchar );
    {
      // tento usek zjistuje, ktera cislice ma byt zabarvena, a zabarvi ji.
      int j     = 0;
      int Where = 0;
      while (j!=KursorPositionInValue())
      {
        Where+=(GetCharacterWidth(g_value[ActValue()].valchar[j]));
        j+=1;
      }
      switch ( ActValue() )
      {
        case 0 :
          InvertLine( CENTR((char*)g_value[0].valchar ) + Where -1, 1, (GetCharacterWidth(g_value[ActValue()].valchar[j])) +1); // Zabarvi editovane cislo. -1, +1 - zajistuje zabarveni jednoho sloupce vlevo pred cislem
          break;
        case 1 :
          InvertLine( CENTR((char*)g_value[1].valchar ) + Where -1, 2, (GetCharacterWidth(g_value[ActValue()].valchar[j])) +1); // Zabarvi editovane cislo. -1, +1 - zajistuje zabarveni jednoho sloupce vlevo pred cislem
          break;
        case 2 :
          InvertLine( CENTR((char*)g_value[2].valchar ) + Where -1, 4, (GetCharacterWidth(g_value[ActValue()].valchar[j])) +1); // Zabarvi editovane cislo. -1, +1 - zajistuje zabarveni jednoho sloupce vlevo pred cislem
          break;
        case 3 :
          InvertLine( CENTR((char*)g_value[3].valchar ) + Where -1, 5, (GetCharacterWidth(g_value[ActValue()].valchar[j])) +1); // Zabarvi editovane cislo. -1, +1 - zajistuje zabarveni jednoho sloupce vlevo pred cislem
          break;
      }
    }
    WriteLastRow( BUTTON_ESC | BUTTON_OK );
    #ifdef CONTROL_OUTPUT
      if ( ControlOutputIsOn )
      {
        sprintf(g_s, "%d", g_scroll);
        Write(0, 1, g_s);
      }
    #endif
    break;

  case 3/*Isel*/:
  /* Skoro jako ShowMenu */
  {
    #define MARK_CHAR   (12)
    #define UNMARK_CHAR (11)
    // g_scroll - radek, na kterem stoji kursor (0-5), kdyz k nenu date + 1, budete mit skutecnej radek(1. radek je popisek)
    // g_value[0].val - Cislo polozky ktera je oznacena sipkou(0..n-1)
    // g_value[1].val - Pocet polozek (1..n)
    // g_value[3].val - Cislo polozky, na ktere stoji kursor  (0..n-1)
    unsigned int  actAddr = g_value[2].val;
    //unsigned char item    = g_value[3].val;          // Cislo polozky, na ktere stoji kursor (0..n)
    unsigned char scroll  = g_value[3].val-g_scroll; // Odrolovani o scroll polozek dolu
    unsigned char count   = g_value[1].val;          // Pocet polozek (1..n)
    const unsigned char cursor  = g_scroll + 1;      // Cislo radku, na kterem stoji kursor (1..6), radky jsou (0..7)
    unsigned char selectedvalue = g_value[0].val;    // Polozka ktera je oznacena MARK_HCAR
    char MarkedChar[2]   =
    {
      MARK_CHAR  ,0
    }
    ;           // Retezec Marked
    char UnMarkedChar[2] =
    {
      UNMARK_CHAR,0
    }
    ;           // Retezec UnMarked
    int i,y;

    actAddr += scroll;
    i = scroll;                      //posun v menu o scroll polozek dolu
    y      = 1;                      // y urcuje, na ktery radek se pise. na 0. radku je text

    while ((i<count) && (y<=LAST_ROW-1))
    {
      // Vypsani textu. -1 na poslednim radku jsou tlacitka ESC a OK
      Write( 1+ SizeInPixel(MarkedChar) + 6, y, ReadStringFromFlash((P_edit[actAddr])+LANGUAGE_OFFSET, g_s) );// prvni sloupec je rezervovany pro oznaceny radek
      if (i==selectedvalue)
        Write( 0 + 1, y, MarkedChar);   // g_value[0].val, ktera je momentalne vybrana (ne kurzorem, ale uplne). +1 vynechany pixel
      else
        Write( 0 + 1, y, UnMarkedChar); // U nevybranych polozek se zobrazuje tento znak. Tyto znaky by meli byt stejne siroke. +1 vynechany pixel
      i+=1;
      y+=1;
      actAddr+=1;
    }

    InvertLine( 0, cursor, NUMBER_OF_COLUMNS ); // Inverze kursoru ( line, od, kolik )
    Write     ( 0, 0, ReadStringFromFlash( g_value[0].tex+LANGUAGE_OFFSET , g_s )); // Prvni radka, vypis popisku k editu
    WriteLastRow( BUTTON_ESC | BUTTON_OK );

    /* Nastaveni sipek v segmentech */
    if (scroll > 0)
      g_segment_display.status|= UP_ARROW;
    else
      g_segment_display.status&=~UP_ARROW;
    if (count > LAST_ROW-1)
    {
      // LAST_ROW-1, minus jeste radka nahore
      if ( scroll < (count-(LAST_ROW-1)) )
        g_segment_display.status|= DOWN_ARROW;// LAST_ROW-1, minus jeste radka nahore
      else
        g_segment_display.status&=~DOWN_ARROW;
    }
    #ifdef CONTROL_OUTPUT
      if ( ControlOutputIsOn )
      {}
    #endif
  }
  break;

  case 4/*Bool*/:
  {
    Write     ( 0,0, ReadStringFromFlash( g_value[0].tex+LANGUAGE_OFFSET, g_s ));
    if ( g_value[0].val == 0 )
    {
      WriteImage(34, 3, IMG_CROSH_IN_WIN);
      WriteImage(84, 3, IMG_CHACK);
    }
    else
    {
      WriteImage(34, 3, IMG_CROSH);
      WriteImage(84, 3, IMG_CHACK_IN_WIN);
    }

    WriteLastRow( BUTTON_ESC | BUTTON_OK );
    #ifdef CONTROL_OUTPUT
      if ( ControlOutputIsOn )
      {}
    #endif
  }
  break;

  case 5/*Time*/:
    Write     ( 0, 0, ReadStringFromFlash( g_value[0].tex+LANGUAGE_OFFSET, g_s ));
    {
      #define TIME_X_START (50) // pozice zacatku textu
      #define TIME_LINE    (4)  // radka, na kterou se pise
      #define NUMBER_WIDTH (5)  // sirka vsech pismenek
      #define SPACE_WIDTH  (2)  // sirka mezer
      #define CHAR_WIDTH   (NUMBER_WIDTH+SPACE_WIDTH) // sirka znaku vcetne mezery
      char str[2] = "";
      str[0] = g_value[0].valchar[0];
      Write( TIME_X_START+0*CHAR_WIDTH, TIME_LINE, str );
      str[0] = g_value[0].valchar[1];
      Write( TIME_X_START+1*CHAR_WIDTH, TIME_LINE, str );
      str[0] = ':';
      Write( TIME_X_START+2*CHAR_WIDTH+3, TIME_LINE, str );
      str[0] = g_value[0].valchar[2];
      Write( TIME_X_START+3*CHAR_WIDTH, TIME_LINE, str );
      str[0] = g_value[0].valchar[3];
      Write( TIME_X_START+4*CHAR_WIDTH, TIME_LINE, str );
      switch (g_scroll)
      {
        case 0 :
          InvertLine( TIME_X_START +0*CHAR_WIDTH -1, TIME_LINE, NUMBER_WIDTH+2 ); // Zabarvi editovane cislo. -1, +2 - zajistuje zabarveni jednoho sloupce vlevo pred cislem a 2=celkovi pocet zabarvenych sloupcu (pred za)
          break;
        case 1 :
          InvertLine( TIME_X_START +1*CHAR_WIDTH -1, TIME_LINE, NUMBER_WIDTH+2 ); // Zabarvi editovane cislo. -1, +2 - zajistuje zabarveni jednoho sloupce vlevo pred cislem a 2=celkovi pocet zabarvenych sloupcu (pred za)
          break;
        case 2 :
          InvertLine( TIME_X_START +3*CHAR_WIDTH -1, TIME_LINE, NUMBER_WIDTH+2 ); // Zabarvi editovane cislo. -1, +2 - zajistuje zabarveni jednoho sloupce vlevo pred cislem a 2=celkovi pocet zabarvenych sloupcu (pred za)
          break;
        case 3 :
          InvertLine( TIME_X_START +4*CHAR_WIDTH -1, TIME_LINE, NUMBER_WIDTH+2 ); // Zabarvi editovane cislo. -1, +2 - zajistuje zabarveni jednoho sloupce vlevo pred cislem a 2=celkovi pocet zabarvenych sloupcu (pred za)
          break;
      }
    }
    WriteLastRow( BUTTON_ESC | BUTTON_OK );
    break;

  case 6/*Date*/:
    Write     ( 0, 0, ReadStringFromFlash( g_value[0].tex+LANGUAGE_OFFSET, g_s ));
    {
      #define DATE_X_START (40) // pozice zacatku textu
      #define DATE_LINE    (4)  // radka, na kterou se pise
      #define NUMBER_WIDTH (5)  // sirka vsech pismenek
      #define SPACE_WIDTH  (2)  // sirka mezer
      #define CHAR_WIDTH   (NUMBER_WIDTH+SPACE_WIDTH) // sirka znaku vcetne mezery
      char str[2] = "";
      str[0] = g_value[0].valchar[0];
      Write( DATE_X_START+0*CHAR_WIDTH, DATE_LINE, str );
      str[0] = g_value[0].valchar[1];
      Write( DATE_X_START+1*CHAR_WIDTH, DATE_LINE, str );
      str[0] = '\\';
      Write( DATE_X_START+2*CHAR_WIDTH-1, DATE_LINE, str );
      str[0] = g_value[0].valchar[2];
      Write( DATE_X_START+3*CHAR_WIDTH, DATE_LINE, str );
      str[0] = g_value[0].valchar[3];
      Write( DATE_X_START+4*CHAR_WIDTH, DATE_LINE, str );
      str[0] = '\\';
      Write( DATE_X_START+5*CHAR_WIDTH-1, DATE_LINE, str );
      str[0] = g_value[0].valchar[4];
      Write( DATE_X_START+6*CHAR_WIDTH, DATE_LINE, str );
      str[0] = g_value[0].valchar[5];
      Write( DATE_X_START+7*CHAR_WIDTH, DATE_LINE, str );
      str[0] = g_value[0].valchar[6];
      Write( DATE_X_START+8*CHAR_WIDTH, DATE_LINE, str );
      str[0] = g_value[0].valchar[7];
      Write( DATE_X_START+9*CHAR_WIDTH, DATE_LINE, str );
      switch (g_scroll)
      {
        case 0 :
          InvertLine( DATE_X_START +0*CHAR_WIDTH -1, DATE_LINE, NUMBER_WIDTH+2 ); // Zabarvi editovane cislo. -1, +2 - zajistuje zabarveni jednoho sloupce vlevo pred cislem a 2=celkovi pocet zabarvenych sloupcu (pred za)
          break;
        case 1 :
          InvertLine( DATE_X_START +1*CHAR_WIDTH -1, DATE_LINE, NUMBER_WIDTH+2 ); // Zabarvi editovane cislo. -1, +2 - zajistuje zabarveni jednoho sloupce vlevo pred cislem a 2=celkovi pocet zabarvenych sloupcu (pred za)
          break;
        case 2 :
          InvertLine( DATE_X_START +3*CHAR_WIDTH -1, DATE_LINE, NUMBER_WIDTH+2 ); // Zabarvi editovane cislo. -1, +2 - zajistuje zabarveni jednoho sloupce vlevo pred cislem a 2=celkovi pocet zabarvenych sloupcu (pred za)
          break;
        case 3 :
          InvertLine( DATE_X_START +4*CHAR_WIDTH -1, DATE_LINE, NUMBER_WIDTH+2 ); // Zabarvi editovane cislo. -1, +2 - zajistuje zabarveni jednoho sloupce vlevo pred cislem a 2=celkovi pocet zabarvenych sloupcu (pred za)
          break;
        case 4 :
          InvertLine( DATE_X_START +6*CHAR_WIDTH -1, DATE_LINE, NUMBER_WIDTH+2 ); // Zabarvi editovane cislo. -1, +2 - zajistuje zabarveni jednoho sloupce vlevo pred cislem a 2=celkovi pocet zabarvenych sloupcu (pred za)
          break;
        case 5 :
          InvertLine( DATE_X_START +7*CHAR_WIDTH -1, DATE_LINE, NUMBER_WIDTH+2 ); // Zabarvi editovane cislo. -1, +2 - zajistuje zabarveni jednoho sloupce vlevo pred cislem a 2=celkovi pocet zabarvenych sloupcu (pred za)
          break;
        case 6 :
          InvertLine( DATE_X_START +8*CHAR_WIDTH -1, DATE_LINE, NUMBER_WIDTH+2 ); // Zabarvi editovane cislo. -1, +2 - zajistuje zabarveni jednoho sloupce vlevo pred cislem a 2=celkovi pocet zabarvenych sloupcu (pred za)
          break;
        case 7 :
          InvertLine( DATE_X_START +9*CHAR_WIDTH -1, DATE_LINE, NUMBER_WIDTH+2 ); // Zabarvi editovane cislo. -1, +2 - zajistuje zabarveni jednoho sloupce vlevo pred cislem a 2=celkovi pocet zabarvenych sloupcu (pred za)
          break;
        }
    }
    WriteLastRow( BUTTON_ESC | BUTTON_OK );
    break;

  case 7/*CalW*/:
    Write      (0,0,"CalW");
    break;

  case 8/*Pasw*/:
      Write     ( 0,0, ReadStringFromFlash( g_value[0].tex+LANGUAGE_OFFSET, g_s ));
      WriteCenter( 4, (char*)g_value[0].valchar );
      WriteLastRow( BUTTON_ESC | BUTTON_OK );
      {
        // tento usek zjistuje, ktera cislice ma byt zabarvena, a zabarvi ji.
        int j     = 0;
        int Where = 0;
        while (j!=g_scroll)
        {
          Where+=(GetCharacterWidth(g_value[0].valchar[j]));
          j+=1;
        }
        InvertLine( CENTR((char*)g_value[0].valchar) + Where -1, 4, (GetCharacterWidth(g_value[0].valchar[j])) +1 ); // Zabarvi editovane cislo. -1, +1 - zajistuje zabarveni jednoho sloupce vlevo pred cislem
      }
      #ifdef CONTROL_OUTPUT
        if ( ControlOutputIsOn )
        {}
      #endif
      break;

  case 9/*Func*/:
    switch (g_value[0].val)
    {
      case 0:
        /* vykresleni infodatumu */
        Write     ( 0, 0, ReadStringFromFlash( g_value[0].tex+LANGUAGE_OFFSET, g_s ));
        {
          char str[12];
          sprintf(str, "%02d\\%02d\\%04d", DateTime.day, DateTime.month, DateTime.year);
          WriteCenter(4, str);
        }
        WriteLastRow( BUTTON_ESC );
        break;
      case 1:
        /* vyrazeno Outputs info */
      case 3:
        /* Statistiky */
      break;
      case 4:
        /* Datum Statistiky */
        // Show
        break;
    }
    break;
  }

  // kdyz je zamnkuta klavesnice vypise se znak zamku na poslednim radku
  if (key_lock)
  {
    char srt[2];
    srt[0] = 21;
    srt[1] = 0;
    Write(20,7,srt);
  }

  //if ( (RAM_Read_Data_Long(CLEAN_STARTNOW)) || ((RAM_Read_Data_Long(CLEAN_POWER)) == 1)) {
  if(flag_cisteni)
  {
    char srt[2];
    srt[0] = 27;
    srt[1] = 0;
    Write(30,7,srt);
  }

  WriteNow();
}

/*-----------------------------------  MENU CAST  ----------------------------------*/
void ShowMenu( void )
{
  // Funkce zobrazuje menu
  //  IN  : g_adr, menu_levels[g_level], g_scroll, g_count
  //  OUT : none
  char g_s1[100];
  unsigned int  actAddr = g_adr;
  unsigned char item    = menu_levels[g_level];
  unsigned char scroll  = menu_levels[g_level]-g_scroll;
  unsigned char cursor  = -1;
  int i,y;

  actAddr += ONE_ITEM_SIZE*scroll;
  i = scroll;                      //posun v menu o scroll polozek dolu

  WriteErase();                    // smazani bufferu;
  y      = 0;
  cursor = 0;

  while ((i<g_count) && (y<=(LAST_ROW-1)))
  {
    // Vypsani textu, (LAST_ROW-1)=na poslednim radku sou tlacitka. Pozor tato hodnota se nastavuje i v control
    Write( 1, y, ReadStringFromFlash((P_menu[(actAddr+TO_TEXT)])+LANGUAGE_OFFSET, g_s1) );
    if (i==item)
    {
      cursor=y;
      //InvertLine( 0, cursor, SizeInPixel(&texty[menu[actAddr+TO_TEXT]+LANGUAGE_OFFSET]) ); // inverze na delku jedne radky
    }

    i += 1;
    y += 1;
    actAddr += ONE_ITEM_SIZE;
  }

  InvertLine( 0, cursor  , LAST_COLUMN ); // Inverze kursoru ( od, line, kolik )
  WriteLastRow( BUTTON_ESC | BUTTON_OK );

  // kdyz je zamnkuta klavesnice vypise se znak zamku na poslednim radku
  if (key_lock)
  {
    char srt[2];
    srt[0] = 21;
    srt[1] = 0;
    Write(20,7,srt);
  }

  //if ( (RAM_Read_Data_Long(CLEAN_STARTNOW)) || ((RAM_Read_Data_Long(CLEAN_POWER)) == 1)) {
  if(flag_cisteni)
  {
    char srt[2];
    srt[0] = 27;
    srt[1] = 0;
    Write(30,7,srt);
  }

  WriteNow  (); // Vypsani obsahu bufferu na obrazovku

  /* Nastaveni sipek v segmentech */
  if (scroll > 0)
    g_segment_display.status|= UP_ARROW;
  else
    g_segment_display.status&=~UP_ARROW;

  if (g_count > LAST_ROW)
  {
    if ( scroll < (g_count-LAST_ROW) )
      g_segment_display.status|= DOWN_ARROW;
    else
      g_segment_display.status&=~DOWN_ARROW;
  }

  #ifdef CONTROL_OUTPUT
    if ( ControlOutputIsOn )
    {
      // Vypis kontrolnich hodnot
      sprintf(g_s, "g_scr%d",g_scroll);
      Write(70,0,g_s);
      WriteNow();
    }
  #endif
}

void control(char key)
// Funkce zpracovava veskere tlacitkove operace, a podle nich provadi zmeny stavu a rezimu
// pracuje v podstate se vsemi stavovimi promennymi
{
  int i,j;
  char g_s1[100];

  switch (g_mode)
  {
    case 0 /* Mereni */ :
      switch (key)
      {
        case 0 /* ENTR */ :
          g_adr               = 0;
          g_level             = 0;
          menu_levels[g_level]= 0;
          g_count             = (P_menu[g_adr]);
          g_scroll            = 0;
          g_mode = (g_count != 0) ? MENU : EDIT;
          control(INIT);
          control(SHOW);
          break;
        case 1 /* ESC  */ : // None
          break;
        case 2 /*  UP  */ :
          if (RAM_Read_Data_Long(SERVICE_MODE) == 0)
          {
              ShiftForwardDispServiceInfoPage();
          }
          else
          {
              ShiftForwardDispMeasurement();
              RAM_Write_Data_Long(g_measurement.act_rot_meas, ACTUAL_TOTALIZER);
          }
          control(SHOW);
          break;
        case 3 /* DOWN */ :
          if (RAM_Read_Data_Long(SERVICE_MODE) == 0)
          {
              ShiftBackDispServiceInfoPage();
          }
          else
          {
              ShiftBackDispMeasurement();
              RAM_Write_Data_Long(g_measurement.act_rot_meas, ACTUAL_TOTALIZER);
          }
          control(SHOW);
          break;
        case 4 /* LEFT */ :
          {
            if (RAM_Read_Data_Long(POINT_UP)>3)
                RAM_Write_Data_Long(3, POINT_UP);

            if (RAM_Read_Data_Long(POINT_UP)>0) // pokud je zobrazene nejake desetinne misto, tak jedno desetinne misto uber.
              RAM_Write_Data_Long(RAM_Read_Data_Long(POINT_UP)-1, POINT_UP);
          }
          control(SHOW);
          break;
        case 5 /* RIGH */ :
          {
            if (RAM_Read_Data_Long(POINT_UP)>3)
                RAM_Write_Data_Long(3, POINT_UP);

            if (RAM_Read_Data_Long(POINT_UP)<3) // pokud je mene jak 3 desetinne mista tak zobrazim o jedno misto navic
              RAM_Write_Data_Long(RAM_Read_Data_Long(POINT_UP)+1, POINT_UP);
          }
          control(SHOW);
          break;
        case 8 /* INIT */ : // None
          break;
        case 9 /* SHOW */ :
          ShowMeasurement();
          break;
      }     //end_key_Mereni
      break;//end_key_Mereni
    case 1 /* Menu   */ :
      switch (key)
      {
        case 0 /* ENTR */ :
          g_adr    = (P_menu[(g_adr+TO_FORWARD+menu_levels[g_level]*ONE_ITEM_SIZE)]);//Wrati FWD
          g_count  = (P_menu[g_adr]);
          g_level++;
          menu_levels[g_level]  = 0;
          g_scroll = 0;
          g_mode = ( g_count!=0 ) ? MENU : EDIT;
          /* Vypnuti sipek v segment disp. */
          g_segment_display.status&=~UP_ARROW;
          g_segment_display.status&=~DOWN_ARROW;
          control(INIT);
          control(SHOW);
          break;
        case 1 /* ESC  */ :
          if (g_adr>0)
          {
            //nenasleduje rezime mereni
            g_adr    = (P_menu[(g_adr+TO_BACK)]);// Vrati Back
            g_count  = (P_menu[g_adr]);
            menu_levels[g_level] = 0;
            g_level--;
            // pozn. nastaveni scrollu je v init. Kvuli tomu, ze je nutne nastavovat scroll i z editu
            g_mode = ( g_count!=0 ) ? MENU : EDIT;
          }
          else
          {
            g_mode = MEASUREMENT; // Mereni
          }
          /* Vypnuti sipek v segment disp. */
          g_segment_display.status&=~UP_ARROW;
          g_segment_display.status&=~DOWN_ARROW;
          control(INIT);
          control(SHOW);
          break;
        case 2 /*  UP  */ :
          if (menu_levels[g_level]>0)
          {
            menu_levels[g_level]--;
            if (g_scroll!=0)
              g_scroll--;
          }
          control(SHOW);
          break;
        case 3 /* DOWN */ :
          if (menu_levels[g_level]<(g_count-1))
          {
            menu_levels[g_level]++;
            if (g_scroll<LAST_ROW-1)
              g_scroll++; // LAST_ROW-1 , na spodni radce je Ok a Esc. Pozor tato hodnota se nastavuje i v showmenu
          }
          control(SHOW);
          break;
        case 4 /* LEFT */ : // None
          break;
        case 5 /* RIGH */ : // None
          break;
        case 8 /* INIT */ :
          g_scroll = menu_levels[g_level];
          if (menu_levels[g_level]>(LAST_ROW-1))
            g_scroll = (LAST_ROW -1);
          break;
        case 9 /* SHOW */ :
          ShowMenu();
          break;
      }      //end_key_menu
      break; //end_key_menu
    case 2 /* Editace */:
      switch (key)
      {
        case 0 /* ENTR */ :
          switch (g_edittyp)
          {
            case 0/*Info*/:
              /* Nic */
              break;
            case 1/*Edit*/:
              j = 0;  // j : 0=OK, 1=bad min, 2, bad max
              i = -1; // -1 = Pri prvnim pruchodu potrebuji 0;
              do
              {
                i++;
                if ( strcmp( (char*)g_value[i].minchar, (char*)g_value[i].valchar ) > 0 )
                {
                  // nastavovana hodnota nespada do minima
                  j = 1;
                }
                if ( strcmp( (char*)g_value[i].maxchar, (char*)g_value[i].valchar ) < 0 )
                {
                  // nastavovana hodnota nespada do maxima
                  j = 2;
                }
              }
              while ( (j == 0)&&( i!=0) ); // 3 = pocet hodnot ktere testujeme -1. U Edit 0, u 4Win 3
              switch (j)
              {
                case 0 :  // Test ptobehl v poradku
                  /* Ulozeni hodnot */
                  for (i=0; i<=3; i++)
                    g_value[i].val = PointStrToLong( (char*)g_value[i].valchar );
                  WriteValues();    // Zapise hodnoty do flash
                  /* Pruchod zpet */
                  if (g_adr>0)
                  {
                    // NEnasleduje rezim mereni
                    g_adr    = (P_menu[(g_adr+TO_BACK)]);// Vrati Back
                    g_count  = (P_menu[g_adr]);
                    menu_levels[g_level] = 0;
                    g_level--;
                    g_scroll = menu_levels[g_level];
                    if ( menu_levels[g_level] > LAST_ROW )
                      g_scroll = LAST_ROW;
                    g_mode = (g_count!=0) ? MENU : EDIT; // Menu nebo Edit
                  }
                  else
                    g_mode = MEASUREMENT;   // Mereni
                  /* Provedeni akce */
                  EditDoFunction(F_OUT, g_fun);
                  break;
                case 1 : // Prekrocena minimalni hodnota
                {
                  char str[60];
                  #ifndef TEST_RUNNING
                    ReadStringFromFlash( mess_MinAllowedValueIs+LANGUAGE_OFFSET, g_s1);
                  #endif
                  sprintf(str, (char*)g_s1, g_value[i].minchar);
                  ShowStrMessage(str);
                }
                break;
                case 2 : // Prekrocena maximalni hodnota
                {
                  char str[60];
                  #ifndef TEST_RUNNING
                    ReadStringFromFlash( mess_MaxAllowedValueIs+LANGUAGE_OFFSET, g_s1 );
                  #endif
                  sprintf(str, (char*)g_s1, g_value[i].maxchar);
                  ShowStrMessage(str);
                }
                break;
              } // end switch
              control(INIT);
              control(SHOW);
              break; // end Edit
            case 2/*4win*/:
              j = 0;  // j : 0=OK, 1=bad min, 2, bad max
              i = -1; // -1 = Pri prvnim pruchodu potrebuji 0;
              do
              {
                  i++;
                  if ( strcmp( (char*)g_value[i].minchar, (char*)g_value[i].valchar ) > 0 )
                  {
                      // nastavovana hodnota nespada do minima
                      j = 1;
                  }
                  if ( strcmp( (char*)g_value[i].maxchar, (char*)g_value[i].valchar ) < 0 )
                  {
                      // nastavovana hodnota nespada do maxima
                      j = 2;
                  }
              }
              while ( (j == 0)&&( i!=3) ); // 3 = pocet hodnot ktere testujeme -1. U Edit 0, u 4Win 3
              switch (j)
              {
                case 0 :  // Test ptobehl v poradku
                  /* Ulozeni hodnot */
                  for (i=0; i<=3; i++)
                      g_value[i].val = PointStrToLong( (char*)g_value[i].valchar );
                  WriteValues();    // Zapise hodnoty do flash
                  /* Pruchod zpet */
                  if (g_adr>0)
                  {
                    // NEnasleduje rezim mereni
                    g_adr    = (P_menu[(g_adr+TO_BACK)]);// Vrati Back
                    g_count  = (P_menu[g_adr]);
                    menu_levels[g_level] = 0;
                    g_level--;
                    g_scroll = menu_levels[g_level];
                    if ( menu_levels[g_level] > LAST_ROW )
                      g_scroll = LAST_ROW;
                    g_mode = (g_count!=0) ? MENU : EDIT; // Menu nebo Edit
                  }
                  else
                    g_mode = MEASUREMENT;   // Mereni
                  /* Provedeni akce */
                  EditDoFunction(F_OUT, g_fun);
                  break;
                case 1 : // Prekrocena minimalni hodnota
                {
                  char str[100];
                  #ifndef TEST_RUNNING
                    ReadStringFromFlash( mess_MinAllowedValueIsFwin+LANGUAGE_OFFSET, g_s1 );
                  #endif
                  sprintf(str, (char*)g_s1, i+1, g_value[i].minchar);
                  ShowStrMessage(str);
                }
                break;
                case 2 : // Prekrocena maximalni hodnota
                {
                  char str[100];
                  #ifndef TEST_RUNNING
                    ReadStringFromFlash( mess_MaxAllowedValueIsFwin+LANGUAGE_OFFSET, g_s1 );
                  #endif
                  sprintf(str, (char*)g_s1, i+1, g_value[i].maxchar);
                  ShowStrMessage(str);
                }
                break;
              } // end switch
              control(INIT);
              control(SHOW);
              break; // end 4Win
            case 3/*Isel*/:
              g_value[0].val=g_value[3].val;
              WriteValues();    // Zapise hodnoty do flash
              /* Provedeni akce */
              //EditDoFunction(F_OUT); // Zde se neprovadi. U iselektu se po Entru nevyskakuje z okna. teoreticky by se ale i tak mela akce provest. Pokud to bude nekdy potreba muze se to odkomentovat
              control(SHOW);
              break;
            case 4/*Bool*/:
              InitAndShowConfirmMessage(&P_texty[g_value[0].tex+LANGUAGE_OFFSET], WriteBoolValuesConfirmed);
              control(INIT);
              control(SHOW);
              break;
            case 5/*Time*/:
              // Pruchod zpet
              if (g_adr>0)
              {
                // NEnasleduje rezim mereni
                g_adr    = (P_menu[(g_adr+TO_BACK)]);// Vrati Back
                g_count  = (P_menu[g_adr]);
                menu_levels[g_level] = 0;
                g_level--;
                g_scroll = menu_levels[g_level];
                if ( menu_levels[g_level] > LAST_ROW )
                    g_scroll = LAST_ROW;
                g_mode = (g_count!=0) ? MENU : EDIT; // Menu nebo Edit
              }
              else
                g_mode = MEASUREMENT;   // Mereni
              /* Provedeni akce */
              EditDoFunction(F_OUT, g_fun);
              control(INIT);
              control(SHOW);
              break;
            case 6/*Date*/:
              // Pruchod zpet
              if (g_adr>0)
              {
                // NEnasleduje rezim mereni
                g_adr    = (P_menu[(g_adr+TO_BACK)]);// Vrati Back
                g_count  = (P_menu[g_adr]);
                menu_levels[g_level] = 0;
                g_level--;
                g_scroll = menu_levels[g_level];
                if ( menu_levels[g_level] > LAST_ROW )
                  g_scroll = LAST_ROW;
                g_mode = (g_count!=0) ? MENU : EDIT; // Menu nebo Edit
              }
              else
                g_mode = MEASUREMENT;   // Mereni
              /* Provedeni akce */
              EditDoFunction(F_OUT, g_fun);
              control(INIT);
              control(SHOW);
              break;
            case 7/*CalW*/:
              // ...
              EditDoFunction(F_OUT, g_fun); // Provedeni akce prislusne ke konkretnimu oknu.
              // ...
              break;
            case 8/*Pasw*/:
              // V soucasnosti je vyhozen test na min a max. Neni problem ho nakoirovat z Edit a nasledujici kod nacpat do jeckce case 0 :
              // Pruchod dopredu = rozdil oproti editu
              if ( PointStrToLong( (char*)g_value[0].valchar ) == g_value[1].val )
              {
                // kontrola na spravnost hesla
                g_adr    = (P_menu[(g_adr+TO_FORWARD)]);  // DOPREDU
                g_count  = (P_menu[g_adr]);
                g_level++;
                g_scroll = 0;
                g_mode = (g_count!=0) ? MENU : EDIT; // Menu nebo Edit
                /* Provedeni akce */
                EditDoFunction(F_OUT, g_fun);
              }
              else
              {
                // Fuck Off
                #ifndef TEST_RUNNING
                  InitAndShowTextMessage(mess_WrongPasword);
                #endif
              }
              control(INIT);
              control(SHOW);
              break;
            case 9/*Func*/:
              switch (g_value[0].val)
              {
                case 0 :
                  /* Vypsani infodatumu */
                  // Nic
                  break;
                case 1 :
                  /* byvaly Outputs Info */
                  // Nic
                  break;
                case 3 :
                  /* Statistiky */
                  break;
                case 4 :
                  /* Datum Statistik */
                  break;
              }
              break;
          }         // switch(edittyp)
          break; // switch(key)_ENTR
      case 1 /* ESC  */ :
          switch (g_edittyp)
          {
          case 3/*Isel*/:
              /* Vypnuti sipek v segment disp. */
              g_segment_display.status&=~UP_ARROW;
              g_segment_display.status&=~DOWN_ARROW;
              /* Po vypnuti sipek v segmentech se provede kod normalniho esc */
              if (g_adr>0)
              {
                  //nenasleduje rezim mereni
                  g_adr    = (P_menu[(g_adr+TO_BACK)]);// Vrati Back
                  g_count  = (P_menu[g_adr]);
                  menu_levels[g_level] = 0;
                  g_level--;
                  g_scroll = menu_levels[g_level];
                  if (menu_levels[g_level]>LAST_ROW)
                      g_scroll = LAST_ROW;
                  g_mode = ( g_count!=0 )? MENU : EDIT ;
              }
              else
              {
                  g_mode = MEASUREMENT; // Mereni
              }
              /* Provedeni akce */
              control(INIT);
              control(SHOW);
              EditDoFunction(F_OUT, g_fun);
              break;
          case 7/*CalW*/:
              break;
          case 9/*Func*/:
              switch (g_value[0].val)
              {
                  // Pro Funkcni okna musi byt samostatne exit funkce, i kdyz je treba kod stejny. Jde o to, ze nemusi byt pro vsechny
              default : /* Vypsani infodatumu, Outputs Info, statistiky, datum statistik */
                  /* Vypnuti sipek v segment disp. */
                  g_segment_display.status&=~UP_ARROW;
                  g_segment_display.status&=~DOWN_ARROW;
                  if (g_adr>0)
                  {
                      //nenasleduje rezim mereni
                      g_adr    = (P_menu[(g_adr+TO_BACK)]);// Vrati Back
                      g_count  = (P_menu[g_adr]);
                      menu_levels[g_level] = 0;
                      g_level--;
                      g_scroll = menu_levels[g_level];
                      if (menu_levels[g_level]>LAST_ROW)
                          g_scroll = LAST_ROW;
                      g_mode = ( g_count!=0 )? MENU : EDIT ;
                  }
                  else
                  {
                      g_mode = MEASUREMENT; // Mereni
                  }
                  control(INIT);
                  control(SHOW);
                  break;
              }
              break;
          default       :
              /* Info, Edit, 4win, Bool, Pasw, Time, Date */
              if (g_adr>0)
              {
                  //nenasleduje rezim mereni
                  g_adr    = (P_menu[(g_adr+TO_BACK)]);// Vrati Back
                  g_count  = (P_menu[g_adr]);
                  menu_levels[g_level] = 0;
                  g_level--;
                  g_scroll = menu_levels[g_level];
                  if (menu_levels[g_level]>LAST_ROW)
                      g_scroll = LAST_ROW;
                  g_mode = ( g_count!=0 )? MENU : EDIT ;
              }
              else
              {
                  g_mode = MEASUREMENT; // Mereni
              }
              control(INIT);
              control(SHOW);
              // pozn. pokud udelas zmenu v ESC, nezapomen ji provest i pro Isel a funkcni okna
              break;
          }         // switch(edittyp)
          break; // switch(key)_ESC
      case 2 /*  UP  */ :
          switch (g_edittyp)
          {
          case 0/*Info*/:
              /* none */
              break;
          case 1/*Edit*/:
              g_value[0].valchar[g_scroll] = ( g_value[0].valchar[g_scroll]<'9' ) ? g_value[0].valchar[g_scroll]+1 : '0';
              control(SHOW);
              break;
          case 2/*4win*/:
              g_value[ActValue()].valchar[KursorPositionInValue()] = ( g_value[ActValue()].valchar[KursorPositionInValue()]<'9' ) ? g_value[ActValue()].valchar[KursorPositionInValue()]+1 : '0';
              control(SHOW);
              break;
          case 3/*Isel*/:
              // g_scroll - radek, na kterem stoji kursor (0-5), kdyz k nenu date + 1, budete mit skutecnej radek(1. radek je popisek)
              // g_value[0].val - Cislo polozky ktera je oznacena sipkou(0..n-1)
              // g_value[1].val - Pocet polozek (1..n)
              // g_value[3].val - Cislo polozky, na ktere stoji kursor  (0..n-1)
              if (g_value[3].val>0)
              {
                  g_value[3].val--;
                  if (g_scroll!=0)
                      g_scroll--;
              }
              control(SHOW);
              break;
          case 4/*Bool*/:
              /* none */
              break;
          case 5/*Time*/:
              switch (g_scroll)
              {
                  // g_scroll urcuje, ktera cislice je editovana (0=desitky hodin, 3=jednotky minut)
              case 0 :
                  if (g_value[0].valchar[1]<'4')
                  {
                      g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]<'2') ? (g_value[0].valchar[g_scroll]+1) : '0';
                  }
                  else
                  {
                      g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]<'1') ? (g_value[0].valchar[g_scroll]+1) : '0';
                  }
                  break;
              case 1 :
                  if (g_value[0].valchar[0]<'2')
                  {
                      g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]<'9') ? (g_value[0].valchar[g_scroll]+1) : '0';
                  }
                  else
                  {
                      g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]<'3') ? (g_value[0].valchar[g_scroll]+1) : '0';
                  }
                  break;
              case 2 :
                  g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]<'5') ? (g_value[0].valchar[g_scroll]+1) : '0';
                  break;
              case 3 :
                  g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]<'9') ? (g_value[0].valchar[g_scroll]+1) : '0';
                  break;
              }
              control(SHOW);
              break;
          case 6/*Date*/:
          {
              char max   = 0;
              char day   = (g_value[0].valchar[0]-'0')*10 + (g_value[0].valchar[1]-'0');
              char month = (g_value[0].valchar[2]-'0')*10 + (g_value[0].valchar[3]-'0');
              int year  = (g_value[0].valchar[4]-'0')*1000 + (g_value[0].valchar[5]-'0')*100 + (g_value[0].valchar[6]-'0')*10 + (g_value[0].valchar[7]-'0');
              switch (month)
              {
              case 01 :
                  max=31;
                  break;
              case 02 :
                  max=((year % 4)==0) ? 29 : 28;
                  break;
              case 03 :
                  max=31;
                  break;
              case 04 :
                  max=30;
                  break;
              case 05 :
                  max=31;
                  break;
              case 06 :
                  max=30;
                  break;
              case 07 :
                  max=31;
                  break;
              case  8 :
                  max=31;
                  break;
              case  9 :
                  max=30;
                  break;
              case 10 :
                  max=31;
                  break;
              case 11 :
                  max=30;
                  break;
              case 12 :
                  max=31;
                  break;
              }
              switch (g_scroll)
              {
              case 0 :
                  /* den desitky*/
                  if (g_value[0].valchar[1]>'0')
                      g_value[0].valchar[g_scroll] = (((day+10)<=max)&&(g_value[0].valchar[g_scroll]<'3')) ? (g_value[0].valchar[g_scroll]+1) : '0';
                  else
                      g_value[0].valchar[g_scroll] = (((day+10)<=max)&&(g_value[0].valchar[g_scroll]<'3')) ? (g_value[0].valchar[g_scroll]+1) : '1';
                  break;
              case 1 :
                  /* den jednotky*/
                  if (g_value[0].valchar[0]>'0')
                      g_value[0].valchar[g_scroll] = (((day+1)<=max)&&(g_value[0].valchar[g_scroll]<'9')) ? (g_value[0].valchar[g_scroll]+1) : '0';
                  else
                      g_value[0].valchar[g_scroll] = (((day+1)<=max)&&(g_value[0].valchar[g_scroll]<'9')) ? (g_value[0].valchar[g_scroll]+1) : '1';
                  break;
              case 2 :
                  /* mesic desitky */
                  /*if (g_value[0].valchar[3]<'3') {
                    g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]<'1') ? (g_value[0].valchar[g_scroll]+1) : '0';
                  }
                  else {
                    g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]<'0') ? (g_value[0].valchar[g_scroll]+1) : '1';
                  }*/
                  switch (month)
                  {
                      // desitky, nahoru
                  case  1:
                      g_value[0].valchar[g_scroll] = '1';
                      break;
                  case  2:
                      g_value[0].valchar[g_scroll] = '1';
                      break;
                  case  3:
                      g_value[0].valchar[g_scroll] = '0';
                      break;
                  case  4:
                      g_value[0].valchar[g_scroll] = '0';
                      break;
                  case  5:
                      g_value[0].valchar[g_scroll] = '0';
                      break;
                  case  6:
                      g_value[0].valchar[g_scroll] = '0';
                      break;
                  case  7:
                      g_value[0].valchar[g_scroll] = '0';
                      break;
                  case  8:
                      g_value[0].valchar[g_scroll] = '0';
                      break;
                  case  9:
                      g_value[0].valchar[g_scroll] = '0';
                      break;
                  case 10:
                      g_value[0].valchar[g_scroll] = '1';
                      break;
                  case 11:
                      g_value[0].valchar[g_scroll] = '0';
                      break;
                  case 12:
                      g_value[0].valchar[g_scroll] = '0';
                      break;
                  }
                  g_value[0].valchar[0] = '0';
                  g_value[0].valchar[1] = '1';
                  break;
              case 3 :
                  /* mesic jednotky */
                  /*if (g_value[0].valchar[2]<'1') {
                    g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]<'9') ? (g_value[0].valchar[g_scroll]+1) : '1';
                  }
                  else {
                    g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]<'2') ? (g_value[0].valchar[g_scroll]+1) : '0';
                  }*/
                  switch (month)
                  {
                      // jednotky, nahoru
                  case  1:
                      g_value[0].valchar[g_scroll] = '2';
                      break;
                  case  2:
                      g_value[0].valchar[g_scroll] = '3';
                      break;
                  case  3:
                      g_value[0].valchar[g_scroll] = '4';
                      break;
                  case  4:
                      g_value[0].valchar[g_scroll] = '5';
                      break;
                  case  5:
                      g_value[0].valchar[g_scroll] = '6';
                      break;
                  case  6:
                      g_value[0].valchar[g_scroll] = '7';
                      break;
                  case  7:
                      g_value[0].valchar[g_scroll] = '8';
                      break;
                  case  8:
                      g_value[0].valchar[g_scroll] = '9';
                      break;
                  case  9:
                      g_value[0].valchar[g_scroll] = '1';
                      break;
                  case 10:
                      g_value[0].valchar[g_scroll] = '1';
                      break;
                  case 11:
                      g_value[0].valchar[g_scroll] = '2';
                      break;
                  case 12:
                      g_value[0].valchar[g_scroll] = '0';
                      break;
                  }
                  g_value[0].valchar[0] = '0';
                  g_value[0].valchar[1] = '1';
                  break;
              case 4 :
                  /* rok tisice */
                  g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]<'9') ? (g_value[0].valchar[g_scroll]+1) : '0';
                  g_value[0].valchar[0] = '0';
                  g_value[0].valchar[1] = '1';
                  g_value[0].valchar[2] = '0';
                  g_value[0].valchar[3] = '1';
                  break;
              case 5 :
                  /* rok stovky */
                  g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]<'9') ? (g_value[0].valchar[g_scroll]+1) : '0';
                  g_value[0].valchar[0] = '0';
                  g_value[0].valchar[1] = '1';
                  g_value[0].valchar[2] = '0';
                  g_value[0].valchar[3] = '1';
                  break;
              case 6 :
                  /* rok desitky */
                  g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]<'9') ? (g_value[0].valchar[g_scroll]+1) : '0';
                  g_value[0].valchar[0] = '0';
                  g_value[0].valchar[1] = '1';
                  g_value[0].valchar[2] = '0';
                  g_value[0].valchar[3] = '1';
                  break;
              case 7 :
                  /* rok jednotky */
                  g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]<'9') ? (g_value[0].valchar[g_scroll]+1) : '0';
                  g_value[0].valchar[0] = '0';
                  g_value[0].valchar[1] = '1';
                  g_value[0].valchar[2] = '0';
                  g_value[0].valchar[3] = '1';
                  break;
              }
          }
          control(SHOW);
          break;
          case 7/*CalW*/:
              break;
          case 8/*Pasw*/:
              g_value[0].valchar[g_scroll] = ( g_value[0].valchar[g_scroll]<'9' ) ? g_value[0].valchar[g_scroll]+1 : '0';
              control(SHOW);
              break;
          case 9/*Func*/:
              switch (g_value[0].val)
              {
                  case 0 : /* Vypsani infodatumu */
                      // Nic
                      break;
                  case 1 : /* byvaly Outputs Info */
                      // Nic
                      break;
                  case 3 : /* Statistiky */
                  break;
                  case 4 : /* Datum Statistik */
                      // UP - stejny kod jako v oknu datumu
                  break;
              }
              break;
          }         // switch(edittyp)
          break; // switch(key)_UP
      case 3 /* DOWN */ :
          switch (g_edittyp)
          {
            case 0/*Info*/:
              /* none */
              break;
            case 1/*Edit*/:
              g_value[0].valchar[g_scroll] = ( g_value[0].valchar[g_scroll]>'0' ) ? g_value[0].valchar[g_scroll]-1 : '9';
              control(SHOW);
              break;
            case 2/*4win*/:
              g_value[ActValue()].valchar[KursorPositionInValue()] = ( g_value[ActValue()].valchar[KursorPositionInValue()]>'0' ) ? g_value[ActValue()].valchar[KursorPositionInValue()]-1 : '9';
              control(SHOW);
              break;
            case 3/*Isel*/:
              // g_scroll - radek, na kterem stoji kursor (0-5), kdyz k nenu date + 1, budete mit skutecnej radek(1. radek je popisek)
              // g_value[0].val - Cislo polozky ktera je oznacena sipkou(0..n-1)
              // g_value[1].val - Pocet polozek (1..n)
              // g_value[3].val - Cislo polozky, na ktere stoji kursor  (0..n-1)
              if (g_value[3].val<(g_value[1].val-1))
              {
                g_value[3].val++;
                if (g_scroll<(LAST_ROW-2))
                  g_scroll++; // -1, na posledni radce je oznaceni ESC OK
              }
              control(SHOW);
              break;
            case 4/*Bool*/:
              /* none */
              break;
            case 5/*Time*/:
              switch (g_scroll)
              {
                  // g_scroll urcuje, ktera cislice je editovana (0=desitky hodin, 3=jednotky minut)
              case 0 :
                  if (g_value[0].valchar[g_scroll]>'0')
                      g_value[0].valchar[g_scroll]--;
                  else
                      g_value[0].valchar[g_scroll] = (g_value[0].valchar[1]<'4') ? '2' : '1';
                  break;
              case 1 :
                  if (g_value[0].valchar[g_scroll]>'0')
                      g_value[0].valchar[g_scroll]--;
                  else
                      g_value[0].valchar[g_scroll] = (g_value[0].valchar[0]<'2') ? '9' : '3';
                  break;
              case 2 :
                  g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]>'0') ? (g_value[0].valchar[g_scroll]-1) : '5';
                  break;
              case 3 :
                  g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]>'0') ? (g_value[0].valchar[g_scroll]-1) : '9';
                  break;
              }
              control(SHOW);
              break;
          case 6/*Date*/:
          {
              char month = (g_value[0].valchar[2]-'0')*10 + (g_value[0].valchar[3]-'0');
              switch (g_scroll)
              {
              case 0 :
                  /* den */
                  if (g_value[0].valchar[1]=='0')
                      g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]>'1') ? (g_value[0].valchar[g_scroll]-1) : g_value[0].valchar[g_scroll];
                  else
                      g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]>'0') ? (g_value[0].valchar[g_scroll]-1) : g_value[0].valchar[g_scroll];
                  break;
              case 1 :
                  /* den */
                  if (g_value[0].valchar[0]=='0')
                      g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]>'1') ? (g_value[0].valchar[g_scroll]-1) : g_value[0].valchar[g_scroll];
                  else
                      g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]>'0') ? (g_value[0].valchar[g_scroll]-1) : g_value[0].valchar[g_scroll];
                  break;
              case 2 :
                  /* mesic */
                  /*if (g_value[0].valchar[g_scroll]>'0')
                    g_value[0].valchar[g_scroll]--;
                  else
                    g_value[0].valchar[g_scroll] = (g_value[0].valchar[3]<'3') ? '1' : '0';*/
                  switch (month)
                  {
                      // desitky, dolu
                  case  1:
                      g_value[0].valchar[g_scroll] = '1';
                      break;
                  case  2:
                      g_value[0].valchar[g_scroll] = '1';
                      break;
                  case  3:
                      g_value[0].valchar[g_scroll] = '0';
                      break;
                  case  4:
                      g_value[0].valchar[g_scroll] = '0';
                      break;
                  case  5:
                      g_value[0].valchar[g_scroll] = '0';
                      break;
                  case  6:
                      g_value[0].valchar[g_scroll] = '0';
                      break;
                  case  7:
                      g_value[0].valchar[g_scroll] = '0';
                      break;
                  case  8:
                      g_value[0].valchar[g_scroll] = '0';
                      break;
                  case  9:
                      g_value[0].valchar[g_scroll] = '0';
                      break;
                  case 10:
                      g_value[0].valchar[g_scroll] = '1';
                      break;
                  case 11:
                      g_value[0].valchar[g_scroll] = '0';
                      break;
                  case 12:
                      g_value[0].valchar[g_scroll] = '0';
                      break;
                  }
                  g_value[0].valchar[0] = '0';
                  g_value[0].valchar[1] = '1';
                  break;
              case 3 :
                  /* mesic */
                  /*if (g_value[0].valchar[g_scroll]>'1')
                    g_value[0].valchar[g_scroll]--;
                  else
                    g_value[0].valchar[g_scroll] = (g_value[0].valchar[2]<'1') ? '9' : '2';*/
                  switch (month)
                  {
                      // jednotky, dolu
                  case  1:
                      g_value[0].valchar[g_scroll] = '9';
                      break;
                  case  2:
                      g_value[0].valchar[g_scroll] = '1';
                      break;
                  case  3:
                      g_value[0].valchar[g_scroll] = '2';
                      break;
                  case  4:
                      g_value[0].valchar[g_scroll] = '3';
                      break;
                  case  5:
                      g_value[0].valchar[g_scroll] = '4';
                      break;
                  case  6:
                      g_value[0].valchar[g_scroll] = '5';
                      break;
                  case  7:
                      g_value[0].valchar[g_scroll] = '6';
                      break;
                  case  8:
                      g_value[0].valchar[g_scroll] = '7';
                      break;
                  case  9:
                      g_value[0].valchar[g_scroll] = '8';
                      break;
                  case 10:
                      g_value[0].valchar[g_scroll] = '1';
                      break;
                  case 11:
                      g_value[0].valchar[g_scroll] = '0';
                      break;
                  case 12:
                      g_value[0].valchar[g_scroll] = '1';
                      break;
                  }
                  g_value[0].valchar[0] = '0';
                  g_value[0].valchar[1] = '1';
                  break;
              case 4 :
                  /* rok tisice */
                  g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]>'0') ? (g_value[0].valchar[g_scroll]-1) : '9';
                  g_value[0].valchar[0] = '0';
                  g_value[0].valchar[1] = '1';
                  g_value[0].valchar[2] = '0';
                  g_value[0].valchar[3] = '1';
                  break;
              case 5 :
                  /* rok stovky */
                  g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]>'0') ? (g_value[0].valchar[g_scroll]-1) : '9';
                  g_value[0].valchar[0] = '0';
                  g_value[0].valchar[1] = '1';
                  g_value[0].valchar[2] = '0';
                  g_value[0].valchar[3] = '1';
                  break;
              case 6 :
                  /* rok desitky */
                  g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]>'0') ? (g_value[0].valchar[g_scroll]-1) : '9';
                  g_value[0].valchar[0] = '0';
                  g_value[0].valchar[1] = '1';
                  g_value[0].valchar[2] = '0';
                  g_value[0].valchar[3] = '1';
                  break;
              case 7 :
                  /* rok jednotky */
                  g_value[0].valchar[g_scroll] = (g_value[0].valchar[g_scroll]>'0') ? (g_value[0].valchar[g_scroll]-1) : '9';
                  g_value[0].valchar[0] = '0';
                  g_value[0].valchar[1] = '1';
                  g_value[0].valchar[2] = '0';
                  g_value[0].valchar[3] = '1';
                  break;
              }
              control(SHOW);
          }
          break;
          case 7/*CalW*/:
              break;
          case 8/*Pasw*/:
              g_value[0].valchar[g_scroll] = ( g_value[0].valchar[g_scroll]>'0' ) ? g_value[0].valchar[g_scroll]-1 : '9';
              control(SHOW);
              break;
          case 9/*Func*/:
              switch (g_value[0].val)
              {
                  case 0 : /* Vypsani infodatumu */
                      // Nic
                      break;
                  case 1 : /* byvaly Outputs Info */
                      // Nic
                      break;
                  case 3 : /* Statistiky */
                  break;
                  case 4 : /* Datum statistik */
                      // Down - stejny kod jako v oknu datumu
                  break;
              }
              break;
          }         // switch(edittyp)
          break; // switch(key)_DOWN
      case 4 /* LEFT */ :
          switch (g_edittyp)
          {
          case 0/*Info*/:
              /* none */
              break;
          case 1/*Edit*/:
              if (g_scroll>0)
              {
                  g_scroll--;
                  if (g_value[0].valchar[g_scroll]=='.')
                      g_scroll--;
                  control(SHOW);
              }
              break;
          case 2/*4win*/:
              if (g_scroll>0)
              {
                  g_scroll--;
                  if (g_value[ActValue()].valchar[KursorPositionInValue()]=='.')
                      g_scroll--;
                  control(SHOW);
              }
              break;
          case 3/*Isel*/:
              break;
          case 4/*Bool*/:
              g_value[0].val = 0;
              control(SHOW);
              break;
          case 5/*Time*/:
              if (g_scroll>0)
                  g_scroll--;
              control(SHOW);
              break;
          case 6/*Date*/:
              if (g_scroll>0)
                  g_scroll--;
              control(SHOW);
              break;
          case 7/*CalW*/:
              break;
          case 8/*Pasw*/:
              if (g_scroll>0)
              {
                  g_scroll--;
                  if (g_value[0].valchar[g_scroll]=='.')
                      g_scroll--;
                  control(SHOW);
              }
              break;
          case 9/*Func*/:
              switch (g_value[0].val)
              {
              case 0 : /* Vypsani infodatumu */
                  // Nic
                  break;
              case 1 : /* byvale Outputs Info */
                  // Nic
                  break;
              case 3 :
                  /* Statistiky */
                  // nic
                  break;
              case 4 :
                  /* Datum Statistik */
                  // Left - stejny kod jako v oknu datumu
                  break;
              }
              break;
          }         // switch(edittyp)
          break; // switch(key)_LEFT
      case 5 /* RIGH */ :
          switch (g_edittyp)
          {
          case 0/*Info*/:
              /* none */
              break;
          case 1/*Edit*/:
              if ( g_scroll<
                      (
                          g_value[0].dig + g_value[0].dec - 1 +
                          (( g_value[0].dec != 0 ) ? 1 : 0)     // Pokud retezec obsahuje desetinne cisla tak jeste + 1
                      )
                 )
              {
                  g_scroll++;
                  if (g_value[0].valchar[g_scroll]=='.')
                      g_scroll++;
                  control(SHOW);
              }
              break;
          case 2/*4win*/:
              if ( g_scroll<
                    (
                      g_value[0].dig + g_value[0].dec   +
                      g_value[1].dig + g_value[1].dec   +
                      g_value[2].dig + g_value[2].dec   +
                      g_value[3].dig + g_value[3].dec   +
                      (( g_value[0].dec != 0 ) ? 1 : 0) +    // Pokud retezec obsahuje desetinne cisla tak jeste + 1
                      (( g_value[1].dec != 0 ) ? 1 : 0) +    // Pokud retezec obsahuje desetinne cisla tak jeste + 1
                      (( g_value[2].dec != 0 ) ? 1 : 0) +    // Pokud retezec obsahuje desetinne cisla tak jeste + 1
                      (( g_value[3].dec != 0 ) ? 1 : 0) -    // Pokud retezec obsahuje desetinne cisla tak jeste + 1
                      1
                    )
                 )
              {
                g_scroll++;
                if (g_value[ActValue()].valchar[KursorPositionInValue()]=='.')
                  g_scroll++;
                control(SHOW);
              }
              break;
          case 3/*Isel*/:
              break;
          case 4/*Bool*/:
              g_value[0].val = 1 ;
              control(SHOW);
              break;
          case 5/*Time*/:
              if (g_scroll<3)
                  g_scroll++;
              control(SHOW);
              break;
          case 6/*Date*/:
              if (g_scroll<7)
                  g_scroll++;
              control(SHOW);
              break;
          case 7/*CalW*/:
              break;
          case 8/*Pasw*/:
              if ( ( (g_scroll<(g_value[0].dig+g_value[0].dec-1)) && (g_value[0].dec==0) ) || ( (g_scroll<(g_value[0].dig+g_value[0].dec-1+1)) && (g_value[0].dec>0) )  )
              {
                  g_scroll++;
                  if (g_value[0].valchar[g_scroll]=='.')
                      g_scroll++;
                  control(SHOW);
              }
              break;
          case 9/*Func*/:
              switch (g_value[0].val)
              {
              case 0 : /* Vypsani infodatumu */
                  // Nic
                  break;
              case 1 : /* byvale Outputs Info */
                  // Nic
                  break;
              case 3 :
                  /* Statistiky */
                  // nic
                  break;
              case 4 :
                  /* Datum Statistik */
                  // Right - stejny kod jako v oknu datumu
                  break;
              }
              break;
          }         // switch(edittyp)
          break; // switch(key)_RIGHT
      case 8 /* INIT */ :
          EditInit();
          break;
      case 9 /* SHOW */ :
          ShowEdit();
          break;
      }     // end_key_editace
      break;// end_key_editace
  case 3 /* Message*/ :
      switch (key)
      {
      case 0 /* ENTR */ :
          switch (g_message.typ)
          {
          case MSG_OK    :
          case MSG_ERROR :
          case MSG_TEXT  :
          case MSG_CONF  :
              g_mode = g_message.prevmode;
              if (g_mode==MENU)
              {
                  g_scroll = menu_levels[g_level];
                  if (menu_levels[g_level]>(LAST_ROW-1))
                      g_scroll = (LAST_ROW -1);
              }
              control(SHOW); // Bez init
              if (g_message.callback != 0)
                g_message.callback();
              break;
          }
          break;
      case 1 /* ESC  */ :
          switch (g_message.typ)
          {
          case MSG_OK    :
          case MSG_ERROR :
          case MSG_CONF :
              g_mode = g_message.prevmode;
              if (g_mode==MENU)
              {
                  g_scroll = menu_levels[g_level];
                  if (menu_levels[g_level]>(LAST_ROW-1))
                      g_scroll = (LAST_ROW -1);
              }
              control(SHOW); // Bez init
              break;
          case MSG_TEXT  :
              // none
              break;
          }
          break;
      case 2 /*  UP  */ :
          switch (g_message.typ)
          {
          case MSG_OK    :
          case MSG_ERROR :
              g_mode = g_message.prevmode;
              if (g_mode==MENU)
              {
                g_scroll = menu_levels[g_level];
                if (menu_levels[g_level]>(LAST_ROW-1))
                  g_scroll = (LAST_ROW -1);
              }
              control(SHOW); // Bez init
              break;
          case MSG_TEXT  :
          case MSG_CONF  :
              // none
              break;
          }
          break;
      case 3 /* DOWN */ :
        switch (g_message.typ)
        {
          case MSG_OK    :
          case MSG_ERROR :
            g_mode = g_message.prevmode;
            if (g_mode==MENU)
            {
              g_scroll = menu_levels[g_level];
              if (menu_levels[g_level]>(LAST_ROW-1))
                g_scroll = (LAST_ROW -1);
            }
            control(SHOW); // Bez init
            break;
          case MSG_TEXT  :
          case MSG_CONF  :
            // none
            break;
        }
        break;
      case 4 /* LEFT */ :
        switch (g_message.typ)
        {
          case MSG_OK    :
          case MSG_ERROR :
            g_mode = g_message.prevmode;
            if (g_mode==MENU)
            {
              g_scroll = menu_levels[g_level];
              if (menu_levels[g_level]>(LAST_ROW-1))
                g_scroll = (LAST_ROW -1);
            }
            control(SHOW); // Bez init
            break;
          case MSG_TEXT  :
          case MSG_CONF  :
            // none
            break;
        }
        break;
      case 5 /* RIGH */ :
        switch (g_message.typ)
        {
          case MSG_OK    :
          case MSG_ERROR :
            g_mode = g_message.prevmode;
            if (g_mode==MENU)
            {
              g_scroll = menu_levels[g_level];
              if (menu_levels[g_level]>(LAST_ROW-1))
                g_scroll = (LAST_ROW -1);
            }
            control(SHOW); // Bez init
            break;
          case MSG_TEXT  :
          case MSG_CONF  :
            // none
            break;
        }
        break;
      case 8 /* INIT */ :
        switch (g_message.typ)
        {
          case MSG_OK    :
          case MSG_ERROR :
          case MSG_TEXT  :
          case MSG_CONF  :
            // none
            break;
        }
        break;
      case 9 /* SHOW */ :
        switch (g_message.typ)
        {
          case MSG_OK    :
            ShowOkMessage();
            break;
          case MSG_ERROR :
            ShowErrorMessage();
            break;
          case MSG_TEXT  :
            ShowTextMessage(); // Predtim musi byt zprava inicializovana InitAndShowTextMessage(Konstanta);
            break;
          case MSG_CONF  :
            ShowConfirmMessage();
            break;
        }
        break;
      }     //end_key_Mereni
      break;//end_key_Mereni
  } //end_mode
}

// funkce nastavi bity v promenne tlacitko aby se nemusela predelavat obsluha preruseni a vyhodnoceni tlacitek

unsigned long stisk()
{
  unsigned char tlacitko = 0;
  if(IOPIN1 & (1<<0))
    tlacitko |= (1<<0);
  if(IOPIN1 & (1<<1))
    tlacitko |= (1<<1);
  if(IOPIN1 & (1<<4))
    tlacitko |= (1<<2);
  return tlacitko;
}

/*
    Vraci index zaznamu v tabulce P_edit podle adresy hodnoty.
    Pokud neni zaznam nalezen, vraci -1.
*/
int GetPeditTableRecordIndex(unsigned long valueOffset)
{
    unsigned int recIdx = 0;
    unsigned int valueIdx = (unsigned int)((valueOffset - VALUES_OFFSET) / 4);

    while (recIdx < (sizeof_P_edit / sizeof(P_edit[0])))
    {
        // podle typu
        switch (P_edit[recIdx])
        {
            case 0/*Info*/:
                if (P_edit[recIdx+1] == valueIdx)
                    return recIdx;

                recIdx += 7;
                break;

            case 1/*Edit*/:
                if (P_edit[recIdx+1] == valueIdx)
                    return recIdx;

                recIdx += 9;
                break;

            case 2/*4win*/:
                if (P_edit[recIdx+1] == valueIdx)
                    return recIdx;
                if (P_edit[recIdx+2] == valueIdx)
                    return recIdx;
                if (P_edit[recIdx+3] == valueIdx)
                    return recIdx;
                if (P_edit[recIdx+4] == valueIdx)
                    return recIdx;

                recIdx += 18;
                break;

            case 3/*Isel*/:
                if (P_edit[recIdx+1] == valueIdx)
                    return recIdx;

                recIdx += 5 + P_edit[recIdx+2];
                break;

            case 4/*Bool*/:
                if (P_edit[recIdx+1] == valueIdx)
                    return recIdx;

                recIdx += 4;
                break;

            case 5/*Time*/:
                if (P_edit[recIdx+1] == valueIdx)
                    return recIdx;

                recIdx += 4;
                break;

            case 6/*Date*/:
                if (P_edit[recIdx+1] == valueIdx)
                    return recIdx;

                recIdx += 4;
                break;

            case 7/*CalW*/:
                recIdx += 1;
                break;

            case 8/*Pasw*/:
                if (P_edit[recIdx+1] == valueIdx)
                    return recIdx;

                recIdx += 9;
                break;

            case 9/*Func*/:
                recIdx += 4;
                break;

            default:
                return -1;
        }
    }

    return -1;
}

/*
    Vraci index textu v tabulce P_unit podle indexu zaznamu v tabulce P_edit a adresy hodnoty.
*/
unsigned int GetPunitTableStringIndex(int peditTableRecIdx, unsigned long valueOffset)
{
    unsigned int valueIdx = (unsigned int)((valueOffset - VALUES_OFFSET) / 4);
    unsigned int unitIdx = 0; // UNI0  -

    // podle typu
    switch (P_edit[peditTableRecIdx])
    {
        case 0/*Info*/:
        case 1/*Edit*/:
            unitIdx = P_edit[peditTableRecIdx+2];
            break;

        case 2/*4win*/:
            if (P_edit[peditTableRecIdx+1] == valueIdx)
                unitIdx = P_edit[peditTableRecIdx+5];
            else if (P_edit[peditTableRecIdx+2] == valueIdx)
                unitIdx = P_edit[peditTableRecIdx+5];
            else if (P_edit[peditTableRecIdx+3] == valueIdx)
                unitIdx = P_edit[peditTableRecIdx+8];
            else if (P_edit[peditTableRecIdx+4] == valueIdx)
                unitIdx = P_edit[peditTableRecIdx+8];
            break;
    }

    return UNIT_SIZE * unitIdx;
}

/*
    Vraci index textu v tabulce P_texty podle indexu zaznamu v tabulce P_edit, ktery byl nalezen
    v tabulce P_menu.
    Pokud neni text nalezen, vraci -1.
*/
long GetPtextyTableStringIndexFromPmenu(int peditTableRecIdx, unsigned long valueOffset)
{
    unsigned int menuIdx = 0;
    unsigned int editType = P_edit[peditTableRecIdx];
    unsigned int valueIdx = (unsigned int)((valueOffset - VALUES_OFFSET) / 4);

    while (menuIdx < (sizeof_P_menu / sizeof(P_menu[0])))
    {
        // podle typu
        switch (P_menu[menuIdx])
        {
            case 0/*EDIT*/:
                if (P_menu[menuIdx+TO_EDIT] == peditTableRecIdx)
                {
                    if (editType == 2)/*4win*/
                    {
                        if ((P_edit[peditTableRecIdx+1] == valueIdx) || (P_edit[peditTableRecIdx+2] == valueIdx))
                            return P_menu[menuIdx+TO_TEXT];
                        else
                            return P_edit[peditTableRecIdx+17];
                    }
                    else
                    {
                        return P_menu[menuIdx+TO_TEXT];
                    }
                }

                menuIdx += 5;
                break;

            default/*MENU*/:
                menuIdx += 2 + P_menu[menuIdx+TO_COUNT] * ONE_ITEM_SIZE;
                break;
        }
    }

    return -1;
}

/*
    Vraci index textu v tabulce P_texty podle poradi textu od pocatecniho indexu.
*/
/*unsigned long GetPtextyTableStringIndex(unsigned long startTextIdx, unsigned int textIdx)
{
    unsigned int i = 0;

    while (i != textIdx)
    {
        while (P_texty[startTextIdx++] != 0)
            ;

        i++;
    }

    return startTextIdx;
}*/

/*
    Vraci menu hodnotu jako text podle indexu v tabulce P_edit a adresy hodnoty.
    Po neuspechu vraci 0.
*/
unsigned char GetMenuValueString(int peditTableRecIdx, unsigned long valueOffset, unsigned long value, char *valueString)
{
    unsigned int valueIdx = (unsigned int)((valueOffset - VALUES_OFFSET) / 4);

    // podle typu
    switch (P_edit[peditTableRecIdx])
    {
        case 0/*Info*/:
            LongToPointStr(value, P_edit[peditTableRecIdx+3], P_edit[peditTableRecIdx+4], valueString);
            return 1;

        case 1/*Edit*/:
            LongToPointStr(value, P_edit[peditTableRecIdx+3], P_edit[peditTableRecIdx+4], valueString);
            return 1;

        case 2/*4win*/:
            if (P_edit[peditTableRecIdx+1] == valueIdx)
            {
                LongToPointStr(value, P_edit[peditTableRecIdx+6], P_edit[peditTableRecIdx+7], valueString);
                return 1;
            }
            else if (P_edit[peditTableRecIdx+2] == valueIdx)
            {
                LongToPointStr(value, P_edit[peditTableRecIdx+6], P_edit[peditTableRecIdx+7], valueString);
                return 1;
            }
            else if (P_edit[peditTableRecIdx+3] == valueIdx)
            {
                LongToPointStr(value, P_edit[peditTableRecIdx+9], P_edit[peditTableRecIdx+10], valueString);
                return 1;
            }
            else if (P_edit[peditTableRecIdx+4] == valueIdx)
            {
                LongToPointStr(value, P_edit[peditTableRecIdx+9], P_edit[peditTableRecIdx+10], valueString);
                return 1;
            }
            return 0;

        case 3/*Isel*/:
            strcpy(valueString, &P_texty[P_edit[peditTableRecIdx+5+value]]);
            return 1;

        case 4/*Bool*/:
        case 5/*Time*/:
        case 6/*Date*/:
        case 7/*CalW*/:
        case 8/*Pasw*/:
        case 9/*Func*/:
            strcpy(valueString, "");
            return 1;
    }

    return 0;
}
