#include "LPC23xx.h"
#include "type.h"
#include "rtc.h"
#include "maine.h"
#include "display.h"
#include "rprintf.h"

volatile unsigned long secondTick = 0; // pocitani sekund od startu systemu
static char timeEvent = 0;
static char dateEvent = 0;

void RTC_init()
{
  ///na zacatku se musi vetsina povypinat a nasledne se muze povolit
  PCONP |= (1<<9);                //PCRTC power ON
  RTC_CCR=0;                      //vypnuti RTc
  RTC_CISS =  0;
  RTC_CIIR=0;
  RTC_ILR=0;
  RTC_AMR=0;
  RTC_PREFRAC=0;
  RTC_PREINT=0;

  RTC_CCR |= (1<<4) | (1<<0); //zapnuti extereniho 32kHz krzstalu
}

void RTC_set_interrupt()
{
  ///preruseni
  RTC_ILR  |= (1<<0);                 //generovani preruseni od casu
  RTC_CIIR |= (1<<0);     //inkrementace sekundy s kazdym prerusenim
  VICVectAddr13=(unsigned long)RTC_interrupt;
  VICIntEnable |= (1<<13);
}


void RTC_set(unsigned char hodina,unsigned char minuta, unsigned char sekunda, unsigned char den, unsigned char mesic, unsigned int rok)
{
  RTC_SEC=sekunda;
  RTC_MIN=minuta;
  RTC_HOUR=hodina;
  RTC_DOM=den;
  RTC_MONTH=mesic;
  RTC_YEAR=rok;

  DateTime.second=sekunda;
  DateTime.minute=minuta;
  DateTime.hour=hodina;
  DateTime.day=den;
  DateTime.month=mesic;
  DateTime.year=rok;

  dateEvent = 1;
  timeEvent = 1;
}

void RTC_read()
{
  DateTime.second=RTC_SEC;
  DateTime.minute=RTC_MIN;
  DateTime.hour=RTC_HOUR;
  DateTime.day=RTC_DOM;
  DateTime.month=RTC_MONTH;
  DateTime.year=RTC_YEAR;
}

void RTC_set_year(unsigned int year)
{
  RTC_YEAR      =year;
  DateTime.year =year;
  dateEvent = 1;
}

void RTC_set_month(unsigned char month)
{
  RTC_MONTH      =month;
  DateTime.month =month;
  dateEvent = 1;
}

void RTC_set_day(unsigned char day)
{
  RTC_DOM       =day;
  DateTime.day  =day;
  dateEvent = 1;
}

void RTC_set_hour(unsigned char hour)
{
  RTC_HOUR      =hour;
  DateTime.hour =hour;
  timeEvent = 1;
}

void RTC_set_minute(unsigned char minute)
{
  RTC_MIN         =minute;
  DateTime.minute =minute;
  timeEvent = 1;
}

void RTC_set_second(unsigned char second)
{
  RTC_SEC         =second;
  DateTime.second =second;
  timeEvent = 1;
}

void RTC_interrupt()
{
  RTC_ILR  |= (1<<0);
  flag_RTC = 1;
  secondTick++;
  VICVectAddr=0;
}

unsigned long RTC_GetSysSeconds(void)
{
    volatile unsigned long ss;
    ss = secondTick;
    return ss;
}

unsigned char RTC_IsSysSecondTimeout(unsigned long userSysSeconds, unsigned long delaySeconds)
{
    if ((RTC_GetSysSeconds() - userSysSeconds) >= delaySeconds)
        return 1;
    else
        return 0;
}

void RTC_EventLogTask(void)
{
    if (dateEvent)
    {
        dateEvent = 0;

        char value[11];
        sprintf(value, "%04d/%02d/%02d", DateTime.year, DateTime.month, DateTime.day);
        WriteEventLogParameter("Date", value, "");
    }

    if (timeEvent)
    {
        timeEvent = 0;

        char value[9];
        sprintf(value, "%02d:%02d:%02d", DateTime.hour, DateTime.minute, DateTime.second);
        WriteEventLogParameter("Time", value, "");
    }
}
