/****************************************************************************
***                  ********************************************************
*****************************************************************************
*
*****************************************************************************
*   Potrebne upravy pri zmene procesoru                                     *
*   - prislusny include zakomentovat/odkomentovat                           *
*   - prislusne Zapojeni vyvodu zakomentovat/odkomentovat                   *
*   - pokud pouzivate desku s reverzne zapojenymi vodici displeje, musite   *
*     jeste odkomentovat telo metody Reverse_Data v modulu tg13653.c        *
****************************************************************************/


#define CONST_STR_NUMBERS "0123456789"
#define CONST_LENGTH_STR_NUMBERS 10
#define FULL_CHARACTER_WIDTH         8

/* Makra */
//#define ROUND(x)                   ( (long)((x) + 0.5))                   // zaokrouhli hodnotu typu float na long
//#define GET_NUMBER_OF_INDEX(array) ( sizeof(array) / sizeof(array[0]) )   // vraci pocet prvku v poli
#define CENTR( str )       ( (NUMBER_OF_COLUMNS - SizeInPixel(str)) / 2 ) // toto makro slouzi pri vypisovani na dispjej k posunu souradnice x tak, aby byl vypysovany text zarpvnany doprostred delky length



#define DISPLAY_A0			  0x00400000
#define DISPLAY_RES			  0x00800000
#define DISPLAY_LIGHT_PIN     (1<<8)
#define DISPLAY_LIGHT_IOSET   IOSET1
#define DISPLAY_LIGHT_IOCLR   IOCLR1
#define DISPLAY_LIGHT_IODIR   IODIR1


/* pin A0  setting to 0 or 1                                                   */
#define DISP_A0(x)              ((x) ? (IOSET1 = DISPLAY_A0)  : (IOCLR1 = DISPLAY_A0) );

/* pin RST setting to 0 or 1                                                   */
#define DISP_RES(x)             ((x) ? (IOSET1 = DISPLAY_RES) : (IOCLR1 = DISPLAY_RES));


/******************************************************************************/

//==========================================================//
//               External Hardware Definitions              //
//==========================================================//
#define NUMBER_OF_COLUMNS 128
#define NUMBER_OF_ROWS     64

#define NUMBER_OF_PAGES     8

#define FIRST_PAGE          0
#define FIRST_COLUMN        0
#define LAST_PAGE           7
#define LAST_COLUMN       (NUMBER_OF_COLUMNS -1)

//==========================================================//
//              Write_Display(COMMAND,data)                 //
//==========================================================//
/* Tyhle flagy se predavaji funkcim, aby vedeli, jestli chceme zapisoivat
   DATA, nebo CMD (command, ridisi hodnoty) */

#define CMD  0x01
#define DATA 0xFF

//==========================================================//
//              Write_Display(command,DATA)                 //
//==========================================================//
/* Nasledujici definice jsou prikazy (nastaveni), ktere se predavaji pri nastaveni
   flagu na CMD. Pomoci techto byte je mozne nastavovat chovani displeje */
#define DISPLAY_ON            0xAF
#define DISPLAY_OFF           0xAE
#define STATIC_INDICATOR_OFF  0xAC
#define STATIC_INDICATOR_ON   0xAD
#define START_LINE_SET        0x40
#define PAGE_ADDRESS_SET      0xB0
/* The Column Address is a two byte operation that writes the most significant
   bits of the address to D3 - D0 and then writes the least significant bits to
   D3- D0. Since the Column Address auto increments after each write, direct
   access is infrequent. */
#define COLUMN_ADDRESS_HIGH   0x10
#define COLUMN_ADDRESS_LOW    0x00
#define ADC_SELECT_NORMAL     0xA0
#define ADC_SELECT_REVERSE    0xA1
#define DISPLAY_NORMAL        0xA6
#define DISPLAY_REVERSE       0xA7
#define ALL_POINTS_ON         0xA5
#define LCD_BIAS_1_9          0xA2
#define LCD_BIAS_1_7          0xA3
#define READ_MODIFY_WRITE     0xE0
#define END                   0xEE
#define RESET_DISPLAY         0xE2
#define SET_INTERNAL_BOOSTER  0xF8
#define COMMON_OUTPUT_NORMAL  0xC0
#define COMMON_OUTPUT_REVERSE 0xC8
/* The power control set value is obtained by OR'ing the values together to
   create the appropriate data value. For example:
   data = (POWER_CONTROL_SET | BOOSTER_CIRCUIT |
   VOLTAGE_REGULATOR | VOLTAGE_FOLLOWER);
   Only the bits that are desired need be OR'ed in because the initial value
   of POWER_CONTROL_SET sets them to zero. */
#define INTERNAL_RESISTOR_SET 0x27
#define POWER_CONTROL_SET     0x28
#define BOOSTER_CIRCUIT       0x04
#define VOLTAGE_REGULATOR     0x02
#define VOLTAGE_FOLLOWER      0x01
/* The initial value of the V5_RESISTOR_RATIO sets the Rb/Ra ratio to the
   smallest setting. The valid range of the ratio is:
   0x20 <= V5_RESISTOR_RATIO <= 0x27 */
#define V5_RESISTOR_RATIO     0x26
/* When the electronic volume command is input, the electronic volume register
   set command becomes enabled. Once the electronic volume mode has been set,
   no other command except for the electronic volume register command can be
   used. Once the electronic volume register set command has been used to set
   data into the register, then the electronic volume mode is released. */
#define ELECTRONIC_VOLUME_SET  0x81
#define ELECTRONIC_VOLUME_INIT 0x16

/* Konstanty urcuji jake tlacitka chceme v konkretnim okne zobrazovat */
#define BUTTON_ESC   0x01 // je mozne je maskovat dohromady (pomoci |)
#define BUTTON_OK    0x02
#define BUTTON_MENU  0x04


//==========================================================//
//                         Metody                           //
//==========================================================//

extern void Print_Buffer(void);
extern void Write( unsigned char xStart, unsigned char line, const char *str );
extern void WriteSpecialFont( unsigned char xStart, unsigned char line, const char *str );
extern void WriteNow(void);
extern void WriteImageResolution(unsigned char start_x, unsigned char start_line, int width, int heigth, int Offset);
extern void WriteImage(unsigned char start_x, unsigned char start_line, int Offset);
extern unsigned int SizeInPixel(const char *str);
extern void Init_Display(void);
extern void Write_Display(unsigned char command, unsigned char data);
extern void InvertLineBuffer( unsigned char xStart, unsigned char HowMany );
extern void InvertLine( unsigned char xStart, unsigned char line, unsigned char HowMany );
extern void WriteLastRow( unsigned char value );
extern void WriteLine( unsigned char xStart, const char *str );
extern void WriteCenter( unsigned char line, const char *str );
extern void EraseLineBuffer (void);
extern void WriteErase(void);
extern void ShowLine(unsigned char row);
extern void WriteBigNumberCentr( unsigned char y, long value_dig, long value_dec, unsigned char dec);
extern void DisplaySetContrast(unsigned char Contrast);


#define BUFFER_SIZE  (1056) // Velikost grafickeho bufferu v byte 128 (display ma velikost 132x64)
#define LAST_ROW     (7)    // Cislo posledni radky displeje

unsigned char g_frame_buffer[BUFFER_SIZE]; //  Graficky buffer, RAM. Nejvetsi zrout pameti. Ve vlash je pomali.


/* Stav rozsviceni segmentoveho displeje */
struct TSEGMENT_DISPLAY {
  unsigned long clock;  // hodiny a minuty v BCD kodu ve formatu 0000HHMM
  unsigned char ShowClock;// zobrazuji se hodiny? 1=On, 0=Off
  unsigned char status;  // Stavove slovo, popisuje zbytek. 1-sviti, 0-nesviti
  unsigned char light_delay; // kolik sekund bude jeste podsvetleny displej, 0=OFF
                          // Vyznam bitu:
                          // 0. Dvojtecka mezi cislicemi
                          // 1. Sipka nahoru
                          // 2. Sipka dolu
                          // 3.
                          // 4.
                          // 5.
                          // 6.
                          // 7.
} g_segment_display;        //

#define UP_ARROW   (0x02)
#define DOWN_ARROW (0x04)
#define BELL       (0x08)

extern unsigned char HiHi (unsigned long long_to_byte);
extern unsigned char HiLo (unsigned long long_to_byte);
extern unsigned char LoHi (unsigned long long_to_byte);
extern unsigned char LoLo (unsigned long long_to_byte);
extern void SegmentDisplayPrint( void );
extern void SegmentDisplayOff( void );

TGraphQuantity GetGraphQuantityType(void);
int GetGraphValuePercent(void);
void WriteBar(void);
void WriteGraph(void);
void WriteBattery(unsigned char x, unsigned char line);
