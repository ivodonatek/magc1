/*****************************************************************************
 *   uart.h:  Header file for NXP LPC23xx Family Microprocessors
 *
 *   Copyright(C) 2006, NXP Semiconductor
 *   All rights reserved.
 *
 *   History
 *   2006.09.01  ver 1.00    Prelimnary version, first Release
 *
 ******************************************************************************/

// modified by Martin Thomas (check for __irq makro)

#ifndef __UART_H
#define __UART_H

#define IER_RBR		0x01
#define IER_THRE	0x02
#define IER_RLS		0x04

#define IIR_PEND	0x01
#define IIR_RLS		0x03
#define IIR_RDA		0x02
#define IIR_CTI		0x06
#define IIR_THRE	0x01

#define LSR_RDR		0x01
#define LSR_OE		0x02
#define LSR_PE		0x04
#define LSR_FE		0x08
#define LSR_BI		0x10
#define LSR_THRE	0x20
#define LSR_TEMT	0x40
#define LSR_RXFE	0x80

#define DATA_RECEIVE IOCLR0|=(1<<24)
#define DATA_SEND    IOSET0|=(1<<24)

#define UART_BUFSIZE 0x40
#define WIFI_FUNC    (1<<3)
#define WIFI_RESET   (1<<2)

DWORD Sensor_init( DWORD baudrate );
void UARTSend( DWORD portNum, const BYTE *BufferPtr, DWORD Length );
void Send_sensor(const BYTE *BufferPtr, DWORD Length );
unsigned char Read_sensor(void);

extern volatile DWORD UART0Count;
extern volatile BYTE  UART0Buffer[UART_BUFSIZE];
extern volatile DWORD UART1Count;
extern volatile BYTE  UART1Buffer[UART_BUFSIZE];

#endif /* end __UART_H */
