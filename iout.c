#include "iout.h"
#include "LPC23xx.h"
#include "menu_tables.h"
#include "maine.h"
#include "heat.h"

void pauza(void);

void DAC7512out(unsigned int value)
{
  if(value>4095)
    value=4095;

  unsigned char count=0;     //pocet uz odoslanych bitov
  Set_port();                  //nastavenie portov
  pauza();
  IV_PORT &= ~IV_SYNC;          //SYNC low, zaciatok prevodu
  pauza();
  //cyklus pre odoslanie 16tich bitov
  for(count = 0; count<16; count++)
  {
    IV_PORT |=IV_SCLK;         //CLK high
    pauza();
	  if(value & 0x8000)       //ak je msb 1 tak posli 1
		  FIO2SET |= IV_DIN;
	  else
		  FIO2CLR |= IV_DIN;      //ak je 0 tak posli 0
    pauza();
    IV_PORT &= ~IV_SCLK;       //CLK low,zapis do registru prevodnika
    pauza();
	  value<<=1;               //posuv hodnoty pre testovanie dalsieho bitu
  }
  Set_port();                 //nastavenie portov
}

// zjisti velicinu pro prime rizeni
static TDirectDrivingQuantity GetQuantityType(void)
{
    unsigned long quantity = RAM_Read_Data_Long(I_DIRECT_DRIVING_QUANTITY);
    if (quantity <= DDQ_Max)
        return (TDirectDrivingQuantity)quantity;
    else
        return DDQ_HEAT_POWER;
}

// zjisti hodnotu veliciny pro prime rizeni
static float GetQuantityValue(TDirectDrivingQuantity quantityType)
{
    switch (quantityType)
    {
    case DDQ_HEAT_POWER:
    default:
        {
            float heatPerform = Heat_GetHeatPerformance();
            if (heatPerform < 0) heatPerform = 0;
            return heatPerform;
        }

    case DDQ_FLOW:
        return g_measurement.actual;
    }
}

void I_Out(void)
{
  if (GetIoutModuleType() != TIM_IV_OUT)
    return;

  switch(RAM_Read_Data_Long(CURRENT_SET))
  {
    case 0: //flow +
      if (g_measurement.actual>0)
        DAC7512out(DAC_10mA);   //vystup 10mA   -> log 1
      else
        DAC7512out(0);          //vystup 4mA -> log 0
    break;
    case 1: //flow -
      if (g_measurement.actual<0)
        DAC7512out(DAC_10mA);
      else
        DAC7512out(0);
    break;
    case 2: //error
      if(actual_error)
        DAC7512out(DAC_10mA);
      else
        DAC7512out(0);
    break;
    case 3: //empty pipe detection
      if(actual_error & ERR_EMPTY_PIPE)
        DAC7512out(DAC_10mA);
      else
        DAC7512out(0);
    break;
    case 4: //fixed
      DAC7512out(DAC_10mA);
    break;
    case 5:
    {
        //podle nastavene veliciny (prutok, ...)
        TDirectDrivingQuantity quantityType = GetQuantityType();
        float quantityValue = GetQuantityValue(quantityType);
        float ac;  // pro vypocet rovnice primky zavislosti proudu na velicine
        float bc;  // pro vypocet rovnice primky zavislosti proudu na velicine
        float cc;  // vypocitany vystupni proud

        // vypocet konstanty a pro rovnici primky zavislosti proudu na velicine
        switch (quantityType)
        {
        case DDQ_HEAT_POWER:
        default:
            ac = (RAM_Read_Data_Long(HEAT_POWER_CURRENT_MAX) - RAM_Read_Data_Long(HEAT_POWER_CURRENT_MIN))*1.0 /
                (RAM_Read_Data_Long(I_HEAT_POWER_MAX)/1000.0 - RAM_Read_Data_Long(I_HEAT_POWER_MIN)/1000.0);
            break;
        case DDQ_FLOW:
            ac = (RAM_Read_Data_Long(CURRENT_MAX) - RAM_Read_Data_Long(CURRENT_MIN))*1.0 /
                (RAM_Read_Data_Long(I_FLOW_MAX)/1000.0 - RAM_Read_Data_Long(I_FLOW_MIN)/1000.0);
            break;
        }

        // vypocet konstanty b pro rovnici primky zavislosti proudu na velicine
        switch (quantityType)
        {
        case DDQ_HEAT_POWER:
        default:
            bc = ((RAM_Read_Data_Long(HEAT_POWER_CURRENT_MAX)*1.0) - ac*(RAM_Read_Data_Long(I_HEAT_POWER_MAX)/1000.0));
            break;
        case DDQ_FLOW:
            bc = ((RAM_Read_Data_Long(CURRENT_MAX)*1.0) - ac*(RAM_Read_Data_Long(I_FLOW_MAX)/1000.0));
            break;
        }

        // vypocitany vystupni proud
        cc  = (ac*quantityValue*1.0 + bc);

        if ((RAM_Read_Data_Long(I_CAL_CONST_1) != 10000) || (RAM_Read_Data_Long(I_CAL_CONST_2) != 10000))  // pokud se ma prepocitat vystup podle kalibrace
        {
            //Vypocet rovnice primky pro upravu vystupu podle kalibrace
            ac = (RAM_Read_Data_Long(I_CAL_POINT_2) * (RAM_Read_Data_Long(I_CAL_CONST_2)/10000.0) -
              RAM_Read_Data_Long(I_CAL_POINT_1) * (RAM_Read_Data_Long(I_CAL_CONST_1))/10000.0) /
             (RAM_Read_Data_Long(I_CAL_POINT_2) - RAM_Read_Data_Long(I_CAL_POINT_1));
            //Vypocet rovnice primky pro upravu vystupu podle kalibrace
            bc = (RAM_Read_Data_Long(I_CAL_POINT_1) * (RAM_Read_Data_Long(I_CAL_CONST_1)/10000.0) -
              RAM_Read_Data_Long(I_CAL_POINT_1) * ac);
            cc = ac * cc + bc; // prepocet vystupu podle kalibracni primky
        }

        if (cc > 20) cc = 20;
        if (cc <  4) cc =  4;

        switch (quantityType)
        {
        case DDQ_HEAT_POWER:
        default:
            if (quantityValue > (RAM_Read_Data_Long(I_HEAT_POWER_MAX)/1000.0))
                cc = RAM_Read_Data_Long(HEAT_POWER_CURRENT_MAX);  // poslani hodnoty na AD prevodnik i-out vystupu - mimo zadany rozsah
            else if (quantityValue < (RAM_Read_Data_Long(I_HEAT_POWER_MIN)/1000.0))
                cc = RAM_Read_Data_Long(HEAT_POWER_CURRENT_MIN);     // poslani hodnoty na AD prevodnik i-out vystupu - mimo zadany rozsah
            break;
        case DDQ_FLOW:
            if (quantityValue > (RAM_Read_Data_Long(I_FLOW_MAX)/1000.0))
                cc = RAM_Read_Data_Long(CURRENT_MAX);  // poslani hodnoty na AD prevodnik i-out vystupu - mimo zadany rozsah
            else if (quantityValue < (RAM_Read_Data_Long(I_FLOW_MIN)/1000.0))
                cc = RAM_Read_Data_Long(CURRENT_MIN);     // poslani hodnoty na AD prevodnik i-out vystupu - mimo zadany rozsah
            break;
        }

        ac = (4095*1.0/16); // vypocet konstanty a pro rovnici primky zavislosti hodnoty AD prevodniku na vypocitanem proudu 16 = 20-4
        bc = (4095 - ac*20); // vypocet konstanty b pro rovnici primky zavislosti hodnoty AD prevodniku na vypocitanem proudu
        cc = (ac*cc + bc); // vypocitana hodnota pro AD prevodnik

        DAC7512out(cc);  // poslani hodnoty na AD prevodnik i-out vystupu
    }
    break;
    case 6: //OFF
      DAC7512out(0);

    default:
      DAC7512out(0);
	}
}

void V_Out(float flow)
{
  return;

  if (GetIoutModuleType() != TIM_IV_OUT)
    return;

  /*switch(RAM_Read_Data_Long(VOLTAGE_SET))
  {
    case 0: //flow +
      if(flow>0)
        DAC7512out(2048);       //2048 - 5V
      else
        DAC7512out(0);
    break;
    case 1: //flow -
      if(flow<0)
        DAC7512out(2048);       //2048 - 5v
      else
        DAC7512out(0);
    break;
    case 2: //error
      if(actual_error)
        DAC7512out(2048);       //2048 - 5v
      else
        DAC7512out(0);
    break;
    case 3: //empty pipe
      if(actual_error & ERR_EMPTY_PIPE)
        DAC7512out(2048);       //2048 - 5v
      else
        DAC7512out(0);
    break;
    case 4: //fixed
      DAC7512out(2048);
    break;
    case 5: //direct driving
    {
      float av;  // pro vypocet rovnice primky zavislosti proudu na prutoku
      float bv;  // pro vypocet rovnice primky zavislosti proudu na prutoku
      float vv;  // vypocitany vystupni proud
      // vypocet konstanty a pro rovnici primky zavislosti napeti na prutoku
      av  = (RAM_Read_Data_Long(VOLTAGE_MAX) - RAM_Read_Data_Long(VOLTAGE_MIN))*1.0 / (RAM_Read_Data_Long(V_FLOW_MAX)/1000.0 - RAM_Read_Data_Long(V_FLOW_MIN)/1000.0);
      bv  = (RAM_Read_Data_Long(VOLTAGE_MAX) - av*(RAM_Read_Data_Long(V_FLOW_MAX)/1000.0)); // vypocet konstanty b pro rovnici primky zavislosti napeti na prutoku
      vv  = (av*flow/1000.0 + bv);  // vypocitany vystupni proud
      if (flow > RAM_Read_Data_Long(V_FLOW_MIN_MAX))
        vv = RAM_Read_Data_Long(VOLTAGE_MAX); // vice jak maximalni nastaveny prutok
      if (flow < RAM_Read_Data_Long(V_FLOW_MIN_MIN))
        vv = RAM_Read_Data_Long(VOLTAGE_MIN);  // mene jak nastaveny minimalni prutok
      av = (4095*1.0/10); // vypocet konstanty a pro rovnici primky zavislosti hodnoty AD prevodniku na vypocitanem napeti 10 = 10-0
      bv = (4095 - av*10); // vypocet konstanty b pro rovnici primky zavislosti hodnoty AD prevodniku na vypocitanem napeti
      vv  = (av*vv + bv); // vypocitana hodnota pro AD prevopdnik
      DAC7512out(vv);  // poslani hodnoty na AD prevodnik i-out vystupu
    }
    break;
    case 6: //off
      DAC7512out(0);
    default:
      DAC7512out(0);
    break;
  }*/
}

void DAC7512_init()
{
  FIO2DIR |= IV_DIN | IV_SCLK | IV_SYNC;
}

void Set_port()
{
  IV_PORT |= IV_SCLK | IV_SYNC;
  FIO2SET |= IV_DIN;
}

void pauza()
{
  for (unsigned int kk=0;kk<20;kk++)
  {
    asm ("nop");
  }
}
