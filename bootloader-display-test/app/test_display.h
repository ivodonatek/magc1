//******************************************************************************
//  Display test.
//*****************************************************************************/

#ifndef TEST_DISPLAY_H_INCLUDED
#define TEST_DISPLAY_H_INCLUDED

#include "test.h"

//------------------------------------------------------------------------------
//  Test function.
//------------------------------------------------------------------------------
TestFuncResult TestDisplay_Task(TestFuncParam param);

#endif // TEST_DISPLAY_H_INCLUDED
