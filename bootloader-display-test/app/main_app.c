#include <string.h>
#include <stdlib.h>
#include "LPC23xx.h"
#include "buttons.h"
#include "maine.h"
#include "display.h"
#include "spi.h"
#include "images.h"
#include "rprintf.h"
#include "string.h"
#include "font.h"
#include "rtc.h"
#include "swi.h"
#include "delay.h"
#include "timer.h"
#include "test.h"

///**************************************************************************//
///                             PRODUCTION TEST                              //
///**************************************************************************//
void app_product_test(void)
{
    Buttons_Init();

    SPI_DisplayInit();
    Init_Display();

    DisplaySetContrast(60);
    DISPLAY_LIGHT_IOSET |= DISPLAY_LIGHT_PIN;

    init_timer1();

    Test_Init();

    while (1)
    {
        Test_Task();
    }
}


///**************************************************************************//
///                          FIRMWARE UPDATE INFO                            //
///**************************************************************************//
void app_fw_update_info(TFwUpdateResult fwUpdateResult)
{
    if (fwUpdateResult == FWUR_NONE)
        return;

    SPI_DisplayInit();
    Init_Display();

    DisplaySetContrast(60);
    DISPLAY_LIGHT_IOSET |= DISPLAY_LIGHT_PIN;

    WriteErase();
    WriteCenter(3, "Firmware update");

    if (fwUpdateResult == FWUR_ERASE_FAILED)
    {
        WriteCenter(4, "Erasing failed");
        WriteNow();
        while(1);
    }
    else if (fwUpdateResult == FWUR_READ_FILE_ERROR)
    {
        WriteCenter(4, "Read file error");
        WriteNow();
        while(1);
    }
    else if (fwUpdateResult == FWUR_WRITE_PROGRAM_ERROR)
    {
        WriteCenter(4, "Write program error");
        WriteNow();
        while(1);
    }
    else if (fwUpdateResult == FWUR_DONE)
    {
        WriteCenter(4, "Done");
        WriteNow();

        // cekani asi 3s
        for (int i = 0; i < 30; i++)
            delay_100ms();

        WriteErase();
        WriteNow();
    }
    else
    {
        WriteCenter(4, "Unknown error");
        WriteNow();
        while(1);
    }
}
