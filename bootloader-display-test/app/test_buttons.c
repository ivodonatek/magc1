//******************************************************************************
//  Buttons test with RTC test.
//*****************************************************************************/

#include <string.h>
#include "LPC23xx.h"
#include "test_buttons.h"
#include "buttons.h"
#include "display.h"
#include "rtc.h"
#include "rprintf.h"
#include "images.h"

static ButtonCode pressedButton;

//------------------------------------------------------------------------------
//  vypsani casu na displej
//------------------------------------------------------------------------------
static void DisplayRTC(void)
{
    RTC_read();
    char str[20];
    sprintf(str,"%02d:%02d:%02d",DateTime.hour,DateTime.minute,DateTime.second);
    Write(86, 0, str);
}

//------------------------------------------------------------------------------
//  vypsani textu stisknuteho tlacitka
//------------------------------------------------------------------------------
static void DisplayPressedButtonText(void)
{
    char *txt = Buttons_GetCodeText(pressedButton);
    WriteCenter(2, txt);
}

//------------------------------------------------------------------------------
//  vykresleni obrazku tlacitka
//------------------------------------------------------------------------------
static void DisplayButtonImage(unsigned char start_x, unsigned char start_line, char pressed)
{
    if (pressed)
        WriteImage(start_x, start_line, IMG_PRESS_BTN);
    else
        WriteImage(start_x, start_line, IMG_UNPRES_BTN);
}

//------------------------------------------------------------------------------
//  vykresleni obrazku vsech tlacitek
//------------------------------------------------------------------------------
static void DisplayButtonsImages(void)
{
    DisplayButtonImage(2, 4, pressedButton == BTC_ESC);
    DisplayButtonImage(60, 4, pressedButton == BTC_UP);
    DisplayButtonImage(118, 4, pressedButton == BTC_ENTER);
    DisplayButtonImage(29, 5, pressedButton == BTC_LEFT);
    DisplayButtonImage(89, 5, pressedButton == BTC_RIGHT);
    DisplayButtonImage(60, 6, pressedButton == BTC_DOWN);
}

//------------------------------------------------------------------------------
//  prekresleni displeje
//------------------------------------------------------------------------------
static void DisplayAll(void)
{
    WriteErase();
    DisplayRTC();
    DisplayPressedButtonText();
    DisplayButtonsImages();
    WriteNow();
}

//------------------------------------------------------------------------------
//  Test function.
//------------------------------------------------------------------------------
TestFuncResult TestButtons_Task(TestFuncParam param)
{
    if (param == TFP_START)
    {
        pressedButton = Buttons_Scan();
        DisplayAll();
    }
    else
    {
        char redraw = 0;

        ButtonCode scanButton = Buttons_Scan();
        if (scanButton != pressedButton)
        {
            pressedButton = scanButton;
            redraw = 1;
        }

        if (flag_RTC)
        {
            flag_RTC = 0;
            redraw = 1;
        }

        if (redraw)
            DisplayAll();
    }

    return TFR_CONTINUE;
}
