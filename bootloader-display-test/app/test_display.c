//******************************************************************************
//  Display test.
//*****************************************************************************/

#include "test_display.h"
#include "display.h"
#include "delay.h"

#define TIME_COUNT  10

// test time counter
static unsigned int timeCounter;

//------------------------------------------------------------------------------
//  Test function.
//------------------------------------------------------------------------------
TestFuncResult TestDisplay_Task(TestFuncParam param)
{
    if (param == TFP_START)
    {
        unsigned int i;
        for (i = 0; i < BUFFER_SIZE; i++)
        {
            g_frame_buffer[i] = 0xFF;
        }

        WriteNow();

        timeCounter = 0;

        return TFR_CONTINUE;
    }
    else
    {
        delay_100ms();

        if (++timeCounter < TIME_COUNT)
            return TFR_CONTINUE;
        else
            return TFR_FINISH;
    }
}
