#include "LPC23xx.h"
#include "rtc.h"
#include "maine.h"
#include "display.h"

volatile unsigned long secondTick = 0; // pocitani sekund od startu systemu
volatile unsigned char flag_RTC = 0;
volatile TDateTime DateTime;

unsigned int flip;

void RTC_init()
{
  ///na zacatku se musi vetsina povypinat a nasledne se muze povolit
  PCONP |= (1<<9);                //PCRTC power ON
  RTC_CCR=0;                      //vypnuti RTc
  RTC_CISS =  0;
  RTC_CIIR=0;
  RTC_ILR=0;
  RTC_AMR=0;
  RTC_PREFRAC=0;
  RTC_PREINT=0;

  RTC_CCR |= (1<<4) | (1<<0); //zapnuti extereniho 32kHz krzstalu
}

void RTC_set_interrupt()
{
  ///preruseni
  RTC_ILR  |= (1<<0);                 //generovani preruseni od casu
  RTC_CIIR |= (1<<0);     //inkrementace sekundy s kazdym prerusenim
  VICVectAddr13=(unsigned long)RTC_interrupt;
  VICIntEnable |= (1<<13);
}

void RTC_read()
{
  DateTime.second=RTC_SEC;
  DateTime.minute=RTC_MIN;
  DateTime.hour=RTC_HOUR;
  DateTime.day=RTC_DOM;
  DateTime.month=RTC_MONTH;
  DateTime.year=RTC_YEAR;
}

void RTC_interrupt()
{
  RTC_ILR  |= (1<<0);
  flag_RTC=1;
  secondTick++;
  VICVectAddr=0;
}

unsigned long RTC_GetSysSeconds(void)
{
    volatile unsigned long ss;
    ss = secondTick;
    return ss;
}

unsigned char RTC_IsSysSecondTimeout(unsigned long userSysSeconds, unsigned long delaySeconds)
{
    if ((RTC_GetSysSeconds() - userSysSeconds) >= delaySeconds)
        return 1;
    else
        return 0;
}

