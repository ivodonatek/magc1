
typedef struct DateTime
{
  float msecond;
  unsigned char second;   //enter the current time, date, month, and year
  unsigned char minute;
  unsigned char hour;
  unsigned char day;
  unsigned char month;
  unsigned int year;
}TDateTime;

void RTC_init(void);
void RTC_read(void);
void RTC_interrupt(void);
void RTC_set_interrupt(void);
unsigned long RTC_GetSysSeconds(void);
unsigned char RTC_IsSysSecondTimeout(unsigned long userSysSeconds, unsigned long delaySeconds);

extern volatile TDateTime DateTime;
extern volatile unsigned char flag_RTC;
