#include "timer.h"
#include "LPC23xx.h"
#include "target.h"
#include "buttons.h"

static void timer1_overflow(void)
{
  T1IR = 1;			/* clear interrupt flag */

  T1MR0         = Fcclk/8;                   // 0.25 pri 57.6MHz

  Buttons_Scan();

  VICVectAddr = 0;		/* Acknowledge Interrupt */
}

void init_timer1(void)      /// casovac pro rolovani tlacitek
{
  T1MR0         = Fcclk/4;                   // 1s pri 57.6MHz
  T1MCR         = 3;                         // Interrupt and Reset on MR0
  VICVectAddr5  = (unsigned long)timer1_overflow;// Set Interrupt Vector
  VICIntEnable  = (1  << 5);                 // Enable Timer0 Interrupt
}
