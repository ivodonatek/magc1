/****************************************************************************
***  images.c    ************************************************************
*****************************************************************************
*  - Tento modul obsahuje obrazky                                           *
*                                                                           *
*                                                                           *
****************************************************************************/
/* Offsety k obrazkum
*  - Offsety je potreba prepocitat, vzdy, pokud odebereme nejaky obrazek.
*  - offset = offset predchoziho + (sirka*vyska)/8 predchoziho + 2.  2=prvni dva byty udavajici rozmery
*/
//  res   | leng.=res/8+2 | offset next leng+predchozi

const unsigned char P_Images[] ={
//---------------------------------- Unpressed button ---------------------------
9, 8,
0xFF, 0x81, 0x81, 0x81, 0x81, 0x81, 0x81, 0x81, 0xFF,
//---------------------------------- Pressed button -----------------------------
9, 8,
0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
};
