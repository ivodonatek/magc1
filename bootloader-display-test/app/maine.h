#define BACKLIGHT_SEC    10

typedef enum
{
    FWUR_NONE,
    FWUR_ERASE_FAILED,
    FWUR_READ_FILE_ERROR,
    FWUR_WRITE_PROGRAM_ERROR,
    FWUR_DONE

} TFwUpdateResult;

extern void app_product_test(void);
extern void app_fw_update_info(TFwUpdateResult fwUpdateResult);

extern void obsluha_tlacitek (void);

extern volatile char odesilaci_buffer[160];
//extern unsigned long debug_code;
extern unsigned long actual_error;
extern unsigned long actual_error_datalogger;
extern long adc_average;
extern unsigned char flag_cisteni;
extern unsigned char flag_used_sensor;
extern volatile unsigned char flag_aktivni_zadost;
extern unsigned char flag_zapis_prutok;
extern unsigned char flag_sensor7;
extern unsigned char flag_sensor8;
extern unsigned char flag_key_lock_time;
extern volatile unsigned char flag_gsm_dokoncil_prijem;
extern volatile unsigned char flag_gsm_datalogger_first_send;

unsigned long GSM_Date_backup;

extern unsigned char pocet_znaku_gsm;
extern volatile unsigned char flag_timeout_timer2;
extern volatile unsigned char flag_timeout_timer3;
extern volatile unsigned int flag_second_gsm;
extern unsigned char flag_totaliser_cycling;
extern unsigned char flag_datalogger;
extern unsigned int flag_zapsat_flash;
extern unsigned char flag_reset_after_load_default;
extern unsigned char flag_start;
extern volatile unsigned char flag_aktualizuji_sensor8;
extern volatile unsigned char flag_komunikacni_modul;
extern volatile unsigned char flag_usb_modul_insert;
extern unsigned int flag_cisteni_cas;
extern unsigned int second_to_datalogger;
extern long zero_constant;
extern char read_datalogger(unsigned long date, unsigned int time, unsigned int pocet);
extern void gsm_busy_pin(unsigned char state);

extern unsigned char flag_event_empty_on_odeslano;
extern unsigned char flag_event_empty_off_odeslano;
extern unsigned char flag_event_zero_on_odeslano;
extern unsigned char flag_event_zero_off_odeslano;

extern unsigned char last_key;
extern unsigned char key_ESC_time_sec;
extern unsigned char key_dummy;
extern unsigned char backlight_second;

extern unsigned char key_lock; // promenna pro zamykani klaves
