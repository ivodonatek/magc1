#include "modbus_bin.h"
#include "spi.h"
#include "maine.h"
#include "delay.h"

#define C0 PF2
#define C1 PF3
#define C2 PF4
#define BEEP PF5
#define CPORT PORTF
#define CDDR DDRF

#define DATA_PORT PORTE
#define DATA_DDR DDRE

#define SPI_PULSE_FRAME_SEND_COUNT    2

char RE1_function=0;
char RE2_function=0;
char RE3_function=0;
char RE4_function=0;
char FO_function=0;

unsigned char pulse_version = 0;

int Act_Err=0;
float Act_Flow=0;

float Qpos=0;
float Qneg=0;
float Dosing=0;
float Comp_F1=0;
float Comp_F2=0;
float Comp_H1=0;
float Comp_H2=0;
int FO_Freq1=0;
int FO_Freq2=0;
float FO_Flow1=0;
float FO_Flow2=0;

static void FrameDelay(void)
{
    delay_1ms();
}

void Send_Float(char code,float data)
{
  long temp=0;
  temp = (data*1000.0);

  for (unsigned char i = 0; i < SPI_PULSE_FRAME_SEND_COUNT; i++)
  {
    SPI_PulseChipSelect(TRUE);

    SPI_PulseTransmit(code);//buffer0

    SPI_PulseTransmit((char)(temp>>24));//buffer1
    SPI_PulseTransmit((char)(temp>>16));//buffer2
    SPI_PulseTransmit((char)(temp>>8));//buffer3
    SPI_PulseTransmit((char)(temp));//buffer4

    SPI_PulseChipSelect(FALSE);

    FrameDelay();
  }
}

void Send_Long(char code,long data)
{
  for (unsigned char i = 0; i < SPI_PULSE_FRAME_SEND_COUNT; i++)
  {
    SPI_PulseChipSelect(TRUE);

    SPI_PulseTransmit(code);//buffer0

    SPI_PulseTransmit((char)(data>>24));//buffer1
    SPI_PulseTransmit((char)(data>>16));//buffer2
    SPI_PulseTransmit((char)(data>>8));//buffer3
    SPI_PulseTransmit((char)(data));//buffer4

    SPI_PulseChipSelect(FALSE);

    FrameDelay();
  }
}

void Send_Func(char output,char func)
{
  for (unsigned char i = 0; i < SPI_PULSE_FRAME_SEND_COUNT; i++)
  {
    SPI_PulseChipSelect(TRUE);

    SPI_PulseTransmit(output);
    SPI_PulseTransmit(func);
    SPI_PulseTransmit(DUMMY);
    SPI_PulseTransmit(DUMMY);
    SPI_PulseTransmit(DUMMY);

    SPI_PulseChipSelect(FALSE);

    FrameDelay();
  }
}

/*
void Set_Duty(char duty)
{
  if (GetPulseModuleType() != TPM_PULSE)
    return;

  SPI_PulseTransmit(FODUTY);//DUTY CYCLE -> 25%
  SPI_PulseTransmit(DUMMY);
  SPI_PulseTransmit(DUMMY);
  SPI_PulseTransmit(DUMMY);
  SPI_PulseTransmit(duty);
}
*/
