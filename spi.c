#include "lpc23xx.h"
#include "spi.h"
#include "delay.h"

// Definice SPI PULSE
/* Power Control for Peripherals register (PCONP) SSP bits
    - The SSP1 interface power/clock enabled */
#define PULSE_PCONP_SSP_MASK   0x400
#define PULSE_PCONP_SSP_VALUE  0x400

/* Peripheral Clock Selection register (PCLKSELx)
    - Peripheral clock selection for SSP1 */
#define PULSE_PCLKSEL PCLKSEL0

/* Peripheral Clock Selection register (PCLKSELx) SSP bits
    - PCLK_xyz = CCLK/4 */
#define PULSE_PCLKSEL_SSP_MASK   0x300000
#define PULSE_PCLKSEL_SSP_VALUE  0x0

/* SSPx Clock Prescale Register (SSPxCPSR) */
#define PULSE_SSPCPSR SSP1CPSR

/* SSPx Clock Prescale Register (SSPxCPSR) value */
#define PULSE_SSPCPSR_VALUE   30

/* Pin Function Select register (PINSELx) */
#define PULSE_PINSEL PINSEL0

/* Pin Function Select register (PINSELx) SSP bits
    - GPIO: SSEL1,
    - SSP: SCK1, MOSI1 */
#define PULSE_PINSEL_SSP_MASK   0xFF000
#define PULSE_PINSEL_SSP_VALUE  0x8A000

/* Pin Mode select register (PINMODEx) */
#define PULSE_PINMODE PINMODE0

/* Pin Mode select register (PINMODEx) SSP bits
    - SSEL1: pull-up resistor enabled
    - SCK1: pull-up resistor enabled
    - MISO1: high impedance
    - MOSI1: pull-up resistor enabled */
#define PULSE_PINMODE_SSP_MASK   0xFF000
#define PULSE_PINMODE_SSP_VALUE  0x20000

/* SSPx Interrupt Mask Set/Clear Register (SSPxIMSC) */
#define PULSE_SSPIMSC SSP1IMSC

/* SSPx Interrupt Mask Set/Clear Register (SSPxIMSC) value
    - all interrupts disabled */
#define PULSE_SSPIMSC_VALUE   0

/* Interrupt Enable Clear Register (VICIntEnClear) value
    - SSP1 */
#define PULSE_VICIntEnClr_SSP_VALUE 0x800

/* SSPx Control Register 0 (SSPxCR0) */
#define PULSE_SSPCR0 SSP1CR0

/* SSPx Control Register 0 (SSPxCR0) value
*/
#define PULSE_SSPCR0_VALUE 0x0087

/* SSPx Control Register 1 (SSPxCR1) */
#define PULSE_SSPCR1 SSP1CR1

/* SSPx Control Register 1 (SSPxCR1) value
    - The SSP controller acts as a master on the bus
    - The SSP controller is disabled
    - normal operation */
#define PULSE_SSPCR1_VALUE 0x0

/* Enable SSPx */
#define PULSE_SSP_ENABLE() (PULSE_SSPCR1 = (PULSE_SSPCR1 & 0xD) | 2)

/* Disable SSPx */
#define PULSE_SSP_DISABLE() (PULSE_SSPCR1 &= 0xD)

/* GPIO Port Direction control register */
#define PULSE_IODIR   IODIR0

/* GPIO Port Direction control register SSP bits
    - SSEL: output */
#define PULSE_IODIR_SSP_MASK   0x20
#define PULSE_IODIR_SSP_VALUE  0x20

/* GPIO Port SSEL pin mask */
#define PULSE_SSEL_PIN_MASK   0x20

/* GPIO Port Output Set register */
#define PULSE_IOSET   IOSET0

/* GPIO Port Output Clear register */
#define PULSE_IOCLR   IOCLR0

/* SSEL pin high */
#define PULSE_SSEL_PIN_HIGH() (PULSE_IOSET = PULSE_SSEL_PIN_MASK)

/* SSEL pin low */
#define PULSE_SSEL_PIN_LOW() (PULSE_IOCLR = PULSE_SSEL_PIN_MASK)

/* SSPx Status Register (SSPxSR) */
#define PULSE_SSPSR  SSP1SR

/* SSPx Busy */
#define PULSE_SSP_BUSY() (PULSE_SSPSR & 0x10)

/* SSPx Transmit FIFO Not Full */
#define PULSE_SSP_TXBUF_READY() (PULSE_SSPSR & 2)

/* SSPx Receive FIFO Empty */
#define PULSE_SSP_RXBUF_EMPTY() (!(PULSE_SSPSR & 4))

/* SSPx Data Register (SSPxDR) */
#define PULSE_SSPDR  SSP1DR

//------------------------------------------------------------------------------
//  Select chip (select==TRUE).
//------------------------------------------------------------------------------
void SPI_PulseChipSelect(BOOL select)
{
    if (select)
    {
        SPI_PulseInit();
        PULSE_SSEL_PIN_LOW();
        delay_us();
    }
    else
    {
        delay_us();
        PULSE_SSEL_PIN_HIGH();
    }
}

void SPI_PulseInit(void) // pulse module
{
    PULSE_SSP_DISABLE();

    PCONP = (PCONP & ~PULSE_PCONP_SSP_MASK) | PULSE_PCONP_SSP_VALUE;
    PULSE_PCLKSEL = (PULSE_PCLKSEL & ~PULSE_PCLKSEL_SSP_MASK) | PULSE_PCLKSEL_SSP_VALUE;
    PULSE_SSPCPSR = PULSE_SSPCPSR_VALUE;
    PULSE_PINSEL = (PULSE_PINSEL & ~PULSE_PINSEL_SSP_MASK) | PULSE_PINSEL_SSP_VALUE;
    PULSE_PINMODE = (PULSE_PINMODE & ~PULSE_PINMODE_SSP_MASK) | PULSE_PINMODE_SSP_VALUE;
    PULSE_SSPIMSC = PULSE_SSPIMSC_VALUE;
    VICIntEnClr = PULSE_VICIntEnClr_SSP_VALUE;
    PULSE_SSPCR0 = PULSE_SSPCR0_VALUE;
    PULSE_SSPCR1 = PULSE_SSPCR1_VALUE;
    IODIR0 &= ~0x100; // P0.8 set to input
    IOCLR0 = 0x100; // P0.8 set to pull down
    PULSE_IODIR = (PULSE_IODIR & ~PULSE_IODIR_SSP_MASK) | PULSE_IODIR_SSP_VALUE;

    SPI_PulseChipSelect(FALSE);
    PULSE_SSP_ENABLE();
}

void SPI_PulseTransmit(unsigned char cData) // pulse module
{
  SSP1DR = 0x0000 + cData;
  while ((SSP1SR & 0x10)) ; // wait for end of transfer
}

void SPI_DisplayInit(void)
{
  IODIR1 |= 0x03700000;
  IOSET1 |= 0x03700000;
  PINSEL3 |= 0x0003CF00; // configure SPI0 pins
  SSP0CR0  = 0x00C7; // CPHA=1, CPOL=1, master mode, MSB first, interrupt enabled
  SSP0CPSR = 0x2;
  SSP0CR1 = 0x0002;
}

void SPI_DisplayTransmit(unsigned char cData)
{
  SSP0DR = 0x0000 + cData;
  while ((SSP0SR & 0x10)) ; // wait for end of transfer
}
