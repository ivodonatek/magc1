//******************************************************************************
//  Systemovy tik
//*****************************************************************************/

#include <LPC23xx.H>
#include "target.h"
#include "type.h"
#include "systick.h"
#include "sensor.h"
#include "port.h"
#include "pulse_outputs.h"
#include "led.h"

volatile unsigned long halfMsecTick = 0; // pocitani 1/2 milisekund od startu systemu
volatile unsigned long msecTick = 0; // pocitani milisekund od startu systemu

//------------------------------------------------------------------------------
//  Hlavni uloha
//------------------------------------------------------------------------------
static void SysTick_Task(void)
{
    halfMsecTick++;
    PulseOutputs_HalfMillisecondTick();

    if ((halfMsecTick & 1) == 0)
    {
        msecTick++;
        Sensor_MillisecondTick();
        //Led_MillisecondTick();
    }
}

//------------------------------------------------------------------------------
//  Rutina preruseni Timeru 3
//------------------------------------------------------------------------------
static void Timer3InterruptHandler(void)
{
    SysTick_Task();
    T3IR |= (1<<2); //clear interrupt flag on MR2
    VICVectAddr = 0; //vynulovani registru preruseni
}

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//------------------------------------------------------------------------------
void SysTick_Init(void)
{
    PCONP |= (1<<23);
    T3MR2 = Fpclk / 2000; // cca 0.5ms
    T3MCR |= (1<<6) | (1<<7); // Interrupt on MR2, Reset on MR2
    VICVectAddr27 = (unsigned long)Timer3InterruptHandler; // Set Interrupt Vector
    VICIntEnable = (1<<27); // Enable Timer3 Interrupt
    T3TCR |= (1<<0);
}

unsigned long SysTick_GetSysMilliSeconds(void)
{
    volatile unsigned long ms;
    ms = msecTick;
    return ms;
}

unsigned char SysTick_IsSysMilliSecondTimeout(unsigned long userSysMilliSeconds, unsigned long delayMilliSeconds)
{
    if ((SysTick_GetSysMilliSeconds() - userSysMilliSeconds) >= delayMilliSeconds)
        return 1;
    else
        return 0;
}
