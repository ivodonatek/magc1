//******************************************************************************
//  Ovladani indikacnich LEDek
//*****************************************************************************/

#ifndef LED_H_INCLUDED
#define LED_H_INCLUDED

#ifndef BOOL_TYPEDEF
#define BOOL_TYPEDEF
typedef unsigned char BOOL;
#endif

#ifndef TRUE
#define TRUE            1
#endif

#ifndef FALSE
#define FALSE           0
#endif

// Identifikator LEDky
typedef enum LEDIdentifier
{
    LED_R1 = 0,
    LED_G1,
    LED_R2,
    LED_G2,
    LED_R3,
    LED_G3,
    LED_R4,
    LED_G4,

    LED_Min = LED_R1,
    LED_Max = LED_G4,
    LED_Count = LED_Max + 1

} TLEDIdentifier;

// Stav LEDky
typedef enum LEDState
{
    LEDS_OFF,  // nesviti
    LEDS_ON    // sviti

} TLEDState;

// Mod LEDek
typedef enum LEDMode
{
    LEDM_ERRORS,        // indikace chyb
    LEDM_BIN_RE_OUTS,   // indikace stavu vystupu BIN RE
    LEDM_OFF,           // zadna indikace

    LEDM_Min = LEDM_ERRORS,
    LEDM_Max = LEDM_OFF,
    LEDM_Count = LEDM_Max + 1

} TLEDMode;

// Casove omezeny stav LEDky
/*typedef struct LEDTimeLimitedState
{
    // stav LEDky
    TLEDState state;

    // doba trvani [ms]
    uint16_t durationMsec;

} TLEDTimeLimitedState;*/

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//  Volat po PulseOutputs_Init.
//------------------------------------------------------------------------------
void Led_Init(void);

//------------------------------------------------------------------------------
//  Hlavni uloha. Volat co nejcasteji.
//------------------------------------------------------------------------------
void Led_Task(void);

//------------------------------------------------------------------------------
//  Nastaveni trvaleho stavu LEDky.
//------------------------------------------------------------------------------
void Led_SetState(TLEDIdentifier id, TLEDState ledState);

//------------------------------------------------------------------------------
//  Nastaveni trvaleho stavu vsech LEDek.
//------------------------------------------------------------------------------
void Led_SetAllState(TLEDState ledState);

//------------------------------------------------------------------------------
//  Nastaveni programu LEDky.
//------------------------------------------------------------------------------
//void Led_SetProgram(TLEDIdentifier id, const TLEDTimeLimitedState *program, uint8_t programLength, BOOL programRepeat);

//------------------------------------------------------------------------------
//  Casovani programu LEDek. Volat kazdou ms.
//------------------------------------------------------------------------------
//void Led_MillisecondTick(void);

//------------------------------------------------------------------------------
//  Udalost zmeny BIN RE vystupu.
//------------------------------------------------------------------------------
void Led_OnBinReChanged(TPulseOutIdentifier id);

//------------------------------------------------------------------------------
//  Udalost zmeny parametru LEDek.
//------------------------------------------------------------------------------
void Led_OnParameterChanged(void);

#endif // LED_H_INCLUDED
