//******************************************************************************
//  Ovladac pulsniho sensoru prutoku
//*****************************************************************************/

#include <stdint.h>
#include <LPC23xx.h>
#include "port.h"
#include "maine.h"
#include "pulse_flow_sensor.h"
#include "systick.h"
#include "extmeas/temperature/exttemp.h"

#define FLOW_PULSE_MEASURING_PERIOD     1000 //ms
#define FLOW_PULSE_HIGH_LEVEL_TIMEOUT   5000 //ms

// Fw module state
static TPulseFlowSensorState state;

// init vzorku
static void InitPulseSample(TPulseSample *ps)
{
    ps->pulseCount = 0;
    ps->time = 0;
    ps->valid = FALSE;
}

//------------------------------------------------------------------------------
//  Vraci pocet pulsu na litr
//------------------------------------------------------------------------------
static unsigned long GetPulsesPerLitre(void)
{
    unsigned long ppl = RAM_Read_Data_Long(PULSES_PER_LITRE);
    unsigned long pplMin = RAM_Read_Data_Long(PULSES_PER_LITRE_MIN);
    unsigned long pplMax = RAM_Read_Data_Long(PULSES_PER_LITRE_MAX);

    if (ppl < pplMin)
        return pplMin;
    else if (ppl > pplMax)
        return pplMax;
    else
        return ppl;
}

//------------------------------------------------------------------------------
//  Vraci timeout pulsu [s]
//------------------------------------------------------------------------------
static unsigned long GetFlowPulseTimeout(void)
{
    unsigned long fpt = RAM_Read_Data_Long(FLOW_PULSE_TIMEOUT);
    unsigned long fptMin = RAM_Read_Data_Long(FLOW_PULSE_TIMEOUT_MIN);
    unsigned long fptMax = RAM_Read_Data_Long(FLOW_PULSE_TIMEOUT_MAX);

    if (fpt < fptMin)
        return fptMin;
    else if (fpt > fptMax)
        return fptMax;
    else
        return fpt;
}

//------------------------------------------------------------------------------
//  Vraci uroven pulsniho vstupu (EXTFLOW pin je vstup do P1.19/CAP1.1)
//------------------------------------------------------------------------------
static BOOL GetFlowPulseInputLevel(void)
{
    if (IOPIN1 & (1<<19))
        return TRUE;
    else
        return FALSE;
}

// vypocet m3 z pulsu
static float GetM3(unsigned long pulses)
{
    unsigned long ppl = GetPulsesPerLitre();
    if (ppl == 0)
        return 0;

    float lpp = 1.0 / ppl;

    return (lpp * pulses) * 0.001;
}

// vypocet hodin z rozdilu casu vzorku
static float GetHour(unsigned long dSampleTime)
{
    return dSampleTime / (60*60*1000.0);
}

// mereni vzorku
static void MeasurePulseSamples(void)
{
    unsigned long dTime = state.actualSample.time - state.oldSample.time;
    if (dTime < FLOW_PULSE_MEASURING_PERIOD)
        return;

    actual_error &= ~ERR_SENSOR;

    state.dTms = dTime;
    float dHour = GetHour(state.dTms);

    state.dPulse = state.actualSample.pulseCount - state.oldSample.pulseCount;
    state.dM3 = GetM3(state.dPulse);

    if (RAM_Read_Data_Long(DEMO)) //neni demo
    {
        if (dHour > 0)
            g_measurement.actual = state.dM3 / dHour;

        // inkrementace totaliseru pokud je measurement ON
        if (!RAM_Read_Data_Long(MEASUREMENT_STATE))
            g_measurement.total += state.dM3 * 1000000;

        Heat_MeasureWithVolume(state.dM3, dTime / 1000.0);
    }

    // posun vzorku
    state.oldSample.pulseCount = state.actualSample.pulseCount;
    state.oldSample.time = state.actualSample.time;
    state.oldSample.valid = TRUE;
    state.actualSample.valid = FALSE;
}

// Kontrola pulsu
// - stav, kdy neprichazi zadny puls
// - dlouhodoby stav high urovne vstupu
static void CheckPulses(void)
{
    if (RAM_Read_Data_Long(DEMO)) //neni demo
    {
        if (SysTick_IsSysMilliSecondTimeout(state.lastPulseTime, GetFlowPulseTimeout() * 1000))
        {
            g_measurement.actual = 0;
            state.lastPulseTime = SysTick_GetSysMilliSeconds();
        }

        if (GetFlowPulseInputLevel())
        {
            if (SysTick_IsSysMilliSecondTimeout(state.highLevelTime, FLOW_PULSE_HIGH_LEVEL_TIMEOUT))
            {
                actual_error |= ERR_SENSOR;
                g_measurement.actual = 0;
                state.highLevelTime = SysTick_GetSysMilliSeconds();
            }
        }
        else
        {
            actual_error &= ~ERR_SENSOR;
            state.highLevelTime = SysTick_GetSysMilliSeconds();
        }
    }
    else // je DEMO
    {
        actual_error &= ~ERR_SENSOR;
    }
}

// ovladac preruseni
static void TimerCounter_interrupt(void)
{
    T1IR = 0xFF;

    if (state.isOpen)
    {
        state.pulseCount++;

        state.actualSample.pulseCount++;
        state.actualSample.time = state.lastPulseTime = state.highLevelTime = SysTick_GetSysMilliSeconds();
        state.actualSample.valid = TRUE;

        if (!state.oldSample.valid)
        {
            state.oldSample.pulseCount = state.actualSample.pulseCount;
            state.oldSample.time = state.actualSample.time;
            state.oldSample.valid = TRUE;
        }
    }

    VICVectAddr=0;
}

// hw init
static void HwInit(void)
{
    // EXTFLOW pin je vstup do P1.19/CAP1.1
    // Timer/Counter 1, CCLK/4
    VICIntEnClr = (1<<5);
    PCONP |= (1 << 2);
    PCLKSEL0 &= ~((1 << 5) | (1 << 4));
    PINSEL3 |= (1 << 7) | (1 << 6);
    T1CCR = (1 << 3) /*| (1 << 4)*/ | (1 << 5);
    T1TCR = (1 << 0);
    //T1TCR &= ~(1 << 1);
    T1CTCR = 0;
    T1PR = 0;
    VICVectAddr5=(unsigned long)TimerCounter_interrupt;
    VICIntEnable = (1<<5);
}

// hw disable
static void HwDisable(void)
{
    // Timer/Counter 1 off
    PCONP &= ~(1 << 2);
    VICIntEnClr = (1<<5);
}

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//------------------------------------------------------------------------------
void PulseFlowSensor_Init(BOOL open)
{
    ENTER_CRITICAL_SECTION();

    state.isOpen = FALSE;
    state.pulseCount = 0;
    state.lastPulseTime = state.highLevelTime = SysTick_GetSysMilliSeconds();

    InitPulseSample(&state.oldSample);
    InitPulseSample(&state.actualSample);

    state.dTms = 0;
    state.dPulse = 0;
    state.dM3 = 0;

    if (open)
        HwInit();
    else
        HwDisable();

    state.isOpen = open;

    EXIT_CRITICAL_SECTION();
}

//------------------------------------------------------------------------------
//  Hlavni uloha. Volat co nejcasteji.
//------------------------------------------------------------------------------
void PulseFlowSensor_Task(void)
{
    if (!state.isOpen)
        return;

    if (state.actualSample.valid && state.oldSample.valid)
    {
        ENTER_CRITICAL_SECTION();
        MeasurePulseSamples();
        EXIT_CRITICAL_SECTION();
    }
    else
    {
        ENTER_CRITICAL_SECTION();
        CheckPulses();
        EXIT_CRITICAL_SECTION();
    }
}

//------------------------------------------------------------------------------
//  Vrati stav.
//------------------------------------------------------------------------------
const TPulseFlowSensorState* PulseFlowSensor_GetState(void)
{
    return &state;
}
