//******************************************************************************
//  Komunikace MBus - slave na sbernici.
//*****************************************************************************/

#ifndef MBUS_SLAVE_H_INCLUDED
#define MBUS_SLAVE_H_INCLUDED

#include <stddef.h>
#include "mbus_protocol.h"

#define MBUS_BUFFER_LENGTH   2048  //delka prijimaciho/vysilaciho buferu

// stav komunikace
typedef enum
{
    MBS_CMSTA_NONE, // nic se nedeje
    MBS_CMSTA_RX, // prijem
    MBS_CMSTA_TX_DLY_BEFORE, // prodleva pred vysilanim
    MBS_CMSTA_TX_DATA, // vysilani dat
    MBS_CMSTA_TX_DLY_AFTER, // prodleva po vysilani

} TMBusSlaveCommStatus;

// MBus baud rates
typedef enum MBusBaudrate
{
    MBUS_BAUDRATE_300 = 0,
    MBUS_BAUDRATE_600,
    MBUS_BAUDRATE_1200,
    MBUS_BAUDRATE_2400,
    MBUS_BAUDRATE_4800,
    MBUS_BAUDRATE_9600,
    MBUS_BAUDRATE_19200,
    MBUS_BAUDRATE_38400,

    MBUS_BAUDRATE_Min = MBUS_BAUDRATE_300,
    MBUS_BAUDRATE_Max = MBUS_BAUDRATE_38400,
    MBUS_BAUDRATE_Count = MBUS_BAUDRATE_Max + 1

} TMBusBaudrate;

// MBus parita
typedef enum MBusParity
{
    MBUS_PAR_EVEN = 0,
    MBUS_PAR_ODD,
    MBUS_PAR_NONE_TWO_STOPBITS,
    MBUS_PAR_NONE,

    MBUS_PAR_Min = MBUS_PAR_EVEN,
    MBUS_PAR_Max = MBUS_PAR_NONE,
    MBUS_PAR_Count = MBUS_PAR_Max + 1

} TMBusParity;

// Prijimaci/vysilaci bufer
typedef struct
{
    // data
    unsigned char data[MBUS_BUFFER_LENGTH];

    // pocet obsazenych bytu v buferu
    size_t dataCount;

    // index bytu v buferu (pouzito k prave odesilane pozici v buferu)
    size_t dataIndex;

    // flag, ze neco bylo prijato
    BOOL received;

} TMBusBuffer;

// Fw module state
typedef struct
{
    // open state
    BOOL isOpen;

    // stav komunikace
    TMBusSlaveCommStatus commStatus;

    // prijimaci/vysilaci bufer
    TMBusBuffer buffer;

    // MBus ramec
    mbus_frame frame;

    // casovac
    unsigned long timer;

    // pocet prijatych ramcu
    unsigned long rxFrameCount;

    // pocet timeoutu prijmu
    unsigned long rxTimeoutCount;

    // kod chyby na prijmu
    int rxErrorCode;

    // pocet odeslanych ramcu
    unsigned long txFrameCount;

} TMBusSlaveState;

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//  (s vyjimkou funkce MBusSlave_DetectLink).
//  Musi byt ale volny UART1 pri open == TRUE.
//------------------------------------------------------------------------------
void MBusSlave_Init(BOOL open);

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void MBusSlave_Task(void);

//------------------------------------------------------------------------------
//  Vrati stav.
//------------------------------------------------------------------------------
const TMBusSlaveState* MBusSlave_GetState(void);

//------------------------------------------------------------------------------
//  Detekce linky MBus (nemusi se pred tim volat MBusSlave_Init).
//------------------------------------------------------------------------------
BOOL MBusSlave_DetectLink(void);

//------------------------------------------------------------------------------
//  Udalost zmeny parametru.
//------------------------------------------------------------------------------
void MBusSlave_OnParameterChanged(void);

#endif // MBUS_SLAVE_H_INCLUDED
