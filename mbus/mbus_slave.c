//******************************************************************************
//  Komunikace MBus - slave na sbernici.
//  - pouzita dokumentace na http://www.m-bus.com/mbusdoc/default.php
//*****************************************************************************/

#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include "LPC23xx.h"
#include "target.h"
#include "type.h"
#include "maine.h"
#include "menu.h"
#include "menu_tables.h"
#include "mbus_protocol.h"
#include "heat.h"
#include "mbus_slave.h"
#include "extmeas/temperature/exttemp.h"
#include "../systick.h"
#include "../pulse_outputs.h"
#include "../led.h"
#include "../conversion.h"

// EN 61107 manufacturer ID (three uppercase letters)
const char MANUFACTURER_ID[] = "ARK";

// Fixed Data Header version of Variable Data Structure
#define VARIABLE_DATA_STRUCTURE_VERSION     1

// Timeout prijmu [ms] (After a time limit of (330 bit periods + 50 ms) the master
// interprets the lack of a reply as a fault and repeats the same telegram up to two times.)
#define RCV_TIMEOUT         45

// chybove kody na prijmu (UART)
#define OVERRUN_RX_ERROR        (-11)   // Overrun Error (OE)
#define PARITY_RX_ERROR         (-12)   // Parity Error (PE)
#define FRAMING_RX_ERROR        (-13)   // Framing Error (FE)

// Fw module state
volatile TMBusSlaveState state;

// for Access No., The Access Number has unsigned binary coding, and is increased by one after each RSP_UD from the slave
static unsigned char rspUdCounter = 0;

// Variable Data Blocks record
static mbus_data_record mbusDataRecord;

//------------------------------------------------------------------------------
//  Start casovace
//------------------------------------------------------------------------------
static void StartTimer(void)
{
    state.timer = SysTick_GetSysMilliSeconds();
}

//------------------------------------------------------------------------------
//  Zjisteni timeoutu
//------------------------------------------------------------------------------
static unsigned char IsTimeout(unsigned long delay)
{
    return SysTick_IsSysMilliSecondTimeout(state.timer, delay);
}

//------------------------------------------------------------------------------
//  Vraci baudrate
//------------------------------------------------------------------------------
static unsigned int GetBaudrate(void)
{
    switch (RAM_Read_Data_Long(MBUS_BAUDRATE))
    {
        case MBUS_BAUDRATE_300:
            return 300;
        case MBUS_BAUDRATE_600:
            return 600;
        case MBUS_BAUDRATE_1200:
            return 1200;
        case MBUS_BAUDRATE_2400:
            return 2400;
        case MBUS_BAUDRATE_4800:
            return 4800;
        case MBUS_BAUDRATE_9600:
            return 9600;
        case MBUS_BAUDRATE_19200:
            return 19200;
        case MBUS_BAUDRATE_38400:
            return 38400;
        default:
            return 9600;
    }
}

//------------------------------------------------------------------------------
//  Vraci paritu
//------------------------------------------------------------------------------
static TMBusParity GetParity(void)
{
    unsigned long parity = RAM_Read_Data_Long(MBUS_PARITY);
    if (parity <= MBUS_PAR_Max)
        return (TMBusParity)parity;
    else
        return MBUS_PAR_EVEN;
}

//------------------------------------------------------------------------------
//  Vraci slave adresu
//------------------------------------------------------------------------------
static unsigned char GetSlaveAddress(void)
{
    unsigned long slaveAdr = RAM_Read_Data_Long(MBUS_SLAVE_ADDRESS);
    if ((MBUS_ADDRESS_SLAVE_MIN <= slaveAdr) && (slaveAdr <= MBUS_ADDRESS_SLAVE_MAX))
        return (unsigned char)slaveAdr;
    else
        return MBUS_ADDRESS_SLAVE_NONE;
}

//------------------------------------------------------------------------------
//  Vraci ID (Identification Number)
//------------------------------------------------------------------------------
static unsigned long GetIdentificationNumber(void)
{
    return RAM_Read_Data_Long(UNIT_NO);
}

//------------------------------------------------------------------------------
//  Vraci Status pole hlavicky dat
//------------------------------------------------------------------------------
static unsigned char GetStatusField(void)
{
    return 0;
}

//------------------------------------------------------------------------------
//  Je prijaty (master->slave) ramec adresovan pro mne?
//------------------------------------------------------------------------------
static BOOL IsM2SFrameForMe(void)
{
    unsigned char frameAdr = state.frame.address;
    unsigned char mySlaveAdr = GetSlaveAddress();

    if (mySlaveAdr == MBUS_ADDRESS_SLAVE_NONE)
        return FALSE;

    if ((MBUS_ADDRESS_SLAVE_MIN <= frameAdr) && (frameAdr <= MBUS_ADDRESS_SLAVE_MAX))
    {
        if (mySlaveAdr == frameAdr)
            return TRUE;
    }
    else
    {
        if (frameAdr == MBUS_ADDRESS_BROADCAST_REPLY)
            return TRUE;

        if (frameAdr == MBUS_ADDRESS_BROADCAST_NOREPLY)
            return TRUE;
    }

    return FALSE;
}

//------------------------------------------------------------------------------
//  Vyplni hlavicku variabilnich dat.
//------------------------------------------------------------------------------
static void FillVariableHeader(mbus_data_variable_header *header)
{
    //Ident.Nr. Manufr. Version Medium Access No. Status  Signature
    //4 Byte    2 Byte  1 Byte  1 Byte   1 Byte   1 Byte  2 Byte

    mbus_data_bcd_encode(header->id_bcd, 4, GetIdentificationNumber());

    unsigned int manId = mbus_manufacturer_id(MANUFACTURER_ID);
    header->manufacturer[0] = (unsigned char)manId;
    header->manufacturer[1] = (unsigned char)(manId >> 8);

    header->version = VARIABLE_DATA_STRUCTURE_VERSION;
    header->medium = MBUS_VARIABLE_DATA_MEDIUM_HEAT_OUT;
    header->access_no = rspUdCounter;
    header->status = GetStatusField();

    header->signature[0] = 0;
    header->signature[1] = 0;
}

//------------------------------------------------------------------------------
//  Vraci velikost bloku dat v bytech uvnitr ramce.
//------------------------------------------------------------------------------
static size_t GetDataRecordSizeInFrame(const mbus_data_record *record)
{
    return 2 + record->drh.dib.ndife + record->drh.vib.nvife + record->data_len;
}

//------------------------------------------------------------------------------
//  Vlozi blok dat do ramce. Pokud se nevleze, vraci FALSE.
//------------------------------------------------------------------------------
static BOOL InsertDataRecordIntoFrame(const mbus_data_record *record)
{
    size_t recordSize = GetDataRecordSizeInFrame(record);
    if ((recordSize + state.frame.data_size) > MBUS_FRAME_DATA_LENGTH)
        return FALSE;

    // DIF
    state.frame.data[state.frame.data_size++] = record->drh.dib.dif;

    // DIFE
    int i;
    for (i = 0; i < record->drh.dib.ndife; i++)
        state.frame.data[state.frame.data_size++] = record->drh.dib.dife[i];

    // VIF
    state.frame.data[state.frame.data_size++] = record->drh.vib.vif;

    // VIFE
    for (i = 0; i < record->drh.vib.nvife; i++)
        state.frame.data[state.frame.data_size++] = record->drh.vib.vife[i];

    // Data
    for (i = 0; i < record->data_len; i++)
        state.frame.data[state.frame.data_size++] = record->data[i];

    return TRUE;
}

//------------------------------------------------------------------------------
//  Vlozi FLOW do ramce. Pokud se nevleze, vraci FALSE.
//------------------------------------------------------------------------------
static BOOL InsertFlowIntoFrame(void)
{
    mbus_data_record_init(&mbusDataRecord);

    mbusDataRecord.drh.dib.dif = MBUS_DATA_RECORD_DIF_INST_REAL32;
    mbusDataRecord.drh.vib.vif = MBUS_DATA_RECORD_VIF_VOLUME_FLOW_1M3H;
    memcpy(mbusDataRecord.data, &g_measurement.actual, sizeof(float));
    mbusDataRecord.data_len = sizeof(float);

    return InsertDataRecordIntoFrame(&mbusDataRecord);
}

//------------------------------------------------------------------------------
//  Vlozi TOTAL do ramce. Pokud se nevleze, vraci FALSE.
//------------------------------------------------------------------------------
static BOOL InsertTotalIntoFrame(void)
{
    mbus_data_record_init(&mbusDataRecord);

    mbusDataRecord.drh.dib.dif = MBUS_DATA_RECORD_DIF_INST_REAL32;
    mbusDataRecord.drh.vib.vif = MBUS_DATA_RECORD_VIF_VOLUME_1M3;
    float value = g_measurement.total/1000000.0;
    memcpy(mbusDataRecord.data, &value, sizeof(float));
    mbusDataRecord.data_len = sizeof(float);

    return InsertDataRecordIntoFrame(&mbusDataRecord);
}

//------------------------------------------------------------------------------
//  Vlozi TEMP do ramce. Pokud se nevleze, vraci FALSE.
//------------------------------------------------------------------------------
static BOOL InsertTempIntoFrame(void)
{
    mbus_data_record_init(&mbusDataRecord);

    mbusDataRecord.drh.dib.dif = MBUS_DATA_RECORD_DIF_INST_REAL32;
    mbusDataRecord.drh.vib.vif = MBUS_DATA_RECORD_VIF_EXTERNAL_TEMPERATURE_1C;
    memcpy(mbusDataRecord.data, &g_measurement.temp, sizeof(float));
    mbusDataRecord.data_len = sizeof(float);

    return InsertDataRecordIntoFrame(&mbusDataRecord);
}

//------------------------------------------------------------------------------
//  Vlozi TEMP1(2) do ramce. Pokud se nevleze, vraci FALSE.
//------------------------------------------------------------------------------
static BOOL InsertTempXIntoFrame(TTempChannel channel)
{
    mbus_data_record_init(&mbusDataRecord);

    mbusDataRecord.drh.dib.dif = MBUS_DATA_RECORD_DIF_INST_REAL32;

    if (channel == TCHNL_1)
        mbusDataRecord.drh.vib.vif = MBUS_DATA_RECORD_VIF_FLOW_TEMPERATURE_1C;
    else
        mbusDataRecord.drh.vib.vif = MBUS_DATA_RECORD_VIF_RETURN_TEMPERATURE_1C;

    float measValue = ExtTemp_GetLastCorrectMeasuredValue(channel);

    if (measValue != 0)
        measValue = Convert_KelvinToCelsius(measValue);
    else
        measValue = 0;

    memcpy(mbusDataRecord.data, &measValue, sizeof(float));
    mbusDataRecord.data_len = sizeof(float);

    return InsertDataRecordIntoFrame(&mbusDataRecord);
}

//------------------------------------------------------------------------------
//  Vlozi HEAT_PERFORM do ramce. Pokud se nevleze, vraci FALSE.
//------------------------------------------------------------------------------
static BOOL InsertHeatPerformIntoFrame(void)
{
    mbus_data_record_init(&mbusDataRecord);

    mbusDataRecord.drh.dib.dif = MBUS_DATA_RECORD_DIF_INST_REAL32;
    mbusDataRecord.drh.vib.vif = MBUS_DATA_RECORD_VIF_POWER_1W;

    float heatPerform = Heat_GetHeatPerformance();
    if (heatPerform < 0)
        heatPerform = 0;

    memcpy(mbusDataRecord.data, &heatPerform, sizeof(float));
    mbusDataRecord.data_len = sizeof(float);

    return InsertDataRecordIntoFrame(&mbusDataRecord);
}

//------------------------------------------------------------------------------
//  Vlozi HEAT_TOTAL do ramce. Pokud se nevleze, vraci FALSE.
//------------------------------------------------------------------------------
static BOOL InsertHeatTotalIntoFrame(void)
{
    mbus_data_record_init(&mbusDataRecord);

    mbusDataRecord.drh.dib.dif = MBUS_DATA_RECORD_DIF_INST_REAL32;
    mbusDataRecord.drh.vib.vif = MBUS_DATA_RECORD_VIF_ENERGY_1KWH;

    float heatTotal = Heat_GetTotalHeat(HU_kWh);
    if (heatTotal < 0)
        heatTotal = 0;

    memcpy(mbusDataRecord.data, &heatTotal, sizeof(float));
    mbusDataRecord.data_len = sizeof(float);

    return InsertDataRecordIntoFrame(&mbusDataRecord);
}

//------------------------------------------------------------------------------
//  Vlozi bloky variabilnich dat do ramce. Pokud se nevleze, vraci FALSE.
//------------------------------------------------------------------------------
static BOOL InsertVariableDataBlocksIntoFrame(void)
{
    if (!InsertFlowIntoFrame())
        return FALSE;

    if (!InsertTotalIntoFrame())
        return FALSE;

    if (!InsertTempIntoFrame())
        return FALSE;

    if (!InsertTempXIntoFrame(TCHNL_1))
        return FALSE;

    if (!InsertTempXIntoFrame(TCHNL_2))
        return FALSE;

    if (!InsertHeatPerformIntoFrame())
        return FALSE;

    if (!InsertHeatTotalIntoFrame())
        return FALSE;

    return TRUE;
}

//------------------------------------------------------------------------------
//  Odesilani dat na sbernici.
//------------------------------------------------------------------------------
static void SendData(void)
{
    if (state.buffer.dataIndex < state.buffer.dataCount)
    {
        U1IER = 0x03; // RBR Interrupt Enable, THRE Interrupt Enable
        U1THR = state.buffer.data[state.buffer.dataIndex];
        state.buffer.dataIndex++;
    }
    else
    {
        U1IER = 0x01; // RBR Interrupt Enable
        state.commStatus = MBS_CMSTA_TX_DLY_AFTER;
        StartTimer();
    }
}

//------------------------------------------------------------------------------
//  Odeslani ACK na sbernici.
//------------------------------------------------------------------------------
static void SendACK(void)
{
    ENTER_CRITICAL_SECTION();

    state.buffer.data[0] = MBUS_FRAME_ACK_START;
    state.buffer.dataCount = 1;
    state.buffer.dataIndex = 0;

    state.commStatus = MBS_CMSTA_TX_DLY_BEFORE;
    StartTimer();

    EXIT_CRITICAL_SECTION();
}

//------------------------------------------------------------------------------
//  Odeslani UD2 (Class 2 Data) na sbernici.
//------------------------------------------------------------------------------
static void SendUD2(void)
{
    ENTER_CRITICAL_SECTION();

    mbus_frame_init(&state.frame, MBUS_FRAME_TYPE_LONG);

    state.frame.control = MBUS_CONTROL_F_RSP_UD;
    state.frame.address = GetSlaveAddress();
    state.frame.control_information = MBUS_CONTROL_INFO_RESP_VARIABLE;

    state.frame.data_size = 0;

    mbus_data_variable_header header;
    FillVariableHeader(&header);

    // pack variable data structure header
    state.frame.data[state.frame.data_size++] = header.id_bcd[0];
    state.frame.data[state.frame.data_size++] = header.id_bcd[1];
    state.frame.data[state.frame.data_size++] = header.id_bcd[2];
    state.frame.data[state.frame.data_size++] = header.id_bcd[3];
    state.frame.data[state.frame.data_size++] = header.manufacturer[0];
    state.frame.data[state.frame.data_size++] = header.manufacturer[1];
    state.frame.data[state.frame.data_size++] = header.version;
    state.frame.data[state.frame.data_size++] = header.medium;
    state.frame.data[state.frame.data_size++] = header.access_no;
    state.frame.data[state.frame.data_size++] = header.status;
    state.frame.data[state.frame.data_size++] = header.signature[0];
    state.frame.data[state.frame.data_size++] = header.signature[1];

    // pack variable data blocks
    BOOL insertResult = InsertVariableDataBlocksIntoFrame();

    int packResult = 0;
    if (insertResult)
        packResult = mbus_frame_pack(&state.frame, state.buffer.data, MBUS_BUFFER_LENGTH);

    if (packResult > 0)
    {
        state.buffer.dataCount = packResult;
        state.buffer.dataIndex = 0;

        state.commStatus = MBS_CMSTA_TX_DLY_BEFORE;
        StartTimer();

        rspUdCounter++;
    }
    else
    {
        state.commStatus = MBS_CMSTA_NONE;
    }

    EXIT_CRITICAL_SECTION();
}

//------------------------------------------------------------------------------
//  Zpracuje timeout prijmu
//------------------------------------------------------------------------------
static void ProcessRcvTimeout(void)
{
    state.commStatus = MBS_CMSTA_NONE;
    state.rxTimeoutCount++;
}

//------------------------------------------------------------------------------
//  Zpracuje timeout prodlevy pred vysilanim
//------------------------------------------------------------------------------
static void ProcessBeforeTxTimeout(void)
{
    ENTER_CRITICAL_SECTION();

    state.commStatus = MBS_CMSTA_TX_DATA;
    SendData();

    EXIT_CRITICAL_SECTION();
}

//------------------------------------------------------------------------------
//  Zpracuje timeout prodlevy po vysilani
//------------------------------------------------------------------------------
static void ProcessAfterTxTimeout(void)
{
    state.commStatus = MBS_CMSTA_NONE;
    state.txFrameCount++;
}

//------------------------------------------------------------------------------
//  Zpracuje prijaty ramec - CONTROL
//------------------------------------------------------------------------------
static void ProcessRcvFrameControl(void)
{
    unsigned char func = state.frame.control & MBUS_CONTROL_MASK_F;

    switch (func)
    {
    case MBUS_CONTROL_F_SND_NKE:
    case MBUS_CONTROL_F_SND_UD:
        {
            if (state.frame.address == MBUS_ADDRESS_BROADCAST_NOREPLY)
                state.commStatus = MBS_CMSTA_NONE;
            else
                SendACK();
        }
        break;

    case MBUS_CONTROL_F_REQ_UD2:
    case MBUS_CONTROL_F_REQ_UD1:
        {
            if (state.frame.address == MBUS_ADDRESS_BROADCAST_NOREPLY)
                state.commStatus = MBS_CMSTA_NONE;
            else
                SendUD2();
        }
        break;

    default:
        state.commStatus = MBS_CMSTA_NONE;
        break;
    }
}

//------------------------------------------------------------------------------
//  Zpracuje prijaty ramec
//------------------------------------------------------------------------------
static void ProcessRcvFrame(void)
{
    switch (state.frame.type)
    {
    case MBUS_FRAME_TYPE_SHORT:
    case MBUS_FRAME_TYPE_CONTROL:
    case MBUS_FRAME_TYPE_LONG:
        if ((mbus_frame_direction(&state.frame) == MBUS_CONTROL_MASK_DIR_M2S) &&
            (IsM2SFrameForMe()))
        {
            ProcessRcvFrameControl();
        }
        else
        {
            state.commStatus = MBS_CMSTA_NONE;
        }
        break;

    default:
        state.commStatus = MBS_CMSTA_NONE;
        break;
    }
}

//------------------------------------------------------------------------------
//  Zpracuje prijata data
//------------------------------------------------------------------------------
static void ProcessRcvData(void)
{
    volatile size_t data_size;

    ENTER_CRITICAL_SECTION();

    state.buffer.received = FALSE;
    data_size = state.buffer.dataCount;

    EXIT_CRITICAL_SECTION();

    memset((void *)&state.frame, 0, sizeof(mbus_frame));

    int parseResult = mbus_parse(&state.frame, state.buffer.data, data_size);

    if (parseResult == 0)
    {
        state.rxErrorCode = 0;
        state.rxFrameCount++;

        ProcessRcvFrame();
    }
    else if (parseResult > 0)
    {
        if ((parseResult + data_size) > MBUS_BUFFER_LENGTH)
        {
            state.rxErrorCode = -2; // not a valid M-bus frame
            state.commStatus = MBS_CMSTA_NONE;
        }
    }
    else // error
    {
        state.rxErrorCode = parseResult;
        state.commStatus = MBS_CMSTA_NONE;
    }
}

//------------------------------------------------------------------------------
//  Obsluha preruseni od UART1
//------------------------------------------------------------------------------
static void OnRxUartHandler(void)
{
    volatile unsigned char lsrReg = U1LSR;

    if (lsrReg & 0x01)
    {
        volatile unsigned char rxByte = U1RBR;

        if ((state.isOpen) && ((state.commStatus == MBS_CMSTA_NONE) || (state.commStatus == MBS_CMSTA_RX)))
        {
            if (state.commStatus == MBS_CMSTA_NONE)
            {
                state.commStatus = MBS_CMSTA_RX;
                state.buffer.dataCount = 0;
            }

            if (lsrReg & 0x02)
            {
                state.rxErrorCode = OVERRUN_RX_ERROR;
                state.commStatus = MBS_CMSTA_NONE;
            }
            else if (lsrReg & 0x04)
            {
                state.rxErrorCode = PARITY_RX_ERROR;
                state.commStatus = MBS_CMSTA_NONE;
            }
            else if (lsrReg & 0x08)
            {
                state.rxErrorCode = FRAMING_RX_ERROR;
                state.commStatus = MBS_CMSTA_NONE;
            }
            else if (state.buffer.dataCount < MBUS_BUFFER_LENGTH)
            {
                state.buffer.data[state.buffer.dataCount] = rxByte;
                state.buffer.dataCount++;
                state.buffer.received = TRUE;
                StartTimer();
            }
        }
    }

    if (lsrReg & (1<<5))
    {
        if ((state.isOpen) && (state.commStatus == MBS_CMSTA_TX_DATA))
            SendData();
    }

    VICVectAddr = 0;
}

//------------------------------------------------------------------------------
//  Vrati prodlevu [ms] pred odeslanim dat na sbernici.
//  Viz. http://www.m-bus.com/mbusdoc/md5.php
//  � After the reception of a valid telegram the slave has to wait between
//  11 bit times and (330 bit times + 50ms) before answering (see also EN1434-3).
//------------------------------------------------------------------------------
static unsigned long GetDelayBeforeSendData(void)
{
    // 12bit pro jistotu, ale ne mensi nez 2ms
    unsigned long delay = (12 * 1000UL) / GetBaudrate();
    if (delay < 2)
        delay = 2;

    return delay;
}

//------------------------------------------------------------------------------
//  Vrati prodlevu [ms] po odeslani dat na sbernici.
//------------------------------------------------------------------------------
static unsigned long GetDelayAfterSendData(void)
{
    // 1ms
    return 1;
}

//------------------------------------------------------------------------------
//  Inicializace hw komunikacniho rozhrani.
//------------------------------------------------------------------------------
static void MBusCommHwInit(void)
{
    PCONP |= (1<<4);      //set power on UART1
    VICIntEnClr = (1<<7); // UART1 disable
    PINSEL4 &= ~(0x0F<<0);  //zruseni TxD1 a RxD1 na pinech 2.0 a 2.1
    PINSEL0 &= ~(3<<30);   //zruseni TxD1 na pinu 0.15
    PINSEL1 &= ~(3<<0);   //zruseni RxD1 na pinu 0.16
    PINSEL0 |= (1<<30);   //nastaveni TxD1 na pin 0.15
    PINSEL1 |= (1<<0);    //nastaveni RxD1 na pin 0.16

    unsigned char cfg = 0x03; // 8bits

    switch (GetParity())
    {
    case MBUS_PAR_NONE:
        break;
    case MBUS_PAR_ODD:
        cfg |= 0x08;
        break;
    case MBUS_PAR_EVEN:
        cfg |= 0x18;
        break;
    case MBUS_PAR_NONE_TWO_STOPBITS:
        cfg |= 0x04;
        break;
    }

    U1LCR = cfg; /* Configure Data Bits and Parity */

    U1FCR = 0x07;   //FIFO Enable and restart tx and rx

    U1LCR |= 0x80;          /* Set DLAB */
    unsigned long Fdiv = ( Fpclk / GetBaudrate() ) / 16UL;	//baud rate
    U1DLM = Fdiv / 256;
    U1DLL = Fdiv % 256;
    U1LCR &= ~0x80;         // Clear DLAB

    // preruseni
    VICVectAddr7 = (unsigned long) OnRxUartHandler;   //set interrupt vector
    VICIntEnable = (1<<7);
    U1IER = 0x01; // RBR Interrupt Enable
}

//------------------------------------------------------------------------------
//  Inicializace stavu.
//------------------------------------------------------------------------------
static void MBusStateInit(BOOL open)
{
    state.isOpen = open;
    state.commStatus = MBS_CMSTA_NONE;
    state.buffer.dataCount = 0;
    state.buffer.received = FALSE;
    state.timer = 0;
    state.rxErrorCode = 0;
    state.rxFrameCount = 0;
    state.rxTimeoutCount = 0;
    state.txFrameCount = 0;
}

//------------------------------------------------------------------------------
//  Inicializace rozhrani, volat pred ostatnimi funkcemi rozhrani
//  (s vyjimkou funkce MBusSlave_DetectLink).
//  Musi byt ale volny UART1 pri open == TRUE.
//------------------------------------------------------------------------------
void MBusSlave_Init(BOOL open)
{
    ENTER_CRITICAL_SECTION();

    MBusStateInit(open);

    if (open)
        MBusCommHwInit();

    EXIT_CRITICAL_SECTION();
}

//------------------------------------------------------------------------------
//  Hlavni uloha, volat pravidelne a co nejcasteji.
//------------------------------------------------------------------------------
void MBusSlave_Task(void)
{
    if (!state.isOpen)
        return;

    if (state.commStatus == MBS_CMSTA_RX)
    {
        if (IsTimeout(RCV_TIMEOUT))
            ProcessRcvTimeout();
        else if (state.buffer.received)
            ProcessRcvData();
    }
    else if (state.commStatus == MBS_CMSTA_TX_DLY_BEFORE)
    {
        if (IsTimeout(GetDelayBeforeSendData()))
            ProcessBeforeTxTimeout();
    }
    else if (state.commStatus == MBS_CMSTA_TX_DLY_AFTER)
    {
        if (!(U1LSR & (1<<6)))
            StartTimer();
        else if (IsTimeout(GetDelayAfterSendData()))
            ProcessAfterTxTimeout();
    }
}

//------------------------------------------------------------------------------
//  Vrati stav.
//------------------------------------------------------------------------------
const TMBusSlaveState* MBusSlave_GetState(void)
{
    return &state;
}

//------------------------------------------------------------------------------
//  Detekce linky MBus (nemusi se pred tim volat MBusSlave_Init).
//------------------------------------------------------------------------------
BOOL MBusSlave_DetectLink(void)
{
    if (IOPIN0 & (1<<17))
        return TRUE;
    else
        return FALSE;
}

//------------------------------------------------------------------------------
//  Udalost zmeny parametru.
//------------------------------------------------------------------------------
void MBusSlave_OnParameterChanged(void)
{
    if (!state.isOpen)
        return;

    ENTER_CRITICAL_SECTION();

    MBusStateInit(TRUE);
    MBusCommHwInit();

    EXIT_CRITICAL_SECTION();
}
