//******************************************************************************
//  Ovladac pulsnich vystupu
//*****************************************************************************/

#ifndef PULSE_OUTPUTS_H_INCLUDED
#define PULSE_OUTPUTS_H_INCLUDED

// Identifikator vystupu
typedef enum PulseOutIdentifier
{
    PULSE_OUT_1 = 0,    // BIN_RE1
    PULSE_OUT_2,        // BIN_RE2
    PULSE_OUT_3,        // BIN_RE3
    PULSE_OUT_4,        // BIN_RE4

    PULSE_OUT_Min = PULSE_OUT_1,
    PULSE_OUT_Max = PULSE_OUT_4,
    PULSE_OUT_Count = PULSE_OUT_Max + 1

} TPulseOutIdentifier;

// Stav vystupu
typedef enum PulseOutState
{
    PULSE_OUT_OFF,
    PULSE_OUT_ON,

    PULSE_OUT_STATE_Min = PULSE_OUT_OFF,
    PULSE_OUT_STATE_Max = PULSE_OUT_ON,
    PULSE_OUT_STATE_Count = PULSE_OUT_STATE_Max + 1

} TPulseOutState;

// Funkce vystupu typu RELAY
typedef enum PulseOutRelayFunc
{
    PULSE_OUT_RF_OFF = 0,
    PULSE_OUT_RF_FIXED,
    PULSE_OUT_RF_ERROR,
    PULSE_OUT_RF_POWER_IN,
    PULSE_OUT_RF_POWER_OUT,
    PULSE_OUT_RF_POWER_SMALLER,
    PULSE_OUT_RF_POWER_BIGGER,

    PULSE_OUT_RF_Min = PULSE_OUT_RF_OFF,
    PULSE_OUT_RF_Max = PULSE_OUT_RF_POWER_BIGGER,
    PULSE_OUT_RF_Count = PULSE_OUT_RF_Max + 1

} TPulseOutRelayFunc;

// Funkce vystupu typu OPTO
typedef enum PulseOutOptoFunc
{
    PULSE_OUT_OF_OFF = 0,
    PULSE_OUT_OF_FIXED,
    PULSE_OUT_OF_ERROR,
    PULSE_OUT_OF_POWER_IN,
    PULSE_OUT_OF_POWER_OUT,
    PULSE_OUT_OF_POWER_SMALLER,
    PULSE_OUT_OF_POWER_BIGGER,
    PULSE_OUT_OF_POWER_FOUT,

    PULSE_OUT_OF_Min = PULSE_OUT_OF_OFF,
    PULSE_OUT_OF_Max = PULSE_OUT_OF_POWER_FOUT,
    PULSE_OUT_OF_Count = PULSE_OUT_OF_Max + 1

} TPulseOutOptoFunc;

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//------------------------------------------------------------------------------
void PulseOutputs_Init(void);

//------------------------------------------------------------------------------
//  Hlavni uloha. Volat co nejcasteji.
//------------------------------------------------------------------------------
void PulseOutputs_Task(void);

//------------------------------------------------------------------------------
//  Vrati stav vystupu.
//------------------------------------------------------------------------------
TPulseOutState PulseOutputs_GetPulseOutState(TPulseOutIdentifier id);

//------------------------------------------------------------------------------
//  Casovani vystupu. Volat kazdou 1/2 ms.
//------------------------------------------------------------------------------
void PulseOutputs_HalfMillisecondTick(void);

//------------------------------------------------------------------------------
//  Udalost zmeny parametru vystupu.
//------------------------------------------------------------------------------
void PulseOutputs_OnParameterChanged(TPulseOutIdentifier id);

#endif // PULSE_OUTPUTS_H_INCLUDED
