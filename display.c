#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <LPC23xx.H>
#include "maine.h"
#include "display.h"
#include "spi.h"
#include "font.h"
#include "images.h"
#include "rprintf.h"
#include "power_man.h"
#include "heat.h"

void Print_Buffer(void) { // external ram + 180 rotated display
  unsigned char page,i;
  for(page=0;page<NUMBER_OF_PAGES;page++)
  {
    Write_Display(CMD,START_LINE_SET);
    Write_Display(CMD,PAGE_ADDRESS_SET + NUMBER_OF_PAGES - 1 - page); // - 1 kvuli novemu displeji jinak - 0
    Write_Display(CMD,COLUMN_ADDRESS_HIGH);
    Write_Display(CMD,COLUMN_ADDRESS_LOW);
    for (i=0;i<NUMBER_OF_COLUMNS;i++)
      Write_Display(DATA, g_frame_buffer[(i+(page*NUMBER_OF_COLUMNS))]); // Ram
 }
}

void WriteNow(void)
{
  Print_Buffer();
}

void Write( unsigned char xStart, unsigned char line, const char *str )
{
  unsigned int indexi;
  unsigned char i;
  unsigned char x;

  indexi = line*NUMBER_OF_COLUMNS+xStart;                  // Nastavi index na zacatek radky
  for ( x=0; x< strlen(str); x++ )
  {
    for ( i=0; i<(GetCharacterWidth( (unsigned char)str[x] )); i++ )
    {
      // Pixely, ktere smeruji mimo obrazovku zahazuji
      if ( (indexi) < (line+1)*NUMBER_OF_COLUMNS )
      {
        g_frame_buffer[indexi] = (GetFont( (str[x]*FULL_CHARACTER_WIDTH) + i ));
        indexi++;
      } // if
    } // for (i v jednom znaku)
  } // for (rada)
}

void WriteSpecialFont( unsigned char xStart, unsigned char line, const char *str )
{
  unsigned int indexi;
  unsigned char i;
  unsigned char x;

  indexi = line*NUMBER_OF_COLUMNS+xStart;                  // Nastavi index na zacatek radky
  for ( x=0; x< strlen(str); x++ )
  {
    for ( i=0; i<(GetSpecialCharacterWidth( (unsigned char)str[x] )); i++ )
    {
      // Pixely, ktere smeruji mimo obrazovku zahazuji
      if ( (indexi) < (line+1)*NUMBER_OF_COLUMNS )
      {
        g_frame_buffer[indexi] = (GetSpecialFont( (str[x]*FULL_CHARACTER_WIDTH) + i ));
        indexi++;
      } // if
    } // for (i v jednom znaku)
  } // for (rada)
}

void WriteImageResolution(unsigned char start_x, unsigned char start_line, int width, int heigth, int Offset)
{
  // Funkce zobrazuje obrazek urceny offsetem ofset. Obrazek bude umisteny od radky line a sloupce x
  //   (levi horni roh). width - sirka obrazku v pixelech, heigth - vyska obrazku v pixelech.
  int line, x, adr, indexi;
  adr = Offset;
  for (line=start_line; line<=(start_line+(heigth/8)-1); line++)
  {
    for (x=start_x; x<(width+start_x); x++)
    {
      // Pixely, ktere smeruji mimo obrazovku zahazuji
      if ((x<NUMBER_OF_COLUMNS)&&(line<=LAST_PAGE))
      {
        indexi = line*NUMBER_OF_COLUMNS+x;
        g_frame_buffer[indexi]= (P_Images[adr]);
        adr++;
      }
    }
  }
}

void WriteImage(unsigned char start_x, unsigned char start_line, int Offset)
{
  // Funkce vola WriteImageResolution, pricemz si nejdrive precte rozmery vykreslovaneho obrazku.
  int width  = (P_Images[Offset]);
  int heigth = (P_Images[Offset+1]);
  WriteImageResolution(start_x, start_line, width, heigth, Offset+2);
}

unsigned int SizeInPixel( const char *str )
{
  unsigned int  length = 0;
  unsigned char i;
  for ( i=0; i<strlen(str); i++ )
  {
    length+=(GetCharacterWidth((unsigned char)str[i]));
  }
  return(length);
}

unsigned int SizeSpecialInPixel( const char *str )
{
  unsigned int  length = 0;
  unsigned char i;
  for ( i=0; i<strlen(str); i++ )
  {
    length+=(GetSpecialCharacterWidth((unsigned char)str[i]));
  }
  return(length);
}

void InvertLineBuffer( unsigned char xStart, unsigned char HowMany )
{
  unsigned char position;
  for ( position=xStart; position<xStart+HowMany; position++ )
  {
    g_frame_buffer[position]=255-g_frame_buffer[position];
  }
}

void InvertLine( unsigned char xStart, unsigned char line, unsigned char HowMany )
{
  int i, indexi;
  for ( i=xStart; i<xStart+HowMany; i++ )
  {
    indexi = (line)*(NUMBER_OF_COLUMNS)+i;
    g_frame_buffer[indexi]=255-g_frame_buffer[indexi];
  }
}

void WriteLastRow( unsigned char value )
{
  // Funkce zobrazuje tlacitka na poslednim radku (OK/ESC)
  char str[2] = {0,0};
  if (value&BUTTON_OK)
  {
    str[0]=2;
    Write( LAST_COLUMN - SizeInPixel(str), LAST_ROW, str);
  }
  if (value&BUTTON_ESC)
  {
    str[0]=18;
    Write( 0, LAST_ROW, str);
  }
  if (value&BUTTON_MENU)
  {
    str[0]=1;
    Write( LAST_COLUMN - SizeInPixel(str), LAST_ROW, str);
  }
  // Sipka nahoru
  if (g_segment_display.status&0x02)
  {
    str[0]=19;
    Write(55, LAST_ROW, str);
  }
  // Sipka dolu
  if (g_segment_display.status&0x04)
  {
    str[0]=20;
    Write(63, LAST_ROW, str);
  }
}

// zapis jednoho radku do linebufferu
void WriteLine( unsigned char xStart, const char *str )
{
  unsigned int position;
  unsigned char i;
  unsigned char x;
  position = xStart;                  // Nastavi index na zacatek radky
  for ( x=0; x< strlen(str); x++ )
  {
    for ( i=0; i<(GetCharacterWidth((unsigned char)(str[x]))); i++ )
    {
      // Pixely, ktere smeruji mimo obrazovku zahazuji
      if ( position < NUMBER_OF_COLUMNS + 1 )
      {
        g_frame_buffer[position] = (GetFont( (str[x]*FULL_CHARACTER_WIDTH) + i ));
        position++;
      } // if
    } // for (i v jednom znaku)
  } // for (rada)
}

// Funkce vykresluje retezec str na radku line vycentrovane. Pokud delka retezce presahne
//  sirku displeje, retezec se kresili od leveho okraje.
void WriteCenter( unsigned char line, const char *str )
{
  int size = SizeInPixel(str);
  int i = ((NUMBER_OF_COLUMNS - size) / 2);
  if (i>0)
  {
    Write( CENTR(str), line, str);
  }
  else
  {
    Write( 0, line, str);
  }
}

void EraseLineBuffer (void)
{
  unsigned char i = 0;
  for (i=0; i<NUMBER_OF_COLUMNS; i++)
    g_frame_buffer[i]=0;
}

void Write_Display(unsigned char command, unsigned char data)
{
  /* Load the control signals and data for a particular command. */
  if(command == CMD)
    DISP_A0(0)  //A0 to low
  else
    DISP_A0(1)  //A0 to HIGH

  SPI_DisplayTransmit(data);
}

// Funkce vymaze FrameBuffer
void WriteErase(void)
{
  unsigned int i;
  for ( i=0; i<BUFFER_SIZE; i++)
  {
    g_frame_buffer[i]=0;
  }
}

// vypis LineBufferu na zvoleny radek
void ShowLine(unsigned char row)
{
  unsigned char i;
  Write_Display(CMD,START_LINE_SET);
  Write_Display(CMD,PAGE_ADDRESS_SET + LAST_ROW - row);
  Write_Display(CMD,COLUMN_ADDRESS_HIGH);
  Write_Display(CMD,COLUMN_ADDRESS_LOW);
  for (i=0;i<NUMBER_OF_COLUMNS;i++)
    Write_Display(DATA, g_frame_buffer[i]); // Ram
}

/*------------------------ Write_Display ---------------------------*/

#define BIG_NUMBER_BEGIN   127 // znak, kterym zacinaji ve fontu velika pismena
#define BIG_NUMBER_WIDTH     2
#define BIG_NUMBER_HEIGHT    2
#define BIG_NUMBER_SIZE      (BIG_NUMBER_WIDTH*BIG_NUMBER_HEIGHT) // pocet byte, ktere zabira velke pismeno
#define BIG_NUMBER_STRLEN   25 // V podstate pocet znaku (sirokych 8), ktere je mozne napsat na jeden radek
#define BIG_NUMBER_E       221
#define BIG_NUMBER_MINUS   225
#define MAX_NUMBERS_COUNT  10 // maximalni pocet cislic na radek (muze mezi nimy byt i tecka)

void WriteBigNumberCentr( unsigned char y, long value_dig, long value_dec, unsigned char dec)
{
  // Funkce vypisuje velika cisla na zvolene radek. Vypis je vycentrovany doprostred obrazovky
  // IN  : x, y, value, dig, dec. Souradnice, hodnota, desetina a cela mista
  // OUT : Vola Write

  char number[MAX_NUMBERS_COUNT+1] = "";
  char BigNumber[BIG_NUMBER_HEIGHT][BIG_NUMBER_STRLEN];
  unsigned char i, j, k;
  unsigned int MaxSize;

  strcpy(BigNumber[0],"");
  strcpy(BigNumber[1],"");

  switch (dec)
  {
    case 0:
      sprintf(number, "%ld", value_dig);
      break;
    case 1:
      sprintf(number, "%ld.%01d", value_dig, value_dec/100 );
      break;
    case 2:
      sprintf(number, "%ld.%02d", value_dig, value_dec/10 );
      break;
    //case 3:
    default:
      sprintf(number, "%ld.%03d", value_dig, value_dec );
      break;
  }

  // osetreni proti preteceni zobrazovaneho spodniho cisla na display
  if ((y == 4) && (g_measurement.act_rot_meas!=MEAS_TEMP) &&
      (g_measurement.act_rot_meas!=MEAS_EXT_TEMP1) && (g_measurement.act_rot_meas!=MEAS_EXT_TEMP2))
  {
    if ((unsigned long)value_dig<9000000)
      sprintf(number, "%lu.%03d", (unsigned long)value_dig, value_dec); // zobrazit se tremi desetinnymi cisly
    if ((unsigned long)value_dig>9000000)
      sprintf(number, "%lu.%02d", (unsigned long)value_dig, value_dec/10); // zobrazit se dvema desetinnymi cisly
    if ((unsigned long)value_dig>90000000)
      sprintf(number, "%lu.%01d", (unsigned long)value_dig, value_dec/100); // zobrazit s jednim desetinnym cislem
    if ((unsigned long)value_dig>900000000)
      sprintf(number, "%lu", (unsigned long)value_dig); // zobrazit bez desetinnych cisel*/
  }

  for ( i=0; i<strlen(number); i++ )
  {
    for ( j=0; j<(BIG_NUMBER_WIDTH); j++ )
    {
      for ( k=0; k<BIG_NUMBER_HEIGHT; k++ )
      {
        strcat( BigNumber[k], "x" );
        switch (number[i])
        {
          case 'e': // Pouziva se pro exponent
            BigNumber[k][strlen(BigNumber[k])-1]=(0)*BIG_NUMBER_SIZE+BIG_NUMBER_E+k+j*BIG_NUMBER_HEIGHT;
            break;
          case '-':
            // Moznost zobrazit - (pro zaporny hodnoty)
            BigNumber[k][strlen(BigNumber[k])-1]=(0)*BIG_NUMBER_SIZE+BIG_NUMBER_MINUS+k+j*BIG_NUMBER_HEIGHT;
            break;
          case '.':
            // +1 = tecku mam za 9tkou. jiank bych ji musel dat tam kam patri
            BigNumber[k][strlen(BigNumber[k])-1]=('9'+1-'0')*BIG_NUMBER_SIZE+BIG_NUMBER_BEGIN+k+j*BIG_NUMBER_HEIGHT;
            break;
          case '0':
          case '1':
          case '2':
          case '3':
          case '4':
          case '5':
          case '6':
          case '7':
          case '8':
          case '9':
            BigNumber[k][strlen(BigNumber[k])-1]=(number[i]-'0')*BIG_NUMBER_SIZE+BIG_NUMBER_BEGIN+k+j*BIG_NUMBER_HEIGHT;
            break;
        }
      }
    }
  }

  MaxSize=0;

  for ( k=0; k<BIG_NUMBER_HEIGHT; k++ )
  {
      MaxSize = ( SizeSpecialInPixel(BigNumber[k])>MaxSize ) ? SizeSpecialInPixel(BigNumber[k]) : MaxSize ;
  }
  for ( k=0; k<BIG_NUMBER_HEIGHT; k++ )
  {
    WriteSpecialFont( (NUMBER_OF_COLUMNS-MaxSize)/2, y+k, BigNumber[k] );
  }
}

void Init_Display(void)
{
  DISPLAY_LIGHT_IODIR |= DISPLAY_LIGHT_PIN; // nastaveni pinu pro spinani podsviceni na vystup
  DISP_RES(0)  //RES = 0
  DISP_RES(1);  //RES = 1
  DISP_A0(1);   //A0 = 1
  DISP_A0(0);  //A0 to low
  SPI_DisplayTransmit(START_LINE_SET); // Display start line 0
  SPI_DisplayTransmit(ADC_SELECT_REVERSE); // ADC reverse none
  SPI_DisplayTransmit(COMMON_OUTPUT_REVERSE); // Reverse direction COM0 - COM63
  SPI_DisplayTransmit(DISPLAY_NORMAL); // Display normal
  SPI_DisplayTransmit(LCD_BIAS_1_9); // Set bias 1/9 (Duty 1/65)
  SPI_DisplayTransmit(0x2F); // Booster, Regulator and Follower on
  SPI_DisplayTransmit(SET_INTERNAL_BOOSTER); // Set internal booster 4x
  SPI_DisplayTransmit(0x00);                 // Set internal booster 4x
  SPI_DisplayTransmit(INTERNAL_RESISTOR_SET ); // Contrast set
  SPI_DisplayTransmit(ELECTRONIC_VOLUME_SET);  // Contrast set
  SPI_DisplayTransmit(ELECTRONIC_VOLUME_INIT); // Contrast set
  SPI_DisplayTransmit(STATIC_INDICATOR_OFF); // Static indicator - No indicator
  SPI_DisplayTransmit(0x00); // Static indicator - No indicator
  SPI_DisplayTransmit(DISPLAY_ON); // Display on
  DISP_A0(1)  //A0 to high
  WriteErase(); //smaze cely display
}

void DisplaySetContrast(unsigned char Contrast)
{
  Write_Display(CMD,ELECTRONIC_VOLUME_SET);
  Write_Display(CMD, Contrast*2 / 9 + 8);
}

// zjisti velicinu, ktera je zobrazena v grafech
TGraphQuantity GetGraphQuantityType(void)
{
    unsigned long quantity = RAM_Read_Data_Long(GRAPH_QUANTITY);
    if (quantity <= GQ_Max)
        return (TGraphQuantity)quantity;
    else
        return GQ_HEAT_POWER;
}

// zjisti hodnotu veliciny, ktera je zobrazena v grafech
static float GetGraphQuantityValue(TGraphQuantity quantityType)
{
    switch (quantityType)
    {
    case GQ_HEAT_POWER:
    default:
        {
            float heatPerform = Heat_GetHeatPerformance();
            if (heatPerform < 0) heatPerform = 0;
            return heatPerform;
        }

    case GQ_FLOW:
        return g_measurement.actual;
    }
}

// zjisti rozsah veliciny, ktera je zobrazena v grafech
static float GetGraphQuantityRange(TGraphQuantity quantityType)
{
    switch (quantityType)
    {
    case GQ_HEAT_POWER:
    default:
        return (RAM_Read_Data_Long(HEAT_POWER_RANGE) / 1000.0);

    case GQ_FLOW:
        return (RAM_Read_Data_Long(FLOW_RANGE) / 1000.0);
    }
}

// zjisti hodnotu veliciny grafu v procentech rozsahu
int GetGraphValuePercent(void)
{
    TGraphQuantity quantityType = GetGraphQuantityType();
    float value = GetGraphQuantityValue(quantityType);
    float range = GetGraphQuantityRange(quantityType) / 100.0;

    if (range == 0)
        return 0;

    int percent = value / range;

    if (percent < 0)
        return 0;
    if (percent > 100)
        return 100;

    return percent;
}

// Funkce vykresli Flow Bar na miste velke hodnoty totalizeru
// na vysku zabira tri radky
void WriteBar(void)
{
  int barPercent = GetGraphValuePercent();

  if(barPercent < 0)
    barPercent = 0;
  if(barPercent > 100)
    barPercent = 100;

  // nastavim index do bufferu displeje na prvni sloupecek, ktery budu kreslit
  // 4 radky * sirka displeje + odsazeni od kraje
  int index = 4 * 128 + 4;
  g_frame_buffer[index] = 15; // krajni ramecek baru
  index++;
  g_frame_buffer[index] = 8;  // mezera mezi rameckem a zacatkem baru
  index++;

  int progress = (int)(1.16 * barPercent); // stav baru 1% = 1,16 dilku

  // vyplnena cast baru v 1. radku
  for (int i = 0; i < progress; i++)
  {
    g_frame_buffer[index] = 11;
    index++;
  }
  // prazdna cast baru v 1. radku
  for (int i = 0; i < 115 - progress; i++)
  {
    g_frame_buffer[index] = 8;
    index++;
  }
  g_frame_buffer[index] = 8;  // mezera mezi rameckem a koncem baru
  index++;
  g_frame_buffer[index] = 15; // krajni ramecek baru

  // posun na zacatk dalsiho radku
  index = 5 * 128 + 4;
  g_frame_buffer[index] = 255;
  index++;
  g_frame_buffer[index] = 0;
  index++;
  // vyplnena cast baru v 2. radku
  for (int i = 0; i < progress; i++)
  {
    g_frame_buffer[index] = 255;
    index++;
  }
  // prazdna cast baru v 2. radku
  for (int i = 0; i < 115 - progress; i++)
  {
    index++;
  }
  g_frame_buffer[index] = 0;
  index++;
  g_frame_buffer[index] = 255;

  index = 6 * 128 + 4;
  g_frame_buffer[index] = 252;
  index++;
  g_frame_buffer[index] = 4;
  index++;
  // vyplnena cast baru v 3. radku
  for (int i = 0; i < progress; i++)
  {
    g_frame_buffer[index] = 244;
    index++;
  }
  // prazdna cast baru v 3. radku
  for (int i = 0; i < 115 - progress; i++)
  {
    g_frame_buffer[index] = 4;
    index++;
  }
  g_frame_buffer[index] = 4;
  index++;
  g_frame_buffer[index] = 252;
}

void WriteGraph()
{
  int indexi = 4 * 128 + 4;
  g_frame_buffer[indexi++] = 15; // krajni ramecek grafu

  for(int i = 0; i < 119; i++)
    g_frame_buffer[indexi++] = graphHistory[i].top;

  indexi = 5 * 128 + 4;
  g_frame_buffer[indexi++] = 255; // krajni ramecek grafu

  for(int i = 0; i < 119; i++)
    g_frame_buffer[indexi++] = graphHistory[i].center;

  indexi = 6 * 128 + 4;
  g_frame_buffer[indexi++] = 252; // krajni ramecek grafu

  for(int i = 0; i < 119; i++)
    g_frame_buffer[indexi++] = graphHistory[i].bottom;
}

//------------------------------------------------------------------------------
//  Zapsani stavu baterie k vykresleni
//------------------------------------------------------------------------------
void WriteBattery(unsigned char x, unsigned char line)
{
    char str[3];

    TBatteryStatus bs = PowerMan_GetBatteryStatus();
    if (bs == BATS_LOW_BATTERY)
    {
        str[0] = 5;
        str[1] = 10;
        str[2] = 0;
        Write(x, line, str);
    }
    else if (bs == BATS_MEDIUM_BATTERY)
    {
        str[0] = 5;
        str[1] = 9;
        str[2] = 0;
        Write(x, line, str);
    }
    else if (bs == BATS_HIGH_BATTERY)
    {
        str[0] = 5;
        str[1] = 8;
        str[2] = 0;
        Write(x, line, str);
    }
    else if (bs == BATS_CHARGING)
    {
        str[0] = 27;
        str[1] = 0;
        Write(x, line, str);
    }
}
