#include "type.h"
#include "gsm.h"
#include "LPC23xx.h"
#include "maine.h"
#include "rtc.h"
#include "rprintf.h"
#include <stdlib.h>
#include "menu_tables.h"
#include "sensor.h"
#include "display.h"
#include "delay.h"
#include <string.h>


//void odesli_gsm_buffer(const char *BufferPtr, unsigned char Length );
unsigned char gsm_recv_buffer[50];
unsigned char gsm_pozice_buff=0;
volatile unsigned long flag_gsm_zije=GSM_TIMEOUT_BUSY_PIN;

void get_flow_rate_gsm()
{
  char buff[100];
  char *ukaz_buff;
  buff[1]=0x01;   //to je zde aby nize strlen nevratilo jen velikost dva

  buff[0]=GSM_GET_FLOWRATE;

  ukaz_buff=&buff[2];
  // 0x01 FLOWRATE 2010.05.15 16:06 15.3 M3/H
  sprintf(ukaz_buff,"FLOWRATE %04d.%02d.%02d %02d:%02d %03f M3/H",DateTime.year,DateTime.month,DateTime.day,DateTime.hour,DateTime.minute,g_measurement.actual);
  buff[1]=strlen(buff);
  odesli_gsm_buffer(buff,buff[1]);
  gsm_busy_pin(0);
  delay_100ms();
  delay_100ms();
}

void get_interval_data_gsm()
{
  char buff[161];
  char *ukaz_buff;
  buff[1]=0x01;   //to je zde aby nize strlen nevratilo jen velikost dva

  buff[0]=GSM_INTERVAL_DATA;
  ukaz_buff=&buff[2];

  buff[1]=strlen(buff);
  //Send_sensor(buff,strlen(buff));
  odesli_gsm_buffer(buff,buff[1]);
  gsm_busy_pin(0);
  delay_100ms();
  delay_100ms();
}

void get_totalneg_gsm()
{
  char buff[100];
  char *ukaz_buff;
  buff[1]=0x01;   //to je zde aby nize strlen nevratilo jen velikost dva

  buff[0]=GSM_GET_TOTALNEG;

  ukaz_buff=&buff[2];
  // 0x01 FLOWRATE 2010.05.15 16:06 15.3 M3/H
  sprintf(ukaz_buff,"TOTALNEG %04d.%02d.%02d %02d:%02d %d.%03d M3",
          DateTime.year,DateTime.month,DateTime.day,DateTime.hour,DateTime.minute,
          (unsigned long)(g_measurement.flow_neg/1000000),
          (unsigned long)((g_measurement.flow_neg%1000000)/1000));
  buff[1]=strlen(buff);
  odesli_gsm_buffer(buff,buff[1]);
  gsm_busy_pin(0);
  delay_100ms();
  delay_100ms();
}

void get_totalpos_gsm()
{
  char buff[100];
  char *ukaz_buff;
  buff[1]=0x01;   //to je zde aby nize strlen nevratilo jen velikost dva

  buff[0]=GSM_GET_TOTALPOS;

  ukaz_buff=&buff[2];
  // 0x01 FLOWRATE 2010.05.15 16:06 15.3 M3/H
  sprintf(ukaz_buff," TOTALPOS %04d.%02d.%02d %02d:%02d %d.%03d M3",
          DateTime.year,DateTime.month,DateTime.day,DateTime.hour,DateTime.minute,
          (unsigned long)(g_measurement.flow_pos/1000000),
          (unsigned long)((g_measurement.flow_pos%1000000)/1000));
  buff[1]=strlen(buff);
  odesli_gsm_buffer(buff,buff[1]);
  gsm_busy_pin(0);
  delay_100ms();
  delay_100ms();
}

void get_total_gsm()
{
  char buff[100];
  char *ukaz_buff;
  buff[1]=0x01;   //to je zde aby nize strlen nevratilo jen velikost dva

  buff[0]=GSM_GET_TOTAL;

  ukaz_buff=&buff[2];
  // 0x01 FLOWRATE 2010.05.15 16:06 15.3 M3/H
  sprintf(ukaz_buff," TOTAL %04d.%02d.%02d %02d:%02d %d.%03d M3",
          DateTime.year,DateTime.month,DateTime.day,DateTime.hour,DateTime.minute,
          (unsigned long)(g_measurement.total/1000000),
          (unsigned long)((g_measurement.total%1000000)/1000));
  buff[1]=strlen(buff);
  odesli_gsm_buffer(buff,buff[1]);
  gsm_busy_pin(0);
  delay_100ms();
  delay_100ms();
}

void get_aux_gsm()
{
  char buff[100];
  char *ukaz_buff;
  buff[1]=0x01;   //to je zde aby nize strlen nevratilo jen velikost dva

  buff[0]=GSM_GET_AUX;

  ukaz_buff=&buff[2];
  // 0x01 FLOWRATE 2010.05.15 16:06 15.3 M3/H
  sprintf(ukaz_buff," TOTALAUX %04d.%02d.%02d %02d:%02d %d.%03d M3",
          DateTime.year,DateTime.month,DateTime.day,DateTime.hour,DateTime.minute,
          (unsigned long)(g_measurement.aux/1000000),
          (unsigned long)((g_measurement.aux%1000000)/1000));
  buff[1]=strlen(buff);
  odesli_gsm_buffer(buff,buff[1]);
  gsm_busy_pin(0);
  delay_100ms();
  delay_100ms();
}

void get_actual_error_gsm()
{
  char buff[100];
  char *ukaz_buff;
  buff[1]=0x01;   //to je zde aby nize strlen nevratilo jen velikost dva

  buff[0]=GSM_GET_ACTUAL_ERR;

  ukaz_buff=&buff[2];
  // 0x01 FLOWRATE 2010.05.15 16:06 15.3 M3/H
  sprintf(ukaz_buff," ERROR %04d.%02d.%02d %02d:%02d %08x",
          DateTime.year,DateTime.month,DateTime.day,DateTime.hour,DateTime.minute,actual_error);
  buff[1]=strlen(buff);
  odesli_gsm_buffer(buff,buff[1]);
  gsm_busy_pin(0);
  delay_100ms();
  delay_100ms();
}

void set_unitno_gsm()
{
  unsigned char buff[8];

  buff[0] = GSM_SET_UNITNO;
  buff[1]=6;      //velikost

  unsigned long pomoc=RAM_Read_Data_Long(UNIT_NO);

  buff[2] = (unsigned char) (pomoc >> 24);
  buff[3] = (unsigned char) (pomoc >> 16);
  buff[4] = (unsigned char) (pomoc >> 8);
  buff[5] = (unsigned char) (pomoc);

  odesli_gsm_buffer(buff,6);
  gsm_busy_pin(0);
}

void get_quality_signal_gsm()
{
  unsigned char buff[2];
  buff[0]=GSM_QUALITY_SIGNAL;
  buff[1]=2;

  //Send_sensor(buff,3);
  odesli_gsm_buffer(buff,2);
}

void get_datalogger_gsm(unsigned char velikost)
{
  char buff[180];
  char *ukaz_buff;
  buff[0] = GSM_GET_DATALOGGER;
  buff[1] = 0x01;   //to je zde aby nize strlen nevratilo jen velikost dva

  ukaz_buff =& buff[2];

  if(flag_gsm_datalogger_first_send)
  {
    sprintf(ukaz_buff,"DATALOGGER %s",odesilaci_buffer);
    buff[1]=0x0D + velikost ;
    pocet_znaku_gsm=160;
    flag_gsm_datalogger_first_send=0;
    //zde sekunda zpozdeni jinak gsm nestaci vyhodit pin a porad mu plnim buffer
    odesli_gsm_buffer(buff,buff[1]);
    delay_100ms();
    delay_100ms();
    delay_100ms();
    delay_100ms();
    delay_100ms();
    delay_100ms();
    delay_100ms();
    delay_100ms();
    delay_100ms();
    delay_100ms();
  }
  else
  {
    sprintf(ukaz_buff,"%s",odesilaci_buffer);
    buff[1]=velikost+2;
    odesli_gsm_buffer(buff,buff[1]);
  }
}

void zpracovat_data_gsm()
{
  flag_probiha_prijem_od_gsm=0;
  actual_error &= ~ERR_GSM_TIMEOUT;
  unsigned char buffik[10];
  switch(gsm_recv_buffer[0])
  {
    case GSM_QUALITY_SIGNAL:
      RAM_Write_Data_Long((long)gsm_recv_buffer[2],GSM_SIGNAL);
      break;
    case GSM_GET_ACTUAL_ERR:
      gsm_busy_pin(1);
      get_actual_error_gsm();
      break;
    case GSM_GET_AUX:
      gsm_busy_pin(1);
      get_aux_gsm();
      break;
    case GSM_GET_FLOWRATE:
      gsm_busy_pin(1);
      get_flow_rate_gsm();
      break;
    case GSM_GET_TOTAL:
      gsm_busy_pin(1);
      get_total_gsm();
      break;
    case GSM_GET_TOTALNEG:
      gsm_busy_pin(1);
      get_totalneg_gsm();
      break;
    case GSM_GET_TOTALPOS:
      gsm_busy_pin(1);
      get_totalpos_gsm();
      break;
    case GSM_SET_UNITNO:
      set_unitno_gsm();
      break;
    case GSM_SET_PHONE_1:
    {
      unsigned long pomoc=0;
      char *ukaz;
      pomoc = ((unsigned long) gsm_recv_buffer[2] ) << 24;
      pomoc += ((unsigned long) gsm_recv_buffer[3] ) << 16;
      pomoc += ((unsigned long) gsm_recv_buffer[4] ) << 8;
      pomoc += ((unsigned long) gsm_recv_buffer[5] );
      RAM_Write_Data_Long(pomoc,GSM_PHONE_1_H);

      pomoc = ((unsigned long) gsm_recv_buffer[6] ) << 24;
      pomoc += ((unsigned long) gsm_recv_buffer[7] ) << 16;
      pomoc += ((unsigned long) gsm_recv_buffer[8] ) << 8;
      pomoc += ((unsigned long) gsm_recv_buffer[9] );

      RAM_Write_Data_Long(pomoc,GSM_PHONE_1_L);

      ukaz=&buffer_gsm_potvrzeni[2];
      buffer_gsm_potvrzeni[1]=100; //to tadz musi bzt;
      buffer_gsm_potvrzeni[0]=GSM_SET_PHONE_1;
      if( RAM_Read_Data_Long(GSM_PHONE_1_L))
        sprintf(ukaz,"PHONE 1 SET TO:+%ld%ld",RAM_Read_Data_Long(GSM_PHONE_1_H),RAM_Read_Data_Long(GSM_PHONE_1_L));
      else
        sprintf(ukaz,"PHONE 1 SET TO: NONE");
      buffer_gsm_potvrzeni[1] = strlen(buffer_gsm_potvrzeni);
      flag_gsm_potvrdit_nastaveni = 1;
    }
    break;
    case GSM_SET_PHONE_2:
    {
      unsigned long pomoc=0;
      char *ukaz;

      pomoc = ((unsigned long) gsm_recv_buffer[2] ) << 24;
      pomoc += ((unsigned long) gsm_recv_buffer[3] ) << 16;
      pomoc += ((unsigned long) gsm_recv_buffer[4] ) << 8;
      pomoc += ((unsigned long) gsm_recv_buffer[5] );

      RAM_Write_Data_Long(pomoc,GSM_PHONE_2_H);

      pomoc = ((unsigned long) gsm_recv_buffer[6] ) << 24;
      pomoc += ((unsigned long) gsm_recv_buffer[7] ) << 16;
      pomoc += ((unsigned long) gsm_recv_buffer[8] ) << 8;
      pomoc += ((unsigned long) gsm_recv_buffer[9] );

      RAM_Write_Data_Long(pomoc,GSM_PHONE_2_L);

      ukaz=&buffer_gsm_potvrzeni[2];
      buffer_gsm_potvrzeni[1]=100; //to tadz musi bzt skrz prvni velikost;
      buffer_gsm_potvrzeni[0]=GSM_SET_PHONE_2;
      if( RAM_Read_Data_Long(GSM_PHONE_2_L))
          sprintf(ukaz,"PHONE 2 SET TO:+%ld%ld",RAM_Read_Data_Long(GSM_PHONE_2_H),RAM_Read_Data_Long(GSM_PHONE_2_L));
      else
          sprintf(ukaz,"PHONE 2 SET TO: NONE");
      buffer_gsm_potvrzeni[1]=strlen(buffer_gsm_potvrzeni);
      flag_gsm_potvrdit_nastaveni=1;
      //odesli_gsm_buffer(buffer_gsm_potvrzeni,buffer_gsm_potvrzeni[1]);
    }
    break;
    case GSM_SET_PHONE_3:
    {
      unsigned long pomoc=0;
      char *ukaz;

      pomoc = ((unsigned long) gsm_recv_buffer[2] ) << 24;
      pomoc += ((unsigned long) gsm_recv_buffer[3] ) << 16;
      pomoc += ((unsigned long) gsm_recv_buffer[4] ) << 8;
      pomoc += ((unsigned long) gsm_recv_buffer[5] );

      RAM_Write_Data_Long(pomoc,GSM_PHONE_3_H);

      pomoc = ((unsigned long) gsm_recv_buffer[6] ) << 24;
      pomoc += ((unsigned long) gsm_recv_buffer[7] ) << 16;
      pomoc += ((unsigned long) gsm_recv_buffer[8] ) << 8;
      pomoc += ((unsigned long) gsm_recv_buffer[9] );

      RAM_Write_Data_Long(pomoc,GSM_PHONE_3_L);

      ukaz=&buffer_gsm_potvrzeni[2];
      buffer_gsm_potvrzeni[1]=100; //to tadz musi bzt;
      buffer_gsm_potvrzeni[0]=GSM_SET_PHONE_3;
      if( RAM_Read_Data_Long(GSM_PHONE_3_L))
          sprintf(ukaz,"PHONE 3 SET TO:+%ld%ld",RAM_Read_Data_Long(GSM_PHONE_3_H),RAM_Read_Data_Long(GSM_PHONE_3_L));
      else
          sprintf(ukaz,"PHONE 3 SET TO: NONE");
      buffer_gsm_potvrzeni[1]=strlen(buffer_gsm_potvrzeni);
      flag_gsm_potvrdit_nastaveni=1;
      //odesli_gsm_buffer(buffer_gsm_potvrzeni,buffer_gsm_potvrzeni[1]);
    }
    break;
    case GSM_ZERO_FLOW_ON:
    {
      char *ukaz;
      RAM_Write_Data_Long(1,GSM_ZERO_FLOW);
      RAM_Write_Data_Long(1,GSM_ZERO_FLOW_SEND);

      if( g_measurement.actual != 0)
      {
        flag_event_zero_on_odeslano=0;
        flag_event_zero_off_odeslano=1;
      }
      else
      {
        flag_event_zero_on_odeslano=1;
        flag_event_zero_off_odeslano=0;
      }
      ukaz =& buffer_gsm_potvrzeni[2];
      buffer_gsm_potvrzeni[1] = 100; //to tadz musi bzt;
      buffer_gsm_potvrzeni[0] = GSM_ZERO_FLOW_ON;
      sprintf(ukaz,"%d EVENT ZEROFLOW SET TO:ON",RAM_Read_Data_Long(UNIT_NO));
      buffer_gsm_potvrzeni[1] = strlen(buffer_gsm_potvrzeni);
      flag_gsm_potvrdit_nastaveni = 1;
    }
    break;
    case GSM_ZERO_FLOW_ON_OFF:
    {
      char *ukaz;
      RAM_Write_Data_Long(1,GSM_ZERO_FLOW);
      RAM_Write_Data_Long(0,GSM_ZERO_FLOW_SEND);

      if( g_measurement.actual != 0)
      {
        flag_event_zero_on_odeslano=0;
        flag_event_zero_off_odeslano=1;
      }
      else
      {
        flag_event_zero_on_odeslano=1;
        flag_event_zero_off_odeslano=0;
      }

      ukaz=&buffer_gsm_potvrzeni[2];
      buffer_gsm_potvrzeni[1]=100; //to tadz musi bzt;
      buffer_gsm_potvrzeni[0]=GSM_ZERO_FLOW_ON_OFF;
      sprintf(ukaz,"%d EVENT ZEROFLOW SET TO:ON-OFF",RAM_Read_Data_Long(UNIT_NO));
      buffer_gsm_potvrzeni[1]=strlen(buffer_gsm_potvrzeni);
      flag_gsm_potvrdit_nastaveni=1;
    }
    break;
    case GSM_ZERO_FLOW_OFF:
    {
      char *ukaz;
      RAM_Write_Data_Long(0,GSM_ZERO_FLOW);
      ukaz=&buffer_gsm_potvrzeni[2];
      buffer_gsm_potvrzeni[1]=100; //to tadz musi bzt;
      buffer_gsm_potvrzeni[0]=GSM_ZERO_FLOW_OFF;
      sprintf(ukaz,"%d EVENT ZEROFLOW SET TO:OFF",RAM_Read_Data_Long(UNIT_NO));
      buffer_gsm_potvrzeni[1]=strlen(buffer_gsm_potvrzeni);
      flag_gsm_potvrdit_nastaveni=1;
    }
    break;
    case GSM_EMPTY_ON:
    {
      char *ukaz;
      RAM_Write_Data_Long(1,GSM_EMPTY_PIPE);
      RAM_Write_Data_Long(1,GSM_EMPTY_PIPE_SEND);

      if( actual_error&ERR_EMPTY_PIPE)
      {
        flag_event_empty_on_odeslano = 1;
        flag_event_empty_off_odeslano = 0;
      }
      else
      {
        flag_event_empty_on_odeslano = 0;
        flag_event_empty_off_odeslano = 1;
      }
      ukaz=&buffer_gsm_potvrzeni[2];
      buffer_gsm_potvrzeni[1] = 100; //to tadz musi bzt;
      buffer_gsm_potvrzeni[0] = GSM_EMPTY_ON;
      sprintf(ukaz,"%d EVENT EMPTYPIPE SET TO:ON",RAM_Read_Data_Long(UNIT_NO));
      buffer_gsm_potvrzeni[1] = strlen(buffer_gsm_potvrzeni);
      flag_gsm_potvrdit_nastaveni = 1;
    }
    break;
    case GSM_EMPTY_ON_OFF:
    {
      char *ukaz;
      RAM_Write_Data_Long(1,GSM_EMPTY_PIPE);
      RAM_Write_Data_Long(0,GSM_EMPTY_PIPE_SEND);

      if( actual_error&ERR_EMPTY_PIPE)
      {
        flag_event_empty_on_odeslano = 1;
        flag_event_empty_off_odeslano = 0;
      }
      else
      {
        flag_event_empty_on_odeslano = 0;
        flag_event_empty_off_odeslano = 1;
      }

      ukaz =& buffer_gsm_potvrzeni[2];
      buffer_gsm_potvrzeni[1] = 100; //to tadz musi bzt;
      buffer_gsm_potvrzeni[0] = GSM_EMPTY_ON_OFF;
      sprintf(ukaz,"%d EVENT EMPTYPIPE SET TO:ON-OFF",RAM_Read_Data_Long(UNIT_NO));
      buffer_gsm_potvrzeni[1] = strlen(buffer_gsm_potvrzeni);
      flag_gsm_potvrdit_nastaveni = 1;
    }
    break;
    case GSM_EMPTY_OFF:
    {
      char *ukaz;
      RAM_Write_Data_Long(0,GSM_EMPTY_PIPE);
      ukaz=&buffer_gsm_potvrzeni[2];
      buffer_gsm_potvrzeni[1]=100; //to tadz musi bzt;
      buffer_gsm_potvrzeni[0]=GSM_EMPTY_OFF;
      sprintf(ukaz,"%d EVENT EMPTYPIPE SET TO:OFF",RAM_Read_Data_Long(UNIT_NO));
      buffer_gsm_potvrzeni[1]=strlen(buffer_gsm_potvrzeni);
      flag_gsm_potvrdit_nastaveni=1;
    }
    break;
    case GSM_ERROR_ON:
    {
      char *ukaz;
      RAM_Write_Data_Long(1,GSM_ERROR_DETECT);

      last_sending_error_gsm = actual_error;
      ukaz =& buffer_gsm_potvrzeni[2];
      buffer_gsm_potvrzeni[1] = 100; //to tadz musi bzt;
      buffer_gsm_potvrzeni[0] = GSM_ERROR_ON;
      sprintf(ukaz,"%d EVENT ERROR DETECT SET TO:ON",RAM_Read_Data_Long(UNIT_NO));
      buffer_gsm_potvrzeni[1] = strlen(buffer_gsm_potvrzeni);
      flag_gsm_potvrdit_nastaveni = 1;
    }
    break;
    case GSM_ERROR_OFF:
    {
      char *ukaz;
      RAM_Write_Data_Long(0,GSM_ERROR_DETECT);
      ukaz=&buffer_gsm_potvrzeni[2];
      buffer_gsm_potvrzeni[1]=100; //to tadz musi bzt;
      buffer_gsm_potvrzeni[0]=GSM_ERROR_OFF;
      sprintf(ukaz,"%d EVENT ERROR DETECT SET TO:OFF",RAM_Read_Data_Long(UNIT_NO));
      buffer_gsm_potvrzeni[1]=strlen(buffer_gsm_potvrzeni);
      flag_gsm_potvrdit_nastaveni=1;
    }
    break;
    case GSM_GET_DATALOGGER:
    {
      ukonceni_odesilani_datalogger_gsm();    //vynuluje vsechny hodnoty pouzite pro datalogger

      unsigned long pomoc_datum=0;
      unsigned int pomoc_cas=0;
      unsigned int pomoc_pocet=0;

      pomoc_datum = ((unsigned long) gsm_recv_buffer[2] ) << 8;
      pomoc_datum +=((unsigned long) gsm_recv_buffer[3] );
      pomoc_datum*=10000;

      pomoc_datum += ((unsigned long) gsm_recv_buffer[4]) << 8;
      pomoc_datum += ((unsigned long) gsm_recv_buffer[5]);

      pomoc_cas = ((unsigned int) gsm_recv_buffer[6] )<< 8;
      pomoc_cas +=((unsigned int) gsm_recv_buffer[7] );

      pomoc_pocet =  ((unsigned int) gsm_recv_buffer[8] )<< 8;
      pomoc_pocet += ((unsigned int) gsm_recv_buffer[9] );

      flag_gsm_datalogger_first_send=1;
      pocet_znaku_gsm=149;

      gsm_busy_pin(1);
      read_datalogger(pomoc_datum,pomoc_cas,pomoc_pocet);
     }
    break;
    case GSM_SET_INTERVAL:
    {
      unsigned int pomoc=0;
      pomoc = ((unsigned int) gsm_recv_buffer[2]) << 8;
      pomoc += ((unsigned int) gsm_recv_buffer[3]);

      flag_second_gsm = 60*pomoc;
      RAM_Write_Data_Long(pomoc,GSM_DATA_INTERVAL);

      char *ukaz;
      ukaz =& buffer_gsm_potvrzeni[2];
      buffer_gsm_potvrzeni[1] = 100; //to tadz musi bzt;
      buffer_gsm_potvrzeni[0] = GSM_SET_INTERVAL;
      sprintf(ukaz,"INTERVAL TIME SET TO: %04dmin",RAM_Read_Data_Long(GSM_DATA_INTERVAL));
      buffer_gsm_potvrzeni[1] = strlen(buffer_gsm_potvrzeni);
      flag_gsm_potvrdit_nastaveni = 1;
    }
    break;
    case GSM_ERROR:
    {
      unsigned long pomoc;

      pomoc = ((unsigned long) gsm_recv_buffer[2] ) << 24;
      pomoc += ((unsigned long) gsm_recv_buffer[3] ) << 16;
      pomoc += ((unsigned long) gsm_recv_buffer[4] ) << 8;
      pomoc += ((unsigned long) gsm_recv_buffer[5] );

      //msui byt v rozmezi tech ctyr erroru
      if( (pomoc >= ERR_GSM_SIGNAL) && (pomoc <= (ERR_GSM_OTHERS+ERR_GSM_SENDING+ERR_GSM_SIGNAL+ERR_GSM_SIMCARD)) || (pomoc == 0) )
      {
        actual_error &= ~ERR_GSM_OTHERS & ~ERR_GSM_SENDING & ~ERR_GSM_SIGNAL & ~ERR_GSM_SIMCARD;
        actual_error |= pomoc;
      }
    }
    break;
    case GSM_START_SEND:
      flag_chci_odeslat_event=0;
      flag_dalsi_zadost_event=0;
      flag_chci_odeslat_interval=0;
      aktualni_cislo_interval=1;
      aktualni_cislo_pro_odeslani=1;
      gsm_busy_pin(0);
      flag_sms_zastaveny=0;
      //flag_second_gsm=15;
      ///datalogger
      ukonceni_odesilani_datalogger_gsm();
      break;
    case GSM_STOP_SEND:
      flag_chci_odeslat_event=0;
      flag_dalsi_zadost_event=0;
      flag_chci_odeslat_interval=0;
      aktualni_cislo_interval=1;
      aktualni_cislo_pro_odeslani=1;
      flag_sms_zastaveny=1;

      ///datalogger
      ukonceni_odesilani_datalogger_gsm();
      gsm_busy_pin(0);
      break;
    default:
      sprintf(buffik,"%x %x %x %x : %d",gsm_recv_buffer[0],gsm_recv_buffer[1],gsm_recv_buffer[2],gsm_recv_buffer[3],gsm_pozice_buff);
      Write(0,3,buffik);
      Write(0,2,"default");
      break;
  }
}

void odesli_gsm_buffer(const char *BufferPtr, unsigned char Length )
{
  while ( Length != 0 )
  {
    while ( !(U1LSR & (1<<5) ) ); // Wait until Transmitter Holding Register is not Empty
    U1THR = *BufferPtr;
    BufferPtr++;
    Length--;
  }
  while ( !(U1LSR & (1<<6) ) );
  flag_gsm_zije=GSM_TIMEOUT_BUSY_PIN;
}
