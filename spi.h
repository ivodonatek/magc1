#ifndef SPI_H_INCLUDED
#define SPI_H_INCLUDED

#ifndef BOOL_TYPEDEF
#define BOOL_TYPEDEF
typedef unsigned char BOOL;
#endif

#ifndef TRUE
#define TRUE            1
#endif

#ifndef FALSE
#define FALSE           0
#endif

void SPI_DisplayInit(void);
void SPI_PulseInit(void);
void SPI_DisplayTransmit(unsigned char cdata); // pro displej
void SPI_PulseTransmit(unsigned char cdata); //Pro pulse
void SPI_PulseChipSelect(BOOL select);

#endif // SPI_H_INCLUDED
