#include <stdlib.h>
#include <stdint.h>
#include <float.h>
#include <math.h>
#include "LPC23xx.h"
#include "type.h"
#include "maine.h"
#include "target.h"
#include "type.h"
#include "display.h"
#include "gsm.h"
#include "spi.h"
#include "images.h"
#include "rprintf.h"
#include "menu_tables.h"
#include "string.h"
#include "font.h"
#include "rtc.h"
#include "VCOM.h"
#include "menu.h"
#include "mb.h"
#include "swi.h"
#include "target.h"
#include "uart.h"
#include "iout.h"
#include "delay.h"
#include "sensor.h"
#include "timer.h"
#include "calibration.h"
#include "modbus_bin.h"
#include "modbus.h"
#include "sbl_iap.h"
#include "sbl_config.h"
#include "gprs.h"
#include "optic_buttons.h"
#include "conversion.h"
#include "fw_check.h"
#include "heat.h"
#include "port.h"
#include "pulse_outputs.h"
#include "led.h"
#include "systick.h"
#include "step_up.h"
#include "pulse_flow_sensor.h"
#include "power_man.h"
#include "mbus/mbus_slave.h"
#include "extmeas/temperature/exttemp.h"

///SD KARTA
#include <string.h>
#include <stdlib.h>
#include "comm.h"
#include "monitor.h"
#include "diskio.h"
#include "tff.h"
#include "fio.h"

FATFS fatfs;		// File system object for each logical drive

const static unsigned char ucSlaveID[] =
{
  0x01, 'A','r','k','o','n',' ',' ',' ',' ',' ',' ',' '
  ,'M','a','g','X','2',0,0,0,0,0,0,0,1,10
};

unsigned long RAM_Read_Data_Long(unsigned long Address)
{
	return(*(unsigned long *)Address);
}

void RAM_Write_Data_Long(unsigned long data, unsigned long Address)
{
  *(unsigned long *)Address = data;
}

float RAM_Read_Data_Float(unsigned long Address)
{
	return(*(float *)Address);
}

void RAM_Write_Data_Float(float data, unsigned long Address)
{
  *(float *)Address = data;
}

void RAM_Write_Data(unsigned long data, unsigned long Address)
{
  *(unsigned long *)Address &= data;
  *(unsigned long *)Address |= data;
}

unsigned char key_lock = 0; // promenna pro zamykani klaves
unsigned char last_key = 0; // posledni stistena klavesa
unsigned char key_dummy = 0; // pro vynechani klavesy enter pri odemceni klavesnice
unsigned char key_ESC_time_sec = 0; // sekund kdy se stiskla klavesa ESC
volatile unsigned char pressed_key = 0xff; // kod stisknuteho tlacitka
volatile static unsigned long empty_pipe = 0; // namerena hodnota z empty pipe
volatile static long flow_avg[SAMPLES_MAX + 1] = {};   //flow_avg[0] vysledny prumer

struct TSensor
{
  long mp1;
  long mp2;
  long mp3;
  long cp1;
  long cp2; // kalibracni konstanty a pointy
  long cp3; // kalibracni konstanty a pointy
}sensor;

/**
    Záznam data loggeru
*/
typedef struct DataloggerRecord
{
    // datum a čas
    unsigned char dt_minute;
    unsigned char dt_hour;
    unsigned char dt_day;
    unsigned char dt_month;
    unsigned int  dt_year;

    // měřené veličiny
    long long meas_total;
    long long meas_flow_pos;
    long long meas_flow_neg;
    long long meas_aux;
    float     meas_temp;
    float     meas_ext_temp1;
    float     meas_ext_temp2;
    float     meas_heat_total;

    // stavy
    unsigned int firmware;      // FW_VERSION
    unsigned char measurement;  // Run=1/Stop=0

    // chybové informace
    unsigned long error_value;
    unsigned long error_minute;
    unsigned long ok_minute;

} TDataloggerRecord;

// max. počet záznamů v cache data loggeru
#define MAX_DATALOGGER_CACHE_RECORDS    (10)

/**
    Cache data loggeru
*/
static struct
{
    // záznamy
    TDataloggerRecord   records[MAX_DATALOGGER_CACHE_RECORDS];

    // aktuální počet záznamů v cache
    unsigned char       count;

    // zapisovací index záznamů
    unsigned char       writeIdx;

    // čtecí index záznamů
    unsigned char       readIdx;

} dataloggerCache;

void FirstRun (void);
void Tlacitko (void);
void pinConfig(void);
void co_sekunda(void);
float Convert_Flow(float in, unsigned char typ);
long long Convert_Volume(long long in, unsigned char typ);
unsigned long long Unit_Volume_Conversion(unsigned long in_hi, unsigned long in_lo);
unsigned long Volume_Conversion(unsigned long in_hi, unsigned long in_lo, unsigned char typ); // prevod spodniho pocitadla do spravne jednotky
void testWifi(void);

///debugovani
#define ERR_LINE    1
#define SHOW_ANS    2
#define ACT_ERROR   5
#define TIME        6
#define INFO        7

//#define SERVICE
#define TOTALISER

///flagy
unsigned int flag_cisteni_cas = 0;
unsigned int flag_zapsat_flash = SEC_TO_WRITE_FLASH;
unsigned char flag_zapis_prutok = 0;      // flag pro vyvolani fce zapisu prutoku do pole
unsigned char flag_kalibrace_send = 0;
unsigned char flag_kalibrace_recv = 0;    // jednotlive biti informuji o stavu dane komunikace
unsigned char flag_cekam = 1;             // neco jako timeoutu v zadosti o data
unsigned char flag_start = 10;
unsigned char flag_outputs_modules = 0;
unsigned char flag_datalogger = 0;
unsigned char flag_cisteni = 0;
unsigned char flag_sensor_zadosti = 0;
unsigned char flag_sensor8 = 0;
unsigned char flag_key_lock_time = 0;
unsigned char flag_totaliser_cycling = 0;
unsigned char flag_reset_after_load_default = 0;
unsigned char flag_second_graph = 0;
unsigned char flag_ir_buttons = 0;
unsigned char flag_timer1;
unsigned char flag_wifi_test;
volatile unsigned int flag_second_gsm = 0;
volatile unsigned char flag_RTC = 0;
volatile unsigned char flag_receive = 0;  // dosla a zpracovala se data
volatile unsigned char flag_aktivni_zadost = 0;    //pokud se nevraci spravna odpoved, pomoci tohoto flagu se odstavi zadost na data
volatile unsigned char flag_tlacitko = 0;
volatile unsigned char flag_sms_zastaveny = 0;
volatile unsigned char flag_gsm_dokoncil_prijem = 0;
volatile unsigned char flag_timeout_timer2 = 0;
volatile unsigned char flag_komunikacni_modul = 0;
volatile unsigned char flag_usb_modul_insert = 0;

///gsm_datalogger
#define DATALOGGER_NONE     0
#define DATALOGGER_FIRST    (1<<1)
#define DATALOGGER_SECOND   (1<<2)
#define DATALOGGER_THIRD    (1<<3)
#define DATALOGGER_FOUR     (1<<4)
#define DATALOGGER_FIVE     (1<<5)
#define DATALOGGER_DATE     (1<<6)
#define DATALOGGER_SPACE_1  (1<<7)
#define DATALOGGER_TIME     (1<<8)
#define DATALOGGER_SPACE_2  (1<<9)
#define DATALOGGER_PLUS     (1<<10)
#define DATALOGGER_SPACE_3  (1<<11)
#define DATALOGGER_NEG      (1<<12)
#define DATALOGGER_SPACE_4  (1<<13)
#define DATALOGGER_MIN      (1<<14)
#define DATALOGGER_SPACE_5  (1<<15)
#define DATALOGGER_ERROR    (1<<16)
#define DATALOGGER_SPACE_6  (1<<17)
#define DATALOGGER_END      (1<<18)

volatile unsigned int zaloha_sum_pozice_pole = 0;
volatile unsigned char zaloha_pozice_pole = 0;
volatile unsigned char flag_data_correct = 0;
volatile unsigned char flag_posli_datalogger_do_gsm = 0;
volatile unsigned char flag_gsm_datalogger_first_send = 0;
volatile unsigned char flag_gsm_potvrdit_nastaveni = 0;
volatile unsigned long flag_datalogger_ukon = 0;
volatile char odesilaci_buffer[160];
volatile long act_adc[SAMPLES_MAX];

int down, left, esc, up, right, enter;
int timer1_running = 0;
unsigned int zaloha_pocet_odeslani = 0;
unsigned int zaloha_datalogger_counter = 1;
unsigned int second_to_datalogger = 0;
unsigned int frekvence_site = 0;
unsigned char pocet_znaku_gsm = 149;
unsigned char aktualni_cislo_pro_odeslani = 1;
unsigned char aktualni_cislo_interval = 1;
unsigned char flag_chci_odeslat_event = 0;
unsigned char flag_chci_odeslat_interval = 0;
unsigned char flag_probiha_prijem_od_gsm = 0;
unsigned char flag_event_empty_on_odeslano = 0;
unsigned char flag_event_empty_off_odeslano = 0;
unsigned char flag_event_zero_on_odeslano = 0;
unsigned char flag_event_zero_off_odeslano = 0;
unsigned char backlight_second;
unsigned char error_sec = 0;
unsigned char ok_sec = 0;
unsigned char pocet_spatnych_prijmu = 0;
unsigned char pocet_pokusu_send = 0;
unsigned char pocet_pokusu_recv = 0;
unsigned char pocet_pokusu_zadost_sensor = 0;
unsigned char aktivni_cisteni = 0;
unsigned char SDread = 0;
unsigned long flag_aktualni_event = 0;
unsigned long flag_dalsi_zadost_event = 0;
//unsigned long debug_code = 0;
unsigned long actual_error = 0;
unsigned long actual_error_datalogger = 0;
unsigned long ep_avg;
unsigned long error_min = 0;
unsigned long ok_min = 0;
unsigned long sensor_fw_version = 0; // fw verze senzoru
unsigned long adress_flash = USER_FLASH_START;
unsigned long last_sending_error_gsm = 0;
char buffer_gsm_event[100];
char buffer_gsm_interval[160];
char buffer_gsm_potvrzeni[100];
long adc_average;
long teplota_avg;

volatile long pole_teplota[VELIKOST_POLE_TEPLOTA_EP]=
{
  8523000,8523000,8523000,8523000,8523000,8523000,8523000,8523000,8523000,8523000,
};
volatile long pole_ep[VELIKOST_POLE_TEPLOTA_EP];
volatile TDateTime DateTime;

// typ modulu pripojeneho pres rozhrani Iout
static TIoutModule ioutModuleType = TIM_IV_OUT;

// aktualni stranka servisnich informaci
unsigned char serviceInfoPage = SERVICE_MAIN_PAGE;

unsigned char inicializace_senzoru(void);   //pro nacteni calibracnich dat atd
void odesli_buffer(unsigned char velikost);
unsigned char check_gsm_modul(void);

static void WriteEventLogLine(const char *texts[], unsigned char textCount);

//*****************************************************************************
  // Volume_Conversion()  ****************************************** 01FEB06 ****
  //*****************************************************************************
  // Tato funkce zajisti prevod mezi jednotkami objemu
  // Zakladni jednotkou je m3
  // Jen zakladni jednotka se bude ukladat do flash pameti
  // Z jednotky m3 se budou prevadet do nasledujicich jednotek:
  // Prevodni tabulka objemu
  // Jednotka         Zkratka  Hodnota      Typ
  // gallon UK        UKG      219.969248   0
  // gallon US        USG      264.172052   1
  // metr krychlový   m3       1            2
  // litr             l        1000         3
  //****************************************************************************/
unsigned long long Unit_Volume_Conversion(unsigned long in_hi, unsigned long in_lo)
{
  unsigned long out_hi = 0;
  unsigned long out_lo = 0;
  unsigned long long out = 0;
  switch (RAM_Read_Data_Long(UNIT_VOLUME))
  switch (2)
  {
    // UKG
    case 0:
      out_hi = (in_hi*219.969248 + (in_lo*219.969248)/1000000);                      // prevod m3 na UKG cela cast
      out_lo = ((in_hi*219.969248 + (in_lo*219.969248)/1000000) - out_hi) * 1000000; // prevod m3 na UKG desetinna cast
      break;
    // USG
    case 1:
      out_hi = (in_hi*264.172052 + (in_lo*264.172052)/1000000);                      // prevod m3 na UKG cela cast
      out_lo = ((in_hi*264.172052 + (in_lo*264.172052)/1000000) - out_hi) * 1000000; // prevod m3 na UKG desetinna cast
      break;
    // m3
    case 2:
      out_hi = in_hi;
      out_lo = in_lo;
      break;
    // l
    case 3:
      out_hi = (in_hi * 1000.0 + (in_lo*1000.0)/1000000);                               // prevod m3 na l cela cast
      out_lo = ((in_hi * 1000.0 + (in_lo*1000.0)/1000000) - out_hi) * 1000000;          // prevod m3 na l desetinna cast
      break;
  } //switch
  out = out_hi;
  out <<= 32;
  out += out_lo;
  return (out);
}

char * ReadStringFromFlash (unsigned int adress, char *g_s1)
{
  // nacita retezec, ktery konci znakem 0
  unsigned char znak = 0xFF;
  unsigned int i = 0;
  do
  {
    znak = (P_texty[adress+i]);
    g_s1[i]= znak;
    i++;
  }
  while (znak != 0x00);
  return g_s1;
};

char * ReadUnitFromFlash (unsigned int adress, char *g_s1)
{
  // nacita retezec (jednotku), ktery konci znakem 0
  unsigned char znak = 0xFF;
  unsigned int i = 0;
  do
  {
      znak = (P_unit[UNIT_SIZE*adress+i]);
      g_s1[i]= znak;
      i++;
  }
  while (znak != 0x00);
  return g_s1;
};

static float GetExtTempInCelsius(TTempChannel channel)
{
    float temp = ExtTemp_GetLastCorrectMeasuredValue(channel);
    if (temp != 0)
        return Convert_KelvinToCelsius(temp);
    else
        return 0;
}

static float GetTotalHeat(THeatUnit heatUnit)
{
    float heatTotal = Heat_GetTotalHeat(heatUnit);
    if (heatTotal >= 0)
        return heatTotal;
    else
        return 0;
}

static float GetFlowVelocity(void)
{
    float semidiameter = RAM_Read_Data_Long(DIAMETER) / 1000.0 / 2;
    if (semidiameter != 0)
    {
        return g_measurement.actual / 3600 / (PI * (semidiameter * semidiameter));
    }
    else
        return 0;
}

unsigned long FloatToUlong(float value)
{
    unsigned long result;
    memcpy(&result, &value, 4);
    return result;
}

void Reload_meas_value (void)
{
}

// low flow cutoff test
BOOL IsActualFlowUnderLowCutoffLevel(void)
{
    float flow = g_measurement.actual;
    if (flow < 0)
        flow = flow / (-1.0);

    switch (RAM_Read_Data_Long(LOW_FLOW_CUTOFF))
    {
        case 0:
        {
            // pokud je prutok mensi jak 0,5% FLOW_RANGE
            if (flow < (RAM_Read_Data_Long(FLOW_RANGE) * 0.000005))
                return TRUE;
            break;
        }
        case 1:
        {
            // pokud je prutok mensi jak 1% FLOW_RANGE
            if (flow < (RAM_Read_Data_Long(FLOW_RANGE) * 0.00001))
                return TRUE;
            break;
        }
        case 2:
        {
            // pokud je prutok mensi jak 2% FLOW_RANGE
            if (flow < (RAM_Read_Data_Long(FLOW_RANGE) * 0.00002))
                return TRUE;
            break;
        }
        case 3:
        {
            // pokud je prutok mensi jak 5% FLOW_RANGE
            if (flow < (RAM_Read_Data_Long(FLOW_RANGE) * 0.00005))
                return TRUE;
            break;
        }
        case 4:
        {
            // pokud je prutok mensi jak 10% FLOW_RANGE
            if (flow < (RAM_Read_Data_Long(FLOW_RANGE) * 0.0001))
                return TRUE;
            break;
        }
    }

    return FALSE;
}

// inicializace bufferu pro graf
void InitGraphHistory(void)
{
    for (int i = 0; i < GRAPH_WIDTH; i++)
    {
        graphHistory[i].bottom = 4;
        graphHistory[i].center = 0;
        graphHistory[i].top = 0;
    }
}

// Test, zda je UART1 vyhrazen pro Modul3
BOOL IsUart1ForModul3(void)
{
    if (RAM_Read_Data_Long(MBUS_MODULE) != 0)
        return TRUE;
    else
        return FALSE;
}

// init funkcnosti pres rozhrani UART1
void InitUart1Function(void)
{
    flag_komunikacni_modul = 0;
    RAM_Write_Data_Long(0,MODULE_PRESSENT);

    BOOL detectModul3 = IsUart1ForModul3();

    if (detectModul3)
    {
        MBusSlave_Init(FALSE);

        smaz_GPRS_errory();
        flag_komunikacni_modul = MODUL_GPRS;
        RAM_Write_Data_Long(0,MODULE_PRESSENT);
        GPRS_Init();

        if( !RAM_Read_Data_Long(MODULE_PRESSENT) ) //pokud neni vlozen GPRS modul
        {
            smaz_GPRS_errory();
            ///kontrola zapojeni gsm modulu (pritomnosti)
            if( check_gsm_modul() )
            {
                flag_komunikacni_modul = MODUL_GSM;
                RAM_Write_Data_Long(1,MODULE_PRESSENT);
                gsm_busy_pin(0); //nahodit pin do nuly
                FIO2DIR &= ~(1<<2);
            }
            else
            {
                // kontrola na pritomnost wifi modulu
                testWifi();
                if ( !RAM_Read_Data_Long(MODULE_PRESSENT) )
                {
                    RAM_Write_Data_Long(0,GSM_SIGNAL);

                    flag_komunikacni_modul = MODUL_TCPIP_BLUE;
                    RAM_Write_Data_Long(1,MODULE_PRESSENT);
                    FIO2DIR |= (1<<3);   //vystupni pin  pro reset TCP/IP modulu
                    FIO2SET |= (1<<3);   //reset - TCP/IP pin P2.3 do "1"
                }
            }
        }
    }
    else
    {
        smaz_GPRS_errory();
        RAM_Write_Data_Long(0,GSM_SIGNAL);

        MBusSlave_Init(TRUE);
    }
}

static void CheckSensor8(void)
{
  volatile unsigned char inicializacni_casovac = RTC_SEC + 3;
  if( inicializacni_casovac >= 60)
    inicializacni_casovac -= 60;
  flag_sensor8 = 1;
  OperQueue_Init();
  while(inicializacni_casovac != RTC_SEC)
  {
      StopSensorReceiving();
      recv_unit_no_sensor8();

      while((!flag_receive) && (!flag_timeout_timer2) && (RTC_SEC!=inicializacni_casovac));

      if (flag_receive)
      {
        zpracovat_data();
        return;
      }
  }

  StopSensorReceiving();
  flag_sensor8 = 0;
}

// init funkcnosti sensoru prutoku
void InitSensorFunction(void)
{
    actual_error &= ~ERR_SENSOR & ~ERR_EMPTY_PIPE & ~ERR_OVERLOAD & ~ERR_ADC & ~ERR_EXCITATION & ~ERR_TEMPERATURE & ~ERR_OVERLOAD2;

    if (RAM_Read_Data_Long(FLOW_SENSOR_TYPE) == 0) // 0=MAGS1, 1=Pulse
    {
        PulseFlowSensor_Init(FALSE);

        CheckSensor8();
        if (!flag_sensor8)
        {
            actual_error |= ERR_SENSOR;
            return;
        }

        Sensor8_Init(TRUE);
    }
    else
    {
        Sensor8_Init(FALSE);
        PulseFlowSensor_Init(TRUE);
    }
}

// init funkcnosti pres rozhrani Iout modulu
void InitIoutModuleFunction(void)
{
    actual_error &= ~ERR_EXT_PRESSURE;
    DAC7512_init();
    ioutModuleType = TIM_IV_OUT;
}

// init funkcnosti pres rozhrani Pulse modulu
void InitPulseModuleFunction(void)
{
    ExtTemp_Close();

    if (RAM_Read_Data_Long(EXT_TEMP_MEAS_STATE) == 0)
    {
        ExtTemp_Open();
    }
    else
    {
        actual_error &= ~ERR_EXT_TEMPERATURE;
    }
}

// vraci typ modulu pripojeneho pres rozhrani Iout
TIoutModule GetIoutModuleType(void)
{
    return ioutModuleType;
}

// externi mereni
void ExternalMeasurements(void)
{
    static unsigned long tempTimer = 0;

    if (RAM_Read_Data_Long(EXT_TEMP_MEAS_STATE) == 0)
    {
        if (SysTick_IsSysMilliSecondTimeout(tempTimer, 10))
        {
            if (ExtTemp_Measure())
                actual_error &= ~ERR_EXT_TEMPERATURE;
            else
                actual_error |= ERR_EXT_TEMPERATURE;

            tempTimer = SysTick_GetSysMilliSeconds();
        }
    }
}

// Is currently displayed measurement enabled?
/*static BOOL IsDispMeasurementEnabled(void)
{
    switch (g_measurement.act_rot_meas)
    {
    case MEAS_EXT_TEMP:
        return RAM_Read_Data_Long(EXT_TEMP_MEAS_STATE) == 0 ? TRUE : FALSE;

    case MEAS_EXT_PRESS:
        return RAM_Read_Data_Long(EXT_PRESS_MEAS_STATE) == 0 ? TRUE : FALSE;

    default:
        return TRUE;
    }
}*/

// Shift forward currently displayed measurement
void ShiftForwardDispMeasurement(void)
{
//    while (1)
//    {
        g_measurement.act_rot_meas = (g_measurement.act_rot_meas < (MEAS_COUNT-1)) ? (g_measurement.act_rot_meas+1) : 0;
//        if (IsDispMeasurementEnabled())
//            return;
//    }
}

// Shift back currently displayed measurement
void ShiftBackDispMeasurement(void)
{
//    while (1)
//    {
        g_measurement.act_rot_meas = (g_measurement.act_rot_meas > 0) ? (g_measurement.act_rot_meas-1) : (MEAS_COUNT-1);
//        if (IsDispMeasurementEnabled())
//            return;
//    }
}

// Shift forward currently displayed service info page
void ShiftForwardDispServiceInfoPage(void)
{
    serviceInfoPage = (serviceInfoPage < (SERVICE_INFO_PAGES-1)) ? (serviceInfoPage+1) : 0;
}

// Shift back currently displayed service info page
void ShiftBackDispServiceInfoPage(void)
{
    serviceInfoPage = (serviceInfoPage > 0) ? (serviceInfoPage-1) : (SERVICE_INFO_PAGES-1);
}

void ShowMeasurement(void)
{
  static char str[50];

  WriteErase();
  if( flag_key_lock_time>0)
  {
    flag_key_lock_time--;
    WriteImage(0, 0, IMG_KEY_HELP);
    WriteNow();
  }
  else
  {
    if (RAM_Read_Data_Long(SERVICE_MODE) == 0)
    {
        // zobrazeni aktualni stranky servisnich informaci
        switch (serviceInfoPage)
        {
        case SERVICE_MAIN_PAGE:
        default:
            {
                // zobrazeni dat ze senzoru
                sprintf(str, "%s%ld", "Ep:",ep_avg);
                Write(60, 5, str);
                sprintf(str, "%s%ld", "A1:",act_adc[0]);
                Write(0, 4, str);
                sprintf(str, "%s%ld", "A2:",act_adc[1]);
                Write(0, 5, str);
                sprintf(str, "%s%ld", "A3:", act_adc[2]);
                Write(0, 6, str);
                sprintf(str, "%s%ld", "A :", adc_average);
                Write(0, 7, str);

                if (flag_sensor8)
                {
                    sprintf(str, "T :%01f", (((signed long)teplota_avg)/10.0));
                    #define CHAR_ST  (176)
                    char stc [4] = { ' ', CHAR_ST, 'C' ,0 };  // Retezec st C
                    strcat(str, stc);
                }
                else
                    sprintf(str, "T :%ld", (teplota_avg));

                Write(60, 4, str);
                sprintf(str, "%s%ld", "M1:", RAM_Read_Data_Long(MEASURED_POINT_ONE));
                Write(60, 1, str);
                sprintf(str, "%s%ld", "M2:", RAM_Read_Data_Long(MEASURED_POINT_TWO));
                Write(60, 2, str);
                sprintf(str, "%s%ld", "M3:", RAM_Read_Data_Long(MEASURED_POINT_THREE));
                Write(60, 3, str);
                sprintf(str, "F :%03f",  g_measurement.actual);
                Write(60, 7, str);
                sprintf(str, "%s%ld", "ZC:",RAM_Read_Data_Long(ZERO_FLOW_CONSTANT ));
                Write(60, 6, str);
                sprintf(str, "%s%ld", "C1:", RAM_Read_Data_Long(CALIBRATION_POINT_ONE));
                Write(0, 1, str);
                sprintf(str, "%s%ld", "C2:", RAM_Read_Data_Long(CALIBRATION_POINT_TWO));
                Write(0, 2, str);
                sprintf(str, "%s%ld", "C3:", RAM_Read_Data_Long(CALIBRATION_POINT_THREE));
                Write(0, 3, str);
            }
            break;

        case SERVICE_OPTIC_BUTTONS_PAGE:
            {
                Write(0, 1, "Optic Buttons Page");
                sprintf(str, "%s%d", "Down:", down);
                Write(0, 2, str);
                sprintf(str, "%s%d", "Up    :", up);
                Write(0, 3, str);
                sprintf(str, "%s%d", "Left  :", left);
                Write(0, 4, str);
                sprintf(str, "%s%d", "Right :", right);
                Write(0, 5, str);
                sprintf(str, "%s%d", "Esc   :", esc);
                Write(0, 6, str);
                sprintf(str, "%s%d", "Enter:", enter);
                Write(0, 7, str);
                //sprintf(str, "%s%d", "Level:", OpticBtn_GetAvgLevel());
                //Write(60, 2, str);
            }
            break;

        case SERVICE_TEMP_PAGE:
            {
                Write(0, 1, "Temperatures Page");

                float temp = g_measurement.temp;
                sprintf(str, "Temp: %ld.%ld", (long)temp, ((long)(fabs(temp)*1000))%1000);
                Write(0, 2, str);

                float temp1 = ExtTemp_GetLastCorrectMeasuredValue(TCHNL_1);
                if (temp1 != 0)
                {
                    temp1 = Convert_KelvinToCelsius(temp1);
                    sprintf(str, "Temp1: %ld.%ld", (long)temp1, ((long)(fabs(temp1)*1000))%1000);
                }
                else
                {
                    sprintf(str, "Temp1: ?");
                }
                Write(0, 3, str);

                float temp2 = ExtTemp_GetLastCorrectMeasuredValue(TCHNL_2);
                if (temp2 != 0)
                {
                    temp2 = Convert_KelvinToCelsius(temp2);
                    sprintf(str, "Temp2: %ld.%ld", (long)temp2, ((long)(fabs(temp2)*1000))%1000);
                }
                else
                {
                    sprintf(str, "Temp2: ?");
                }
                Write(0, 4, str);

                if ((ExtTemp_GetLastCorrectMeasuredValue(TCHNL_1) != 0) && (ExtTemp_GetLastCorrectMeasuredValue(TCHNL_2) != 0))
                {
                    temp = temp1 - temp2;
                    sprintf(str, "dTemp12: %ld.%ld", (long)temp, ((long)(fabs(temp)*1000))%1000);
                }
                else
                {
                    sprintf(str, "dTemp12: ?");
                }
                Write(0, 5, str);
            }
            break;

        case SERVICE_EXT_TEMP1_PAGE:
        case SERVICE_EXT_TEMP2_PAGE:
            {
                TTempChannel tempChannel = (serviceInfoPage == SERVICE_EXT_TEMP1_PAGE) ? TCHNL_1 : TCHNL_2;

                sprintf(str, "Temp%d Page", tempChannel + 1);
                Write(0, 1, str);
                sprintf(str, "ADC: %08x", ExtTemp_GetAdcData(tempChannel));
                Write(0, 2, str);
                sprintf(str, "Que: %d", ExtTemp_GetValuesInQueue(tempChannel));
                Write(0, 3, str);
                sprintf(str, "Err: %s", ExtTemp_GetErrorText(tempChannel));
                Write(0, 4, str);

                float temp = ExtTemp_GetLastCorrectMeasuredValue(tempChannel);
                if (temp != 0)
                {
                    temp = Convert_KelvinToCelsius(temp);
                    sprintf(str, "Temp: %ld.%ld", (long)temp, ((long)(fabs(temp)*1000))%1000);
                }
                else
                {
                    sprintf(str, "Temp: ?");
                }
                Write(0, 5, str);

                float Rref = ExtTemp_GetADCRefResistance(tempChannel);
                sprintf(str, "Rref: %ld.%ld", (long)Rref, ((long)(fabs(Rref)*1000))%1000);
                Write(0, 6, str);
            }
            break;

        case SERVICE_EXT_TEMP1_AD_PAGE:
        case SERVICE_EXT_TEMP2_AD_PAGE:
            {
                TTempChannel tempChannel = (serviceInfoPage == SERVICE_EXT_TEMP1_AD_PAGE) ? TCHNL_1 : TCHNL_2;

                sprintf(str, "Temp%d AD Page", tempChannel + 1);
                Write(0, 1, str);

                unsigned char x, y, count;
                uint32_t *p = ExtTemp_GetAdcArray(tempChannel);

                count = ExtTemp_GetValuesInQueue(tempChannel);
                if (count > 12) count = 12;

                for (unsigned char i = 0; i < count; i++)
                {
                    if (i < 6)
                    {
                        x = 0;
                        y = 2 + i;
                    }
                    else
                    {
                        x = 60;
                        y = i - 4;
                    }

                    sprintf(str, "%08x", p[i]);
                    Write(x, y, str);
                }
            }
            break;

        case SERVICE_MBUS_PAGE:
            {
                const TMBusSlaveState *pState = MBusSlave_GetState();

                Write(0, 1, "MBus Page");
                sprintf(str, "Rx Frames: %ld", pState->rxFrameCount);
                Write(0, 2, str);
                sprintf(str, "Rx Timeouts: %ld", pState->rxTimeoutCount);
                Write(0, 3, str);
                sprintf(str, "Rx Error Code: %d", pState->rxErrorCode);
                Write(0, 4, str);
                sprintf(str, "Tx Frames: %ld", pState->txFrameCount);
                Write(0, 5, str);

                sprintf(str, "Buf:");
                BYTE byteArrayLength = pState->buffer.dataCount;
                if (byteArrayLength > 20) byteArrayLength = 20;
                ConvertByteArrayToHexStr(pState->buffer.data, byteArrayLength, &str[4]);
                Write(0, 6, str);
            }
            break;

        case SERVICE_PULSE_FLOW_SENSOR_PAGE:
            {
                const TPulseFlowSensorState *pState = PulseFlowSensor_GetState();

                Write(0, 1, "Pulse Flow Sensor Page");
                sprintf(str, "Pulses: %ld", pState->pulseCount);
                Write(0, 2, str);
                sprintf(str, "dTms: %ld", pState->dTms);
                Write(0, 3, str);
                sprintf(str, "dPulse: %ld", pState->dPulse);
                Write(0, 4, str);
                sprintf(str, "dM3: %03f", pState->dM3);
                Write(0, 5, str);
                sprintf(str, "Flow: %03f", g_measurement.actual);
                Write(0, 6, str);
            }
            break;

        case SERVICE_HEAT_TEST_PAGE:
            {
                const THeatTestState *pState = Heat_GetTestState();

                Write(0, 1, "Heat Test Page");
                sprintf(str, "Seconds: %ld", pState->valid ? pState->actualSeconds : 0);
                Write(0, 2, str);
                sprintf(str, "Flow[m3/h]: %03f", g_measurement.actual);
                Write(0, 3, str);
                sprintf(str, "Volume[m3]: %03f", pState->valid ? pState->totalVolume : 0);
                Write(0, 4, str);
                sprintf(str, "Heat[J]: %ld", pState->valid ? (long)pState->totalHeat : 0);
                Write(0, 5, str);
            }
            break;

        case SERVICE_CHARGER_PAGE:
            {
                const TPowerManState *pState = PowerMan_GetState();

                Write(0, 1, "Charger Page");
                sprintf(str, "Status1 [0B]: %04x", pState->chargerStatus1);
                Write(0, 2, str);
                sprintf(str, "Status2 [0C]: %04x", pState->chargerStatus2);
                Write(0, 3, str);
            }
            break;
        }

      if(actual_error)    //pokud je error tak jej vypisu
      {
        sprintf(str, "ER:%08x", actual_error);
        Write(0, 0, str);
      }

      if (key_lock)
      {
        char srti[2];
        srti[0] = 21;
        srti[1] = 0;
        Write(63,0,srti);
      }

      if(flag_cisteni)
      {
        char srti[2];
        srti[0] = 27;
        srti[1] = 0;
        Write(71,0,srti);
      }
      if( RAM_Read_Data_Long(DEMO) == 0 ) //pokud je DEMO, nactu symbol
      {
        char stri[4];
        stri[0]=29;
        stri[1]=0;
        Write(78,0,stri);
      }
    }
    else
    {
      // Vykresleni horniho  cisla
      /// musi zde byt jinak se pri navratu z menu vykreslili na prvnim radku xxxxx
      //char iii [2];
      //sprintf(iii, "%d", 1);

      // aktualni tepelny vykon
      long long pomocna = 0;
      pomocna = Heat_GetHeatPerformance() * 1000;
      WriteBigNumberCentr( 2, pomocna/1000, labs(pomocna%1000) , RAM_Read_Data_Long(POINT_UP));

      // low flow cutoff sign
      if (!flag_start && IsActualFlowUnderLowCutoffLevel())
        Write(0, 2, "*");

      // jednotka
      Write(40, 0, "W" );

      // stav baterie
      WriteBattery(48, 0);

      // Vykresleni spodniho cisla vcetne jeho oznaceni
      volatile unsigned char unit_volume = RAM_Read_Data_Long(UNIT_VOLUME);    ///pomocna ktera se pouziva pri preteceni totaliseru pres 4000000
      switch (g_measurement.act_rot_meas)
      {
        // Prednastavene max 3 des. mista u vsech pocitadel.  U teploty zvlast.
        case MEAS_TOTAL:
          if((g_measurement.total/1000000) > 4000000)
            unit_volume = 2;
          pomocna = Convert_Volume(g_measurement.total,unit_volume);
          WriteBigNumberCentr( 4, pomocna/1000000, labs(pomocna%1000000/1000) , 3);
          Write( 0, 7, "Total" );
          break;
        case MEAS_AUX:
          if( (g_measurement.aux/1000000) > 4000000)
            unit_volume = 2;
          pomocna = Convert_Volume(g_measurement.aux,unit_volume);
          WriteBigNumberCentr( 4, pomocna/1000000, labs(pomocna%1000000/1000), 3);
          Write( 0, 7, "Aux+" );
          break;
        case MEAS_FLOW_POS:
          if( (g_measurement.flow_pos/1000000) > 4000000)
            unit_volume=2;
          pomocna = Convert_Volume(g_measurement.flow_pos,unit_volume);
          WriteBigNumberCentr( 4, pomocna/1000000, labs(pomocna%1000000/1000), 3);
          Write( 0, 7, "Total+" );
          break;
        case MEAS_FLOW_NEG:
          if( (g_measurement.flow_neg/1000000) > 4000000)
            unit_volume = 2;
          pomocna = Convert_Volume(g_measurement.flow_neg,unit_volume);
          WriteBigNumberCentr( 4, pomocna/1000000, labs(pomocna%1000000/1000), 3);
          Write( 0, 7, "Total-" );
          break;
        case MEAS_TEMP:
          if(!RAM_Read_Data_Long(UNIT_TEMPERATURE))
            WriteBigNumberCentr( 4, (long)(g_measurement.temp),labs(((long)(g_measurement.temp*100))%100*10) , 2);
          else
            WriteBigNumberCentr( 4, (long)((g_measurement.temp*1.8 + 32)), labs(((long)((g_measurement.temp*1.8 + 32)*100))%100*10), 2); // FAHRNHEIT

          WriteImage(110, 4, IMG_TEMP);
          Write( 0, 7, "Temp" );
          break;
        case MEAS_SPEED:
          {
              float speed = GetFlowVelocity();
              WriteBigNumberCentr( 4, (long)speed, labs(((long)(speed*1000))%1000), 3);
              Write( 0, 7, "Vel" );
          }
          break;
        case MEAS_BAR:
          WriteBar();
          Write( 0, 7, "Bar" );
          break;
        case MEAS_GRAPH:
          WriteGraph();
          Write( 0, 7, "Graph" );
          break;
        case MEAS_EXT_TEMP1:
        case MEAS_EXT_TEMP2:
          {
              TTempChannel tempChannel = (g_measurement.act_rot_meas == MEAS_EXT_TEMP1) ? TCHNL_1 : TCHNL_2;

              float temp = ExtTemp_GetLastCorrectMeasuredValue(tempChannel);
              if (temp != 0)
              {
                  temp = RAM_Read_Data_Long(UNIT_TEMPERATURE) == 0 ?
                    Convert_KelvinToCelsius(temp) : Convert_KelvinToFahrenheit(temp);

                  WriteBigNumberCentr( 4, (long)temp, labs(((long)(temp*100))%100*10), 2);
              }
              else
              {
                  WriteBigNumberCentr( 4, 0, 0, 2);
              }

              WriteImage(110, 4, IMG_TEMP);

              if (tempChannel == TCHNL_1)
                Write( 0, 7, "XTemp1" );
              else
                Write( 0, 7, "XTemp2" );
          }
          break;
        case MEAS_FLOW:
          {
                pomocna = Convert_Flow(g_measurement.actual,RAM_Read_Data_Long(UNIT_FLOW))*1000;
                WriteBigNumberCentr( 4, pomocna/1000, labs(pomocna%1000) , 3);
                Write( 0, 7, "Flow" );
          }
          break;
        case MEAS_TOTAL_HEAT:
          {
                float totalHeat = Heat_GetTotalHeat(RAM_Read_Data_Long(UNIT_HEAT));
                WriteBigNumberCentr( 4, (long)totalHeat, labs(((long)(totalHeat*1000))%1000), 3);
                Write( 0, 7, "Heat" );
          }
          break;
      } // switch

      // Jednotka spodniho pocitadla
      if( flag_start )    //pokud je start delay tak zobraz verzi FW misto jednotky a FW checksum
      {
          char pole[20];

          sprintf(pole,"v%02d.%02d",(RAM_Read_Data_Long(FIRM_WARE)/100),(RAM_Read_Data_Long(FIRM_WARE)%100));
          Write(95,7,pole);
      }
      else if ((g_measurement.act_rot_meas == MEAS_TEMP) ||
               (g_measurement.act_rot_meas == MEAS_EXT_TEMP1) || (g_measurement.act_rot_meas == MEAS_EXT_TEMP2))
      {
          // podle pocitadla vyberu jednotku teploty nebo objemu. 4==teplota
          #define CHAR_ST  (176)
          char stc [3] =
          {
            CHAR_ST, 'C' ,0
          }
          ;  // Retezec st C
          char stf [3] =
          {
            CHAR_ST, 'F' ,0
          }
          ;  // Retezec st F
          switch (RAM_Read_Data_Long(UNIT_TEMPERATURE))
          {
            case 0 /* st.C*/:
              Write( 112, 7, stc );
              break;
            case 1 /* st.F */:
              Write( 112, 7, stf );
              break;
          }
      }
      else if (g_measurement.act_rot_meas == MEAS_SPEED)
      {
          Write( 105, 7, "m/s" );
      }
      else if (g_measurement.act_rot_meas == MEAS_FLOW)
      {
            // Jednotky aktualniho prutoku
            switch (RAM_Read_Data_Long(UNIT_FLOW))
            {
                // UKG/min
                case 0:
                    Write(105, 7, "UKG/min" );
                    break;
                // USG/min
                case 1:
                    Write(105, 7, "USG/min" );
                    break;
                // m3/h
                case 2:
                    Write(105, 7, "m3/h" );
                    break;
                // l/min
                case 3:
                    Write(105, 7, "l/min" );
                    break;
                // l/s
                case 4:
                    Write(105, 7, "l/s" );
                    break;
            }
      }
      else if (g_measurement.act_rot_meas == MEAS_TOTAL_HEAT)
      {
          Write( 105, 7, Heat_GetUnitText(RAM_Read_Data_Long(UNIT_HEAT)) );
      }
      else if ((g_measurement.act_rot_meas == MEAS_BAR) || (g_measurement.act_rot_meas == MEAS_GRAPH))
      {
        // pokud je vybrana obrazovka graf nebo bar, tak se jednotky neukazuji
      }
      /*else if (g_measurement.act_rot_meas == MEAS_EXT_PRESS)
      {
          switch (RAM_Read_Data_Long(UNIT_PRESSURE))
          {
            case 0 :
              Write( 110, 7, "bar" );
              break;
            case 1 :
              Write( 110, 7, "psi" );
              break;
          }
      }*/
      else
      {
          switch (unit_volume)    ///unit volume se vycte kousek nahore kde se vykresluje BIG NUMBER (resi se tam preteceni displeje)
          {
            case 0 /* UKG */:
              Write( 110, 7, "UKG" );
              break;
            case 1 /* USG */:
              Write( 110, 7, "USG" );
              break;
            case 2 /* m3 */:
              Write( 110, 7, "m3" );
              break;
            case 3 /* l */:
              Write( 110, 7, "l" );
              break;
          }
      }

      if(actual_error)
      {
        sprintf(str,"!%08x",actual_error);
        //sprintf(str,"D%08x",debug_code);
        Write(39,7,str);
      }

      // Znacka, urcujici, jestli mereni jede
      {
        #define CHAR_RUN  (13)
        #define CHAR_STOP (14)
        char Run [2] = { CHAR_RUN ,0 };  // Retezec Run
        char Stop[2] = { CHAR_STOP,0 };  // Retezec Stop

        if ( RAM_Read_Data_Long(MEASUREMENT_STATE) == 0 )
        {
          // Zapnuto
          Write(0,0, Run);
        }
        else
        {
          Write(0,0, Stop);
        }
      }
      // zobrazeni dat ze senzoru konec
      if (key_lock)
      {
        char srt[2];
        srt[0] = 21;
        srt[1] = 0;
        Write(9,0,srt);
      }

      if(flag_cisteni)
      {
        char srt[2];
        srt[0] = 27;
        srt[1] = 0;
        Write(17,0,srt);
      }

      if( RAM_Read_Data_Long(SD_PRESSENT))
      {
        str[0]=28;
        str[1]=0;
        Write(24,0,str);
      }
      if( RAM_Read_Data_Long(DEMO) == 0 ) //pokud je DEMO, nactu symbol
      {
        str[0]=29;
        str[1]=0;
        Write(32,0,str);
      }
    }

    // vypsani casu na displej
    RTC_read();
    sprintf(str,"%02d:%02d:%02d",DateTime.hour,DateTime.minute,DateTime.second);
    Write(86, 0, str);
    //#warning WriteLastRow(BUTTON_MENU);
    WriteNow();
  }
}

// zkopiruje dalsi 4 bytovou hodnotu z user flash pameti do RAM
static void CopyNextUserFlash4bIntoRAM(unsigned long* pUserFlashAddr, unsigned long ramAddr)
{
    unsigned long userFlashAddr = *pUserFlashAddr;

    RAM_Write_Data_Long(RAM_Read_Data_Long(userFlashAddr), ramAddr);

    userFlashAddr += 4;
    *pUserFlashAddr = userFlashAddr;
}

// Pokud je procesor spusteny poprve, nakopiruji se defaultni hodnoty na prislusne misto v EEPROM. Pote je
// jeste vytvorena jejich kopie. Ta je ulozena za prvni tabulku + je mezi nimi mezera VALUES_OFFSET.
// Delka techto tabulek se muze menit
void FirstRun( void )
{
  if (RAM_Read_Data_Long( FIRM_WARE ) != FW_VERSION)
  {
    unsigned long i;
    // Prvni spusteni
    for (i=1; i<(sizeof_P_values/sizeof(P_values[0])); i++)
    {
      //Vytvorit kopii tabulky values, primo za soucasnou tabulku values
      // Z flash do fram
      // nulta pozice je first run check point
      {
        RAM_Write_Data_Long( P_values[i], VALUES_OFFSET + i*4 ); // nahrani defaultnich hodnot z flash do ram
        // Kopie vramci eeprom
        //RAM_Write_Data_Long( P_values[i], VALUES_OFFSET + sizeof_P_values + i*4 ); // nahrani defaultnich hodnot z flash do zalohovaci ram
      }
    }

    RAM_Write_Data_Long( IS_NOT_FIRST, CONTROL_POINTER ); // Poznaceni, ze procesor nenabiha poprve
    RAM_Write_Data_Long(3, POINT_UP); // nastavim na 3 des. mista
    RAM_Write_Data_Long(FW_VERSION, FIRM_WARE); // nastavim na 3 des. mista
    RTC_set(13,48,00,3,6,2010); // pri prvnim initu se nastavi datum a cas

    WriteErase();
    char strrr[10];
    WriteCenter(3, "Update");
    WriteCenter(4, "Successful");
    sprintf(strrr, "%ld", sizeof_P_values/sizeof(P_values[0]));
    WriteCenter(5, strrr);
    WriteNow  ();

    for (unsigned int iii = 0; iii<0xffff; iii++)
      for (unsigned char jjj = 0; jjj<100; jjj++)
        asm volatile("nop");

    if (RAM_Read_Data_Long(USER_FLASH_START) != 0xffffffff) //pokud je zapis ve flashce nactou se hodnoty
    {
        RAM_Write_Data_Long(RAM_Read_Data_Long(USER_FLASH_START),UNIT_NO); /*01*/

        g_measurement.total=(RAM_Read_Data_Long(USER_FLASH_START+4))*1000000.0; // obnova DIG /*02*/
        g_measurement.total+=(RAM_Read_Data_Long(USER_FLASH_START+8))*1000.0;   // obnova dec /*03*/

        g_measurement.flow_pos=(RAM_Read_Data_Long(USER_FLASH_START+12))*1000000.0; /*04*/
        g_measurement.flow_pos+=(RAM_Read_Data_Long(USER_FLASH_START+16))*1000.0; /*05*/

        g_measurement.flow_neg=(RAM_Read_Data_Long(USER_FLASH_START+20))*1000000.0; /*06*/
        g_measurement.flow_neg+=(RAM_Read_Data_Long(USER_FLASH_START+24))*1000.0; /*07*/

        g_measurement.aux=(RAM_Read_Data_Long(USER_FLASH_START+28))*1000000.0; /*08*/
        g_measurement.aux+=(RAM_Read_Data_Long(USER_FLASH_START+32))*1000.0; /*09*/

        RAM_Write_Data_Long(g_measurement.total/1000000, VOLUME_TOTAL_DIG);
        RAM_Write_Data_Long(g_measurement.total%1000000, VOLUME_TOTAL_DEC);
        RAM_Write_Data_Long(g_measurement.flow_pos/1000000, VOLUME_POS_DIG);
        RAM_Write_Data_Long(g_measurement.flow_pos%1000000, VOLUME_POS_DEC);
        RAM_Write_Data_Long(g_measurement.flow_neg/1000000, VOLUME_NEG_DIG);
        RAM_Write_Data_Long(g_measurement.flow_neg%1000000, VOLUME_NEG_DEC);
        RAM_Write_Data_Long(g_measurement.aux/1000000, VOLUME_AUX_DIG);
        RAM_Write_Data_Long(g_measurement.aux%1000000, VOLUME_AUX_DEC);

        unsigned long userFlashAddr = USER_FLASH_START+36;

        CopyNextUserFlash4bIntoRAM(&userFlashAddr, POINT_UP); /*10*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, ZERO_FLOW_CONSTANT); /*11*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_PHONE_1_H); /*12*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_PHONE_1_L); /*13*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_PHONE_2_H); /*14*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_PHONE_2_L); /*15*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_PHONE_3_H); /*16*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_PHONE_3_L); /*17*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_GATEWAY0); /*18*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_GATEWAY1); /*19*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_GATEWAY2); /*20*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_GATEWAY3); /*21*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_GATEWAY4); /*22*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_GATEWAY5); /*23*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_GATEWAY6); /*24*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_GATEWAY7); /*25*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_GATEWAY8); /*26*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_GATEWAY9); /*27*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_GATEWAY10); /*28*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_GATEWAY11); /*29*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_GATEWAY12); /*30*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_GATEWAY13); /*31*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_GATEWAY14); /*32*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_GATEWAY15); /*33*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_USER0); /*34*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_USER1); /*35*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_USER2); /*36*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_PASSWORD0); /*37*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_PASSWORD1); /*38*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_PASSWORD2); /*39*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_PORT); /*40*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_PIN); /*41*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, ACTUAL_TOTALIZER); /*42*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, HEAT_TOTAL_FLOAT); /*43*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, FLOW_SENSOR_UNIT_NO); /*44*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, ERROR_MIN); /*45*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, OK_MIN); /*46*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, DIAMETER); /*47*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, FLOW_RANGE); /*48*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, TEMP_SENSOR1_UNIT_NO); /*49*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, TEMP_SENSOR2_UNIT_NO); /*50*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, UNIT_FLOW); /*51*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, UNIT_VOLUME); /*52*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, UNIT_TEMPERATURE); /*53*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, UNIT_HEAT); /*54*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, ACT_LANGUAGE); /*55*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, CONTRAST); /*56*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BACKLIGHT); /*57*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, STATUS_LED_MODE); /*58*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, PASSWORD_USER); /*59*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, DATALOGGER_INTERVAL); /*60*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, CSV_FORMAT); /*61*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, AIR_DETECTOR); /*62*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, AVERAGE_SAMPLES); /*63*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GRAPH_QUANTITY); /*64*/

        CopyNextUserFlash4bIntoRAM(&userFlashAddr, HEAT_POWER_RANGE); /*01*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP_MEAS_STATE); /*02*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP_SENSOR_TYPE); /*03*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP_SENSOR_CONN); /*04*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_CAL_TEMPERATURE1); /*05*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_CAL_TEMPERATURE2); /*06*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP1_REF_RESISTANCE_0); /*07*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP1_REF_RESISTANCE_1); /*08*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP1_REF_RESISTANCE_2); /*09*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP1_REF_RESISTANCE_3); /*10*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP1_REF_RESISTANCE_4); /*11*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP1_REF_RESISTANCE_5); /*12*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP1_REF_RESISTANCE_6); /*13*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP1_REF_RESISTANCE_7); /*14*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP1_REF_RESISTANCE_8); /*15*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP1_REF_RESISTANCE_9); /*16*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP1_REF_RESISTANCE_10); /*17*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP1_REF_RESISTANCE_11); /*18*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP2_REF_RESISTANCE_0); /*19*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP2_REF_RESISTANCE_1); /*20*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP2_REF_RESISTANCE_2); /*21*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP2_REF_RESISTANCE_3); /*22*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP2_REF_RESISTANCE_4); /*23*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP2_REF_RESISTANCE_5); /*24*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP2_REF_RESISTANCE_6); /*25*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP2_REF_RESISTANCE_7); /*26*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP2_REF_RESISTANCE_8); /*27*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP2_REF_RESISTANCE_9); /*28*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP2_REF_RESISTANCE_10); /*29*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXT_TEMP2_REF_RESISTANCE_11); /*30*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, CURRENT_SET); /*31*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, I_DIRECT_DRIVING_QUANTITY); /*32*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, I_HEAT_POWER_MIN); /*33*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, I_HEAT_POWER_MAX); /*34*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, HEAT_POWER_CURRENT_MIN); /*35*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, HEAT_POWER_CURRENT_MAX); /*36*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, I_FLOW_MIN); /*37*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, I_FLOW_MAX); /*38*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, CURRENT_MIN); /*39*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, CURRENT_MAX); /*40*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, I_CAL_POINT_1); /*41*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, I_CAL_POINT_2); /*42*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, I_CAL_CONST_1); /*43*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, I_CAL_CONST_2); /*44*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, RE1_SET); /*45*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, RE2_SET); /*46*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, RE_FLOW_1); /*47*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, RE_FLOW_2); /*48*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, RE_HYSTERESIS_1); /*49*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, RE_HYSTERESIS_2); /*50*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, RE3_SET); /*51*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, RE4_SET); /*52*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, RE_VOLUME_PLUS); /*53*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, RE_VOLUME_MINUS); /*54*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, RE_DOSE); /*55*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, F_SET); /*56*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, F_FLOW1); /*57*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, F_FLOW2); /*58*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, F_FREQ1); /*59*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, F_FREQ2); /*60*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, F_DUTY_CYCLE); /*61*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, MODBUS_SLAVE_ADDRESS); /*62*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, MODBUS_BAUDRATE); /*63*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, MODBUS_PARITY); /*64*/

        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE1_SET); /*01*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE1_HEAT_POWER_1); /*02*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE1_HEAT_POWER_2); /*03*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE1_HEAT_POWER_HYSTERESIS_1); /*04*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE1_HEAT_POWER_HYSTERESIS_2); /*05*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE2_SET); /*06*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE2_HEAT_POWER_1); /*07*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE2_HEAT_POWER_2); /*08*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE2_HEAT_POWER_HYSTERESIS_1); /*09*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE2_HEAT_POWER_HYSTERESIS_2); /*10*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE3_SET); /*11*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE3_HEAT_POWER_1); /*12*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE3_HEAT_POWER_2); /*13*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE3_HEAT_POWER_HYSTERESIS_1); /*14*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE3_HEAT_POWER_HYSTERESIS_2); /*15*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE3_F_HEAT_POWER1); /*16*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE3_F_HEAT_POWER2); /*17*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE3_F_HEAT_POWER_FREQ1); /*18*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE3_F_HEAT_POWER_FREQ2); /*19*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE4_SET); /*20*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE4_HEAT_POWER_1); /*21*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE4_HEAT_POWER_2); /*22*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE4_HEAT_POWER_HYSTERESIS_1); /*23*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE4_HEAT_POWER_HYSTERESIS_2); /*24*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE4_F_HEAT_POWER1); /*25*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE4_F_HEAT_POWER2); /*26*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE4_F_HEAT_POWER_FREQ1); /*27*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, BIN_RE4_F_HEAT_POWER_FREQ2); /*28*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, MBUS_SLAVE_ADDRESS); /*29*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, MBUS_BAUDRATE); /*30*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, MBUS_PARITY); /*31*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, TOTALIZER_CYCLING); /*32*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_DATA_INTERVAL); /*33*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_EMPTY_PIPE); /*34*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_ZERO_FLOW); /*35*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_ERROR_DETECT); /*36*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_EMPTY_PIPE_SEND); /*37*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, GSM_ZERO_FLOW_SEND); /*38*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, DEMO); /*39*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, SIMULATED_FLOW); /*40*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, MEASUREMENT_STATE); /*41*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, AIR_CONSTANT); /*42*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, START_DELAY); /*43*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, INVERT_FLOW); /*44*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, CLEAN_TIME); /*45*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, LOW_FLOW_CUTOFF); /*46*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, SD_CARD_BUFFER); /*47*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, MBUS_MODULE); /*48*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, EXCITATION_FREQUENCY); /*49*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, OPT_DU_BTN_LEVEL); /*50*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, OPT_LR_BTN_LEVEL); /*51*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, OPT_EE_BTN_LEVEL); /*52*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, FLOW_SENSOR_TYPE); /*53*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, PULSES_PER_LITRE); /*54*/
        CopyNextUserFlash4bIntoRAM(&userFlashAddr, FLOW_PULSE_TIMEOUT); /*55*/

        RTC_set_month(((*(unsigned char *)(userFlashAddr))&0x0000000F) + /*56*/
                  10*(((*(unsigned char *)(userFlashAddr))>>4)&0x0000000F));
        userFlashAddr++;
        RTC_set_day(((*(unsigned char *)(userFlashAddr))&0x0000000F) +
                10*(((*(unsigned char *)(userFlashAddr))>>4)&0x0000000F));
        userFlashAddr++;

        unsigned long date = 0;
        date = 100*((*(unsigned char *)(userFlashAddr))&0x0000000F) +
             1000*(((*(unsigned char *)(userFlashAddr))>>4)&0x0000000F);
        userFlashAddr++;
        date += ((*(unsigned char *)(userFlashAddr))&0x0000000F) +
            10*(((*(unsigned char *)(userFlashAddr))>>4)&0x0000000F);
        userFlashAddr++;
        RTC_set_year(date);

        RTC_set_minute(((*(unsigned char *)(userFlashAddr))&0x0000000F) + /*57*/
                   10*(((*(unsigned char *)(userFlashAddr))>>4)&0x0000000F));
        userFlashAddr++;
        RTC_set_second(((*(unsigned char *)(userFlashAddr))&0x0000000F) +
                   10*(((*(unsigned char *)(userFlashAddr))>>4)&0x0000000F));
        userFlashAddr++;
        RTC_set_hour(((*(unsigned char *)(userFlashAddr))&0x0000000F) +
                 10*(((*(unsigned char *)(userFlashAddr))>>4)&0x0000000F));
        userFlashAddr += 2;

        /// nove veci v menu vzdy pridavat na konec

        g_fun[F_OUT]=14;
        EditDoFunction(F_OUT, g_fun);
        g_fun[F_OUT]=40;
        EditDoFunction(F_OUT, g_fun);
        g_fun[F_OUT]=45;
        EditDoFunction(F_OUT, g_fun);
        g_fun[F_OUT]=47;
        EditDoFunction(F_OUT, g_fun);
        g_fun[F_OUT]=59;
        EditDoFunction(F_OUT, g_fun);
    }
  }
  else
  {
    WriteErase();
    char strrr[10];
    WriteCenter(3, "Number of");
    WriteCenter(4, "Values");
    sprintf(strrr, "%ld", sizeof_P_values/sizeof(P_values[0]));
    WriteCenter(5, strrr);
    WriteNow  ();
  }
}

//------------------------------------------------------------------------------
//  Hlavni uloha bez sensoru 8. Volat co nejcasteji.
//------------------------------------------------------------------------------
static void NoSensor8_Task(void)
{
    ///prevzeti actual error do erroru pro datalogger
    actual_error_datalogger |= actual_error;
    /// jednou za sekundu
    if (flag_RTC) // jednou za sekundu
    {
      if (RAM_Read_Data_Long(DEMO) == 0) //je demo
        Heat_MeasureWithFlow(g_measurement.actual, 1);

      if( flag_komunikacni_modul & MODUL_GSM )
      {
        if( flag_gsm_dokoncil_prijem)
        {
          flag_gsm_dokoncil_prijem=0;
          zpracovat_data_gsm();
        }

        if( !flag_sms_zastaveny)    //pokud nejsou zastaveny sms
        {
          if( flag_probiha_prijem_od_gsm )
          {
            if( flag_probiha_prijem_od_gsm > 2)
            {
              flag_probiha_prijem_od_gsm=3;
              actual_error |= ERR_GSM_TIMEOUT;
            }
            else
              flag_probiha_prijem_od_gsm++;
          }
          //pokud je volna linka na gsm a chci odeslat dalsi data od dataloggeru
          if( flag_posli_datalogger_do_gsm && !gsm_odesila() )
          {
            delay_100ms();
            delay_100ms();
            read_datalogger(GSM_Date_backup,0,0);
          }

          //pokud je volna linka na gsm a chci odeslat event data
          if( flag_chci_odeslat_event && !gsm_odesila() && !flag_posli_datalogger_do_gsm )
          {
            odesli_gsm_buffer(buffer_gsm_event,buffer_gsm_event[1]);
            delay_100ms();
            delay_100ms();
            flag_chci_odeslat_event=0;
            if( aktualni_cislo_pro_odeslani < 3)
            {
              aktualni_cislo_pro_odeslani++;
              gsm_odesli_event( flag_aktualni_event );
            }
            else if ( aktualni_cislo_pro_odeslani == 3)
            {
              aktualni_cislo_pro_odeslani=1;
              gsm_busy_pin(0);
            }
          }

          //pokud je tu zadost o dalsi event, neceka zadna aktualni event na odeslani a je volna linka
          if( flag_dalsi_zadost_event && !flag_chci_odeslat_event && !gsm_odesila() && !flag_posli_datalogger_do_gsm)
          {
            if( flag_dalsi_zadost_event & GSM_NEXT_EMPTY_ON )
            {
              flag_dalsi_zadost_event &= ~GSM_NEXT_EMPTY_ON;
              gsm_odesli_event(GSM_EMPTY_ON);
            }
            else if( flag_dalsi_zadost_event & GSM_NEXT_ZERO_ON )
            {
              flag_dalsi_zadost_event &= ~GSM_NEXT_ZERO_ON;
              gsm_odesli_event(GSM_ZERO_FLOW_ON);
            }
            else if( flag_dalsi_zadost_event & GSM_NEXT_ERROR_ON )
            {
              flag_dalsi_zadost_event &= ~GSM_NEXT_ERROR_ON;
              gsm_odesli_event(GSM_ERROR_ON);
            }
            else if( flag_dalsi_zadost_event & GSM_NEXT_EMPTY_OFF )
            {
              flag_dalsi_zadost_event &= ~GSM_NEXT_EMPTY_OFF;
              gsm_odesli_event(GSM_EMPTY_OFF);
            }
            else if( flag_dalsi_zadost_event & GSM_NEXT_ZERO_OFF )
            {
              flag_dalsi_zadost_event &= ~GSM_NEXT_ZERO_OFF;
              gsm_odesli_event(GSM_ZERO_FLOW_OFF);
            }
          }

          //pokud je volna linka na gsm a chci odeslat event data
          if( flag_chci_odeslat_interval && !gsm_odesila() && !flag_posli_datalogger_do_gsm )
          {
            odesli_gsm_buffer(buffer_gsm_interval,buffer_gsm_interval[1]);
            delay_100ms();
            delay_100ms();
            flag_chci_odeslat_interval=0;
            if( aktualni_cislo_interval < 3)
            {
              aktualni_cislo_interval++;
              gsm_odesli_interval( );
            }
            else
            {
              aktualni_cislo_interval=1;
              gsm_busy_pin(0);
            }
          }

          //odpovi na spravne nastaveni
          if( flag_gsm_potvrdit_nastaveni && !gsm_odesila() && !flag_posli_datalogger_do_gsm)
          {
            odesli_gsm_buffer(buffer_gsm_potvrzeni,buffer_gsm_potvrzeni[1]);
            delay_100ms();
            delay_100ms();
            flag_gsm_potvrdit_nastaveni=0;
          }

          if( flag_gsm_zije)
          {
            flag_gsm_zije--;
            if( flag_gsm_zije == 0)
            {
              gsm_busy_pin(0);
              ukonceni_odesilani_datalogger_gsm();
            }
          }
        }
      }

      if(backlight_second>0)
      {
        DISPLAY_LIGHT_IOSET |= DISPLAY_LIGHT_PIN;
        backlight_second--;
      }
      else
      {
        if ( !RAM_Read_Data_Long(BACKLIGHT) ) // pokud je nastaveno na 10 seconds tak se podsviceni vypne
          DISPLAY_LIGHT_IOCLR |= DISPLAY_LIGHT_PIN;
      }

      flag_RTC = 0;
      write_to_RAM();
      co_sekunda();
      control(SHOW);     // prekresleni displeje
    }

    /// zmena nastaveni modbus
    if ((ChangeCommunicationParam)&&(modbus_transmitt_state == 1))
    {
      modbus_transmitt_state = 0;
      #ifdef  RTS_ENABLE
        //while (RTS_PORT&(1<<RTS_PIN))
        //while((IOPIN0&(1<<10))==(1<<10))
        asm volatile ("nop");//cekej na odvysilani cele zpravy
      #endif
      Modbus_init();
      ChangeCommunicationParam=false;
    }

    obsluha_tlacitek();

    if (modbus_read_datalogger_flag)
    {
      modbus_read_datalogger_flag = 0;
      read_datalogger(modbus_datalogger_date, modbus_datalogger_time, 1);
    }
}


///**************************************************************************//
///                                 MAIN                                     //
///**************************************************************************//
int main(void)
{
  PowerMan_Init();

  // prvotni kompletni test firmware
  FwCheck_Task(FWCHECK_Complete);

  // inicializace cache dataloggeru
  dataloggerCache.count = 0;
  dataloggerCache.writeIdx = 0;
  dataloggerCache.readIdx = 0;

  // init fronty ukonu
  OperQueue_Init();

  // zkouska jestli jsou IR tlacitka
  /*if (IOPIN0 & (1<<25))
    flag_ir_buttons = 1;
  // kdyz jsou IR tlacitka, tak proved nastaveni pinu a AD prevodniku
  if (flag_ir_buttons)
    pinConfig();*/

  StepUp_Init();
  StepUp_Set(STPUP_ON);

  pressed_key = 0xff; // kod stisknuteho tlacitka
  modbus_transmitt_state = 0;
  ChangeCommunicationParam = false;
  // nastaveni flagu na wifi modul pro test
  flag_komunikacni_modul = MODUL_WIFI;
  SPI_DisplayInit();
  Init_Display();

  DisplaySetContrast(60);
  DISPLAY_LIGHT_IOSET |= DISPLAY_LIGHT_PIN;

  FirstRun();

  // fw checksum do RAM (kvuli info)
  RAM_Write_Data_Long((unsigned long)FwCheck_GetChecksum(), FW_CHECKSUM);

  /// prepocitani authorize password
  RAM_Write_Data_Long((unsigned int)(((RAM_Read_Data_Long(UNIT_NO) ^ 33333333)) & 0x03FFFFFF), PASSWORD_AUTHORIZE);

  ///vykresleni obvrazku
  WriteErase();
  WriteImage(0, 0, IMG_ARKON);
  WriteNow();

  if (!flag_ir_buttons)
  {
    ///tlacitka preruseni
    IO0_INT_EN_R |= (1<<23);
    IO0_INT_EN_F |= (1<<23);
    VICVectAddr17  = (unsigned long)Tlacitko;// Set Interrupt Vector
    VICIntEnable  = (1  << 17);
  }

  ExtTemp_Init();
  InitializationVCOM();
  IntEnable();
  Sensor_init(9600);  //inicializace senzoru 7
  InitIoutModuleFunction();
  InitPulseModuleFunction();
  Heat_Init();
  PulseOutputs_Init();
  Led_Init();

  Modbus_init();      //inicializace modbusu
  eMBSetSlaveID( 27, TRUE, ucSlaveID, 27 );		// ulozeni slave id do modbusu

  RTC_init();         //inicializace RTC
  RTC_set_interrupt();//nastaveni preruseni od RTC
  RTC_read();

  init_timer1();

  ///========== inicial SD cards =================================
  if( f_mount( 0, &fatfs) != FR_OK )
  {
    //xputs("Unknown FAT\r\n");
  }
  if( disk_initialize( 0 ) != RES_OK )
  {
    //actual_error |= ERR_NOT_INSERT_CARD;
    RAM_Write_Data_Long(0,SD_PRESSENT);
  }
  else
  {
    RAM_Write_Data_Long(1,SD_PRESSENT);
    actual_error &= ~ERR_NOT_INSERT_CARD;
    actual_error &= ~ERR_OPEN_FILE;
  }

  WriteEventLogMessage("Start");

  // inicializace bufferu pro graf
  InitGraphHistory();

  ///***********************************************************************
  ///     NACITANI VECI Z EPROMKY
  ///***********************************************************************
  ///
  /// samples per average
  ///
  error_min=RAM_Read_Data_Long(ERROR_MIN);
  ok_min=RAM_Read_Data_Long(OK_MIN);
  g_measurement.total = RAM_Read_Data_Long(VOLUME_TOTAL_DIG)*1000000.0 + RAM_Read_Data_Long(VOLUME_TOTAL_DEC);
  g_measurement.flow_pos = RAM_Read_Data_Long(VOLUME_POS_DIG)*1000000.0 + RAM_Read_Data_Long(VOLUME_POS_DEC);
  g_measurement.flow_neg=RAM_Read_Data_Long(VOLUME_NEG_DIG)*1000000.0 + RAM_Read_Data_Long(VOLUME_NEG_DEC);
  g_measurement.aux=RAM_Read_Data_Long(VOLUME_AUX_DIG)*1000000.0 + RAM_Read_Data_Long(VOLUME_AUX_DEC);
  g_measurement.act_rot_meas = RAM_Read_Data_Long(ACTUAL_TOTALIZER);

  flag_start=RAM_Read_Data_Long(START_DELAY);
  switch(RAM_Read_Data_Long(DATALOGGER_INTERVAL))
  {
    case 0: //off
      flag_datalogger=0;
      second_to_datalogger=0;
      break;
    case 1: //1 minuta
      flag_datalogger=1;
      second_to_datalogger=60;
      break;
    case 2: //5 minut
      flag_datalogger=1;
      second_to_datalogger=300;
      break;
    case 3: //10 minut
      flag_datalogger=1;
      second_to_datalogger=600;
      break;
    case 4: //15 minut
      flag_datalogger=1;
      second_to_datalogger=900;
      break;
    case 5: //30 minuit
      flag_datalogger=1;
      second_to_datalogger=1800;
      break;
    case 6: //1 hodina
      flag_datalogger=1;
      second_to_datalogger=3600;
      break;
    case 7: //2 hodiny
      flag_datalogger=1;
      second_to_datalogger=7200;
      break;
    case 8: //6 hodin
      flag_datalogger=1;
      second_to_datalogger=21600;
      break;
    case 9: //12 hodin
      flag_datalogger=1;
      second_to_datalogger=43200;
      break;
    case 10:    //1 den
      flag_datalogger=1;
      second_to_datalogger=86400;
      break;
    default:
      flag_datalogger=0;
      second_to_datalogger=0;
      break;
  }
  flag_second_gsm=((unsigned int)RAM_Read_Data_Long(GSM_DATA_INTERVAL))*60;
  ///inicializace BINu
  unsigned char fun[2];

  fun[F_OUT] = 15;    // RE1_SET
  EditDoFunction(F_OUT, fun); // provedeni vystupni funkce
  fun[F_OUT] = 16;    // RE2_SET
  EditDoFunction(F_OUT, fun); // provedeni vystupni funkce
  fun[F_OUT] = 17;    // RE1 a RE2 komparatory
  EditDoFunction(F_OUT, fun); // provedeni vystupni funkce
  fun[F_OUT] = 18;    // RE3_SET
  EditDoFunction(F_OUT, fun); // provedeni vystupni funkce
  fun[F_OUT] = 19;    // RE4_SET
  EditDoFunction(F_OUT, fun); // provedeni vystupni funkce
  fun[F_OUT] = 20;    // RE_VOLUME_PLUS
  EditDoFunction(F_OUT, fun); // provedeni vystupni funkce
  fun[F_OUT] = 21;    // RE_VOLUME_MINUS
  EditDoFunction(F_OUT, fun); // provedeni vystupni funkce
  fun[F_OUT] = 22;    // RE_DOSE
  EditDoFunction(F_OUT, fun); // provedeni vystupni funkce
  fun[F_OUT] = 27;    // F_FLOW A FREQ KOMPARACE
  EditDoFunction(F_OUT, fun); // provedeni vystupni funkce
  fun[F_OUT] = 28;    // DUTY CYCLE
  EditDoFunction(F_OUT, fun); // provedeni vystupni funkce
  fun[F_OUT] = 23;    // FO_SET
  EditDoFunction(F_OUT, fun); // provedeni vystupni funkce

  ///inicializace gsm event
  fun[F_OUT] = 63;    // EMPTY PIPE EVENT
  EditDoFunction(F_OUT, fun); // provedeni vystupni funkce
  fun[F_OUT] = 64;    // ZERO FLOW EVENT
  EditDoFunction(F_OUT, fun); // provedeni vystupni funkce
  fun[F_OUT] = 65;    // ERROR EVENT
  EditDoFunction(F_OUT, fun); // provedeni vystupni funkce
  //Send_Long(FO_F,F_OFF);

  ///inicializace I-OUT a V-OUT
  fun[F_OUT] = 32;    // I-out setting signal
  EditDoFunction(F_OUT, fun); // provedeni vystupni funkce
  fun[F_OUT] = 33;    // I-out direct driving and calibration
  EditDoFunction(F_OUT, fun); // provedeni vystupni funkce

  /// inicializace menu
  g_adr               = 0;
  g_level             = 0;
  menu_levels[g_level]= 0;
  g_count             = (P_menu[g_adr]);
  g_scroll            = 0;
  g_mode              = MEASUREMENT;

  backlight_second = BACKLIGHT_SEC;

  char gg[10];
  sprintf(gg,"%x",RAM_Read_Data_Long(0x110));
  Write(0,0,gg);

  // MBus nebo Modul3
  InitUart1Function();

  // vymazani serioveho cisla sensoru, pokud je pripojen sensor 8 tak se automaticky aktualizuje.
  // Tohle se uz provadet nebude, v zalohovane RAM musi byt cislo posledne pripojeneho sensoru.
  //RAM_Write_Data_Long(0, SENSOR_UNIT_NO);

  /// check pulse module version
  // check MISO - P0.8
  // 0 = old version - aktualizace 1x za sekundu
  // 1 = new version - aktualizace 3x za sekundu
  if( IOPIN0 & (1<<8)) // kontrola pinu P0.8
    pulse_version = PULSE_V2;
  else
    pulse_version = PULSE_V1;

  SysTick_Init();

  RTC_init();
  RTC_set_interrupt();

  init_timer2();
  disable_timer2();

  // init funkcnosti sensoru prutoku (sensor8/pulse)
  InitSensorFunction();


  ///*******************************************************************************
  ///                                                                            ///
  ///                     HLAVNI SMYCKA                                          ///
  ///                                                                            ///
  ///*******************************************************************************
  while(1)
  {
    FwCheck_Task(FWCHECK_Step); // dalsi krok testu firmware
    PowerMan_Task();
    EventLogTask();
    Led_Task();
    PulseOutputs_Task();
    PulseFlowSensor_Task();
    MBusSlave_Task();
    ExternalMeasurements();
    Heat_TestTask();

    if (flag_sensor8)
    {
        if (!flag_cekam_odpoved_od_sensor8)     //pokud komunikuje sensor s transmittrem nekontroluje se pozadavek od PC
            ( void )eMBPoll(  ); // modbus pool

        Sensor8_Task();
    }
    else
    {
        ( void )eMBPoll(  ); // modbus pool
        NoSensor8_Task();
    }
  }

  return 0;
}

void obsluha_tlacitek (void)
{
  if(flag_ir_buttons)
    readButtons();
  if (flag_tlacitko || flag_timer1)
  {
    // kvuli osetreni samomackani tlacitek
    if( (flag_komunikacni_modul & MODUL_GPRS) || ( flag_komunikacni_modul & MODUL_GSM) )
    {
      if( key_lock && flag_timer1)
      {
        flag_key_lock_time=9;
        control(SHOW);
      }
      else
        flag_key_lock_time=0;
    }
    else
    {
      if(key_lock)
      {
        flag_key_lock_time=9;
        control(SHOW);
      }
      else
        flag_key_lock_time=0;
    }

    unsigned char flag_osetreni_GSM = 0; // osetreni proti samomackani kdyz tam je GSM nebo GPRS module

    // kvuli osetreni samomackani tlacitek
    if( (flag_komunikacni_modul & MODUL_GPRS) || ( flag_komunikacni_modul & MODUL_GSM) )
    {
      if (flag_timer1)
        flag_osetreni_GSM = 1;
    }
    else
      flag_osetreni_GSM = 1;

    flag_tlacitko = 0;
    if (pressed_key != 0xFF)              // bylo tlacitko stisknuto?
    {
      switch (pressed_key)                           // ktere tlacitko bylo stisknuto
      {
        case 4 : // UP
          if ((flag_osetreni_GSM) && (!key_lock))
          {
            control(UP);
            DISPLAY_LIGHT_IOSET |= DISPLAY_LIGHT_PIN;
            backlight_second=BACKLIGHT_SEC;
            last_key = UP;
            flag_osetreni_GSM = 0;
          }
          enable_timer1();
          break;
        case 2 : // DOWN
          if ((flag_osetreni_GSM) && (!key_lock))
          {
            control(DOWN);
            DISPLAY_LIGHT_IOSET |= DISPLAY_LIGHT_PIN;
            backlight_second=BACKLIGHT_SEC;
            last_key = DOWN;
            flag_osetreni_GSM = 0;
          }
          enable_timer1();
          break;
        case 1 : // RIGHT
          if ((flag_osetreni_GSM) && (!key_lock))
          {
            control(RIGHT);
            DISPLAY_LIGHT_IOSET |= DISPLAY_LIGHT_PIN;
            backlight_second=BACKLIGHT_SEC;
            last_key = RIGHT;
            flag_osetreni_GSM = 0;
          }
          enable_timer1();
          break;
        case 0 : // ENTER
          if (flag_osetreni_GSM)
          {
            if (g_mode == MEASUREMENT)
            {
              RTC_read(); // aktualizace casove struktury - kvuli kontrole zamku klaves
              // pokud je ESC a Enter stisteny do dvou sekund
              if ( (DateTime.second-key_ESC_time_sec == 1) ||
                   (DateTime.second-key_ESC_time_sec == 0) ||
                   ( (key_ESC_time_sec == 59) && (DateTime.second == 00) ) )
              {
                // pokud bylo posledni tlacitko ESC a ted je ENTER tak se zamkne klavesnice
                if ((last_key == ESC) && (key_lock == 0))
                  key_lock = 1;
                else if ( (last_key == ESC) && (key_lock == 1) )
                {
                  key_lock = 0; // pokud byla zamknuta klavesnice tak se odemkne
                  key_dummy = 1; // vynechani klavesy ENTER
                  flag_key_lock_time=0;//vypne se cas pro zobrazeni napovedy pro odemknuti
                  control(SHOW);
                }
              }
            }
            if (!(key_lock) && !(key_dummy)) control(ENTR);
            DISPLAY_LIGHT_IOSET |= DISPLAY_LIGHT_PIN;
            backlight_second=BACKLIGHT_SEC;
            key_dummy = 0;
            last_key = ENTR;
          }
          enable_timer1();
          break;
        case 5 : // ESC
          if (flag_osetreni_GSM)
          {
            control(ESC);
            DISPLAY_LIGHT_IOSET |= DISPLAY_LIGHT_PIN;
            backlight_second=BACKLIGHT_SEC;
            if (g_mode == MEASUREMENT)
            {
              RTC_read(); // aktualizace casove struktury
              key_ESC_time_sec = DateTime.second;
            }
            last_key = ESC;
            flag_osetreni_GSM = 0;
          }
          enable_timer1();
          break;
        case 3 : // LEFT
          if ((flag_osetreni_GSM) && (!key_lock))
          {
            control(LEFT);
            DISPLAY_LIGHT_IOSET |= DISPLAY_LIGHT_PIN;
            backlight_second=BACKLIGHT_SEC;
            last_key = LEFT;
            flag_osetreni_GSM = 0;
          }
          enable_timer1();
          break;
        default:
          break;
      }
      pressed_key = 0xff;
    }
    else                                                        // tlacitko bylo uvolneno
    {
      disable_timer1();
    }
    flag_timer1 = 0;
  }
}

void co_sekunda()
{
//    //test---
//    static unsigned long pocitacka = 0;
//    VCOM_putchar('.');
//    xprint("co_sekunda\r\n");
//    if (++pocitacka > 30)
//    {
//        if( disk_initialize( 0 ) != RES_OK )
//        {
//            //actual_error |= ERR_NOT_INSERT_CARD;
//            RAM_Write_Data_Long(0,SD_PRESSENT);
//        }
//        else
//        {
//            RAM_Write_Data_Long(1,SD_PRESSENT);
//            actual_error &= ~ERR_NOT_INSERT_CARD;
//            actual_error &= ~ERR_OPEN_FILE;
//        }
//    }
//    //test---

  ///kontrola cyclovani totaliseru
  if( ++flag_totaliser_cycling >=3)
  {
    if( !(RAM_Read_Data_Long( TOTALIZER_CYCLING)) ) //pokud je aktivni cyklovacni totalizeru
    {
      ShiftForwardDispMeasurement();
    }
    flag_totaliser_cycling=0;//vynulovani pocitadla;
  }

  ///kontrola zapisu do flashky
  if( --flag_zapsat_flash == 0 )
  {
    flag_zapsat_flash=SEC_TO_WRITE_FLASH;
    write_data_to_flash();
  }

  ///kontrola error min,Ok min
  if(actual_error)
  {
    error_sec++;
    if (error_sec >= 60)
    {
      error_sec -= 60;
      error_min++;
    }
  }
  else
  {
    ok_sec++;
    if (ok_sec >= 60)
    {
      ok_sec -= 60;
      ok_min++;
    }
  }

  ///zapsani aktualniho prutoku na 0.pozici
  act_adc[0]=2222222;        //kdyz prijdou data tak se zapisi, kdyz byl timeout zapise se nula


  ///vypocet prumerne hodnoty
  adc_average=3333333;

  if( RAM_Read_Data_Long( DEMO))  //OFF -> 0=ON,1=OFF
  {
    //g_measurement.actual=0;
  }
  else    //JE DEMO
  {
    g_measurement.actual = ((float)RAM_Read_Data_Long(SIMULATED_FLOW))/1000.0;
  }

  if(flag_start>0)            //10 sekund po startu se plni nulama
  {
    if( g_measurement.actual != 0 )  //skrze event u gsm, aby neposilal sms hned na zacatku
    {
      flag_event_zero_off_odeslano=1;
      flag_event_zero_on_odeslano=0;
    }
    else
    {
      flag_event_zero_off_odeslano=0;
      flag_event_zero_on_odeslano=1;
    }
    g_measurement.actual=0; //vynulovani aktualniho prutoku skrze start delay
  }


  ///low float cutoff
  if( RAM_Read_Data_Long(LOW_FLOW_CUTOFF) != 5)
  {
    float pomocny_prutok=0;
    if(g_measurement.actual < 0)      //pokud je zaporny prutok
    {
      pomocny_prutok=g_measurement.actual;  //ulozi se do promenne
      g_measurement.actual/=(-1.0);               //a prutok se invertuje
    }

    switch ( RAM_Read_Data_Long( LOW_FLOW_CUTOFF))
    {
      case 0 :
      {
        // pokud je prutok mensi jak 0,5% FLOW_RANGE tak se prutok nastavi na 0
        if (g_measurement.actual < (RAM_Read_Data_Long(FLOW_RANGE) * 0.000005))
          g_measurement.actual = 0;
        break;
      }
      case 1 :
      {
        // pokud je prutok mensi jak 1% FLOW_RANGE tak se prutok nastavi na 0
        if (g_measurement.actual < (RAM_Read_Data_Long(FLOW_RANGE) * 0.00001))
          g_measurement.actual = 0;
        break;
      }
      case 2 :
      {
        // pokud je prutok mensi jak 2% FLOW_RANGE tak se prutok nastavi na 0
        if (g_measurement.actual < (RAM_Read_Data_Long(FLOW_RANGE) * 0.00002))
          g_measurement.actual = 0;
        break;
      }
      case 3 :
      {
        // pokud je prutok mensi jak 5% FLOW_RANGE tak se prutok nastavi na 0
        if (g_measurement.actual < (RAM_Read_Data_Long(FLOW_RANGE) * 0.00005))
          g_measurement.actual = 0;
        break;
      }
      case 4 :
      {
        // pokud je prutok mensi jak 10% FLOW_RANGE tak se prutok nastavi na 0
        if (g_measurement.actual < (RAM_Read_Data_Long(FLOW_RANGE) * 0.0001))
          g_measurement.actual = 0;
        break;
      }
    }

    if( (pomocny_prutok != 0) && (g_measurement.actual!=0) ) //pokud je naplena promenna a neni nulovy prutok
      g_measurement.actual=pomocny_prutok;
  }

  //kontrola na zero flow envent
  //pokud je start delay tak se nebude upozornovat
  if( RAM_Read_Data_Long( GSM_ZERO_FLOW ) && (flag_start == 0))
  {
    //pokud je nulovy prutok a ten jeste nebyl poslan
    if( g_measurement.actual == 0 && !flag_event_zero_on_odeslano)
    {
      flag_dalsi_zadost_event |= GSM_NEXT_ZERO_ON;
      flag_event_zero_on_odeslano=1;
      flag_event_zero_off_odeslano=0;
    }
    //pokud neni nulovy prutok a jeste o tom nebyl informovan
    else if( g_measurement.actual != 0 && !flag_event_zero_off_odeslano)
    {
      if( !( RAM_Read_Data_Long(GSM_ZERO_FLOW_SEND)) )    //0-on i off, 1 jen on
        flag_dalsi_zadost_event |= GSM_NEXT_ZERO_OFF;   //pokud je i off tak se posle do gsm
      flag_event_zero_off_odeslano=1;
      flag_event_zero_on_odeslano=0;
    }
  }

  ///kontrola INVERT FLOW
  if( RAM_Read_Data_Long(INVERT_FLOW) ) //invert flow
    g_measurement.actual=g_measurement.actual*(-1.0);

  ///vypocet average teploty a ep
  teplota_avg=8523000;
  ep_avg=1111111;

  g_measurement.temp = (0.0001939237*teplota_avg - 1430.9631545)/10;       //ve stupnich celsia

  //ExternalMeasurements(); // externi mereni

  ///kontrola jestli neni vetsi prutok nez povoluje DN
  if( (g_measurement.actual > ( (RAM_Read_Data_Long(FLOW_RANGE)/1000)*4.0)) ||
      (g_measurement.actual < ( (RAM_Read_Data_Long(FLOW_RANGE)/1000)*(-4.0))))
  {
    g_measurement.actual = 0;
    actual_error |= ERR_OVERLOAD2;
  }
  else
    actual_error &= ~ERR_OVERLOAD2;

  ///inkrementace totaliseru pokud je measurement ON
  if (!RAM_Read_Data_Long(MEASUREMENT_STATE))
  {
    if(g_measurement.actual>0)
    {
      g_measurement.total+=((g_measurement.actual)/36.0)*10000.0;      //3600 -> m3/h na m3/s
      g_measurement.flow_pos+=((g_measurement.actual)/36.0)*10000.0;
      g_measurement.aux+=((g_measurement.actual)/36.0)*10000.0;
    }
    else
    {
      g_measurement.total+= ((((g_measurement.actual)*(-1.0))/36.0)*10000.0);
      g_measurement.flow_neg+= ((((g_measurement.actual)*(-1.0))/36.0)*10000.0);
    }
  }

  /// vynulovani totalizeru, pokud je 999999999m3
  if (g_measurement.total > 999999999000000)
    g_measurement.total = 0;
  if (g_measurement.flow_pos > 999999999000000)
    g_measurement.flow_pos = 0;
  if (g_measurement.flow_neg > 999999999000000)
    g_measurement.flow_neg = 0;
  if (g_measurement.aux > 999999999000000)
    g_measurement.aux = 0;

  ///zkouska jestli se ma poslat prutok do gsm
  if((--flag_second_gsm == 0) && (RAM_Read_Data_Long(GSM_DATA_INTERVAL)))
  {
    if( flag_chci_odeslat_event )   //pokud je aktivni zadost na gsm neposilam interval a zkusim za sekundu
      flag_second_gsm=1;
    else    //jinak posilam hned a naplnim pocitadlo
    {
      flag_second_gsm=((unsigned int)RAM_Read_Data_Long(GSM_DATA_INTERVAL))*60;
      gsm_odesli_interval();    //odesle data do gsm
    }
  }

  if (++flag_second_graph > 1)
  {

    flag_second_graph = 0;
    // posun cely graficky buffer doleva
    for(int i = 0; i < GRAPH_WIDTH-1; i++)
    {
      graphHistory[i].top = graphHistory[i+1].top;
      graphHistory[i].center = graphHistory[i+1].center;
      graphHistory[i].bottom = graphHistory[i+1].bottom;
    }

    // nacti novou hodnotu prutoku do grafu
    int percent = GetGraphValuePercent();
    int pixels = 0.17 * percent;

    graphHistory[GRAPH_WIDTH-1].top = 0;
    graphHistory[GRAPH_WIDTH-1].center = 0;
    graphHistory[GRAPH_WIDTH-1].bottom = 4;

    if (pixels <= 5)
    {
      for(int i = 0; i < pixels; i++)
        graphHistory[GRAPH_WIDTH-1].bottom |= ( 1<<(i+3) );
    }
    else if (pixels > 5 && pixels <= 13)
    {
      graphHistory[GRAPH_WIDTH-1].bottom = 252;
      for(int i = 0; i < pixels-5; i++)
        graphHistory[GRAPH_WIDTH-1].center |= (1<<i);
    }
    else if(pixels > 13)
    {
      graphHistory[GRAPH_WIDTH-1].bottom = 252;
      graphHistory[GRAPH_WIDTH-1].center = 255;
      for(int i = 0; i < pixels-13; i++)
        graphHistory[GRAPH_WIDTH-1].top |= (1<<i);
    }
  }

  ///zkouska jestli se ma zapsat do dataloggru
  if( ((--second_to_datalogger) == 0) && (flag_datalogger) )
  {
    write_datalogger();

    switch(RAM_Read_Data_Long(DATALOGGER_INTERVAL))
    {
      case 0: //off
        flag_datalogger=0;
        second_to_datalogger=0;
        break;
      case 1: //1 minuta
        flag_datalogger=1;
        second_to_datalogger=60;
        break;
      case 2: //5 minut
        flag_datalogger=1;
        second_to_datalogger=300;
        break;
      case 3: //10 minut
        flag_datalogger=1;
        second_to_datalogger=600;
        break;
      case 4: //15 minut
        flag_datalogger=1;
        second_to_datalogger=900;
        break;
      case 5: //30 minuit
        flag_datalogger=1;
        second_to_datalogger=1800;
        break;
      case 6: //1 hodina
        flag_datalogger=1;
        second_to_datalogger=3600;
        break;
      case 7: //2 hodiny
        flag_datalogger=1;
        second_to_datalogger=7200;
        break;
      case 8: //6 hodin
        flag_datalogger=1;
        second_to_datalogger=21600;
        break;
      case 9: //12 hodin
        flag_datalogger=1;
        second_to_datalogger=43200;
        break;
      case 10:    //1 den
        flag_datalogger=1;
        second_to_datalogger=86400;
        break;
      default:
        flag_datalogger=0;
        second_to_datalogger=0;
        break;
    }
  }

  ///odeslani dat do BINu
  Send_Float(FLOW,g_measurement.actual);
  delay_1ms();
  Send_Long(ERR,(long)( actual_error ));

  switch(flag_outputs_modules)
  {
    case IOUT_MODUL:        //pokud je aktivovan IOUT
      I_Out();
      break;
    case VOUT_MODUL:        //pokud je aktivovan VOUT
      V_Out(g_measurement.actual);
      break;
    default:
      break;
  }

  //kontrola GPRS
  if ((GPRS_INSIDE) && (!(GPRS_init_error)))      //pokud je GPRS pritomen a nebyla chyba pri inicializaci
  {
    if (((GPRS_Error_State) || (GPRS_Error)) && (GPRS_PIN & GPRS_ER))
    {
      GPRS_Error_State = GPRS_NO_ERROR;
      GPRS_Error = GPRS_NO_ERROR;
    }
    if (!(GPRS_PIN & GPRS_ER))                  // chyba hlasena GPRS modulem
    {
      unsigned long U1IER_zaloha = U1IER;   //vytvoreni zalohy kvuli MODBUSu
      U1IER = 0;
      GPRS_Error = GPRS_NO_ERROR;             //vynulovani chyby prenosu
      GPRS_Error_State = GPRS_NO_ERROR;       // vynulovani chyby GPRS
      smaz_GPRS_errory();
      ///InitAndShowErrorMessage(GPRS_Error_test());   //zjisteni cisla chyby kterou vrati GPRS modul
      actual_error |= GPRS_Error_test();

      if (GPRS_Error_State == 10)
        RAM_Write_Data_Long(0,GSM_IP);    //pokud byl vypadek signalu vymazu IP adresu

      U1IER = U1IER_zaloha;                 //obnova zalohy
    }
    else    //pokud neni error
      smaz_GPRS_errory();

    if (RAM_Read_Data_Long(GSM_IP) == 0)        //kdyz nemam IP tak si o nej reknu
    {
      unsigned long U1IER_zaloha = U1IER;   //vytvoreni zalohy kvuli MODBUSu
      U1IER = 0;
      GPRS_Error = GPRS_NO_ERROR;             //vynulovani chyby
      GPRS_get_IP();                          //ziskani IP adresy (funkce automaticky naplni GSM_IP
      if (GPRS_Error)                         //pokud doslo k chybe pri dotazu na IP
      {
        ///InitAndShowErrorMessage(GPRS_Error);   //zobrazeni cisla chyby vznikle pri komunikaci s GPRS
        actual_error |= GPRS_Error;
      }
      U1IER = U1IER_zaloha;                 //obnova zalohy
    }
  }
  else ///if((GPRS_INSIDE) && (GPRS_init_error))    //pokud byla chyba pri inicializaci modulu GPRS
  {
    ///InitAndShowErrorMessage(GPRS_Error);   //zjisteni cisla chyby vznikle pri komunikaci s GPRS
    actual_error |= GPRS_Error;
  }

  if(flag_start>0)
    flag_start--;
}

/**
    Pokud je ještě místo v cache dataloggeru,
    zapíše záznam dataloggeru na aktuální volnou pozici v cache.
*/
static void write_datalogger_cache( void )
{
    if (dataloggerCache.count >= MAX_DATALOGGER_CACHE_RECORDS)
        return;

    TDataloggerRecord *pRec = &dataloggerCache.records[dataloggerCache.writeIdx];

    pRec->dt_year       = DateTime.year;
    pRec->dt_month      = DateTime.month;
    pRec->dt_day        = DateTime.day;
    pRec->dt_hour       = DateTime.hour;
    pRec->dt_minute     = DateTime.minute;

    pRec->meas_flow_pos     = g_measurement.flow_pos;
    pRec->meas_flow_neg     = g_measurement.flow_neg;
    pRec->meas_total        = g_measurement.total;
    pRec->meas_aux          = g_measurement.aux;
    pRec->meas_temp         = g_measurement.temp;
    pRec->meas_ext_temp1    = GetExtTempInCelsius(TCHNL_1);
    pRec->meas_ext_temp2    = GetExtTempInCelsius(TCHNL_2);
    pRec->meas_heat_total   = GetTotalHeat(HU_kWh);

    pRec->firmware      = FW_VERSION;
    pRec->measurement   = !RAM_Read_Data_Long(MEASUREMENT_STATE) ? 1 : 0;

    pRec->error_value   = actual_error_datalogger;
    pRec->error_minute  = error_min;
    pRec->ok_minute     = ok_min;

    dataloggerCache.count++;
    dataloggerCache.writeIdx++;
    if (dataloggerCache.writeIdx >= MAX_DATALOGGER_CACHE_RECORDS)
        dataloggerCache.writeIdx = 0;
}

/**
*   Date and time,
*   FW number,
*   Measurement(Run=1/Stop=0),
*   Total, Total+, Total-, Aux,
*   Error Min, OK min, Error Code,
*   Temperature, External Temperature (if present), External Pressure (if present)
*/
unsigned char write_datalogger( void )
{
    write_datalogger_cache();
    actual_error_datalogger = 0;

    if (disk_initialize(0) != RES_OK)
    {
        actual_error |= ERR_NOT_INSERT_CARD;
        RAM_Write_Data_Long(0,SD_PRESSENT);
        return 0;
    }
    else
    {
        actual_error &= ~ERR_NOT_INSERT_CARD;
        RAM_Write_Data_Long(1,SD_PRESSENT);
    }

    // kontrola, zda se bude ukládat do souboru (omezení častých zápisů kvůli životnosti SD karty),
    // test on/off primeho zapisu na SD
    if (RAM_Read_Data_Long(SD_CARD_BUFFER) == 0)
    {
        unsigned long dlInterval = RAM_Read_Data_Long(DATALOGGER_INTERVAL);
        unsigned char minRecCount = 1;
        if (dlInterval == 1) // 1 minuta
            minRecCount = 10;
        else if (dlInterval == 2) // 5 minut
            minRecCount = 2;

        if (dataloggerCache.count < minRecCount)
            return 1;
    }

    // zápis do souboru
    char pole[40];
    FIL *file = NULL;
    unsigned char fileMonth = 0;
    unsigned long csvFormat = RAM_Read_Data_Long(CSV_FORMAT);
    char dataSeparator = csvFormat ? ';' : ',';
    char decimalPoint = csvFormat ? ',' : '.';

    while (dataloggerCache.count > 0)
    {
        TDataloggerRecord *pRec = &dataloggerCache.records[dataloggerCache.readIdx];

        // kontrola záznamu určeného pro jiný soubor
        if ((file != NULL) && (fileMonth != pRec->dt_month))
        {
            fclose(file);
            file = NULL;
        }

        if (file == NULL)
        {
            sprintf(pole, "/MagC1/%08d/%04d_%02d.csv", RAM_Read_Data_Long(UNIT_NO), pRec->dt_year, pRec->dt_month);
            file = fopen(pole, "a");
            if (file == NULL)
            {
                actual_error |= ERR_OPEN_FILE;
                return 0;
            }
            actual_error &= ~ERR_OPEN_FILE;
            fileMonth = pRec->dt_month;
        }

        sprintf(pole, "%04d/%02d/%02d", pRec->dt_year, pRec->dt_month, pRec->dt_day);
        fputs(pole , file);
        fputc(dataSeparator, file);

        sprintf(pole,"%02d:%02d", pRec->dt_hour, pRec->dt_minute);
        fputs(pole, file);
        fputc(dataSeparator, file);

        sprintf(pole, "%04d", pRec->firmware);
        fputs(pole , file);
        fputc(dataSeparator, file);

        sprintf(pole, "%d", pRec->measurement);
        fputs(pole , file);
        fputc(dataSeparator, file);

        sprintf(pole, "%lu", pRec->meas_total / 1000000);
        fputs(pole , file);
        fputc(decimalPoint, file);
        sprintf(pole, "%03d", (unsigned long)(pRec->meas_total % 1000000) / 1000);
        fputs(pole , file);
        fputc(dataSeparator, file);

        sprintf(pole, "%lu", pRec->meas_flow_pos / 1000000);
        fputs(pole , file);
        fputc(decimalPoint, file);
        sprintf(pole, "%03d", (unsigned long)(pRec->meas_flow_pos % 1000000) / 1000);
        fputs(pole , file);
        fputc(dataSeparator, file);

        sprintf(pole, "%lu", pRec->meas_flow_neg / 1000000);
        fputs(pole , file);
        fputc(decimalPoint, file);
        sprintf(pole, "%03d", (unsigned long)(pRec->meas_flow_neg % 1000000) / 1000);
        fputs(pole , file);
        fputc(dataSeparator, file);

        sprintf(pole, "%lu", pRec->meas_aux / 1000000);
        fputs(pole , file);
        fputc(decimalPoint, file);
        sprintf(pole, "%03d", (unsigned long)(pRec->meas_aux % 1000000) / 1000);
        fputs(pole , file);
        fputc(dataSeparator, file);

        sprintf(pole,"%lu", (unsigned long)pRec->error_minute);
        fputs(pole ,file);
        fputc(dataSeparator, file);

        sprintf(pole,"%lu", (unsigned long)pRec->ok_minute);
        fputs(pole ,file);
        fputc(dataSeparator, file);

        sprintf(pole, "%08x", pRec->error_value);
        fputs(pole , file);
        fputc(dataSeparator, file);

        sprintf(pole, "%d", (int)pRec->meas_temp);
        fputs(pole , file);
        fputc(decimalPoint, file);
        sprintf(pole, "%d", ((unsigned long)(pRec->meas_temp * 10)) % 10);
        fputs(pole , file);
        fputc(dataSeparator, file);

        sprintf(pole, "%d", (int)pRec->meas_ext_temp1);
        fputs(pole , file);
        fputc(decimalPoint, file);
        sprintf(pole, "%d", ((unsigned long)(pRec->meas_ext_temp1 * 10)) % 10);
        fputs(pole , file);
        fputc(dataSeparator, file);

        sprintf(pole, "%d", (int)pRec->meas_ext_temp2);
        fputs(pole , file);
        fputc(decimalPoint, file);
        sprintf(pole, "%d", ((unsigned long)(pRec->meas_ext_temp2 * 10)) % 10);
        fputs(pole , file);

        sprintf(pole, "%lu", (unsigned long)pRec->meas_heat_total);
        fputs(pole , file);
        fputc(decimalPoint, file);
        sprintf(pole, "%03d", ((unsigned long)(pRec->meas_heat_total * 1000)) % 1000);
        fputs(pole , file);

        fputs("\r\n", file);

        dataloggerCache.count--;
        dataloggerCache.readIdx++;
        if (dataloggerCache.readIdx >= MAX_DATALOGGER_CACHE_RECORDS)
            dataloggerCache.readIdx = 0;
    }

    if (file != NULL)
        fclose(file);

    return 1;
}

/**
*   Write event log line.
*/
static void WriteEventLogLine(const char *texts[], unsigned char textCount)
{
    if (!RAM_Read_Data_Long(SD_PRESSENT)) //zdrzovalo obsluhu v menu
        return;

    if (textCount == 0)
        return;

    if (disk_initialize(0) != RES_OK)
    {
        //actual_error |= ERR_NOT_INSERT_CARD;
        RAM_Write_Data_Long(0,SD_PRESSENT);
        return;
    }
    else
    {
        actual_error &= ~ERR_NOT_INSERT_CARD;
        RAM_Write_Data_Long(1,SD_PRESSENT);
    }

    // zápis do souboru
    char pole[40];
    FIL *file = NULL;
    char dataSeparator = ';';

    sprintf(pole, "/MagC1/%08d/events.csv", RAM_Read_Data_Long(UNIT_NO));
    file = fopen(pole, "a");
    if (file == NULL)
    {
        actual_error |= ERR_OPEN_FILE;
        return;
    }
    actual_error &= ~ERR_OPEN_FILE;

    RTC_read();

    sprintf(pole, "%04d/%02d/%02d", DateTime.year, DateTime.month, DateTime.day);
    fputs(pole , file);
    fputc(dataSeparator, file);

    sprintf(pole, "%02d:%02d:%02d", DateTime.hour, DateTime.minute, DateTime.second);
    fputs(pole, file);

    for (unsigned char textIdx = 0; textIdx < textCount; textIdx++)
    {
        fputc(dataSeparator, file);
        if (texts[textIdx] != NULL)
            fputs(texts[textIdx], file);
    }

    fputs("\r\n", file);

    fclose(file);
}

/**
*   Write event log message.
*/
void WriteEventLogMessage(const char *message)
{
    char* texts[1];
    texts[0] = message;
    WriteEventLogLine(texts, 1);
}

/**
*   Write event log parameter change.
*/
void WriteEventLogParameterChange(unsigned long valueOffset, unsigned long data)
{
    if (RAM_Read_Data_Long(valueOffset) == data)
    {
        return;
    }

    int peditRecIdx = GetPeditTableRecordIndex(valueOffset);
    if (peditRecIdx < 0)
    {
        return;
    }

    long ptextyIdx = GetPtextyTableStringIndexFromPmenu(peditRecIdx, valueOffset);
    if (ptextyIdx < 0)
    {
        return;
    }

    char valueString[30];
    if (!GetMenuValueString(peditRecIdx, valueOffset, data, valueString))
    {
        return;
    }

    char* texts[3];
    texts[0] = &P_texty[ptextyIdx];
    texts[1] = valueString;
    texts[2] = &P_unit[GetPunitTableStringIndex(peditRecIdx, valueOffset)];
    WriteEventLogLine(texts, 3);
}

/**
*   Write event log parameter.
*/
void WriteEventLogParameter(const char *name, const char *value, const char *unit)
{
    char* texts[3];
    texts[0] = name;
    texts[1] = value;
    texts[2] = unit;
    WriteEventLogLine(texts, 3);
}

//*****************************************************************************
// Flow_Conversion()    ****************************************** 01FEB06 ****
//*****************************************************************************
// Tato funkce zajisti prevod mezi jednotkami proteceneho objemu
// Zakladni jednotkou je m3
// Jen zakladni jednotka se bude ukladat do pameti
// Z jednotky m3 se budou prevadet do nasledujicich jednotek:
// Prevodni tabulka proteceneho objemu
//*****************************************************************************
long long Convert_Volume(long long in, unsigned char typ)
{
  long long out = 0.0;
  switch (typ)
  {
    case 0 /* UKG */     :
      out = in *  219.9692483;    // prevod m3/h na UKG/min
      break;
    case 1 /* USG */     :
      out = in *  264.17205236;    // prevod m3/h na USG/min
      break;
    case 2 /* m3 */   :
      out = in;                   // prevod m3/h na m3/h
      break;
    case 3 /* l */  :
      out = in * 1000;        // prevod m3/h na l/min
      break;
    default /* m3 */  :
      out = in;
      break;
  }  //switch
  return (out);
}

//*****************************************************************************
// Flow_Conversion()    ****************************************** 01FEB06 ****
//*****************************************************************************
// Tato funkce zajisti prevod mezi jednotkami prutoku
// Zakladni jednotkou je m3/h
// Jen zakladni jednotka se bude ukladat do pameti
// Z jednotky m3/h se budou prevadet do nasledujicich jednotek:
// Prevodni tabulka prutoku
//*****************************************************************************
float Convert_Flow(float in, unsigned char typ)
{
  float out = 0.0;
  switch (typ)
  {
    case 0 /* UKG/min */     :
      out = in * 3.6661541383;    // prevod m3/h na UKG/min
      break;
    case 1 /* USG/min */     :
      out = in * 4.4028675393;    // prevod m3/h na USG/min
      break;
    case 2 /* m3/h */   :
      out = in;                   // prevod m3/h na m3/h
      break;
    case 3 /* l/min */  :
      out = in * 16.66666666666;        // prevod m3/h na l/min
      break;
    case 4 /* l/s */  :
      out = in *  0.27777777778;        // prevod m3/h na l/s
      break;
    default :
      out = in;
      break;
  }  //switch
  return (out);
}

void write_to_RAM() ///zapis promennych do RAMky
{
  RAM_Write_Data_Long(error_min,ERROR_MIN);
  RAM_Write_Data_Long(ok_min,OK_MIN);
  RAM_Write_Data_Long(actual_error,ACTUAL_ERROR);
  //RAM_Write_Data_Float((float)(g_measurement.total/1000000.0), VOLUME_TOTAL_FLOAT);
  RAM_Write_Data_Long(g_measurement.total/1000000, VOLUME_TOTAL_DIG);
  RAM_Write_Data_Long(g_measurement.total%1000000, VOLUME_TOTAL_DEC);
  //RAM_Write_Data_Float((float)(g_measurement.aux/1000000.0), VOLUME_AUX_FLOAT);
  RAM_Write_Data_Long(g_measurement.aux/1000000, VOLUME_AUX_DIG);
  RAM_Write_Data_Long(g_measurement.aux%1000000, VOLUME_AUX_DEC);
  //RAM_Write_Data_Float((float)(g_measurement.flow_neg/1000000.0), VOLUME_NEG_FLOAT);
  RAM_Write_Data_Long(g_measurement.flow_neg/1000000, VOLUME_NEG_DIG);
  RAM_Write_Data_Long(g_measurement.flow_neg%1000000, VOLUME_NEG_DEC);
  //RAM_Write_Data_Float((float)(g_measurement.flow_pos/1000000.0), VOLUME_POS_FLOAT);
  RAM_Write_Data_Long(g_measurement.flow_pos/1000000, VOLUME_POS_DIG);
  RAM_Write_Data_Long(g_measurement.flow_pos%1000000, VOLUME_POS_DEC);
}

void Tlacitko (void)
{
  if (IO0_INT_STAT_R & (1<<23)) // rissing interrupt
  {
    //DISPLAY_LIGHT_IOSET |= DISPLAY_LIGHT_PIN;
    flag_tlacitko = 1;
    pressed_key = stisk();
  }
  else if (IO0_INT_STAT_F & (1<<23)) // falling interrupt
    disable_timer1();

  IO0_INT_CLR |= (1<<23);
  VICVectAddr=0;
}

char read_datalogger(unsigned long date, unsigned int time, unsigned int pocet)
{
  GSM_Date_backup = date;
  #define VELIKOST_ZASOBNIKU 100 // maximalni delka jednoho radku v dataloggeru
  char pole[VELIKOST_ZASOBNIKU];
  unsigned char pozice_odes_buff = 0;
  unsigned long sum_position_pole = 0;
  unsigned long file_velikost = 0;
  unsigned char flag_last_read = 0;
  gsm_busy_pin(1);

  for (unsigned char i=0; i<sizeof(odesilaci_buffer); i++) // vymazani odesilaciho bufferu
    odesilaci_buffer[i] = 0;

  if( disk_initialize( 0 ) != RES_OK )
  {
    actual_error |= ERR_NOT_INSERT_CARD;
    RAM_Write_Data_Long(0,SD_PRESSENT);

    sprintf(odesilaci_buffer,"SDCARD NOT PRESSENT");
    get_datalogger_gsm(19);
    ukonceni_odesilani_datalogger_gsm();
    return 0;
  }
  else
  {
    actual_error &= ~ERR_NOT_INSERT_CARD;
    RAM_Write_Data_Long(1,SD_PRESSENT);
  }

  ///sprintf(pole,"/MagC1/%d.csv",RAM_Read_Data_Long(UNIT_NO));
  sprintf(pole,"/MagC1/%08d/%04d_%02d.csv",RAM_Read_Data_Long(UNIT_NO), date/10000, (date%10000)/100);
  FIL *file = fopen (pole,"r");
  if(file == NULL)
  {
    actual_error |= ERR_OPEN_FILE;
    sprintf(odesilaci_buffer,"CAN'T OPEN FILE SD/MagC1/%08d/%04d_%02d.csv",RAM_Read_Data_Long(UNIT_NO), date/10000, (date%10000)/100 );
    get_datalogger_gsm(42);
    ukonceni_odesilani_datalogger_gsm();
    return 0;
  }
  actual_error &= ~ERR_OPEN_FILE;

  ///kontrola maximalniho poctu znaku
  if(pocet>0) //vetsi nez nula se bude volat pouze kdyz prijde zadost od gsm
    zaloha_pocet_odeslani=pocet;
  if(pocet>1000)  //osetreni maxima
    pocet=1000;
  if(pocet==0)  //pokud se vola s nulou, tak to volam ja uz po nekolikate a nacte se pocet ze zalohy
    pocet=zaloha_pocet_odeslani;

  file_velikost = file->fsize;
  rewind(file);

  sum_position_pole = zaloha_sum_pozice_pole;
  fseek(file,sum_position_pole,SEEK_SET);
  fgets(pole,VELIKOST_ZASOBNIKU,file);

  unsigned int pozice_pole = zaloha_pozice_pole;

  while (!(flag_data_correct))   //nez se potvrdi spravnost dat
  {
    unsigned long datum=0;
    unsigned int cas=0;
    unsigned long pos_datum=10000000;
    ///Datum
    datum=((pole[0])-0x30)*10000000 +
          ((pole[1])-0x30)*1000000 +
          ((pole[2])-0x30)*100000 +
          ((pole[3])-0x30)*10000 +
          ((pole[5])-0x30)*1000 +
          ((pole[6])-0x30)*100 +
          ((pole[8])-0x30)*10 +
          ((pole[9])-0x30);

    pozice_pole = 9;

    if(date < datum) //jestlize je zadane datum mensi (starsi) nez je prvni zapis v dataloggru
    {
      flag_data_correct = 1;
    }
    else if (date > datum)
    {
      while ( pole[pozice_pole] != '\n')  // najdi konec radku
      {
        pozice_pole++;
        if( pozice_pole>VELIKOST_ZASOBNIKU)
        {
          return -2;
        }
      }
      pozice_pole++;
      sum_position_pole+=pozice_pole;
      zaloha_sum_pozice_pole=sum_position_pole;

      if( (sum_position_pole+VELIKOST_ZASOBNIKU) > file_velikost)
      {
        sprintf(odesilaci_buffer,"OUT OF DATE");
        get_datalogger_gsm(11);
        gsm_busy_pin(0);

        return -3;
      }

      fseek(file,sum_position_pole,SEEK_SET);
      fgets(pole,VELIKOST_ZASOBNIKU,file);
    }
    else if( date == datum )
    {
      while (!(flag_data_correct))
      {
        datum=0;
        pos_datum=10000000;
        ///Datum
        for(pozice_pole=0; pozice_pole<4 ; pozice_pole++,pos_datum/=10)
        {
          datum+=pos_datum*((pole[pozice_pole])-0x30);
        }

        for(pozice_pole=5;pozice_pole<7;pozice_pole++,pos_datum/=10)
        {
          datum+=pos_datum*((pole[pozice_pole])-0x30);
        }

        for(pozice_pole=8;pozice_pole<10;pozice_pole++,pos_datum/=10)
        {
          datum+=pos_datum*((pole[pozice_pole])-0x30);
        }
        ///cas
        cas = 0;
        pos_datum=1000;
        for(pozice_pole=11;pozice_pole<13;pozice_pole++,pos_datum/=10)
        {
          cas+=pos_datum*((pole[pozice_pole])-0x30);
        }

        for(pozice_pole=14;pozice_pole<16;pozice_pole++,pos_datum/=10)
        {
          cas+=pos_datum*((pole[pozice_pole])-0x30);
        }

        if ( (cas == time) && (date==datum) )
        {
          flag_data_correct=1;
        }
        else
        {
          if( (time < cas) || (date<datum) )  //znamena, ze cas v dataloggru presel pres zadanay cas
          {
            flag_data_correct = 1;
          }
        }
        pozice_pole++;

        if( !(flag_data_correct))
        {
          while( pole[pozice_pole] != '\n')
          {
            pozice_pole++;
            if( pozice_pole > VELIKOST_ZASOBNIKU)
            {
              return -2;
            }
          }
          pozice_pole++;
          sum_position_pole+=pozice_pole;
          zaloha_sum_pozice_pole=sum_position_pole;
          if ( (sum_position_pole + VELIKOST_ZASOBNIKU) > file_velikost)
          {
            fclose(file);
            sprintf(odesilaci_buffer,"OUT OF DATE");
            get_datalogger_gsm(11);
            gsm_busy_pin(0);
            return -3;
          }
          fseek(file,sum_position_pole,SEEK_SET);
          fgets(pole,VELIKOST_ZASOBNIKU,file);
        } // if
      } // while
    } // else if( date == datum )
  } // while

  //jestlize se nasla spravna data, v poli je aktualni pouzitelny logger pro vystup
  if( flag_data_correct )
  {
    unsigned int counter=zaloha_datalogger_counter;
    for(;counter<pocet+1;counter++,zaloha_datalogger_counter++)
    {
      switch(flag_datalogger_ukon)
      {
        case DATALOGGER_NONE:
        case DATALOGGER_FIRST:
        case DATALOGGER_SECOND:
        case DATALOGGER_THIRD:
        case DATALOGGER_FOUR:
        case DATALOGGER_FIVE:
          ///pozice
          if(counter<10)
          {
            switch(flag_datalogger_ukon)
            {
              case DATALOGGER_NONE:
              case DATALOGGER_FIRST:
                odesilaci_buffer[pozice_odes_buff]='0';//doplnuje se na tvar 000x
                flag_datalogger_ukon=DATALOGGER_SECOND;
                pozice_odes_buff++;
                if( pozice_odes_buff>=pocet_znaku_gsm)
                {
                  odesli_buffer(pozice_odes_buff);
                  zaloha_pozice_pole=pozice_pole;
                  flag_posli_datalogger_do_gsm=1;
                  fclose(file);
                  return 5;
                  pozice_odes_buff=0;
                }
              case DATALOGGER_SECOND:
                odesilaci_buffer[pozice_odes_buff]='0';//doplnuje se na tvar 000x
                flag_datalogger_ukon=DATALOGGER_THIRD;
                pozice_odes_buff++;
                if( pozice_odes_buff>=pocet_znaku_gsm)
                {
                  odesli_buffer(pozice_odes_buff);
                  zaloha_pozice_pole=pozice_pole;
                  flag_posli_datalogger_do_gsm=1;
                  fclose(file);
                  return 5;
                  pozice_odes_buff=0;
                }
              case DATALOGGER_THIRD:
                odesilaci_buffer[pozice_odes_buff]='0';//doplnuje se na tvar 000x
                flag_datalogger_ukon=DATALOGGER_FOUR;
                pozice_odes_buff++;
                if( pozice_odes_buff>=pocet_znaku_gsm)
                {
                  odesli_buffer(pozice_odes_buff);
                  zaloha_pozice_pole=pozice_pole;
                  flag_posli_datalogger_do_gsm=1;
                  fclose(file);
                  return 5;
                  pozice_odes_buff=0;
                }
              case DATALOGGER_FOUR:
                odesilaci_buffer[pozice_odes_buff]=counter+48;//offset pro cisla v ascii tabulce je
                flag_datalogger_ukon=DATALOGGER_FIVE;
                pozice_odes_buff++;
                if( pozice_odes_buff>=pocet_znaku_gsm)
                {
                  odesli_buffer(pozice_odes_buff);
                  zaloha_pozice_pole=pozice_pole;
                  flag_posli_datalogger_do_gsm=1;
                  fclose(file);
                  return 5;
                  pozice_odes_buff=0;
                }
              case DATALOGGER_FIVE:
                odesilaci_buffer[pozice_odes_buff]=' ';
                flag_datalogger_ukon=DATALOGGER_DATE;
                pozice_odes_buff++;
                if( pozice_odes_buff>=pocet_znaku_gsm)
                {
                  flag_datalogger_ukon=DATALOGGER_DATE;
                  odesli_buffer(pozice_odes_buff);
                  zaloha_pozice_pole=pozice_pole;
                  flag_posli_datalogger_do_gsm=1;
                  fclose(file);
                  return 5;
                  pozice_odes_buff=0;
                }
            } // switch
          }
          else if( (counter>=10) && (counter<100))
          {
            switch(flag_datalogger_ukon)
            {
              case DATALOGGER_NONE:
              case DATALOGGER_FIRST:
                odesilaci_buffer[pozice_odes_buff]='0';//doplnuje se na tvar 000x
                flag_datalogger_ukon=DATALOGGER_SECOND;
                pozice_odes_buff++;
                if( pozice_odes_buff>=pocet_znaku_gsm)
                {
                  odesli_buffer(pozice_odes_buff);
                  zaloha_pozice_pole=pozice_pole;
                  flag_posli_datalogger_do_gsm=1;
                  fclose(file);
                  return 5;
                  pozice_odes_buff=0;
                }
              case DATALOGGER_SECOND:
                odesilaci_buffer[pozice_odes_buff]='0';//doplnuje se na tvar 000x
                flag_datalogger_ukon=DATALOGGER_THIRD;
                pozice_odes_buff++;
                if( pozice_odes_buff>=pocet_znaku_gsm)
                {
                  odesli_buffer(pozice_odes_buff);
                  zaloha_pozice_pole=pozice_pole;
                  flag_posli_datalogger_do_gsm=1;
                  fclose(file);
                  return 5;
                  pozice_odes_buff=0;
                }
              case DATALOGGER_THIRD:
              {
                unsigned int buf=counter/10;
                odesilaci_buffer[pozice_odes_buff]=buf+48;//offset pro cisla v ascii tabulce je
                flag_datalogger_ukon=DATALOGGER_FOUR;
                pozice_odes_buff++;
                if( pozice_odes_buff>=pocet_znaku_gsm)
                {
                  odesli_buffer(pozice_odes_buff);
                  zaloha_pozice_pole=pozice_pole;
                  flag_posli_datalogger_do_gsm=1;
                  fclose(file);
                  return 5;
                  pozice_odes_buff=0;
                }
              }
              case DATALOGGER_FOUR:
              {
                unsigned int buf_desitky=counter/10;
                unsigned int buf=counter-(buf_desitky*10);
                odesilaci_buffer[pozice_odes_buff]=buf+48;//offset pro cisla v ascii tabulce je
                flag_datalogger_ukon=DATALOGGER_FIVE;
                pozice_odes_buff++;
                if( pozice_odes_buff>=pocet_znaku_gsm)
                {
                  odesli_buffer(pozice_odes_buff);
                  zaloha_pozice_pole=pozice_pole;
                  flag_posli_datalogger_do_gsm=1;
                  fclose(file);
                  return 5;
                  pozice_odes_buff=0;
                }
              }
              case DATALOGGER_FIVE:
                odesilaci_buffer[pozice_odes_buff]=' ';
                flag_datalogger_ukon=DATALOGGER_DATE;
                pozice_odes_buff++;
                if( pozice_odes_buff>=pocet_znaku_gsm)
                {
                  flag_datalogger_ukon=DATALOGGER_DATE;
                  odesli_buffer(pozice_odes_buff);
                  zaloha_pozice_pole=pozice_pole;
                  flag_posli_datalogger_do_gsm=1;
                  fclose(file);
                  return 5;
                  pozice_odes_buff=0;
                }
            } // switch
          }
          else if ( (counter>=100) && (counter<1000))
          {
            switch(flag_datalogger_ukon)
            {
              case DATALOGGER_NONE:
              case DATALOGGER_FIRST:
                odesilaci_buffer[pozice_odes_buff]='0';//doplnuje se na tvar 000x
                flag_datalogger_ukon=DATALOGGER_SECOND;
                pozice_odes_buff++;
                if( pozice_odes_buff>=pocet_znaku_gsm)
                {
                  odesli_buffer(pozice_odes_buff);
                  zaloha_pozice_pole=pozice_pole;
                  flag_posli_datalogger_do_gsm=1;
                  fclose(file);
                  return 5;
                  pozice_odes_buff=0;
                }
              case DATALOGGER_SECOND:
              {
                unsigned int buf=counter/100;
                odesilaci_buffer[pozice_odes_buff]=buf+48;//offset pro cisla v ascii tabulce je
                flag_datalogger_ukon=DATALOGGER_THIRD;
                pozice_odes_buff++;
                if( pozice_odes_buff>=pocet_znaku_gsm)
                {
                  odesli_buffer(pozice_odes_buff);
                  zaloha_pozice_pole=pozice_pole;
                  flag_posli_datalogger_do_gsm=1;
                  fclose(file);
                  return 5;
                  pozice_odes_buff=0;
                }
              }
              case DATALOGGER_THIRD:
              {//napriklad 243
                unsigned int buf=counter/100;//dostanu stovky, napr 2
                unsigned int buf2=counter-(buf*100);//dostanu cislo za stovkama, napr 43 (243-200)
                buf=buf2/10;//dostanu desitku, napr 4 ( 43:10 = 4 )
                odesilaci_buffer[pozice_odes_buff]=buf+48;//offset pro cisla v ascii tabulce je
                flag_datalogger_ukon=DATALOGGER_FOUR;
                pozice_odes_buff++;
                if( pozice_odes_buff>=pocet_znaku_gsm)
                {
                  odesli_buffer(pozice_odes_buff);
                  zaloha_pozice_pole=pozice_pole;
                  flag_posli_datalogger_do_gsm=1;
                  fclose(file);
                  return 5;
                  pozice_odes_buff=0;
                }
              }
              case DATALOGGER_FOUR:
              {
                unsigned int buf=counter/100;//dostanu stovky, napr 2
                unsigned int buf2=counter-(buf*100);//dostanu cislo za stovkama, napr 43 (243-200)
                buf=buf2/10;//dostanu desitku, napr 4 ( 43:10 = 4 )
                buf2=buf2-(buf*10);//dostanu zbytek, napr (43- 4*10 = 3)
                odesilaci_buffer[pozice_odes_buff]=buf2+48;//offset pro cisla v ascii tabulce je
                flag_datalogger_ukon=DATALOGGER_FIVE;
                pozice_odes_buff++;
                if( pozice_odes_buff>=pocet_znaku_gsm)
                {
                  odesli_buffer(pozice_odes_buff);
                  zaloha_pozice_pole=pozice_pole;
                  flag_posli_datalogger_do_gsm=1;
                  fclose(file);
                  return 5;
                  pozice_odes_buff=0;
                }
              }
              case DATALOGGER_FIVE:
                odesilaci_buffer[pozice_odes_buff]=' ';
                flag_datalogger_ukon=DATALOGGER_DATE;
                pozice_odes_buff++;
                if( pozice_odes_buff>=pocet_znaku_gsm)
                {
                  flag_datalogger_ukon=DATALOGGER_DATE;
                  odesli_buffer(pozice_odes_buff);
                  zaloha_pozice_pole=pozice_pole;
                  flag_posli_datalogger_do_gsm=1;
                  fclose(file);
                  return 5;
                  pozice_odes_buff=0;
                }
            } // switch
          }
          else if( counter == 1000)
          {
            switch(flag_datalogger_ukon)
            {
              case DATALOGGER_NONE:
              case DATALOGGER_FIRST:
                odesilaci_buffer[pozice_odes_buff]='1';
                flag_datalogger_ukon=DATALOGGER_SECOND;
                pozice_odes_buff++;
                if( pozice_odes_buff>=pocet_znaku_gsm)
                {
                  odesli_buffer(pozice_odes_buff);
                  zaloha_pozice_pole=pozice_pole;
                  flag_posli_datalogger_do_gsm=1;
                  fclose(file);
                  return 5;
                  pozice_odes_buff=0;
                }
              case DATALOGGER_SECOND:
                odesilaci_buffer[pozice_odes_buff]='0';
                flag_datalogger_ukon=DATALOGGER_THIRD;
                pozice_odes_buff++;
                if( pozice_odes_buff>=pocet_znaku_gsm)
                {
                  odesli_buffer(pozice_odes_buff);
                  zaloha_pozice_pole=pozice_pole;
                  flag_posli_datalogger_do_gsm=1;
                  fclose(file);
                  return 5;
                  pozice_odes_buff=0;
                }
              case DATALOGGER_THIRD:
                odesilaci_buffer[pozice_odes_buff]='0';
                flag_datalogger_ukon=DATALOGGER_FOUR;
                pozice_odes_buff++;
                if( pozice_odes_buff>=pocet_znaku_gsm)
                {
                  odesli_buffer(pozice_odes_buff);
                  zaloha_pozice_pole=pozice_pole;
                  flag_posli_datalogger_do_gsm=1;
                  fclose(file);
                  return 5;
                  pozice_odes_buff=0;
                }
              case DATALOGGER_FOUR:
                odesilaci_buffer[pozice_odes_buff]='0';
                flag_datalogger_ukon=DATALOGGER_FIVE;
                pozice_odes_buff++;
                if( pozice_odes_buff>=pocet_znaku_gsm)
                {
                  odesli_buffer(pozice_odes_buff);
                  zaloha_pozice_pole=pozice_pole;
                  flag_posli_datalogger_do_gsm=1;
                  fclose(file);
                  return 5;
                  pozice_odes_buff=0;
                }
              case DATALOGGER_FIVE:
                odesilaci_buffer[pozice_odes_buff]=' ';
                flag_datalogger_ukon=DATALOGGER_DATE;
                pozice_odes_buff++;
                if( pozice_odes_buff>=pocet_znaku_gsm)
                {
                  flag_datalogger_ukon=DATALOGGER_DATE;
                  odesli_buffer(pozice_odes_buff);
                  zaloha_pozice_pole=pozice_pole;
                  flag_posli_datalogger_do_gsm=1;
                  fclose(file);
                  return 5;
                  pozice_odes_buff=0;
                }
            }
          }
        case DATALOGGER_DATE:
          ///datum
          flag_datalogger_ukon=DATALOGGER_DATE;
          pozice_pole=zaloha_pozice_pole;
          for( ; pozice_pole<10;pozice_pole++)
          {
            if( (pozice_pole == 4) || (pozice_pole == 7) )
              ;
            else
            {
              odesilaci_buffer[pozice_odes_buff]=pole[pozice_pole];
              pozice_odes_buff++;
              if( pozice_odes_buff >= pocet_znaku_gsm)
              {
                odesli_buffer(pozice_odes_buff);
                zaloha_pozice_pole=pozice_pole+1;
                flag_posli_datalogger_do_gsm=1;
                fclose(file);
                return 5;
                pozice_odes_buff=0;
              }
            }
          }
          zaloha_pozice_pole=pozice_pole;
        case DATALOGGER_SPACE_1:
          pozice_pole++;
          zaloha_pozice_pole=pozice_pole;
          //mezere
          flag_datalogger_ukon=DATALOGGER_TIME;
          odesilaci_buffer[pozice_odes_buff]=' ';
          pozice_odes_buff++;
          if( pozice_odes_buff >= pocet_znaku_gsm)
          {
            odesli_buffer(pozice_odes_buff);
            zaloha_pozice_pole=pozice_pole;
            flag_posli_datalogger_do_gsm=1;
            fclose(file);
            return 5;
            pozice_odes_buff=0;
          }
        case DATALOGGER_TIME:
          ///cas
          flag_datalogger_ukon=DATALOGGER_TIME;
          pozice_pole=zaloha_pozice_pole;
          for(;pozice_pole<16;pozice_pole++)
          {
            if(pozice_pole == 13)
              ;
            else
            {
              odesilaci_buffer[pozice_odes_buff]=pole[pozice_pole];
              pozice_odes_buff++;
              if(pozice_odes_buff>=pocet_znaku_gsm)
              {
                odesli_buffer(pozice_odes_buff);
                zaloha_pozice_pole=pozice_pole+1;   //plus jedna, protoze neprobehne pricitani pri foru
                flag_posli_datalogger_do_gsm=1;
                fclose(file);
                return 5;
                pozice_odes_buff=0;
              }
            }
            zaloha_pozice_pole=pozice_pole+1;
          }
        case DATALOGGER_SPACE_2:
          //mezera
          pozice_pole++;
          zaloha_pozice_pole=pozice_pole;
          flag_datalogger_ukon=DATALOGGER_PLUS;
          odesilaci_buffer[pozice_odes_buff]=' ';
          pozice_odes_buff++;
          if( pozice_odes_buff >= pocet_znaku_gsm)
          {
            odesli_buffer(pozice_odes_buff);
            zaloha_pozice_pole=pozice_pole;
            flag_posli_datalogger_do_gsm=1;
            fclose(file);
            return 5;
            pozice_odes_buff=0;
          }
        case DATALOGGER_PLUS:
          ///data
          flag_datalogger_ukon=DATALOGGER_PLUS;
          //while( (pole[pozice_pole] != ';') && (pole[pozice_pole] != ',')  )
          while(pole[pozice_pole] != pole[16]) // na pozici pole[16] je ulozeny oddelovac ; nebo ,
          {
            odesilaci_buffer[pozice_odes_buff]=pole[pozice_pole];
            if (odesilaci_buffer[pozice_odes_buff] == ',') // pokud je desetinny oddelovac carka tak ho zmenim na tecku
              odesilaci_buffer[pozice_odes_buff] = '.';
            pozice_pole++;
            if ( pozice_pole > VELIKOST_ZASOBNIKU )
              return -2;
            pozice_odes_buff++;
            if( pozice_odes_buff >= pocet_znaku_gsm)
            {
              odesli_buffer(pozice_odes_buff);
              zaloha_pozice_pole=pozice_pole;
              flag_posli_datalogger_do_gsm=1;
              fclose(file);
              return 5;
              pozice_odes_buff=0;
            }
          }
        case DATALOGGER_SPACE_3:
          pozice_pole++;
          flag_datalogger_ukon=DATALOGGER_NEG;
          //mezera
          odesilaci_buffer[pozice_odes_buff]=' ';
          pozice_odes_buff++;
          if( pozice_odes_buff >= pocet_znaku_gsm)
          {
            odesli_buffer(pozice_odes_buff);
            zaloha_pozice_pole=pozice_pole;
            flag_posli_datalogger_do_gsm=1;
            fclose(file);
            return 5;
            pozice_odes_buff=0;
          }
        case DATALOGGER_NEG:
          ///data
          flag_datalogger_ukon=DATALOGGER_NEG;
          //while( (pole[pozice_pole] != ';') && (pole[pozice_pole] != ',')  )
          while(pole[pozice_pole] != pole[16]) // na pozici pole[16] je ulozeny oddelovac ; nebo ,
          {
            odesilaci_buffer[pozice_odes_buff]=pole[pozice_pole];
            if (odesilaci_buffer[pozice_odes_buff] == ',') // pokud je desetinny oddelovac carka tak ho zmenim na tecku
              odesilaci_buffer[pozice_odes_buff] = '.';
            pozice_pole++;
            if ( pozice_pole > VELIKOST_ZASOBNIKU )
              return -2;
            pozice_odes_buff++;
            if( pozice_odes_buff >= pocet_znaku_gsm)
            {
              odesli_buffer(pozice_odes_buff);
              zaloha_pozice_pole=pozice_pole;
              flag_posli_datalogger_do_gsm=1;
              fclose(file);
              return 5;
              pozice_odes_buff=0;
            }
          }
        case DATALOGGER_SPACE_4:
          pozice_pole++;
          flag_datalogger_ukon=DATALOGGER_MIN;
          //mezera
          odesilaci_buffer[pozice_odes_buff]=' ';
          pozice_odes_buff++;
          if( pozice_odes_buff >= pocet_znaku_gsm)
          {
            odesli_buffer(pozice_odes_buff);
            zaloha_pozice_pole=pozice_pole;
            flag_posli_datalogger_do_gsm=1;
            fclose(file);
            return 5;
            pozice_odes_buff=0;
          }

        case DATALOGGER_MIN:
          ///data
          flag_datalogger_ukon=DATALOGGER_MIN;
          //while( (pole[pozice_pole] != ';') && (pole[pozice_pole] != ',')  )
          while(pole[pozice_pole] != pole[16]) // na pozici pole[16] je ulozeny oddelovac ; nebo ,
          {
            odesilaci_buffer[pozice_odes_buff]=pole[pozice_pole];
            pozice_pole++;
            if ( pozice_pole > VELIKOST_ZASOBNIKU )
              return -2;
            pozice_odes_buff++;
            if( pozice_odes_buff >= pocet_znaku_gsm)
            {
              odesli_buffer(pozice_odes_buff);
              zaloha_pozice_pole=pozice_pole;
              flag_posli_datalogger_do_gsm=1;
              fclose(file);
              return 5;
              pozice_odes_buff=0;
            }
          }
        case DATALOGGER_SPACE_5:
          pozice_pole++;
          flag_datalogger_ukon=DATALOGGER_ERROR;
          //mezera
          odesilaci_buffer[pozice_odes_buff]=' ';
          pozice_odes_buff++;
          if( pozice_odes_buff >= pocet_znaku_gsm)
          {
            odesli_buffer(pozice_odes_buff);
            zaloha_pozice_pole=pozice_pole;
            flag_posli_datalogger_do_gsm=1;
            fclose(file);
            return 5;
            pozice_odes_buff=0;
          }
        case DATALOGGER_ERROR:
          flag_datalogger_ukon=DATALOGGER_ERROR;
          while( pole[pozice_pole] != '\r')
          {
            odesilaci_buffer[pozice_odes_buff]=pole[pozice_pole];
            pozice_pole++;
            zaloha_pozice_pole=pozice_pole;
            if( pozice_pole > VELIKOST_ZASOBNIKU )
            {
              return -2;
            }
            pozice_odes_buff++;
            if( pozice_odes_buff >= pocet_znaku_gsm)
            {
              odesli_buffer(pozice_odes_buff);
              zaloha_pozice_pole=pozice_pole;
              flag_posli_datalogger_do_gsm=1;
              fclose(file);

              if( pole[pozice_pole] == '\r')
              {
                flag_datalogger_ukon=DATALOGGER_END;
              }
              return 5;
              pozice_odes_buff=0;
            }
          }
        case DATALOGGER_END:
        {
          unsigned char ukonci=0;
          flag_datalogger_ukon=DATALOGGER_NONE;
          pozice_pole+=2;
          //mezera
          odesilaci_buffer[pozice_odes_buff]=';';
          pozice_odes_buff++;
          if( pozice_odes_buff >= pocet_znaku_gsm)
          {
              odesli_buffer(pozice_odes_buff);
              zaloha_pozice_pole=pozice_pole;
              flag_posli_datalogger_do_gsm = 1;
              fclose(file);
              zaloha_datalogger_counter++;
              ukonci = 1;
          }

          ///vezmuti dalsiho pole
          if ( flag_last_read )
          {
            odesli_buffer(pozice_odes_buff);
            fclose(file);
            ukonceni_odesilani_datalogger_gsm();
            return 2;
          }
          zaloha_pozice_pole = 0;
          sum_position_pole+=pozice_pole;
          zaloha_sum_pozice_pole=sum_position_pole;   //ulozeni zalohy pro opetovne volani

          if(ukonci)
          {
            zaloha_pozice_pole = 0;
            return 5;
          }

          if ( (sum_position_pole + VELIKOST_ZASOBNIKU) > file_velikost )
          {
            fseek(file,sum_position_pole,SEEK_SET);
            fgets(pole,file_velikost-sum_position_pole,file);
            flag_last_read = 1;
            pozice_pole=0;
          }
          else
          {
            fseek(file,sum_position_pole,SEEK_SET);
            fgets(pole,VELIKOST_ZASOBNIKU,file);
            pozice_pole=0;
          }
          break;
        }
        default:
        {
          gsm_busy_pin(0);
          ukonceni_odesilani_datalogger_gsm();
        }
      }
    }
  }
  fclose(file);
  ///posledni odeslani,vynuluji se promenne pro pripadny dalsi datalogger
  get_datalogger_gsm(pozice_odes_buff);
  ukonceni_odesilani_datalogger_gsm();
  return 1;
}

void odesli_buffer(unsigned char velikost)
{
  get_datalogger_gsm(velikost);
}

void ukonceni_odesilani_datalogger_gsm(void)
{
  zaloha_pocet_odeslani=1;
  zaloha_datalogger_counter=1;
  zaloha_sum_pozice_pole=0;
  zaloha_pozice_pole=0;
  flag_posli_datalogger_do_gsm=0;
  flag_data_correct=0;
  flag_datalogger_ukon=DATALOGGER_NONE;

  delay_100ms();
  delay_100ms();

  gsm_busy_pin(0); //nuluj pin pro odeslani
}

// vlozi dalsi 4 bytovou hodnotu do buferu
static void InsertNext4bIntoBuffer(char** ppBuffer, const long* pValue)
{
    char *pBuf = *ppBuffer;
    long value = *pValue;

    *pBuf++ = (char)value;
    *pBuf++ = (char)(value >> 8);
    *pBuf++ = (char)(value >> 16);
    *pBuf++ = (char)(value >> 24);

    *ppBuffer = pBuf;
}

void write_data_to_flash(void)
{
    /// nove veci v menu vzdy pridavat na konec
    char buffer[FLASH_BUF_SIZE];
    char *pBuf;
    long value;

    erase_user_flash();

    pBuf = buffer;

    InsertNext4bIntoBuffer(&pBuf, (long*)UNIT_NO); /*01*/
    value = g_measurement.total/1000000;
    InsertNext4bIntoBuffer(&pBuf, &value); /*02*/
    value = (g_measurement.total%1000000)/1000;
    InsertNext4bIntoBuffer(&pBuf, &value); /*03*/
    value = g_measurement.flow_pos/1000000;
    InsertNext4bIntoBuffer(&pBuf, &value); /*04*/
    value = (g_measurement.flow_pos%1000000)/1000;
    InsertNext4bIntoBuffer(&pBuf, &value); /*05*/
    value = g_measurement.flow_neg/1000000;
    InsertNext4bIntoBuffer(&pBuf, &value); /*06*/
    value = (g_measurement.flow_neg%1000000)/1000;
    InsertNext4bIntoBuffer(&pBuf, &value); /*07*/
    value = g_measurement.aux/1000000;
    InsertNext4bIntoBuffer(&pBuf, &value); /*08*/
    value = (g_measurement.aux%1000000)/1000;
    InsertNext4bIntoBuffer(&pBuf, &value); /*09*/
    InsertNext4bIntoBuffer(&pBuf, (long*)POINT_UP); /*10*/
    InsertNext4bIntoBuffer(&pBuf, (long*)ZERO_FLOW_CONSTANT); /*11*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_PHONE_1_H); /*12*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_PHONE_1_L); /*13*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_PHONE_2_H); /*14*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_PHONE_2_L); /*15*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_PHONE_3_H); /*16*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_PHONE_3_L); /*17*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_GATEWAY0); /*18*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_GATEWAY1); /*19*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_GATEWAY2); /*20*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_GATEWAY3); /*21*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_GATEWAY4); /*22*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_GATEWAY5); /*23*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_GATEWAY6); /*24*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_GATEWAY7); /*25*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_GATEWAY8); /*26*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_GATEWAY9); /*27*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_GATEWAY10); /*28*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_GATEWAY11); /*29*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_GATEWAY12); /*30*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_GATEWAY13); /*31*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_GATEWAY14); /*32*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_GATEWAY15); /*33*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_USER0); /*34*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_USER1); /*35*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_USER2); /*36*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_PASSWORD0); /*37*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_PASSWORD1); /*38*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_PASSWORD2); /*39*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_PORT); /*40*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_PIN); /*41*/
    InsertNext4bIntoBuffer(&pBuf, (long*)ACTUAL_TOTALIZER); /*42*/
    InsertNext4bIntoBuffer(&pBuf, (long*)HEAT_TOTAL_FLOAT); /*43*/
    InsertNext4bIntoBuffer(&pBuf, (long*)FLOW_SENSOR_UNIT_NO); /*44*/
    InsertNext4bIntoBuffer(&pBuf, (long*)ERROR_MIN); /*45*/
    InsertNext4bIntoBuffer(&pBuf, (long*)OK_MIN); /*46*/
    InsertNext4bIntoBuffer(&pBuf, (long*)DIAMETER); /*47*/
    InsertNext4bIntoBuffer(&pBuf, (long*)FLOW_RANGE); /*48*/
    InsertNext4bIntoBuffer(&pBuf, (long*)TEMP_SENSOR1_UNIT_NO); /*49*/
    InsertNext4bIntoBuffer(&pBuf, (long*)TEMP_SENSOR2_UNIT_NO); /*50*/
    InsertNext4bIntoBuffer(&pBuf, (long*)UNIT_FLOW); /*51*/
    InsertNext4bIntoBuffer(&pBuf, (long*)UNIT_VOLUME); /*52*/
    InsertNext4bIntoBuffer(&pBuf, (long*)UNIT_TEMPERATURE); /*53*/
    InsertNext4bIntoBuffer(&pBuf, (long*)UNIT_HEAT); /*54*/
    InsertNext4bIntoBuffer(&pBuf, (long*)ACT_LANGUAGE); /*55*/
    InsertNext4bIntoBuffer(&pBuf, (long*)CONTRAST); /*56*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BACKLIGHT); /*57*/
    InsertNext4bIntoBuffer(&pBuf, (long*)STATUS_LED_MODE); /*58*/
    InsertNext4bIntoBuffer(&pBuf, (long*)PASSWORD_USER); /*59*/
    InsertNext4bIntoBuffer(&pBuf, (long*)DATALOGGER_INTERVAL); /*60*/
    InsertNext4bIntoBuffer(&pBuf, (long*)CSV_FORMAT); /*61*/
    InsertNext4bIntoBuffer(&pBuf, (long*)AIR_DETECTOR); /*62*/
    InsertNext4bIntoBuffer(&pBuf, (long*)AVERAGE_SAMPLES); /*63*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GRAPH_QUANTITY); /*64*/

    unsigned char err = 0;

    if (write_flash((unsigned *)USER_FLASH_START, buffer, (unsigned int)sizeof(buffer)))
        err = 1;

    pBuf = buffer;

    InsertNext4bIntoBuffer(&pBuf, (long*)HEAT_POWER_RANGE); /*01*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP_MEAS_STATE); /*02*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP_SENSOR_TYPE); /*03*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP_SENSOR_CONN); /*04*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_CAL_TEMPERATURE1); /*05*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_CAL_TEMPERATURE2); /*06*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP1_REF_RESISTANCE_0); /*07*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP1_REF_RESISTANCE_1); /*08*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP1_REF_RESISTANCE_2); /*09*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP1_REF_RESISTANCE_3); /*10*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP1_REF_RESISTANCE_4); /*11*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP1_REF_RESISTANCE_5); /*12*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP1_REF_RESISTANCE_6); /*13*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP1_REF_RESISTANCE_7); /*14*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP1_REF_RESISTANCE_8); /*15*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP1_REF_RESISTANCE_9); /*16*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP1_REF_RESISTANCE_10); /*17*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP1_REF_RESISTANCE_11); /*18*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP2_REF_RESISTANCE_0); /*19*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP2_REF_RESISTANCE_1); /*20*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP2_REF_RESISTANCE_2); /*21*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP2_REF_RESISTANCE_3); /*22*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP2_REF_RESISTANCE_4); /*23*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP2_REF_RESISTANCE_5); /*24*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP2_REF_RESISTANCE_6); /*25*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP2_REF_RESISTANCE_7); /*26*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP2_REF_RESISTANCE_8); /*27*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP2_REF_RESISTANCE_9); /*28*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP2_REF_RESISTANCE_10); /*29*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXT_TEMP2_REF_RESISTANCE_11); /*30*/
    InsertNext4bIntoBuffer(&pBuf, (long*)CURRENT_SET); /*31*/
    InsertNext4bIntoBuffer(&pBuf, (long*)I_DIRECT_DRIVING_QUANTITY); /*32*/
    InsertNext4bIntoBuffer(&pBuf, (long*)I_HEAT_POWER_MIN); /*33*/
    InsertNext4bIntoBuffer(&pBuf, (long*)I_HEAT_POWER_MAX); /*34*/
    InsertNext4bIntoBuffer(&pBuf, (long*)HEAT_POWER_CURRENT_MIN); /*35*/
    InsertNext4bIntoBuffer(&pBuf, (long*)HEAT_POWER_CURRENT_MAX); /*36*/
    InsertNext4bIntoBuffer(&pBuf, (long*)I_FLOW_MIN); /*37*/
    InsertNext4bIntoBuffer(&pBuf, (long*)I_FLOW_MAX); /*38*/
    InsertNext4bIntoBuffer(&pBuf, (long*)CURRENT_MIN); /*39*/
    InsertNext4bIntoBuffer(&pBuf, (long*)CURRENT_MAX); /*40*/
    InsertNext4bIntoBuffer(&pBuf, (long*)I_CAL_POINT_1); /*41*/
    InsertNext4bIntoBuffer(&pBuf, (long*)I_CAL_POINT_2); /*42*/
    InsertNext4bIntoBuffer(&pBuf, (long*)I_CAL_CONST_1); /*43*/
    InsertNext4bIntoBuffer(&pBuf, (long*)I_CAL_CONST_2); /*44*/
    InsertNext4bIntoBuffer(&pBuf, (long*)RE1_SET); /*45*/
    InsertNext4bIntoBuffer(&pBuf, (long*)RE2_SET); /*46*/
    InsertNext4bIntoBuffer(&pBuf, (long*)RE_FLOW_1); /*47*/
    InsertNext4bIntoBuffer(&pBuf, (long*)RE_FLOW_2); /*48*/
    InsertNext4bIntoBuffer(&pBuf, (long*)RE_HYSTERESIS_1); /*49*/
    InsertNext4bIntoBuffer(&pBuf, (long*)RE_HYSTERESIS_2); /*50*/
    InsertNext4bIntoBuffer(&pBuf, (long*)RE3_SET); /*51*/
    InsertNext4bIntoBuffer(&pBuf, (long*)RE4_SET); /*52*/
    InsertNext4bIntoBuffer(&pBuf, (long*)RE_VOLUME_PLUS); /*53*/
    InsertNext4bIntoBuffer(&pBuf, (long*)RE_VOLUME_MINUS); /*54*/
    InsertNext4bIntoBuffer(&pBuf, (long*)RE_DOSE); /*55*/
    InsertNext4bIntoBuffer(&pBuf, (long*)F_SET); /*56*/
    InsertNext4bIntoBuffer(&pBuf, (long*)F_FLOW1); /*57*/
    InsertNext4bIntoBuffer(&pBuf, (long*)F_FLOW2); /*58*/
    InsertNext4bIntoBuffer(&pBuf, (long*)F_FREQ1); /*59*/
    InsertNext4bIntoBuffer(&pBuf, (long*)F_FREQ2); /*60*/
    InsertNext4bIntoBuffer(&pBuf, (long*)F_DUTY_CYCLE); /*61*/
    InsertNext4bIntoBuffer(&pBuf, (long*)MODBUS_SLAVE_ADDRESS); /*62*/
    InsertNext4bIntoBuffer(&pBuf, (long*)MODBUS_BAUDRATE); /*63*/
    InsertNext4bIntoBuffer(&pBuf, (long*)MODBUS_PARITY); /*64*/

    if (write_flash((unsigned *)(USER_FLASH_START + 1*FLASH_BUF_SIZE), buffer, (unsigned int)sizeof(buffer)))
        err = 2;

    pBuf = buffer;

    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE1_SET); /*01*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE1_HEAT_POWER_1); /*02*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE1_HEAT_POWER_2); /*03*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE1_HEAT_POWER_HYSTERESIS_1); /*04*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE1_HEAT_POWER_HYSTERESIS_2); /*05*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE2_SET); /*06*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE2_HEAT_POWER_1); /*07*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE2_HEAT_POWER_2); /*08*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE2_HEAT_POWER_HYSTERESIS_1); /*09*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE2_HEAT_POWER_HYSTERESIS_2); /*10*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE3_SET); /*11*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE3_HEAT_POWER_1); /*12*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE3_HEAT_POWER_2); /*13*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE3_HEAT_POWER_HYSTERESIS_1); /*14*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE3_HEAT_POWER_HYSTERESIS_2); /*15*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE3_F_HEAT_POWER1); /*16*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE3_F_HEAT_POWER2); /*17*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE3_F_HEAT_POWER_FREQ1); /*18*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE3_F_HEAT_POWER_FREQ2); /*19*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE4_SET); /*20*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE4_HEAT_POWER_1); /*21*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE4_HEAT_POWER_2); /*22*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE4_HEAT_POWER_HYSTERESIS_1); /*23*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE4_HEAT_POWER_HYSTERESIS_2); /*24*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE4_F_HEAT_POWER1); /*25*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE4_F_HEAT_POWER2); /*26*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE4_F_HEAT_POWER_FREQ1); /*27*/
    InsertNext4bIntoBuffer(&pBuf, (long*)BIN_RE4_F_HEAT_POWER_FREQ2); /*28*/
    InsertNext4bIntoBuffer(&pBuf, (long*)MBUS_SLAVE_ADDRESS); /*29*/
    InsertNext4bIntoBuffer(&pBuf, (long*)MBUS_BAUDRATE); /*30*/
    InsertNext4bIntoBuffer(&pBuf, (long*)MBUS_PARITY); /*31*/
    InsertNext4bIntoBuffer(&pBuf, (long*)TOTALIZER_CYCLING); /*32*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_DATA_INTERVAL); /*33*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_EMPTY_PIPE); /*34*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_ZERO_FLOW); /*35*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_ERROR_DETECT); /*36*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_EMPTY_PIPE_SEND); /*37*/
    InsertNext4bIntoBuffer(&pBuf, (long*)GSM_ZERO_FLOW_SEND); /*38*/
    InsertNext4bIntoBuffer(&pBuf, (long*)DEMO); /*39*/
    InsertNext4bIntoBuffer(&pBuf, (long*)SIMULATED_FLOW); /*40*/
    InsertNext4bIntoBuffer(&pBuf, (long*)MEASUREMENT_STATE); /*41*/
    InsertNext4bIntoBuffer(&pBuf, (long*)AIR_CONSTANT); /*42*/
    InsertNext4bIntoBuffer(&pBuf, (long*)START_DELAY); /*43*/
    InsertNext4bIntoBuffer(&pBuf, (long*)INVERT_FLOW); /*44*/
    InsertNext4bIntoBuffer(&pBuf, (long*)CLEAN_TIME); /*45*/
    InsertNext4bIntoBuffer(&pBuf, (long*)LOW_FLOW_CUTOFF); /*46*/
    InsertNext4bIntoBuffer(&pBuf, (long*)SD_CARD_BUFFER); /*47*/
    InsertNext4bIntoBuffer(&pBuf, (long*)MBUS_MODULE); /*48*/
    InsertNext4bIntoBuffer(&pBuf, (long*)EXCITATION_FREQUENCY); /*49*/
    InsertNext4bIntoBuffer(&pBuf, (long*)OPT_DU_BTN_LEVEL); /*50*/
    InsertNext4bIntoBuffer(&pBuf, (long*)OPT_LR_BTN_LEVEL); /*51*/
    InsertNext4bIntoBuffer(&pBuf, (long*)OPT_EE_BTN_LEVEL); /*52*/
    InsertNext4bIntoBuffer(&pBuf, (long*)FLOW_SENSOR_TYPE); /*53*/
    InsertNext4bIntoBuffer(&pBuf, (long*)PULSES_PER_LITRE); /*54*/
    InsertNext4bIntoBuffer(&pBuf, (long*)FLOW_PULSE_TIMEOUT); /*55*/
    *pBuf++ = (DateTime.month/10)<<4 | DateTime.month%10;  // mesic /*56*/
    *pBuf++ = (DateTime.day/10)<<4 | DateTime.day%10;
    *pBuf++ = (DateTime.year/1000)<<4  | (DateTime.year%1000/100); // rok
    *pBuf++ = (DateTime.year%100/10)<<4 | (DateTime.year%10);      // rok
    *pBuf++ = (DateTime.minute/10)<<4 | DateTime.minute%10;  // minuta /*57*/
    *pBuf++ = (DateTime.second/10)<<4 | DateTime.second%10;  // sekunda
    *pBuf++ = (DateTime.hour/10)<<4 | DateTime.hour%10;      // hodina
    *pBuf++ = 0;

    // vyplneni zbytku mista hodnotou 0xff
    value = 0xff;
    for (unsigned int i = 58; i <= 64; i++)
    {
        InsertNext4bIntoBuffer(&pBuf, &value);
    }

    if (write_flash((unsigned *)(USER_FLASH_START + 2*FLASH_BUF_SIZE), buffer, (unsigned int)sizeof(buffer)))
        err = 3;

    if (err) // pokud nastala chyba pri zapisu do flash
        actual_error |= ERR_WRITE_FLASH;
    else
        actual_error &= ~ERR_WRITE_FLASH;

    if (flag_reset_after_load_default)
    {
        PINSEL1 = 0x00000000;
        FIO2CLR = (1<<9);
        FIO0CLR = (1<<29);
        FIO0CLR = (1<<30);
        flag_reset_after_load_default = 0;
        delay_100ms();
        asm("mov pc,#0"); // restart
    }
}

unsigned char check_gsm_modul()
{
  FIO2DIR &= ~(1<<2) & ~(1<<3);
  PINMODE4=0xF0;  //2.2 a 2.3 pull down

  delay_100ms();

  if( ( FIO2PIN & (1<<2) ) && ( FIO2PIN & (1<<3) ) )
    return 1;
  else
  {
    flag_komunikacni_modul=0;
    return 0;
  }
}

void gsm_busy_pin(unsigned char state)
{
  FIO2DIR |= (1<<3);
  if(state) //hodit do jednicky
  {
    FIO2SET |= (1<<3);
  }
  else
  {
    FIO2CLR |= (1<<3);
  }
}

unsigned char gsm_odesila(void)
{

  if( FIO2PIN & (1<<2) )
    return 1;
  else
    return 0;
}

void gsm_odesli_event(unsigned long udalost)
{
  gsm_busy_pin(1);
  char *ukazatel;
  buffer_gsm_event[0]=GSM_SEND_EVENT;
  buffer_gsm_event[1]=125;//to je jen tak, jinak by se pri prvni inicializaci ukazalo jako jednoprvkove pole (strlen)
  ukazatel=&buffer_gsm_event[2];
  //kolik cisel
  switch(aktualni_cislo_pro_odeslani)
  {
    case 1:
      if( RAM_Read_Data_Long(GSM_PHONE_1_L) ) //jestlize je vyplni se cislo a ukonci se switch
      {
        if( RAM_Read_Data_Long(GSM_PHONE_1_H))
          sprintf(ukazatel,"\"+%ld%ld\"\r\n",RAM_Read_Data_Long(GSM_PHONE_1_H),RAM_Read_Data_Long(GSM_PHONE_1_L));
        else
          sprintf(ukazatel,"\"+%ld\"\r\n",RAM_Read_Data_Long(GSM_PHONE_1_L));

        ukazatel=&buffer_gsm_event[strlen(buffer_gsm_event)];
        flag_chci_odeslat_event=1;
        break;
      }
      else //jinak se zvetsi poradove cislo telefonniho cisla a prejde se na druhy switch
      {
        aktualni_cislo_pro_odeslani++;
      }
    case 2:
      if( RAM_Read_Data_Long(GSM_PHONE_2_L) ) //jestlize je vyplni se cislo a ukonci se switch
      {
        if( RAM_Read_Data_Long(GSM_PHONE_2_H))
          sprintf(ukazatel,"\"+%ld%ld\"\r\n",RAM_Read_Data_Long(GSM_PHONE_2_H),RAM_Read_Data_Long(GSM_PHONE_2_L));
        else
          sprintf(ukazatel,"\"+%ld\"\r\n",RAM_Read_Data_Long(GSM_PHONE_2_L));
        ukazatel=&buffer_gsm_event[strlen(buffer_gsm_event)];
        flag_chci_odeslat_event=1;
        break;
      }
      else //jinak se zvetsi poradove cislo telefonniho cisla a prejde se na treti switch
      {
        aktualni_cislo_pro_odeslani++;
      }
    case 3:
      if( RAM_Read_Data_Long(GSM_PHONE_3_L) ) //jestlize je vyplni se cislo a ukonci se switch
      {
        if( RAM_Read_Data_Long(GSM_PHONE_3_H))
          sprintf(ukazatel,"\"+%ld%ld\"\r\n",RAM_Read_Data_Long(GSM_PHONE_3_H),RAM_Read_Data_Long(GSM_PHONE_3_L));
        else
          sprintf(ukazatel,"\"+%ld\"\r\n",RAM_Read_Data_Long(GSM_PHONE_3_L));
        ukazatel=&buffer_gsm_event[strlen(buffer_gsm_event)];
        flag_chci_odeslat_event=1;
        break;
      }
      else //jinak se zvetsi poradove cislo telefonniho cisla a prejde se na druhy switch
      {
        aktualni_cislo_pro_odeslani=1;
        flag_chci_odeslat_event=0;
        delay_100ms();
        gsm_busy_pin(0);
        return;
      }
    default:
      aktualni_cislo_pro_odeslani=1;
      return;
      break;
  } // switch

  flag_aktualni_event=udalost;
  //co se ma udelat
  switch(udalost)
  {
    case GSM_EMPTY_ON:
      sprintf(ukazatel,"EVENT %ld EMPTY PIPE DETECTED",RAM_Read_Data_Long( UNIT_NO ));
      break;
    case GSM_EMPTY_OFF:
      sprintf(ukazatel,"EVENT %ld EMPTY PIPE NOT DETECTED",RAM_Read_Data_Long( UNIT_NO ));
      break;
    case GSM_ZERO_FLOW_ON:
      sprintf(ukazatel,"EVENT %ld ZERO FLOW DETECTED",RAM_Read_Data_Long( UNIT_NO ));
      break;
    case GSM_ZERO_FLOW_OFF:
      sprintf(ukazatel,"EVENT %ld ZERO FLOW NOT DETECTED",RAM_Read_Data_Long( UNIT_NO ));
      break;
    case GSM_ERROR_ON:
      if( last_sending_error_gsm )    //pokud je error
        sprintf(ukazatel,"EVENT %ld ERROR DETECTED: %08x",RAM_Read_Data_Long( UNIT_NO ),last_sending_error_gsm);
      else
        sprintf(ukazatel,"EVENT %ld NO ERROR",RAM_Read_Data_Long( UNIT_NO ));
      break;
    case GSM_ERROR_OFF:
      sprintf(ukazatel,"ERROR NOT DETECTED");
      break;
  }
  //dokonceni odeslani, odesle se buffer
  buffer_gsm_event[1]=strlen(buffer_gsm_event);
}

void gsm_odesli_interval()
{
  gsm_busy_pin(1);
  char *ukazatel;
  buffer_gsm_interval[0]=GSM_INTERVAL_DATA;
  buffer_gsm_interval[1]=125;//to je jen tak, jinak by se pri prvni inicializaci ukazalo jako jednoprvkove pole (strlen)
  ukazatel=&buffer_gsm_interval[2];
  //kolik cisel
  switch(aktualni_cislo_interval)
  {
    case 1:
      if( RAM_Read_Data_Long(GSM_PHONE_1_L) ) //jestlize je vyplni se cislo a ukonci se switch
      {
        if( RAM_Read_Data_Long(GSM_PHONE_1_H))
          sprintf(ukazatel,"\"+%ld%ld\"\r\n",RAM_Read_Data_Long(GSM_PHONE_1_H),RAM_Read_Data_Long(GSM_PHONE_1_L));
        else
          sprintf(ukazatel,"\"+%ld%ld\"\r\n",RAM_Read_Data_Long(GSM_PHONE_1_H),RAM_Read_Data_Long(GSM_PHONE_1_L));

        ukazatel=&buffer_gsm_interval[strlen(buffer_gsm_interval)];
        flag_chci_odeslat_interval=1;
        break;
      }
      else //jinak se zvetsi poradove cislo telefonniho cisla a prejde se na druhy switch
      {
        aktualni_cislo_interval++;
      }
    case 2:
      if( RAM_Read_Data_Long(GSM_PHONE_2_L) ) //jestlize je vyplni se cislo a ukonci se switch
      {
        if( RAM_Read_Data_Long(GSM_PHONE_2_H ))
          sprintf(ukazatel,"\"+%ld%ld\"\r\n",RAM_Read_Data_Long(GSM_PHONE_2_H),RAM_Read_Data_Long(GSM_PHONE_2_L));
        else
          sprintf(ukazatel,"\"+%ld\"\r\n",RAM_Read_Data_Long(GSM_PHONE_2_H),RAM_Read_Data_Long(GSM_PHONE_2_L));

        ukazatel=&buffer_gsm_interval[strlen(buffer_gsm_interval)];
        flag_chci_odeslat_interval=1;
        break;
      }
      else //jinak se zvetsi poradove cislo telefonniho cisla a prejde se na treti switch
      {
        aktualni_cislo_interval++;
      }
    case 3:
      if( RAM_Read_Data_Long(GSM_PHONE_3_L) ) //jestlize je vyplni se cislo a ukonci se switch
      {
        if( RAM_Read_Data_Long(GSM_PHONE_3_H))
          sprintf(ukazatel,"\"+%ld%ld\"\r\n",RAM_Read_Data_Long(GSM_PHONE_3_H),RAM_Read_Data_Long(GSM_PHONE_3_L));
        else
          sprintf(ukazatel,"\"+%ld\"\r\n",RAM_Read_Data_Long(GSM_PHONE_3_H),RAM_Read_Data_Long(GSM_PHONE_3_L));

        ukazatel =& buffer_gsm_interval[strlen(buffer_gsm_interval)];
        flag_chci_odeslat_interval = 1;
        break;
      }
      else //jinak se zvetsi poradove cislo telefonniho cisla a prejde se na druhy switch
      {
        aktualni_cislo_interval=1;
        flag_chci_odeslat_interval=0;
        delay_100ms();
        gsm_busy_pin(0);
        return;
      }
    default:
      aktualni_cislo_interval=1;
      return;
      break;
  } // switch
  sprintf(ukazatel,
          "UNITNO %08d %04d.%02d.%02d %02d:%02d FLOWRATE %03f M3/H TOTALPOS %03f M3 TOTALNEG %03f M3",
          RAM_Read_Data_Long( UNIT_NO),
          DateTime.year,
          DateTime.month,
          DateTime.day,
          DateTime.hour,
          DateTime.minute,
          g_measurement.actual,
          (float)(g_measurement.flow_pos/1000000.0),
          (float)(g_measurement.flow_neg/1000000.0));
  //dokonceni
  buffer_gsm_interval[1]=strlen(buffer_gsm_interval);
}

void pinConfig()
{
  IODIR1 |= (1<<1);
  IODIR1 |= (1<<0);

  IOCLR1 |= (1<<0);
  IOCLR1 |= (1<<1);

  // ADC power on
  PCONP |= (1<<12);
  AD0CR &= ~(1<<21);
  // select clock for ADC
  PCLKSEL0 |= (1 << 25);
  PCLKSEL0 |= (1 << 24);
  // AD0.0 AD0.2 and AD0.3
  PINSEL1 = (PINSEL1 & ~(0x3 << 14)) | (0x1 << 14);
  PINSEL1 = (PINSEL1 & ~(0x3 << 16)) | (0x1 << 16);
  PINSEL1 = (PINSEL1 & ~(0x3 << 18)) | (0x1 << 18);
  PINSEL1 = (PINSEL1 & ~(0x3 << 20)) | (0x1 << 20);

  //PINSEL1 |= 0x144000;
  AD0CR &= ~(1<<0);
  AD0CR &= ~(1<<1);
  AD0CR &= ~(1<<2);
  AD0CR &= ~(1<<3);
  AD0CR &= ~(1<<4);
  AD0CR &= ~(1<<5);
  AD0CR &= ~(1<<6);
  AD0CR &= ~(1<<7);

  AD0CR |= (1<<21);
}

void testWifi()
{
  flag_komunikacni_modul = MODUL_WIFI;
  FIO2DIR |= (1<<2) | (1<<3);
  FIO2SET |= (1<<3);

  // reset wifi modulu
  FIO2CLR |= (1<<2);
  delay_100ms();
  FIO2SET |= (1<<2);

  // nastaveni flagu, ktery v preruseni RX zpracuje prijate znaky
  flag_wifi_test = 1;

  int timeout = 0;

  while(flag_wifi_test == 1 && timeout < 5000 )
  {
    timeout++;
    delay_1ms();
  }
}

void EventLogTask(void)
{
    RTC_EventLogTask();
}

/*****************************************************************************
  Converts 4bits to hex char.
  ***************************************************************************/
BYTE Convert4BitsToHexChar(BYTE fourBits)
{
	if (fourBits < 10)
		return '0' + fourBits;
	else
		return 'A' + fourBits - 10;
}

/*****************************************************************************
  Converts byte array to 0 terminated hex string.
  ***************************************************************************/
void ConvertByteArrayToHexStr(const BYTE* byteArray, BYTE byteArrayLength, BYTE* hexString)
{
	BYTE i, b, hexChar, fourBits;

	for (i = 0; i < byteArrayLength; i++)
	{
		b = byteArray[i];

		fourBits = (BYTE)(b >> 4);
		hexChar = Convert4BitsToHexChar(fourBits);
		hexString[2 * i] = hexChar;

		fourBits = (BYTE)(b & 0x0F);
		hexChar = Convert4BitsToHexChar(fourBits);
		hexString[2 * i + 1] = hexChar;
	}

	hexString[2 * byteArrayLength] = 0;
}
