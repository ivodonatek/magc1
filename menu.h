#include "maine.h"

// rezimy
#define MEASUREMENT          0
#define MENU                 1
#define EDIT                 2
#define MESSAGE              3

// Typ zobrazovane hlasky
#define MSG_OK             (0)
#define MSG_ERROR          (1)
#define MSG_TEXT           (2)
#define MSG_CONF           (3)

// klavesy
#define ENTR                 0
#define ESC                  1
#define UP                   2
#define DOWN                 3
#define LEFT                 4
#define RIGHT                5
#define INIT                 8 // Fiktivni klavesa, inicializuje polozku aktualniho rezimu
#define SHOW                 9 // Fiktivni klavesa, zobrazuje na displeji polozku aktualniho rezimu

// 16ti bitove
#define TO_COUNT             0
#define TO_BACK              1
#define TO_FORWARD           2
#define TO_TEXT              3
#define TO_EDIT              4
#define ONE_ITEM_SIZE        3

unsigned long Exponent ( unsigned long e, unsigned long n );
unsigned long PointStrToLong ( char * str );
char * LongToPointStr ( unsigned long number, unsigned char digits,unsigned char decimal, char *str );
void ShowTextMessage(void);
void ShowOkMessage(void);
void ShowErrorMessage(void);
void ShowConfirmMessage(void);
void ShowStrMessage(char *str);
void InitAndShowTextMessage(const unsigned int value/*, char *g_s1*/);
void InitAndShowErrorMessage(unsigned char value);
void InitAndShowConfirmMessage(const char *str, TSimpleHandler *pYesHandler);
unsigned char ActValue ( void );
void ReadValues ( void );
void WriteValues ( void );
//void RestoreConstantFromDefault(unsigned long value);
void Save_Settings (void);
void EditDoFunction( unsigned char action, unsigned char fun[]);
unsigned char KursorPositionInValue ( void );
void EditInit(void);
void ShowEdit(void);
void ShowMenu( void );
void control(char key);
unsigned long stisk(void);
unsigned long FloatToUlong(float value);

int GetPeditTableRecordIndex(unsigned long valueOffset);
unsigned int GetPunitTableStringIndex(int peditTableRecIdx, unsigned long valueOffset);
long GetPtextyTableStringIndexFromPmenu(int peditTableRecIdx, unsigned long valueOffset);
//unsigned long GetPtextyTableStringIndex(unsigned long startTextIdx, unsigned int textIdx);
unsigned char GetMenuValueString(int peditTableRecIdx, unsigned long valueOffset, unsigned long value, char *valueString);

