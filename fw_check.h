//******************************************************************************
//  Kontrola firmware
//*****************************************************************************/

#ifndef FW_CHECK_H_INCLUDED
#define FW_CHECK_H_INCLUDED

#include <stdint.h>

#ifndef BOOL_TYPEDEF
#define BOOL_TYPEDEF
typedef unsigned char BOOL;
#endif

#ifndef TRUE
#define TRUE            1
#endif

#ifndef FALSE
#define FALSE           0
#endif

// Typ pocitani checksumu firmware
typedef enum
{
    FWCHECK_Complete, // cely rozsah, vypocetne narocne
    FWCHECK_Step,     // po krocich, volat co nejcasteji

} TFwCheckType;

//------------------------------------------------------------------------------
//  Uloha pocitani checksumu firmware
//------------------------------------------------------------------------------
void FwCheck_Task(TFwCheckType fwCheckType);

//------------------------------------------------------------------------------
//  Vrati checksum firmware
//------------------------------------------------------------------------------
unsigned short FwCheck_GetChecksum(void);

#endif // FW_CHECK_H_INCLUDED
