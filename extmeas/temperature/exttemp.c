//******************************************************************************
//  This file contains functions that allow to access external measurements
//  of temperature.
//*****************************************************************************/

#include <stdint.h>
#include <math.h>
#include "exttemp.h"
#include "../../maine.h"
#include "../../menu_tables.h"
#include "../../conversion.h"
#include "../../devices/LMP90100/TI_LMP90100.h"
#include "../../devices/LMP90100/TI_LMP90100_spi.h"
#include "TI_LMP90100_register_settings.h"

// The measurement queue length for calculating the mean value.
#define MEASUREMENT_QUEUE_LENGTH    100

// The measurement filter length.
#define MEASUREMENT_FILTER_LENGTH   3

// International Temperature Standard 90 (ITS-90) constants
// for Platinum Resistance Thermometer (resistance to temperature equations).
#define PRT_A_CONST     (3.9083E-3)
#define PRT_B_CONST     (-5.775E-7)
#define PRT_C_CONST     (-4.183E-12)

// Board LMP90100 GPIO pins
#define GPIO_IB1_DRIVER_PIN     0
#define GPIO_IB2_DRIVER_PIN     1
#define GPIO_CH1_PT100_PIN      2
#define GPIO_CH1_PT500_PIN      3
#define GPIO_CH2_PT100_PIN      4
#define GPIO_CH2_PT500_PIN      5

// Board LMP90100 signals
#define IB1_23W_SIGNAL          (0 << GPIO_IB1_DRIVER_PIN)
#define IB1_4W_SIGNAL           (1 << GPIO_IB1_DRIVER_PIN)
#define IB2_23W_SIGNAL          (0 << GPIO_IB2_DRIVER_PIN)
#define IB2_4W_SIGNAL           (1 << GPIO_IB2_DRIVER_PIN)
#define CH1_PT100_SIGNAL        ((1 << GPIO_CH1_PT500_PIN) | (1 << GPIO_CH1_PT100_PIN))
#define CH1_PT500_SIGNAL        ((1 << GPIO_CH1_PT500_PIN) | (0 << GPIO_CH1_PT100_PIN))
#define CH1_PT1000_SIGNAL       ((0 << GPIO_CH1_PT500_PIN) | (0 << GPIO_CH1_PT100_PIN))
#define CH2_PT100_SIGNAL        ((1 << GPIO_CH2_PT500_PIN) | (1 << GPIO_CH2_PT100_PIN))
#define CH2_PT500_SIGNAL        ((1 << GPIO_CH2_PT500_PIN) | (0 << GPIO_CH2_PT100_PIN))
#define CH2_PT1000_SIGNAL       ((0 << GPIO_CH2_PT500_PIN) | (0 << GPIO_CH2_PT100_PIN))

// resistances
#define EXT_TEMP_REF_RESISTANCE_COUNT   12

// Platinum Resistance Thermometer type
typedef enum PRT_Type
{
    PRT_Pt100  = 0,   // (R0 = 100Ω)
    PRT_Pt200  = 1,   // (R0 = 200Ω)
    PRT_Pt500  = 2,   // (R0 = 500Ω)
    PRT_Pt1000 = 3,   // (R0 = 1000Ω)

    PRT_COUNT  = 4

} TPRT_Type;

// RTD wire connection
typedef enum RTD_Conn
{
    RTDC_2WIRE = 0,   // (2-Wire)
    RTDC_3WIRE = 1,   // (3-Wire)
    RTDC_4WIRE = 2,   // (4-Wire)

    RTDC_COUNT = 3

} TRTD_Conn;

// Error code type
typedef enum ErrorCode
{
    ERRC_NONE,              // No error
    ERRC_CLOSED,            // fw module closed
    ERRC_CFG_DEVICE,        // Unsuccessful configuration of the measuring device
    ERRC_NO_DATA,           // ADC Data Not Available
    ERRC_OVER_RANGE,        // ADC value over range
    ERRC_EMPTY_QUEUE,       // empty queue of ADC samples
    ERRC_INVALID_PRT,       // invalid PRT type
    ERRC_UNWANTED_CHNL,     // unwanted ADC data channel
    ERRC_MISSING_CHNL       // missing ADC data channel

} TErrorCode;

// Fw module state
static struct
{
    // open state
    BOOL isOpen;

    // measuring device configuration state
    BOOL isMeasureDeviceConfigured;

    // measured temperature mean value in degrees Kelvin
    float measuredMeanValue[TCHNL_COUNT];

    // last correct measured temperature mean value in degrees Kelvin
    float lastCorrectMeasuredMeanValue[TCHNL_COUNT];

    // measurement queue of ADC samples for calculating the mean value in degrees Kelvin
    uint32_t measurementQueue[TCHNL_COUNT][MEASUREMENT_QUEUE_LENGTH];

    // number of valid values in measurement queue
    uint8_t valuesInQueue[TCHNL_COUNT];

    // index of next value in measurement queue
    uint8_t indexInQueue[TCHNL_COUNT];

    // raw adc output data
    uint32_t adcData[TCHNL_COUNT];

    // error code
    TErrorCode errorCode[TCHNL_COUNT];

} state;

// Measuring device state
// LMP90100
static struct
{
    // Previous Upper Register Address (URA)
    uint8_t prevURA;

} deviceState;

// ref resistance addresses
const unsigned long EXT_TEMP_REF_RESISTANCE_ADDRESSES[TCHNL_COUNT][EXT_TEMP_REF_RESISTANCE_COUNT] =
{
    {
        EXT_TEMP1_REF_RESISTANCE_0,
        EXT_TEMP1_REF_RESISTANCE_1,
        EXT_TEMP1_REF_RESISTANCE_2,
        EXT_TEMP1_REF_RESISTANCE_3,
        EXT_TEMP1_REF_RESISTANCE_4,
        EXT_TEMP1_REF_RESISTANCE_5,
        EXT_TEMP1_REF_RESISTANCE_6,
        EXT_TEMP1_REF_RESISTANCE_7,
        EXT_TEMP1_REF_RESISTANCE_8,
        EXT_TEMP1_REF_RESISTANCE_9,
        EXT_TEMP1_REF_RESISTANCE_10,
        EXT_TEMP1_REF_RESISTANCE_11
    },
    {
        EXT_TEMP2_REF_RESISTANCE_0,
        EXT_TEMP2_REF_RESISTANCE_1,
        EXT_TEMP2_REF_RESISTANCE_2,
        EXT_TEMP2_REF_RESISTANCE_3,
        EXT_TEMP2_REF_RESISTANCE_4,
        EXT_TEMP2_REF_RESISTANCE_5,
        EXT_TEMP2_REF_RESISTANCE_6,
        EXT_TEMP2_REF_RESISTANCE_7,
        EXT_TEMP2_REF_RESISTANCE_8,
        EXT_TEMP2_REF_RESISTANCE_9,
        EXT_TEMP2_REF_RESISTANCE_10,
        EXT_TEMP2_REF_RESISTANCE_11
    }
};

// filter coefficients
const float FILTER_COEFS[MEASUREMENT_FILTER_LENGTH] =
{
    0.7, 0.2, 0.1
};

//------------------------------------------------------------------------------
//  Get address index of ref resistance.
//------------------------------------------------------------------------------
static unsigned char GetRefResistanceAddressIndex(TRTD_Conn rtdConn, TPRT_Type prtType)
{
    unsigned char index = rtdConn * PRT_COUNT + prtType;
    return index < EXT_TEMP_REF_RESISTANCE_COUNT ? index : 0;
}

//------------------------------------------------------------------------------
//  Convert error code to text.
//------------------------------------------------------------------------------
static char* ConvertErrorCodeToText(TErrorCode errorCode)
{
    switch (errorCode)
    {
    case ERRC_NONE:
        return "OK";

    case ERRC_CLOSED:
        return "Closed";

    case ERRC_CFG_DEVICE:
        return "Config Device";

    case ERRC_NO_DATA:
        return "Data Not Available";

    case ERRC_OVER_RANGE:
        return "ADC Over Range";

    case ERRC_EMPTY_QUEUE:
        return "Empty Queue";

    case ERRC_INVALID_PRT:
        return "Invalid PRT type";

    case ERRC_UNWANTED_CHNL:
        return "Unwanted channel";

    case ERRC_MISSING_CHNL:
        return "Missing channel";

    default:
        return "Unknown error code";
    }
}

//------------------------------------------------------------------------------
//  Clear all measured temperatures.
//------------------------------------------------------------------------------
static void ClearAllMeasurements(void)
{
    for (unsigned char i = 0; i < TCHNL_COUNT; i++)
    {
        state.measuredMeanValue[i] = UNKNOWN_TEMPERATURE;
        state.lastCorrectMeasuredMeanValue[i] = 0;
        state.valuesInQueue[i] = 0;
        state.indexInQueue[i] = 0;
        state.adcData[i] = UNKNOWN_ADC_TEMPERATURE;
    }
}

//------------------------------------------------------------------------------
//  Get Platinum Resistance Thermometer type.
//------------------------------------------------------------------------------
static TPRT_Type ExtTemp_GetPRTType(void)
{
    return (TPRT_Type)RAM_Read_Data_Long(EXT_TEMP_SENSOR_TYPE);
}

//------------------------------------------------------------------------------
//  Get RTD wire connection.
//------------------------------------------------------------------------------
static TRTD_Conn ExtTemp_GetRTDConnection(void)
{
    return (TRTD_Conn)RAM_Read_Data_Long(EXT_TEMP_SENSOR_CONN);
}

//------------------------------------------------------------------------------
// The function calculates the electrical resistance for a Platinum Resistance
// Thermometer (PRT) at given temperature and defined resistance R0 at 0°C for
// a PRT element. E.g. for a Pt1000 element resistance R0=1000.
//------------------------------------------------------------------------------
// Parameters:
//  aTemp        Input temperature in ITS-90                   [°C]
//  R0           Type of PRT (resistance at 0°C; e.g. Pt100)   [Ohm]
//------------------------------------------------------------------------------
// Returns:
//  Resistance [Ohm] of R0 type PRT at aTemp
//------------------------------------------------------------------------------
static float PRT_CelsiusToOhm(float aTemp, float R0)
{
    // aTemp decides which polynomial to use
    if (aTemp >= 0)
    {
        return R0 * ((PRT_B_CONST * aTemp + PRT_A_CONST) * aTemp + 1);
    }
    else
    {
        return R0 * ((((aTemp - 100) * PRT_C_CONST * aTemp + PRT_B_CONST) * aTemp + PRT_A_CONST) * aTemp + 1);
    }
}

//------------------------------------------------------------------------------
// The function calculates the temperature measured by a Platinum Resistance
// Thermometer (PRT) at given electrical resistance and defined resistance R0 at
// 0°C for a PRT element. E.g. for a Pt1000 element resistance R0=1000.
//------------------------------------------------------------------------------
// Parameters:
//  aResistance  Input electrical resistance                   [Ohm]
//  R0           Type of PRT (electrical resistance at 0°C)    [Ohm]
//------------------------------------------------------------------------------
// Returns:
//  ITS-90 Temperature [°C]
//------------------------------------------------------------------------------
static float PRT_OhmToCelsius(float aResistance, float R0)
{
    return (-R0 * PRT_A_CONST + sqrt(R0 * R0 * PRT_A_CONST * PRT_A_CONST - 4 * R0 * PRT_B_CONST * (R0 - aResistance))) /
           (2 * R0 * PRT_B_CONST);
}

//------------------------------------------------------------------------------
// Get ADC input circuit reference electrical resistance [Ohm].
//------------------------------------------------------------------------------
float ExtTemp_GetADCRefResistance(TTempChannel channel)
{
    unsigned char adrIdx = GetRefResistanceAddressIndex(ExtTemp_GetRTDConnection(), ExtTemp_GetPRTType());
    return RAM_Read_Data_Float(EXT_TEMP_REF_RESISTANCE_ADDRESSES[channel][adrIdx]);
}

//------------------------------------------------------------------------------
// Set ADC input circuit reference electrical resistance [Ohm].
//------------------------------------------------------------------------------
static void SetADCRefResistance(TTempChannel channel, float Rref)
{
    unsigned char adrIdx = GetRefResistanceAddressIndex(ExtTemp_GetRTDConnection(), ExtTemp_GetPRTType());
    RAM_Write_Data_Float(Rref, EXT_TEMP_REF_RESISTANCE_ADDRESSES[channel][adrIdx]);
}

//------------------------------------------------------------------------------
// Get defined resistance R0 at 0°C for a PRT element [Ohm].
// E.g. for a Pt1000 element resistance R0=1000.
// Returns 0 for unsupported input parameter value.
//------------------------------------------------------------------------------
static uint16_t GetPRTR0Resistance(TPRT_Type prtType)
{
    switch (prtType)
    {
    case PRT_Pt100:
        return 100;

    case PRT_Pt200:
        return 200;

    case PRT_Pt500:
        return 500;

    case PRT_Pt1000:
        return 1000;

    default:
        return 0;
    }
}

//------------------------------------------------------------------------------
// The function calculates the resistance of a Platinum Resistance Thermometer
// (PRT) with ADC input circuit (reference electrical resistance) at given
// ADC_DOUT sample value and reference electrical resistance.
//------------------------------------------------------------------------------
// Parameters:
//  adcSample    ADC_DOUT sample value                         [-]
//  Rref         Reference electrical resistance               [Ohm]
//------------------------------------------------------------------------------
// Returns:
//  Resistance of a PRT [Ohm]
//------------------------------------------------------------------------------
static float ADC_SampleToPRTOhm(uint32_t adcSample, float Rref)
{
    return (((float)adcSample) / TI_LMP90100_ADC_DOUT_HALF_VALUE_RANGE) * Rref;
}

//------------------------------------------------------------------------------
// The function calculates the reference electrical resistance of ADC input
// circuit at given ADC_DOUT sample value and the resistance of a Platinum
// Resistance Thermometer (PRT).
//------------------------------------------------------------------------------
// Parameters:
//  adcSample    ADC_DOUT sample value (must not be 0)         [-]
//  Rprt         Resistance of a PRT                           [Ohm]
//------------------------------------------------------------------------------
// Returns:
//  Reference electrical resistance [Ohm] or -1 if invalid parameter adcSample
//------------------------------------------------------------------------------
static float ADC_SampleToRefOhm(uint32_t adcSample, float Rprt)
{
    if (adcSample == 0)
        return -1;

    return (((float)TI_LMP90100_ADC_DOUT_HALF_VALUE_RANGE) / adcSample) * Rprt;
}

//------------------------------------------------------------------------------
//  Configuration of the measuring device communication interface.
//------------------------------------------------------------------------------
static void ConfigureMeasureDeviceCommIface(void)
{
    TI_LMP90100_SPISetup();
}

//------------------------------------------------------------------------------
//  Configuration of the measuring device channels.
//  Returns TRUE if success or FALSE if error.
//------------------------------------------------------------------------------
static BOOL ConfigureMeasureDeviceChannels(void)
{
    // buffer size for device registers write/read operations
    #define DEV_BUF_SIZE    5

    // Device stream write/read start address
    #define DEV_STREAM_START_ADDR    TI_LMP90100_CH_SCAN_REG

    // device register buffers for write/read operations
    uint8_t writeBuf[DEV_BUF_SIZE], readBuf[DEV_BUF_SIZE];

    // GPIO Direction
    if (!TI_LMP90100_SPIWriteReadReg(TI_LMP90100_GPIO_DIRCN_REG, TI_LMP90100_GPIO_DIRCN_REG_VALUE, &deviceState.prevURA))
        return FALSE;

    // GPIO Data
    uint8_t gpioDatValue = 0;

    TRTD_Conn rtdConn = ExtTemp_GetRTDConnection();
    switch (rtdConn)
    {
    case RTDC_2WIRE:
    case RTDC_3WIRE:
        gpioDatValue |= IB1_23W_SIGNAL | IB2_23W_SIGNAL;
        break;

    case RTDC_4WIRE:
        gpioDatValue |= IB1_4W_SIGNAL | IB2_4W_SIGNAL;
        break;

    default:
        return FALSE;
    }

    TPRT_Type prtType = ExtTemp_GetPRTType();
    switch (prtType)
    {
    case PRT_Pt100:
        gpioDatValue |= CH1_PT100_SIGNAL | CH2_PT100_SIGNAL;
        break;

    case PRT_Pt200:
    case PRT_Pt500:
        gpioDatValue |= CH1_PT500_SIGNAL | CH2_PT500_SIGNAL;
        break;

    case PRT_Pt1000:
        gpioDatValue |= CH1_PT1000_SIGNAL | CH2_PT1000_SIGNAL;
        break;

    default:
        return FALSE;
    }

    TI_LMP90100_SPIWriteReg(TI_LMP90100_GPIO_DAT_REG, gpioDatValue, &deviceState.prevURA);
    uint8_t gpioDatValueRead = TI_LMP90100_SPIReadReg(TI_LMP90100_GPIO_DAT_REG, &deviceState.prevURA);
    if ((gpioDatValueRead & TI_LMP90100_GPIO_DAT_REG_VALUE_USED_MASK) != gpioDatValue)
        return FALSE;

    // Channel Status test
    uint8_t chnlStaTestCount = 0;
    do
    {
        uint8_t chnlSta = TI_LMP90100_SPIReadReg(TI_LMP90100_CH_STS_REG, &deviceState.prevURA);
        if ((chnlSta & TI_LMP90100_CH_SCAN_NRDY) == 0)
            break; // Update not pending, CH_SCAN register is okay to program
    }
    while (++chnlStaTestCount < 100);

    if (chnlStaTestCount == 100)
        return FALSE;

    // Initialize buffer with register values for stream write to channel registers
    uint8_t streamWriteCount = rtdConn == RTDC_2WIRE ? 5 : 9;

    if (rtdConn == RTDC_2WIRE)
    {
        writeBuf[0] = TI_LMP90100_CH_SCAN_2WIRE_REG_VALUE;
        writeBuf[1] = TI_LMP90100_CH0_INPUTCN_2WIRE_REG_VALUE;
        writeBuf[2] = TI_LMP90100_CHX_CONFIG_REG_VALUE;
        writeBuf[3] = TI_LMP90100_CH1_INPUTCN_2WIRE_REG_VALUE;
        writeBuf[4] = TI_LMP90100_CHX_CONFIG_REG_VALUE;
    }
    else
    {
        writeBuf[0] = TI_LMP90100_CH_SCAN_34WIRE_REG_VALUE;
        writeBuf[1] = TI_LMP90100_CH0_INPUTCN_34WIRE_REG_VALUE;
        writeBuf[2] = TI_LMP90100_CHX_CONFIG_REG_VALUE;
        writeBuf[3] = TI_LMP90100_CH1_INPUTCN_34WIRE_REG_VALUE;
        writeBuf[4] = TI_LMP90100_CHX_CONFIG_REG_VALUE;
        writeBuf[5] = TI_LMP90100_CH2_INPUTCN_34WIRE_REG_VALUE;
        writeBuf[6] = TI_LMP90100_CHX_CONFIG_REG_VALUE;
        writeBuf[7] = TI_LMP90100_CH3_INPUTCN_34WIRE_REG_VALUE;
        writeBuf[8] = TI_LMP90100_CHX_CONFIG_REG_VALUE;
    }

    // Stream write/read to/from channel registers
    return TI_LMP90100_SPINormalStreamWriteReadReg(DEV_STREAM_START_ADDR, writeBuf, readBuf, streamWriteCount, &deviceState.prevURA);
}

//------------------------------------------------------------------------------
//  Configuration of the measuring device. Returns TRUE if success or FALSE
//  if error.
//------------------------------------------------------------------------------
static BOOL ConfigureMeasureDevice(void)
{
    // Initialize URA to invalid segment
    deviceState.prevURA = LMP90100_URA_END;

    // Device Reset Control
    TI_LMP90100_SPIWriteReg(TI_LMP90100_RESETCN_REG, TI_LMP90100_RESETCN_REG_VALUE, &deviceState.prevURA);

    // Set registers
    if (!TI_LMP90100_SPIWriteReadReg(TI_LMP90100_SPI_STREAMCN_REG, TI_LMP90100_SPI_STREAMCN_REG_VALUE, &deviceState.prevURA))
        return FALSE;

    TI_LMP90100_SPIWriteReg(TI_LMP90100_PWRCN_REG, TI_LMP90100_PWRCN_REG_VALUE, &deviceState.prevURA);

    if (!TI_LMP90100_SPIWriteReadReg(TI_LMP90100_BGCALCN_REG, TI_LMP90100_BGCALCN_REG_VALUE, &deviceState.prevURA))
        return FALSE;

    if (!ConfigureMeasureDeviceChannels())
        return FALSE;

    if (!TI_LMP90100_SPIWriteReadReg(TI_LMP90100_ADC_AUXCN_REG, TI_LMP90100_ADC_AUXCN_REG_VALUE, &deviceState.prevURA))
        return FALSE;

    return TRUE;
}

//------------------------------------------------------------------------------
//  Vrati pole coeficientu filtru.
//------------------------------------------------------------------------------
static float* GetFilterCoefs(void)
{
    return FILTER_COEFS;
}

//------------------------------------------------------------------------------
//  Vrati starou hodnotu ve fronte (podle velikosti kroku zpet).
//------------------------------------------------------------------------------
static uint32_t GetOldMeasuredValueInQueue(TTempChannel channel, uint8_t step)
{
    uint32_t value = 0;
    uint8_t indexInQueue = state.indexInQueue[channel];
    step++;

    while (step > 0)
    {
        step--;

        if (indexInQueue > 0)
            indexInQueue--;
        else
            indexInQueue = MEASUREMENT_QUEUE_LENGTH - 1;

        value = state.measurementQueue[channel][indexInQueue];
    }

    return value;
}

//------------------------------------------------------------------------------
//  Filter new measured ADC value. Returns filtered value.
//------------------------------------------------------------------------------
static uint32_t FilterMeasuredValue(TTempChannel channel, uint32_t value)
{
    float filteredValue = (float)value;
    float* filterCoefs = GetFilterCoefs();

    for (unsigned char i = 0; i < MEASUREMENT_FILTER_LENGTH; i++)
    {
        if (i == 0)
        {
            filteredValue = value * filterCoefs[0];
        }
        else
        {
            filteredValue += GetOldMeasuredValueInQueue(channel, i - 1) * filterCoefs[i];
        }
    }

    return (uint32_t)filteredValue;
}

//------------------------------------------------------------------------------
//  Push new measured ADC value into queue.
//------------------------------------------------------------------------------
static void PushMeasuredValueIntoQueue(TTempChannel channel, uint32_t value)
{
    state.measurementQueue[channel][state.indexInQueue[channel]] = value;

    if (state.valuesInQueue[channel] < MEASUREMENT_QUEUE_LENGTH)
        state.valuesInQueue[channel]++;

    state.indexInQueue[channel]++;
    if (state.indexInQueue[channel] >= MEASUREMENT_QUEUE_LENGTH)
        state.indexInQueue[channel] = 0;
}

//------------------------------------------------------------------------------
//  Calculate and returns mean sample in queue.
//------------------------------------------------------------------------------
static uint32_t CalculateMeanADCSampleInQueue(TTempChannel channel)
{
    if (state.valuesInQueue[channel] == 0)
        return 0;

    uint32_t sum = 0;
    for (uint8_t i = 0; i < state.valuesInQueue[channel]; i++)
        sum += state.measurementQueue[channel][i];

    return (uint32_t)((((float)sum) / state.valuesInQueue[channel]) + 0.5f); // round
}

//------------------------------------------------------------------------------
//  Calculate and returns measured mean value in queue.
//------------------------------------------------------------------------------
static float CalculateMeasuredMeanValueInQueue(TTempChannel channel)
{
    if (state.valuesInQueue[channel] == 0)
        return UNKNOWN_TEMPERATURE;

    // R0 resistance of PRT type
    uint16_t R0 = GetPRTR0Resistance(ExtTemp_GetPRTType());
    if (R0 == 0)
    {
        state.errorCode[channel] = ERRC_INVALID_PRT;
        return UNKNOWN_TEMPERATURE; // invalid PRT type
    }

    uint32_t meanSample = CalculateMeanADCSampleInQueue(channel);

    // resistance of PRT
    float Rprt = ADC_SampleToPRTOhm(meanSample, ExtTemp_GetADCRefResistance(channel));

    // temperature
    float tempCelsius = PRT_OhmToCelsius(Rprt, R0);

    return Convert_CelsiusToKelvin(tempCelsius);
}

//------------------------------------------------------------------------------
//  Read ADC data from LMP90100.
//  Returns FALSE if error.
//------------------------------------------------------------------------------
static BOOL ReadAdcDataFromLMP90100(void)
{
    BOOL channelRead[4];
    uint32_t channelAdcData[4];
    uint8_t maxChannelNumber = ExtTemp_GetRTDConnection() == RTDC_2WIRE ? 1 : 3;

    for (unsigned char i = 0; i < 4; i++)
        channelRead[i] = FALSE;

    // Read all channel data
    for (uint8_t noAllChannelDataCount = 0; noAllChannelDataCount < 5; noAllChannelDataCount++)
    {
        for (uint8_t ch = 0; ch <= maxChannelNumber; ch++)
        {
            // Stream Read
            // - ADC Data Available test
            // - Sensor Diagnostic Flags
            // - ADC_DOUT Conversion Data 2,1,0
            uint8_t dataBuf[5];
            for (uint8_t noDataCount = 0; noDataCount < 50; noDataCount++)
            {
                TI_LMP90100_SPINormalStreamReadReg(TI_LMP90100_ADC_DONE_REG, dataBuf, 5, &deviceState.prevURA);

                // ADC Data Available test
                if (dataBuf[0] != TI_LMP90100_ADC_DONE_NOT_AVAILABLE_VALUE)
                    break;
            }

            // ADC Data Available test
            if (dataBuf[0] == TI_LMP90100_ADC_DONE_NOT_AVAILABLE_VALUE)
            {
                for (unsigned char i = 0; i < TCHNL_COUNT; i++)
                    state.errorCode[i] = ERRC_NO_DATA;

                return FALSE; // Data Not Available
            }

            // Extract Channel Number
            uint8_t channel = dataBuf[1] & LMP90100_CH_NUM_MASK;
            if (channel > maxChannelNumber)
            {
                for (unsigned char i = 0; i < TCHNL_COUNT; i++)
                    state.errorCode[i] = ERRC_UNWANTED_CHNL;

                return FALSE;
            }

            // Form raw adc output data
            channelAdcData[channel] = ((uint32_t)dataBuf[2] << 16) | ((uint16_t)dataBuf[3] << 8) | dataBuf[4];
            channelRead[channel] = TRUE;
        }

        // Test all channel data
        BOOL allChannelData = TRUE;
        for (uint8_t ch = 0; ch <= maxChannelNumber; ch++)
        {
            if (!channelRead[ch])
            {
                allChannelData = FALSE;
                break;
            }
        }

        if (allChannelData)
            break;
    }

    // Test all channel data
    for (uint8_t ch = 0; ch <= maxChannelNumber; ch++)
    {
        if (!channelRead[ch])
        {
            for (unsigned char i = 0; i < TCHNL_COUNT; i++)
                state.errorCode[i] = ERRC_MISSING_CHNL;

            return FALSE;
        }
    }

    // Form complete raw adc output data
    if (maxChannelNumber == 1)
    {
        state.adcData[TCHNL_1] = channelAdcData[0];
        state.adcData[TCHNL_2] = channelAdcData[1];
    }
    else
    {
        state.adcData[TCHNL_1] = channelAdcData[0] - channelAdcData[1];
        state.adcData[TCHNL_2] = channelAdcData[2] - channelAdcData[3];
    }

    return TRUE;
}

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//------------------------------------------------------------------------------
void ExtTemp_Init(void)
{
    state.isOpen = state.isMeasureDeviceConfigured = FALSE;
    ClearAllMeasurements();

    for (unsigned char i = 0; i < TCHNL_COUNT; i++)
        state.errorCode[i] = ERRC_NONE;
}

//------------------------------------------------------------------------------
//  Allocation of temperature measurement resources. Call only once before
//  measurement.
//------------------------------------------------------------------------------
void ExtTemp_Open(void)
{
    ExtTemp_Init();

    state.isOpen = TRUE;

    ConfigureMeasureDeviceCommIface();
}

//------------------------------------------------------------------------------
//  Free temperature measurement resources. Call only once after measurement.
//------------------------------------------------------------------------------
void ExtTemp_Close(void)
{
    ExtTemp_Init();
}

//------------------------------------------------------------------------------
//  Temperature measurement. Try call periodically for updating of the measured
//  value. Before calls the measurement must be enabled by calling ExtTemp_Open
//  function. Returns TRUE if success, FALSE if error.
//------------------------------------------------------------------------------
BOOL ExtTemp_Measure(void)
{
    if (!state.isOpen)
    {
        for (unsigned char i = 0; i < TCHNL_COUNT; i++)
            state.errorCode[i] = ERRC_CLOSED;

        return FALSE;
    }

    if (!state.isMeasureDeviceConfigured)
    {
        state.isMeasureDeviceConfigured = ConfigureMeasureDevice();
        if (!state.isMeasureDeviceConfigured)
        {
            for (unsigned char i = 0; i < TCHNL_COUNT; i++)
                state.errorCode[i] = ERRC_CFG_DEVICE;

            return FALSE;
        }
    }

    // Power Mode Control and Status test
    uint8_t pwrcn = TI_LMP90100_SPIReadReg(TI_LMP90100_PWRCN_REG, &deviceState.prevURA);
    if ((pwrcn & LMP90100_PWRCN_MASK) != LMP90100_PWRCN_ACTIVE_MODE_REG_VALUE)
    {
        TI_LMP90100_SPIWriteReg(TI_LMP90100_PWRCN_REG, LMP90100_PWRCN_ACTIVE_MODE_REG_VALUE, &deviceState.prevURA);
        state.isMeasureDeviceConfigured = FALSE;

        for (unsigned char i = 0; i < TCHNL_COUNT; i++)
            state.errorCode[i] = ERRC_CFG_DEVICE;

        return FALSE;
    }

    // Read data from LMP90100
    if (!ReadAdcDataFromLMP90100())
        return FALSE;

    BOOL result = TRUE;
    for (unsigned char channel = 0; channel < TCHNL_COUNT; channel++)
    {
        // test ADC value
        if (state.adcData[channel] >= TI_LMP90100_ADC_DOUT_MAX_POS_VALUE)
        {
            state.errorCode[channel] = ERRC_OVER_RANGE;
            result = FALSE; // over range
            continue;
        }

        // filter
        uint32_t filteredValue = FilterMeasuredValue(channel, state.adcData[channel]);

        // save sample value
        PushMeasuredValueIntoQueue(channel, filteredValue);

        // Calculate mean value
        state.measuredMeanValue[channel] = CalculateMeasuredMeanValueInQueue(channel);

        if (state.measuredMeanValue[channel] != UNKNOWN_TEMPERATURE)
            state.lastCorrectMeasuredMeanValue[channel] = state.measuredMeanValue[channel];

        state.errorCode[channel] = ERRC_NONE;
    }

    return result;
}

//------------------------------------------------------------------------------
//  Get the measured value in degrees Kelvin or UNKNOWN_TEMPERATURE if no
//  temperature value available.
//------------------------------------------------------------------------------
/*float ExtTemp_GetMeasuredValue(TTempChannel channel)
{
    return state.measuredMeanValue[channel];
}*/

//------------------------------------------------------------------------------
//  Get last correct measured value in degrees Kelvin or 0 if no
//  correct temperature value available.
//------------------------------------------------------------------------------
float ExtTemp_GetLastCorrectMeasuredValue(TTempChannel channel)
{
    return state.lastCorrectMeasuredMeanValue[channel];
}

//------------------------------------------------------------------------------
//  Calibrate temperature measurement. Input parameter is expected temperature
//  in Kelvin.
//------------------------------------------------------------------------------
void ExtTemp_CalibrateMeasurement(TTempChannel channel, float tempKelvin)
{
    if (state.valuesInQueue[channel] == 0)
    {
        state.errorCode[channel] = ERRC_EMPTY_QUEUE;
        return;
    }

    // R0 resistance of PRT type
    uint16_t R0 = GetPRTR0Resistance(ExtTemp_GetPRTType());
    if (R0 == 0)
    {
        state.errorCode[channel] = ERRC_INVALID_PRT;
        return; // invalid PRT type
    }

    float tempCelsius = Convert_KelvinToCelsius(tempKelvin);
    float Rprt = PRT_CelsiusToOhm(tempCelsius, R0);
    uint32_t meanSample = CalculateMeanADCSampleInQueue(channel);
    float Rref = ADC_SampleToRefOhm(meanSample, Rprt);

    if (Rref >= 0)
    {
        SetADCRefResistance(channel, Rref);
    }
}

//------------------------------------------------------------------------------
//  Call after temperature sensor type changed.
//------------------------------------------------------------------------------
void ExtTemp_OnSensorTypeChanged(void)
{
    ClearAllMeasurements();
    state.isMeasureDeviceConfigured = FALSE;
}

//------------------------------------------------------------------------------
//  Call after temperature sensor connection changed.
//------------------------------------------------------------------------------
void ExtTemp_OnSensorConnectionChanged(void)
{
    ClearAllMeasurements();
    state.isMeasureDeviceConfigured = FALSE;
}

//------------------------------------------------------------------------------
//  Get the raw adc output data.
//------------------------------------------------------------------------------
uint32_t ExtTemp_GetAdcData(TTempChannel channel)
{
    return state.adcData[channel];
}

//------------------------------------------------------------------------------
//  Get the raw adc output data in array.
//------------------------------------------------------------------------------
uint32_t* ExtTemp_GetAdcArray(TTempChannel channel)
{
    return state.measurementQueue[channel];
}

//------------------------------------------------------------------------------
//  Get number of valid values in measurement queue
//------------------------------------------------------------------------------
uint8_t ExtTemp_GetValuesInQueue(TTempChannel channel)
{
    return state.valuesInQueue[channel];
}

//------------------------------------------------------------------------------
//  Get error text.
//------------------------------------------------------------------------------
char* ExtTemp_GetErrorText(TTempChannel channel)
{
    return ConvertErrorCodeToText(state.errorCode[channel]);
}
