//******************************************************************************
//  This file contains functions that allow to access external measurements
//  of pressure.
//*****************************************************************************/

#ifndef EXTPRESS_H_INCLUDED
#define EXTPRESS_H_INCLUDED

#include <stdint.h>

#ifndef BOOL_TYPEDEF
#define BOOL_TYPEDEF
typedef unsigned char BOOL;
#endif

#ifndef TRUE
#define TRUE            1
#endif

#ifndef FALSE
#define FALSE           0
#endif

#define UNKNOWN_PRESSURE     (-1)

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//------------------------------------------------------------------------------
void ExtPress_Init(void);

//------------------------------------------------------------------------------
//  Allocation of pressure measurement resources. Call only once before
//  measurement.
//------------------------------------------------------------------------------
void ExtPress_Open(void);

//------------------------------------------------------------------------------
//  Free pressure measurement resources. Call only once after measurement.
//------------------------------------------------------------------------------
void ExtPress_Close(void);

//------------------------------------------------------------------------------
//  Pressure measurement. Try call periodically for updating of the measured
//  value. Before calls the measurement must be enabled by calling ExtPress_Open
//  function. Returns TRUE if success, FALSE if error.
//------------------------------------------------------------------------------
BOOL ExtPress_Measure(void);

//------------------------------------------------------------------------------
//  Get the measured value in Pa (pascal) or UNKNOWN_PRESSURE if no
//  pressure value available.
//------------------------------------------------------------------------------
int32_t ExtPress_GetMeasuredValue(void);

//------------------------------------------------------------------------------
//  Call after current/pressure transform parameters changed.
//------------------------------------------------------------------------------
void ExtPress_OnCurrentToPressParametersChanged(void);

//------------------------------------------------------------------------------
//  Get the raw adc output data or UNKNOWN_PRESSURE.
//------------------------------------------------------------------------------
int16_t ExtPress_GetAdcData(void);

//------------------------------------------------------------------------------
//  Get number of valid values in measurement queue
//------------------------------------------------------------------------------
uint8_t ExtPress_GetValuesInQueue(void);

//------------------------------------------------------------------------------
//  Get error text.
//------------------------------------------------------------------------------
char* ExtPress_GetErrorText(void);

#endif // EXTPRESS_H_INCLUDED
