//******************************************************************************
//  Ovladani indikacnich LEDek
//*****************************************************************************/

#include <stdint.h>
#include <LPC23xx.h>
#include "port.h"
#include "pulse_outputs.h"
#include "led.h"
#include "maine.h"

// hw definice LEDek

// LEDR1

#define LEDR1_IODIR FIO4DIR
#define LEDR1_IOSET FIO4SET
#define LEDR1_IOCLR FIO4CLR
#define LEDR1_IOPIN (1<<28)

#define LEDR1_INIT  {LEDR1_IODIR |= LEDR1_IOPIN;}
#define LEDR1_HIGH  {LEDR1_IOSET |= LEDR1_IOPIN;}
#define LEDR1_LOW   {LEDR1_IOCLR |= LEDR1_IOPIN;}

// LEDG1

#define LEDG1_IODIR FIO4DIR
#define LEDG1_IOSET FIO4SET
#define LEDG1_IOCLR FIO4CLR
#define LEDG1_IOPIN (1<<29)

#define LEDG1_INIT  {LEDG1_IODIR |= LEDG1_IOPIN;}
#define LEDG1_HIGH  {LEDG1_IOSET |= LEDG1_IOPIN;}
#define LEDG1_LOW   {LEDG1_IOCLR |= LEDG1_IOPIN;}

// LEDR2

#define LEDR2_IODIR IODIR1
#define LEDR2_IOSET IOSET1
#define LEDR2_IOCLR IOCLR1
#define LEDR2_IOPIN (1<<17)

#define LEDR2_INIT  {LEDR2_IODIR |= LEDR2_IOPIN;}
#define LEDR2_HIGH  {LEDR2_IOSET |= LEDR2_IOPIN;}
#define LEDR2_LOW   {LEDR2_IOCLR |= LEDR2_IOPIN;}

// LEDG2

#define LEDG2_IODIR IODIR1
#define LEDG2_IOSET IOSET1
#define LEDG2_IOCLR IOCLR1
#define LEDG2_IOPIN (1<<16)

#define LEDG2_INIT  {LEDG2_IODIR |= LEDG2_IOPIN;}
#define LEDG2_HIGH  {LEDG2_IOSET |= LEDG2_IOPIN;}
#define LEDG2_LOW   {LEDG2_IOCLR |= LEDG2_IOPIN;}

// LEDR3

#define LEDR3_IODIR IODIR1
#define LEDR3_IOSET IOSET1
#define LEDR3_IOCLR IOCLR1
#define LEDR3_IOPIN (1<<15)

#define LEDR3_INIT  {LEDR3_IODIR |= LEDR3_IOPIN;}
#define LEDR3_HIGH  {LEDR3_IOSET |= LEDR3_IOPIN;}
#define LEDR3_LOW   {LEDR3_IOCLR |= LEDR3_IOPIN;}

// LEDG3

#define LEDG3_IODIR IODIR1
#define LEDG3_IOSET IOSET1
#define LEDG3_IOCLR IOCLR1
#define LEDG3_IOPIN (1<<14)

#define LEDG3_INIT  {LEDG3_IODIR |= LEDG3_IOPIN;}
#define LEDG3_HIGH  {LEDG3_IOSET |= LEDG3_IOPIN;}
#define LEDG3_LOW   {LEDG3_IOCLR |= LEDG3_IOPIN;}

// LEDR4

#define LEDR4_IODIR IODIR1
#define LEDR4_IOSET IOSET1
#define LEDR4_IOCLR IOCLR1
#define LEDR4_IOPIN (1<<10)

#define LEDR4_INIT  {LEDR4_IODIR |= LEDR4_IOPIN;}
#define LEDR4_HIGH  {LEDR4_IOSET |= LEDR4_IOPIN;}
#define LEDR4_LOW   {LEDR4_IOCLR |= LEDR4_IOPIN;}

// LEDG4

#define LEDG4_IODIR IODIR1
#define LEDG4_IOSET IOSET1
#define LEDG4_IOCLR IOCLR1
#define LEDG4_IOPIN (1<<9)

#define LEDG4_INIT  {LEDG4_IODIR |= LEDG4_IOPIN;}
#define LEDG4_HIGH  {LEDG4_IOSET |= LEDG4_IOPIN;}
#define LEDG4_LOW   {LEDG4_IOCLR |= LEDG4_IOPIN;}

// Typ - prototyp funkce pro nastaveni stavu LEDky.
typedef void (*TSetLEDStateHandler) (TLEDState ledState);

// zadny program pro LEDku
//#define NO_LED_PROGRAM   ((TLEDTimeLimitedState *)0)

// Udalost ulohy LEDek
typedef enum LedTaskEvent
{
    LED_TASK_EVENT_INIT,  // Inicializace ulohy
    LED_TASK_EVENT_MAIN,  // Hlavni uloha
    LED_TASK_EVENT_CHANGE_STATE  // Zmena stavu

} TLedTaskEvent;

// Typ - prototyp funkce ulohy LEDek.
typedef void (*TLedTask) (TLedTaskEvent taskEvent);

// Stav programu LEDky
/*typedef struct
{
    // program blikani LEDky
    TLEDTimeLimitedState *program;

    // pocet polozek programu
    uint8_t itemCount;

    // index aktualne vykonavane polozky programu
    uint8_t itemIndex;

    // jaka doba [ms] ubehla od zacatku aktualni polozky programu
    uint16_t itemTime;

    // nastaveni, zda opakovat program porad dokola
    BOOL programRepeat;

} TLedProgramState;*/

// Fw module state
static struct
{
    // Uloha LEDek
    TLedTask task;

    // programy LEDek
    //TLedProgramState ledProgramStates[LED_Count];

} state;

//------------------------------------------------------------------------------
//  Nastavovaci funkce stavu LEDek
//------------------------------------------------------------------------------
static void SetLEDState_R1(TLEDState ledState)
{
    if (ledState == LEDS_ON) { LEDR1_HIGH; }
    else { LEDR1_LOW; }
}

static void SetLEDState_G1(TLEDState ledState)
{
    if (ledState == LEDS_ON) { LEDG1_HIGH; }
    else { LEDG1_LOW; }
}

static void SetLEDState_R2(TLEDState ledState)
{
    if (ledState == LEDS_ON) { LEDR2_HIGH; }
    else { LEDR2_LOW; }
}

static void SetLEDState_G2(TLEDState ledState)
{
    if (ledState == LEDS_ON) { LEDG2_HIGH; }
    else { LEDG2_LOW; }
}

static void SetLEDState_R3(TLEDState ledState)
{
    if (ledState == LEDS_ON) { LEDR3_HIGH; }
    else { LEDR3_LOW; }
}

static void SetLEDState_G3(TLEDState ledState)
{
    if (ledState == LEDS_ON) { LEDG3_HIGH; }
    else { LEDG3_LOW; }
}

static void SetLEDState_R4(TLEDState ledState)
{
    if (ledState == LEDS_ON) { LEDR4_HIGH; }
    else { LEDR4_LOW; }
}

static void SetLEDState_G4(TLEDState ledState)
{
    if (ledState == LEDS_ON) { LEDG4_HIGH; }
    else { LEDG4_LOW; }
}

const TSetLEDStateHandler SET_LED_STATE_HANDLERS[LED_Count] =
{
    SetLEDState_R1,
    SetLEDState_G1,
    SetLEDState_R2,
    SetLEDState_G2,
    SetLEDState_R3,
    SetLEDState_G3,
    SetLEDState_R4,
    SetLEDState_G4
};

//------------------------------------------------------------------------------
//  Mapovani BIN RE vystupu do LEDek
//------------------------------------------------------------------------------
const TLEDIdentifier MAP_BIN_RE_TO_LED_R[PULSE_OUT_Count] =
{
    LED_R1,
    LED_R2,
    LED_R3,
    LED_R4
};

const TLEDIdentifier MAP_BIN_RE_TO_LED_G[PULSE_OUT_Count] =
{
    LED_G1,
    LED_G2,
    LED_G3,
    LED_G4
};

const TLEDState MAP_BIN_RE_TO_LED_STATE[PULSE_OUT_STATE_Count] =
{
    LEDS_OFF,
    LEDS_ON
};

//------------------------------------------------------------------------------
//  Test, zda doslo k chybe mereni prutoku
//------------------------------------------------------------------------------
static BOOL IsFlowMeasurementError(void)
{
    if ((actual_error & ERR_EMPTY_PIPE) ||
        (actual_error & ERR_OVERLOAD) ||
        (actual_error & ERR_EXCITATION) ||
        (actual_error & ERR_SENSOR) ||
        (actual_error & ERR_ADC) ||
        (actual_error & ERR_OVERLOAD2))
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

//------------------------------------------------------------------------------
//  Test, zda doslo k chybe mereni externi teploty
//------------------------------------------------------------------------------
static BOOL IsExtTempMeasurementError(void)
{
    if (actual_error & ERR_EXT_TEMPERATURE)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

//------------------------------------------------------------------------------
//  Test, zda doslo k nejake chybe
//------------------------------------------------------------------------------
static BOOL IsError(void)
{
    if (actual_error != 0)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

//------------------------------------------------------------------------------
//  Nastaveni trvaleho stavu LEDky podle BIN RE vystupu.
//------------------------------------------------------------------------------
static void Led_SetBinReState(TPulseOutIdentifier id, TPulseOutState outState)
{
    if (outState == PULSE_OUT_ON)
    {
        Led_SetState(MAP_BIN_RE_TO_LED_R[(int)id], LEDS_ON);
        Led_SetState(MAP_BIN_RE_TO_LED_G[(int)id], LEDS_OFF);
    }
    else
    {
        Led_SetState(MAP_BIN_RE_TO_LED_R[(int)id], LEDS_OFF);
        Led_SetState(MAP_BIN_RE_TO_LED_G[(int)id], LEDS_ON);
    }
}

//------------------------------------------------------------------------------
//  Init programu LEDek
//------------------------------------------------------------------------------
/*static void InitLedProgramStates(void)
{
    for (unsigned int i = 0; i < LED_Count; i++)
    {
        state.ledProgramStates[i].program = NO_LED_PROGRAM;
    }
}*/

//------------------------------------------------------------------------------
//  Uloha LEDek - indikace chyb
//------------------------------------------------------------------------------
static void LedTask_Errors(TLedTaskEvent taskEvent)
{
    if ((taskEvent == LED_TASK_EVENT_INIT) || (taskEvent == LED_TASK_EVENT_MAIN))
    {
        if (IsError())
        {
            LEDR1_HIGH;
            LEDG1_LOW;
        }
        else
        {
            LEDR1_LOW;
            LEDG1_HIGH;
        }

        if (IsFlowMeasurementError())
        {
            LEDR2_HIGH;
            LEDG2_LOW;
        }
        else
        {
            LEDR2_LOW;
            LEDG2_HIGH;
        }

        if (IsExtTempMeasurementError())
        {
            LEDR3_HIGH;
            LEDG3_LOW;
        }
        else
        {
            LEDR3_LOW;
            LEDG3_HIGH;
        }

        // Ledky 4 zatim nevyuzity, zhasnout
        LEDR4_LOW;
        LEDG4_LOW;
    }
}

//------------------------------------------------------------------------------
//  Uloha LEDek - indikace stavu vystupu BIN RE
//------------------------------------------------------------------------------
static void LedTask_BinReOuts(TLedTaskEvent taskEvent)
{
    if ((taskEvent == LED_TASK_EVENT_INIT) || (taskEvent == LED_TASK_EVENT_CHANGE_STATE))
    {
        for (TPulseOutIdentifier i = PULSE_OUT_Min; i < PULSE_OUT_Count; i++)
        {
            Led_SetBinReState(i, PulseOutputs_GetPulseOutState(i));
        }
    }
}

//------------------------------------------------------------------------------
//  Uloha LEDek - zadna indikace
//------------------------------------------------------------------------------
static void LedTask_Off(TLedTaskEvent taskEvent)
{
    if (taskEvent == LED_TASK_EVENT_INIT)
    {
        Led_SetAllState(LEDS_OFF);
    }
}

//------------------------------------------------------------------------------
//  Nastavi task LEDek podle setupu
//------------------------------------------------------------------------------
static void SetTask(void)
{
    unsigned long mode = RAM_Read_Data_Long(STATUS_LED_MODE);

    switch (mode)
    {
    case LEDM_ERRORS:
        state.task = LedTask_Errors;
        break;

    case LEDM_BIN_RE_OUTS:
        state.task = LedTask_BinReOuts;
        break;

    case LEDM_OFF:
    default:
        state.task = LedTask_Off;
        break;
    }
}

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//  Volat po PulseOutputs_Init.
//------------------------------------------------------------------------------
void Led_Init(void)
{
    ENTER_CRITICAL_SECTION();

    //InitLedProgramStates();

    LEDR1_INIT;
    LEDG1_INIT;
    LEDR2_INIT;
    LEDG2_INIT;
    LEDR3_INIT;
    LEDG3_INIT;
    LEDR4_INIT;
    LEDG4_INIT;

    SetTask();
    state.task(LED_TASK_EVENT_INIT);

    EXIT_CRITICAL_SECTION();
}

//------------------------------------------------------------------------------
//  Hlavni uloha. Volat co nejcasteji.
//------------------------------------------------------------------------------
void Led_Task(void)
{
    ENTER_CRITICAL_SECTION();
    state.task(LED_TASK_EVENT_MAIN);
    EXIT_CRITICAL_SECTION();
}

//------------------------------------------------------------------------------
//  Nastaveni trvaleho stavu LEDky.
//------------------------------------------------------------------------------
void Led_SetState(TLEDIdentifier id, TLEDState ledState)
{
    //state.ledProgramStates[(int)id].program = NO_LED_PROGRAM;
    SET_LED_STATE_HANDLERS[(int)id](ledState);
}

//------------------------------------------------------------------------------
//  Nastaveni trvaleho stavu vsech LEDek.
//------------------------------------------------------------------------------
void Led_SetAllState(TLEDState ledState)
{
    for (unsigned int i = 0; i < LED_Count; i++)
    {
        Led_SetState((TLEDIdentifier)i, ledState);
    }
}

//------------------------------------------------------------------------------
//  Nastaveni programu LEDky.
//------------------------------------------------------------------------------
/*void Led_SetProgram(TLEDIdentifier id, const TLEDTimeLimitedState *program, uint8_t programLength, BOOL programRepeat)
{
    TLedProgramState *pLedProgramState = &state.ledProgramStates[(int)id];

    pLedProgramState->program = (TLEDTimeLimitedState *)program;
    pLedProgramState->itemCount = programLength;
    pLedProgramState->itemIndex = 0;
    pLedProgramState->itemTime = 0;
    pLedProgramState->programRepeat = programRepeat;
    if ((program != NO_LED_PROGRAM) && (programLength != 0))
        SET_LED_STATE_HANDLERS[(int)id](program->state);
}*/

//------------------------------------------------------------------------------
//  Casovani programu LEDek. Volat kazdou ms.
//------------------------------------------------------------------------------
/*void Led_MillisecondTick(void)
{
    for (unsigned int i = 0; i < LED_Count; i++)
    {
        TLedProgramState *pLedProgramState = &state.ledProgramStates[i];
        if ((pLedProgramState->program != NO_LED_PROGRAM) && (pLedProgramState->itemCount != 0))
        {
            TLEDTimeLimitedState *pItemState = &pLedProgramState->program[pLedProgramState->itemIndex];
            if (++pLedProgramState->itemTime >= pItemState->durationMsec)
            {

            }
        }
    }
}*/

//------------------------------------------------------------------------------
//  Udalost zmeny BIN RE vystupu.
//------------------------------------------------------------------------------
void Led_OnBinReChanged(TPulseOutIdentifier id)
{
    state.task(LED_TASK_EVENT_CHANGE_STATE);
}

//------------------------------------------------------------------------------
//  Udalost zmeny parametru LEDek.
//------------------------------------------------------------------------------
void Led_OnParameterChanged(void)
{
    ENTER_CRITICAL_SECTION();

    SetTask();
    state.task(LED_TASK_EVENT_INIT);

    EXIT_CRITICAL_SECTION();
}
