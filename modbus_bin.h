//head

#define RE1_F  1//Rele 1 funkce
#define RE2_F  2//Rele 2 funkce
#define RE3_F  3//Rele 3 funkce
#define RE4_F  4//Rele 4 funkce
#define FO_F   5//fout funkce
#define QPLUS  6//imp/litr+
#define QMINUS 7//imp/litr-
#define DOSE   8//dosing
#define CF1    9//comparator F1
#define CF2    10//comparator F2
#define CH1    11//comparator H1
#define CH2    12//comparator H2
#define FLOW   13//actual flow
#define ERR    14//actual err
#define FOFL1  15//Fout flow1
#define FOFL2  16//Fout flow2
#define FOFR1  17//Fout freq1
#define FOFR2  18//Fout freq2
#define FODUTY 19//Fout Duty cycle
#define DUMMY  20//dummy send
#define FLOW_FAST   21//actual flow
//function code
#define F_OFF    0//rele off
#define F_FIX    1//rele fixed
#define F_QPLUS  2//rele Q+
#define F_QMINUS 3//rele Q-
#define F_DOSE   4//rele Dosing
#define F_FPLUS  5//rele F+
#define F_FMINUS 6//rele F-
#define F_ERR    7//rele error
#define F_AIR    8//rele Air detected
#define F_CONIN  9//rele comparator on in
#define F_CONOUT 10//rele comparator on out
#define F_CONVF1 11//rele comparator ON > F1
#define F_CONMF1 12//rele comparator ON < F1
#define F_DIRECT 13//direct driving

//#define ERR_EMPTY_PIPE 0x01//error empty pipe stejny jako v MB
//registry

void Send_Float(char code,float data);
void Send_Long(char code,long data);
void Send_Func(char output,char func);
//void Set_Duty(char duty);

extern unsigned char pulse_version;
#define PULSE_V1    1
#define PULSE_V2    2
