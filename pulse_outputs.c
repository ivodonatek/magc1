//******************************************************************************
//  Ovladac pulsnich vystupu
//*****************************************************************************/

#include <stdint.h>
#include <LPC23xx.h>
#include "port.h"
#include "maine.h"
#include "pulse_outputs.h"
#include "led.h"
#include "heat.h"

// hw definice vystupu

// PULSE4 (BIN_RE1)

#define PULSE4_IODIR IODIR1
#define PULSE4_IOSET IOSET1
#define PULSE4_IOCLR IOCLR1
#define PULSE4_IOPIN (1<<29)

#define PULSE4_INIT  {PULSE4_IODIR |= PULSE4_IOPIN;}
#define PULSE4_HIGH  {PULSE4_IOSET |= PULSE4_IOPIN;}
#define PULSE4_LOW   {PULSE4_IOCLR |= PULSE4_IOPIN;}

// PULSE5 (BIN_RE3)

#define PULSE5_IODIR IODIR1
#define PULSE5_IOSET IOSET1
#define PULSE5_IOCLR IOCLR1
#define PULSE5_IOPIN (1<<28)

#define PULSE5_INIT  {PULSE5_IODIR |= PULSE5_IOPIN;}
#define PULSE5_HIGH  {PULSE5_IOSET |= PULSE5_IOPIN;}
#define PULSE5_LOW   {PULSE5_IOCLR |= PULSE5_IOPIN;}

// PULSE6 (BIN_RE2)

#define PULSE6_IODIR IODIR1
#define PULSE6_IOSET IOSET1
#define PULSE6_IOCLR IOCLR1
#define PULSE6_IOPIN (1<<26)

#define PULSE6_INIT  {PULSE6_IODIR |= PULSE6_IOPIN;}
#define PULSE6_HIGH  {PULSE6_IOSET |= PULSE6_IOPIN;}
#define PULSE6_LOW   {PULSE6_IOCLR |= PULSE6_IOPIN;}

// PULSE7 (BIN_RE4)

#define PULSE7_IODIR IODIR1
#define PULSE7_IOSET IOSET1
#define PULSE7_IOCLR IOCLR1
#define PULSE7_IOPIN (1<<23)

#define PULSE7_INIT  {PULSE7_IODIR |= PULSE7_IOPIN;}
#define PULSE7_HIGH  {PULSE7_IOSET |= PULSE7_IOPIN;}
#define PULSE7_LOW   {PULSE7_IOCLR |= PULSE7_IOPIN;}

// Udalost ulohy vystupu
typedef enum PulseOutTaskEvent
{
    PULSE_OUT_TASK_EVENT_INIT,  // Inicializace ulohy
    PULSE_OUT_TASK_EVENT_MAIN,  // Hlavni uloha
    PULSE_OUT_TASK_EVENT_TICK,  // Casovani vystupu
    PULSE_OUT_TASK_EVENT_PARAM  // Udalost zmeny parametru vystupu

} TPulseOutTaskEvent;

// Stav komparatoru typu On In
typedef enum CompOnInState
{
    COMP_ON_IN_X1,
    //COMP_ON_IN_H1,
    COMP_ON_IN_ON,
    //COMP_ON_IN_H2,
    COMP_ON_IN_X2

} TCompOnInState;

// Stav komparatoru typu On Out
typedef enum CompOnOutState
{
    COMP_ON_OUT_X1,
    //COMP_ON_OUT_H1,
    COMP_ON_OUT_OFF,
    //COMP_ON_OUT_H2,
    COMP_ON_OUT_X2

} TCompOnOutState;

// Stav komparatoru typu On >
typedef enum CompOnBiggerState
{
    COMP_ON_BIGGER_X1,
    //COMP_ON_BIGGER_H1,
    COMP_ON_BIGGER_ON

} TCompOnBiggerState;

// Stav komparatoru typu On <
typedef enum CompOnSmallerState
{
    COMP_ON_SMALLER_X1,
    //COMP_ON_SMALLER_H1,
    COMP_ON_SMALLER_OFF

} TCompOnSmallerState;

// Typ - prototyp funkce pro nastaveni stavu vystupu.
typedef void (*TSetPulseOutStateHandler) (TPulseOutState outState);

// Typ - prototyp funkce ulohy vystupu.
typedef void (*TPulseOutTask) (TPulseOutIdentifier identifier, TPulseOutTaskEvent taskEvent);

// Stav ulohy typu Comp On In
typedef struct
{
    // Stav komparatoru
    TCompOnInState compState;

} TCompOnInTaskState;

// Stav ulohy typu Comp On Out
typedef struct
{
    // Stav komparatoru
    TCompOnOutState compState;

} TCompOnOutTaskState;

// Stav ulohy typu Comp On <
typedef struct
{
    // Stav komparatoru
    TCompOnSmallerState compState;

} TCompOnSmallerTaskState;

// Stav ulohy typu Comp On >
typedef struct
{
    // Stav komparatoru
    TCompOnBiggerState compState;

} TCompOnBiggerTaskState;

// Stav ulohy typu Fout
typedef struct
{
    // pocet tiku na pulperiodu
    unsigned int semiPeriodTicks;

    // aktualni pocet tiku
    unsigned int tickCount;

} TFoutTaskState;

// Stav ulohy vystupu
typedef struct
{
    // Stav vystupu
    TPulseOutState outState;

    // Stav zvolene ulohy/funkce
    union {
        TCompOnInTaskState compOnIn;
        TCompOnOutTaskState compOnOut;
        TCompOnSmallerTaskState compOnSmaller;
        TCompOnBiggerTaskState compOnBigger;
        TFoutTaskState fout;
    } funcState;

} TPulseOutTaskState;

// Fw module state
static struct
{
    // Stavy uloh vystupu
    TPulseOutTaskState taskStates[PULSE_OUT_Count];

    // Ulohy vystupu
    TPulseOutTask tasks[PULSE_OUT_Count];

} state;

//------------------------------------------------------------------------------
//  Nastavovaci funkce vystupu
//------------------------------------------------------------------------------
static void SetPulseOutState_1(TPulseOutState outState)
{
    if (outState == PULSE_OUT_ON) { PULSE4_HIGH; }
    else { PULSE4_LOW; }
}

static void SetPulseOutState_2(TPulseOutState outState)
{
    if (outState == PULSE_OUT_ON) { PULSE6_HIGH; }
    else { PULSE6_LOW; }
}

static void SetPulseOutState_3(TPulseOutState outState)
{
    if (outState == PULSE_OUT_ON) { PULSE5_HIGH; }
    else { PULSE5_LOW; }
}

static void SetPulseOutState_4(TPulseOutState outState)
{
    if (outState == PULSE_OUT_ON) { PULSE7_HIGH; }
    else { PULSE7_LOW; }
}

const TSetPulseOutStateHandler SET_PULSE_OUT_STATE_HANDLERS[] =
{
    SetPulseOutState_1,
    SetPulseOutState_2,
    SetPulseOutState_3,
    SetPulseOutState_4
};

static void SetPulseOutState(TPulseOutIdentifier id, TPulseOutState outState)
{
    SET_PULSE_OUT_STATE_HANDLERS[(int)id](outState);
}

//------------------------------------------------------------------------------
//  Vraci Heat Power 1 komparatoru podle setupu
//------------------------------------------------------------------------------
static float GetCompHeatPower1(TPulseOutIdentifier id)
{
    unsigned long lValue;

    switch (id)
    {
    case PULSE_OUT_1:
        lValue = RAM_Read_Data_Long(BIN_RE1_HEAT_POWER_1);
        break;

    case PULSE_OUT_2:
        lValue = RAM_Read_Data_Long(BIN_RE2_HEAT_POWER_1);
        break;

    case PULSE_OUT_3:
        lValue = RAM_Read_Data_Long(BIN_RE3_HEAT_POWER_1);
        break;

    case PULSE_OUT_4:
        lValue = RAM_Read_Data_Long(BIN_RE4_HEAT_POWER_1);
        break;

    default:
        lValue = 0;
        break;
    }

    return lValue / 1000.0;
}

//------------------------------------------------------------------------------
//  Vraci Heat Power 2 komparatoru podle setupu
//------------------------------------------------------------------------------
static float GetCompHeatPower2(TPulseOutIdentifier id)
{
    unsigned long lValue;

    switch (id)
    {
    case PULSE_OUT_1:
        lValue = RAM_Read_Data_Long(BIN_RE1_HEAT_POWER_2);
        break;

    case PULSE_OUT_2:
        lValue = RAM_Read_Data_Long(BIN_RE2_HEAT_POWER_2);
        break;

    case PULSE_OUT_3:
        lValue = RAM_Read_Data_Long(BIN_RE3_HEAT_POWER_2);
        break;

    case PULSE_OUT_4:
        lValue = RAM_Read_Data_Long(BIN_RE4_HEAT_POWER_2);
        break;

    default:
        lValue = 0;
        break;
    }

    return lValue / 1000.0;
}

//------------------------------------------------------------------------------
//  Vraci Heat Power Hysteresis 1 komparatoru podle setupu
//------------------------------------------------------------------------------
static float GetCompHeatPowerHysteresis1(TPulseOutIdentifier id)
{
    unsigned long lValue;

    switch (id)
    {
    case PULSE_OUT_1:
        lValue = RAM_Read_Data_Long(BIN_RE1_HEAT_POWER_HYSTERESIS_1);
        break;

    case PULSE_OUT_2:
        lValue = RAM_Read_Data_Long(BIN_RE2_HEAT_POWER_HYSTERESIS_1);
        break;

    case PULSE_OUT_3:
        lValue = RAM_Read_Data_Long(BIN_RE3_HEAT_POWER_HYSTERESIS_1);
        break;

    case PULSE_OUT_4:
        lValue = RAM_Read_Data_Long(BIN_RE4_HEAT_POWER_HYSTERESIS_1);
        break;

    default:
        lValue = 0;
        break;
    }

    return lValue / 1000.0;
}

//------------------------------------------------------------------------------
//  Vraci Heat Power Hysteresis 2 komparatoru podle setupu
//------------------------------------------------------------------------------
static float GetCompHeatPowerHysteresis2(TPulseOutIdentifier id)
{
    unsigned long lValue;

    switch (id)
    {
    case PULSE_OUT_1:
        lValue = RAM_Read_Data_Long(BIN_RE1_HEAT_POWER_HYSTERESIS_2);
        break;

    case PULSE_OUT_2:
        lValue = RAM_Read_Data_Long(BIN_RE2_HEAT_POWER_HYSTERESIS_2);
        break;

    case PULSE_OUT_3:
        lValue = RAM_Read_Data_Long(BIN_RE3_HEAT_POWER_HYSTERESIS_2);
        break;

    case PULSE_OUT_4:
        lValue = RAM_Read_Data_Long(BIN_RE4_HEAT_POWER_HYSTERESIS_2);
        break;

    default:
        lValue = 0;
        break;
    }

    return lValue / 1000.0;
}

//------------------------------------------------------------------------------
//  Vraci Fout Heat Power min podle setupu
//------------------------------------------------------------------------------
static float GetFoutHeatPowerMin(TPulseOutIdentifier id)
{
    unsigned long lValue;

    switch (id)
    {
    case PULSE_OUT_3:
        lValue = RAM_Read_Data_Long(BIN_RE3_F_HEAT_POWER1);
        break;

    case PULSE_OUT_4:
        lValue = RAM_Read_Data_Long(BIN_RE4_F_HEAT_POWER1);
        break;

    default:
        lValue = 0;
        break;
    }

    return lValue / 1000.0;
}

//------------------------------------------------------------------------------
//  Vraci Fout Heat Power max podle setupu
//------------------------------------------------------------------------------
static float GetFoutHeatPowerMax(TPulseOutIdentifier id)
{
    unsigned long lValue;

    switch (id)
    {
    case PULSE_OUT_3:
        lValue = RAM_Read_Data_Long(BIN_RE3_F_HEAT_POWER2);
        break;

    case PULSE_OUT_4:
        lValue = RAM_Read_Data_Long(BIN_RE4_F_HEAT_POWER2);
        break;

    default:
        lValue = 0;
        break;
    }

    return lValue / 1000.0;
}

//------------------------------------------------------------------------------
//  Vraci Fout Heat Power min frekvenci podle setupu
//------------------------------------------------------------------------------
static float GetFoutHeatPowerFreqMin(TPulseOutIdentifier id)
{
    unsigned long lValue;

    switch (id)
    {
    case PULSE_OUT_3:
        lValue = RAM_Read_Data_Long(BIN_RE3_F_HEAT_POWER_FREQ1);
        break;

    case PULSE_OUT_4:
        lValue = RAM_Read_Data_Long(BIN_RE4_F_HEAT_POWER_FREQ1);
        break;

    default:
        lValue = 0;
        break;
    }

    return lValue;
}

//------------------------------------------------------------------------------
//  Vraci Fout Heat Power max frekvenci podle setupu
//------------------------------------------------------------------------------
static float GetFoutHeatPowerFreqMax(TPulseOutIdentifier id)
{
    unsigned long lValue;

    switch (id)
    {
    case PULSE_OUT_3:
        lValue = RAM_Read_Data_Long(BIN_RE3_F_HEAT_POWER_FREQ2);
        break;

    case PULSE_OUT_4:
        lValue = RAM_Read_Data_Long(BIN_RE4_F_HEAT_POWER_FREQ2);
        break;

    default:
        lValue = 0;
        break;
    }

    return lValue;
}

//------------------------------------------------------------------------------
//  Vrati stav komparatoru typu On In podle zadanych parametru.
//------------------------------------------------------------------------------
static TCompOnInState GetCompOnInState(float x, float x1, float x2, float h1, float h2, TCompOnInState actualState)
{
    switch (actualState)
    {
    case COMP_ON_IN_X1:
        {
            if ((x >= (x1 + h1)) && (x <= x2))
                return COMP_ON_IN_ON;
            else if (x >= x2)
                return COMP_ON_IN_X2;
        }
        break;

    case COMP_ON_IN_ON:
        {
            if (x <= x1)
                return COMP_ON_IN_X1;
            else if (x >= x2)
                return COMP_ON_IN_X2;
        }
        break;

    case COMP_ON_IN_X2:
        {
            if ((x >= x1) && (x <= (x2 - h2)))
                return COMP_ON_IN_ON;
            else if (x <= x1)
                return COMP_ON_IN_X1;
        }
        break;
    }

    return actualState;
}

//------------------------------------------------------------------------------
//  Vrati stav komparatoru typu On Out podle zadanych parametru.
//------------------------------------------------------------------------------
static TCompOnOutState GetCompOnOutState(float x, float x1, float x2, float h1, float h2, TCompOnOutState actualState)
{
    switch (actualState)
    {
    case COMP_ON_OUT_X1:
        {
            if ((x >= (x1 + h1)) && (x <= x2))
                return COMP_ON_OUT_OFF;
            else if (x >= x2)
                return COMP_ON_OUT_X2;
        }
        break;

    case COMP_ON_OUT_OFF:
        {
            if (x <= x1)
                return COMP_ON_OUT_X1;
            else if (x >= x2)
                return COMP_ON_OUT_X2;
        }
        break;

    case COMP_ON_OUT_X2:
        {
            if ((x >= x1) && (x <= (x2 - h2)))
                return COMP_ON_OUT_OFF;
            else if (x <= x1)
                return COMP_ON_OUT_X1;
        }
        break;
    }

    return actualState;
}

//------------------------------------------------------------------------------
//  Vrati stav komparatoru typu On < podle zadanych parametru.
//------------------------------------------------------------------------------
static TCompOnSmallerState GetCompOnSmallerState(float x, float x1, float h1, TCompOnSmallerState actualState)
{
    switch (actualState)
    {
    case COMP_ON_SMALLER_X1:
        {
            if (x >= (x1 + h1))
                return COMP_ON_SMALLER_OFF;
        }
        break;

    case COMP_ON_SMALLER_OFF:
        {
            if (x <= x1)
                return COMP_ON_SMALLER_X1;
        }
        break;
    }

    return actualState;
}

//------------------------------------------------------------------------------
//  Vrati stav komparatoru typu On > podle zadanych parametru.
//------------------------------------------------------------------------------
static TCompOnBiggerState GetCompOnBiggerState(float x, float x1, float h1, TCompOnBiggerState actualState)
{
    switch (actualState)
    {
    case COMP_ON_BIGGER_X1:
        {
            if (x >= (x1 + h1))
                return COMP_ON_BIGGER_ON;
        }
        break;

    case COMP_ON_BIGGER_ON:
        {
            if (x <= x1)
                return COMP_ON_BIGGER_X1;
        }
        break;
    }

    return actualState;
}

//------------------------------------------------------------------------------
//  Konverze stavu komparatoru typu On In do vystupu.
//------------------------------------------------------------------------------
static TPulseOutState ConvertCompOnInStateToOut(TCompOnInState compState)
{
    switch (compState)
    {
    case COMP_ON_IN_ON:
        return PULSE_OUT_ON;

    /*case COMP_ON_IN_X1:
    case COMP_ON_IN_H1:
    case COMP_ON_IN_H2:
    case COMP_ON_IN_X2:*/
    default:
        return PULSE_OUT_OFF;
    }
}

//------------------------------------------------------------------------------
//  Konverze stavu komparatoru typu On Out do vystupu.
//------------------------------------------------------------------------------
static TPulseOutState ConvertCompOnOutStateToOut(TCompOnOutState compState)
{
    switch (compState)
    {
    case COMP_ON_OUT_OFF:
        return PULSE_OUT_OFF;

    /*case COMP_ON_OUT_X1:
    case COMP_ON_OUT_H1:
    case COMP_ON_OUT_H2:
    case COMP_ON_OUT_X2:*/
    default:
        return PULSE_OUT_ON;
    }
}

//------------------------------------------------------------------------------
//  Konverze stavu komparatoru typu On < do vystupu.
//------------------------------------------------------------------------------
static TPulseOutState ConvertCompOnSmallerStateToOut(TCompOnSmallerState compState)
{
    switch (compState)
    {
    case COMP_ON_SMALLER_OFF:
        return PULSE_OUT_OFF;

    /*case COMP_ON_SMALLER_X1:
    case COMP_ON_SMALLER_H1:*/
    default:
        return PULSE_OUT_ON;
    }
}

//------------------------------------------------------------------------------
//  Konverze stavu komparatoru typu On > do vystupu.
//------------------------------------------------------------------------------
static TPulseOutState ConvertCompOnBiggerStateToOut(TCompOnBiggerState compState)
{
    switch (compState)
    {
    case COMP_ON_BIGGER_ON:
        return PULSE_OUT_ON;

    /*case COMP_ON_BIGGER_X1:
    case COMP_ON_BIGGER_H1:*/
    default:
        return PULSE_OUT_OFF;
    }
}

//------------------------------------------------------------------------------
//  Vrati pocet tiku na pulperiodu pro Fout podle zadanych parametru.
//------------------------------------------------------------------------------
static unsigned int GetFoutSemiPeriodTicks(float x, float xMin, float xMax, float freqMin, float freqMax)
{
    float dx = xMax - xMin;
    if (dx <= 0)
        return 0;

    float df = freqMax - freqMin;
    if (df < 0)
        return 0;

    float k = df / dx;
    float f = k * x + freqMin;
    if (f <= 0)
        return 0;

    return 2000 / f;
}

//------------------------------------------------------------------------------
//  Vrati stav tasku.
//------------------------------------------------------------------------------
static TPulseOutTaskState* GetTaskState(TPulseOutIdentifier id)
{
    return &state.taskStates[id];
}

//------------------------------------------------------------------------------
//  Uloha vystupu - OFF
//------------------------------------------------------------------------------
static void PulseOutTask_Off(TPulseOutIdentifier identifier, TPulseOutTaskEvent taskEvent)
{
    if (taskEvent == PULSE_OUT_TASK_EVENT_INIT)
    {
        TPulseOutTaskState* taskState = GetTaskState(identifier);
        if (taskState->outState != PULSE_OUT_OFF)
        {
            SetPulseOutState(identifier, PULSE_OUT_OFF);
            taskState->outState = PULSE_OUT_OFF;
            Led_OnBinReChanged(identifier);
        }
    }
}

//------------------------------------------------------------------------------
//  Uloha vystupu - FIXED
//------------------------------------------------------------------------------
static void PulseOutTask_Fixed(TPulseOutIdentifier identifier, TPulseOutTaskEvent taskEvent)
{
    if (taskEvent == PULSE_OUT_TASK_EVENT_INIT)
    {
        TPulseOutTaskState* taskState = GetTaskState(identifier);
        if (taskState->outState != PULSE_OUT_ON)
        {
            SetPulseOutState(identifier, PULSE_OUT_ON);
            taskState->outState = PULSE_OUT_ON;
            Led_OnBinReChanged(identifier);
        }
    }
}

//------------------------------------------------------------------------------
//  Uloha vystupu - ERROR
//------------------------------------------------------------------------------
static void PulseOutTask_Error(TPulseOutIdentifier identifier, TPulseOutTaskEvent taskEvent)
{
    if ((taskEvent == PULSE_OUT_TASK_EVENT_INIT) || (taskEvent == PULSE_OUT_TASK_EVENT_MAIN))
    {
        TPulseOutTaskState* taskState = GetTaskState(identifier);
        TPulseOutState outState = (actual_error == 0) ? PULSE_OUT_OFF : PULSE_OUT_ON;
        if (taskState->outState != outState)
        {
            SetPulseOutState(identifier, outState);
            taskState->outState = outState;
            Led_OnBinReChanged(identifier);
        }
    }
}

//------------------------------------------------------------------------------
//  Uloha vystupu - POWER IN
//------------------------------------------------------------------------------
static void PulseOutTask_PowerIn(TPulseOutIdentifier identifier, TPulseOutTaskEvent taskEvent)
{
    if ((taskEvent == PULSE_OUT_TASK_EVENT_INIT) || (taskEvent == PULSE_OUT_TASK_EVENT_MAIN))
    {
        TPulseOutTaskState* taskState = GetTaskState(identifier);

        if (taskEvent == PULSE_OUT_TASK_EVENT_INIT)
            taskState->funcState.compOnIn.compState = COMP_ON_IN_X1;

        float power = Heat_GetHeatPerformance();
        float x1 = GetCompHeatPower1(identifier);
        float x2 = GetCompHeatPower2(identifier);
        float h1 = GetCompHeatPowerHysteresis1(identifier);
        float h2 = GetCompHeatPowerHysteresis2(identifier);

        taskState->funcState.compOnIn.compState = GetCompOnInState(power, x1, x2, h1, h2, taskState->funcState.compOnIn.compState);
        TPulseOutState outState = ConvertCompOnInStateToOut(taskState->funcState.compOnIn.compState);
        if (taskState->outState != outState)
        {
            SetPulseOutState(identifier, outState);
            taskState->outState = outState;
            Led_OnBinReChanged(identifier);
        }
    }
}

//------------------------------------------------------------------------------
//  Uloha vystupu - POWER OUT
//------------------------------------------------------------------------------
static void PulseOutTask_PowerOut(TPulseOutIdentifier identifier, TPulseOutTaskEvent taskEvent)
{
    if ((taskEvent == PULSE_OUT_TASK_EVENT_INIT) || (taskEvent == PULSE_OUT_TASK_EVENT_MAIN))
    {
        TPulseOutTaskState* taskState = GetTaskState(identifier);

        if (taskEvent == PULSE_OUT_TASK_EVENT_INIT)
            taskState->funcState.compOnOut.compState = COMP_ON_OUT_OFF;

        float power = Heat_GetHeatPerformance();
        float x1 = GetCompHeatPower1(identifier);
        float x2 = GetCompHeatPower2(identifier);
        float h1 = GetCompHeatPowerHysteresis1(identifier);
        float h2 = GetCompHeatPowerHysteresis2(identifier);

        taskState->funcState.compOnOut.compState = GetCompOnOutState(power, x1, x2, h1, h2, taskState->funcState.compOnOut.compState);
        TPulseOutState outState = ConvertCompOnOutStateToOut(taskState->funcState.compOnOut.compState);
        if (taskState->outState != outState)
        {
            SetPulseOutState(identifier, outState);
            taskState->outState = outState;
            Led_OnBinReChanged(identifier);
        }
    }
}

//------------------------------------------------------------------------------
//  Uloha vystupu - POWER SMALLER
//------------------------------------------------------------------------------
static void PulseOutTask_PowerSmaller(TPulseOutIdentifier identifier, TPulseOutTaskEvent taskEvent)
{
    if ((taskEvent == PULSE_OUT_TASK_EVENT_INIT) || (taskEvent == PULSE_OUT_TASK_EVENT_MAIN))
    {
        TPulseOutTaskState* taskState = GetTaskState(identifier);

        if (taskEvent == PULSE_OUT_TASK_EVENT_INIT)
            taskState->funcState.compOnSmaller.compState = COMP_ON_SMALLER_OFF;

        float power = Heat_GetHeatPerformance();
        float x1 = GetCompHeatPower1(identifier);
        float h1 = GetCompHeatPowerHysteresis1(identifier);

        taskState->funcState.compOnSmaller.compState = GetCompOnSmallerState(power, x1, h1, taskState->funcState.compOnSmaller.compState);
        TPulseOutState outState = ConvertCompOnSmallerStateToOut(taskState->funcState.compOnSmaller.compState);
        if (taskState->outState != outState)
        {
            SetPulseOutState(identifier, outState);
            taskState->outState = outState;
            Led_OnBinReChanged(identifier);
        }
    }
}

//------------------------------------------------------------------------------
//  Uloha vystupu - POWER BIGGER
//------------------------------------------------------------------------------
static void PulseOutTask_PowerBigger(TPulseOutIdentifier identifier, TPulseOutTaskEvent taskEvent)
{
    if ((taskEvent == PULSE_OUT_TASK_EVENT_INIT) || (taskEvent == PULSE_OUT_TASK_EVENT_MAIN))
    {
        TPulseOutTaskState* taskState = GetTaskState(identifier);

        if (taskEvent == PULSE_OUT_TASK_EVENT_INIT)
            taskState->funcState.compOnBigger.compState = COMP_ON_BIGGER_X1;

        float power = Heat_GetHeatPerformance();
        float x1 = GetCompHeatPower1(identifier);
        float h1 = GetCompHeatPowerHysteresis1(identifier);

        taskState->funcState.compOnBigger.compState = GetCompOnBiggerState(power, x1, h1, taskState->funcState.compOnBigger.compState);
        TPulseOutState outState = ConvertCompOnBiggerStateToOut(taskState->funcState.compOnBigger.compState);
        if (taskState->outState != outState)
        {
            SetPulseOutState(identifier, outState);
            taskState->outState = outState;
            Led_OnBinReChanged(identifier);
        }
    }
}

//------------------------------------------------------------------------------
//  Uloha vystupu - POWER FOUT
//------------------------------------------------------------------------------
static void PulseOutTask_PowerFout(TPulseOutIdentifier identifier, TPulseOutTaskEvent taskEvent)
{
    if ((taskEvent == PULSE_OUT_TASK_EVENT_INIT) || (taskEvent == PULSE_OUT_TASK_EVENT_TICK))
    {
        TPulseOutTaskState* taskState = GetTaskState(identifier);
        TPulseOutState outState = taskState->outState;

        if (taskEvent == PULSE_OUT_TASK_EVENT_INIT)
        {
            taskState->funcState.fout.tickCount = 0;

            float power = Heat_GetHeatPerformance();
            float powerMin = GetFoutHeatPowerMin(identifier);
            float powerMax = GetFoutHeatPowerMax(identifier);
            float freqMin = GetFoutHeatPowerFreqMin(identifier);
            float freqMax = GetFoutHeatPowerFreqMax(identifier);

            taskState->funcState.fout.semiPeriodTicks = GetFoutSemiPeriodTicks(power, powerMin, powerMax, freqMin, freqMax);
            if (taskState->funcState.fout.semiPeriodTicks > 0)
                outState = PULSE_OUT_ON;
            else
                outState = PULSE_OUT_OFF;
        }
        else
        {
            taskState->funcState.fout.tickCount++;
            if (taskState->funcState.fout.tickCount >= taskState->funcState.fout.semiPeriodTicks)
            {
                taskState->funcState.fout.tickCount = 0;
                if (outState == PULSE_OUT_ON)
                {
                    outState = PULSE_OUT_OFF;
                }
                else
                {
                    float power = Heat_GetHeatPerformance();
                    float powerMin = GetFoutHeatPowerMin(identifier);
                    float powerMax = GetFoutHeatPowerMax(identifier);
                    float freqMin = GetFoutHeatPowerFreqMin(identifier);
                    float freqMax = GetFoutHeatPowerFreqMax(identifier);

                    taskState->funcState.fout.semiPeriodTicks = GetFoutSemiPeriodTicks(power, powerMin, powerMax, freqMin, freqMax);
                    if (taskState->funcState.fout.semiPeriodTicks > 0)
                        outState = PULSE_OUT_ON;
                    else
                        outState = PULSE_OUT_OFF;
                }
            }
        }

        if (taskState->outState != outState)
        {
            SetPulseOutState(identifier, outState);
            taskState->outState = outState;
            Led_OnBinReChanged(identifier);
        }
    }
}

//------------------------------------------------------------------------------
//  Vraci funkci vystupu podle setupu
//------------------------------------------------------------------------------
static TPulseOutTask GetFunction(TPulseOutIdentifier id)
{
    unsigned long funcSet;

    switch (id)
    {
    case PULSE_OUT_1:
    case PULSE_OUT_2:
        {
            if (id == PULSE_OUT_1)
                funcSet = RAM_Read_Data_Long(BIN_RE1_SET);
            else
                funcSet = RAM_Read_Data_Long(BIN_RE2_SET);

            if (funcSet > PULSE_OUT_RF_Max)
                return PulseOutTask_Off;

            switch ((TPulseOutRelayFunc)funcSet)
            {
            case PULSE_OUT_RF_OFF:
                return PulseOutTask_Off;
            case PULSE_OUT_RF_FIXED:
                return PulseOutTask_Fixed;
            case PULSE_OUT_RF_ERROR:
                return PulseOutTask_Error;
            case PULSE_OUT_RF_POWER_IN:
                return PulseOutTask_PowerIn;
            case PULSE_OUT_RF_POWER_OUT:
                return PulseOutTask_PowerOut;
            case PULSE_OUT_RF_POWER_SMALLER:
                return PulseOutTask_PowerSmaller;
            case PULSE_OUT_RF_POWER_BIGGER:
                return PulseOutTask_PowerBigger;
            default:
                return PulseOutTask_Off;
            }
        }
        break;

    case PULSE_OUT_3:
    case PULSE_OUT_4:
        {
            if (id == PULSE_OUT_3)
                funcSet = RAM_Read_Data_Long(BIN_RE3_SET);
            else
                funcSet = RAM_Read_Data_Long(BIN_RE4_SET);

            if (funcSet > PULSE_OUT_OF_Max)
                return PulseOutTask_Off;

            switch ((TPulseOutOptoFunc)funcSet)
            {
            case PULSE_OUT_OF_OFF:
                return PulseOutTask_Off;
            case PULSE_OUT_OF_FIXED:
                return PulseOutTask_Fixed;
            case PULSE_OUT_OF_ERROR:
                return PulseOutTask_Error;
            case PULSE_OUT_OF_POWER_IN:
                return PulseOutTask_PowerIn;
            case PULSE_OUT_OF_POWER_OUT:
                return PulseOutTask_PowerOut;
            case PULSE_OUT_OF_POWER_SMALLER:
                return PulseOutTask_PowerSmaller;
            case PULSE_OUT_OF_POWER_BIGGER:
                return PulseOutTask_PowerBigger;
            case PULSE_OUT_OF_POWER_FOUT:
                return PulseOutTask_PowerFout;
            default:
                return PulseOutTask_Off;
            }
        }
        break;

    default:
        return PulseOutTask_Off;
    }
}

//------------------------------------------------------------------------------
//  Init stavu tasku.
//------------------------------------------------------------------------------
static void InitTaskState(TPulseOutIdentifier id)
{
    SetPulseOutState(id, PULSE_OUT_OFF);
    state.taskStates[id].outState = PULSE_OUT_OFF;
}

//------------------------------------------------------------------------------
//  Init stavu vsech tasku.
//------------------------------------------------------------------------------
static void InitTasksStates(void)
{
    TPulseOutIdentifier id;

    for (id = PULSE_OUT_Min; id <= PULSE_OUT_Max; id++)
        InitTaskState(id);
}

//------------------------------------------------------------------------------
//  Start tasku.
//------------------------------------------------------------------------------
static void StartTask(TPulseOutIdentifier id)
{
    state.tasks[id] = GetFunction(id);
    state.tasks[id](id, PULSE_OUT_TASK_EVENT_INIT);
}

//------------------------------------------------------------------------------
//  Start vsech tasku.
//------------------------------------------------------------------------------
static void StartTasks(void)
{
    TPulseOutIdentifier id;

    for (id = PULSE_OUT_Min; id <= PULSE_OUT_Max; id++)
        StartTask(id);
}

//------------------------------------------------------------------------------
//  Fw module state init. Call only once in main function before usage of other
//  functions in this module.
//------------------------------------------------------------------------------
void PulseOutputs_Init(void)
{
    ENTER_CRITICAL_SECTION();

    PULSE4_INIT;
    PULSE5_INIT;
    PULSE6_INIT;
    PULSE7_INIT;

    InitTasksStates();
    StartTasks();

    EXIT_CRITICAL_SECTION();
}

//------------------------------------------------------------------------------
//  Hlavni uloha. Volat co nejcasteji.
//------------------------------------------------------------------------------
void PulseOutputs_Task(void)
{
    TPulseOutIdentifier id;

    for (id = PULSE_OUT_Min; id <= PULSE_OUT_Max; id++)
    {
        ENTER_CRITICAL_SECTION();
        state.tasks[id](id, PULSE_OUT_TASK_EVENT_MAIN);
        EXIT_CRITICAL_SECTION();
    }
}

//------------------------------------------------------------------------------
//  Vrati stav vystupu.
//------------------------------------------------------------------------------
TPulseOutState PulseOutputs_GetPulseOutState(TPulseOutIdentifier id)
{
    return state.taskStates[id].outState;
}

//------------------------------------------------------------------------------
//  Casovani vystupu. Volat kazdou 1/2 ms.
//------------------------------------------------------------------------------
void PulseOutputs_HalfMillisecondTick(void)
{
    TPulseOutIdentifier id;

    for (id = PULSE_OUT_Min; id <= PULSE_OUT_Max; id++)
        state.tasks[id](id, PULSE_OUT_TASK_EVENT_TICK);
}

//------------------------------------------------------------------------------
//  Udalost zmeny parametru vystupu.
//------------------------------------------------------------------------------
void PulseOutputs_OnParameterChanged(TPulseOutIdentifier id)
{
    if ((PULSE_OUT_Min <= id) && (id <= PULSE_OUT_Max))
    {
        ENTER_CRITICAL_SECTION();

        TPulseOutTask outTask = GetFunction(id);

        if (outTask == state.tasks[id])
            state.tasks[id](id, PULSE_OUT_TASK_EVENT_PARAM);
        else
            StartTask(id);

        EXIT_CRITICAL_SECTION();
    }
}
